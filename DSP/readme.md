# Sitecore Base
The Sitecore base project can accellerate Sitecore CMS implementations by providing a base structure and shared components. The Sitecore base project is included as a git submodule into your Sitecore CMS implementation. 

Refer to [this wiki page for the full documentation][1].

Additional documentation can be found on the network drive:
\\aumelcl001\common$\MSS\Online\Accounts\Deloitte Digital Sitecore Base Project

[1]: https://deloitteonline.jira.com/wiki/display/DELENG/How+to+create+a+new+Sitecore+project
