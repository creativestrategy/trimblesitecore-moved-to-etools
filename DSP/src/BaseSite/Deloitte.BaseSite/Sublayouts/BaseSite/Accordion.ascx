﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Accordion.ascx.cs" Inherits="Deloitte.BaseSite.UI.Sublayouts.BaseSite.Accordion" %>

<!-- START: Accordion -->
<sc:EditFrame runat="server" ID="efActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/BaseSite/AccordionActions">
    <a id="title" runat="server" class="expandcollapse expand"><sc:FieldRenderer ID="frTitle" runat="server" FieldName="AccordionTitle" DisableWebEditing="true"/></a>
    <div id="text" runat="server"><p><sc:FieldRenderer ID="frText" runat="server" FieldName="AccordionText" DisableWebEditing="true"/></p></div>
</sc:EditFrame>
<!-- END: Accordion -->