﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwoColumnLeft.ascx.cs" Inherits="Deloitte.BaseSite.UI.Sublayouts.BaseSite.Containers.TwoColumnLeft" %>

<!-- START Content Container -->
<div id="main" class="two-column-left" role="main">
    <div class="margins">

        <!-- START Content Column -->
        <div id="contentColumn">
                        
            <!-- START Content Header -->   
            <sc:Placeholder runat="server" ID="phContentHeader" Key="phcontentheader" />
            <!-- END Content Header -->

            <!-- START Content Body -->
            <div id="contentBody">
                <sc:Placeholder runat="server" ID="phContentBody" Key="phcontentbody" />
            </div>
            <!-- END Content Body -->

        </div>
        <!-- END Content Column -->

        <!-- START Sidebar -->
        <div id="aside" role="complimentary">
                <sc:Placeholder runat="server" ID="phAside" Key="phaside" />    
        </div>
        <!-- END Sidebar -->
        <div class="clearfix"></div>
    </div>         
</div>
<!-- END Content Container -->
