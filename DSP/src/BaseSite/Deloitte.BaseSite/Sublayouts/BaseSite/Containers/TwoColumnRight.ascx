﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwoColumnRight.ascx.cs" Inherits="Deloitte.BaseSite.UI.Sublayouts.BaseSite.Containers.TwoColumnRight" %>

<!-- START Content Container -->
<div id="main" class="two-column-right" role="main">
    <div class="margins">

        <!-- START Seconday Nav -->
        <div id="secondaryNav" role="navigation">
            <sc:Placeholder runat="server" ID="phSecondaryNav" Key="phsecondarynav" />  
        </div>
        <!-- END Seconday Nav -->

        <!-- START Content Column -->
        <div id="contentColumn">
                        
            <!-- START Content Header -->   
            <sc:Placeholder runat="server" ID="phContentHeader" Key="phcontentheader" />
            <!-- END Content Header -->

            <!-- START Content Body -->
            <sc:Placeholder runat="server" ID="phContentBody" Key="phcontentbody" />
            <!-- END Content Body -->

        </div>
        <!-- END Content Column -->
         <div class="clearfix"></div>
    </div>         
</div>
<!-- END Content Container -->    
