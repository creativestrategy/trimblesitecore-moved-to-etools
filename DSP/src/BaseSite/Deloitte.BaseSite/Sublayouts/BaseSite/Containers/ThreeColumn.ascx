﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThreeColumn.ascx.cs" Inherits="Deloitte.BaseSite.UI.Sublayouts.BaseSite.Containers.ThreeColumn" %>

<!-- START Content Container -->
<div id="main" class="three-column" role="main">
    <div class="margins">

        <!-- START Seconday Nav -->
        <div id="secondaryNav" role="navigation">
            <sc:Placeholder runat="server" ID="phSecondaryNav" Key="phsecondarynav" />  
        </div>
        <!-- END Secondary Nav -->

        <!-- START Content Column -->
        <div id="contentColumn">
                        
            <!-- START Content Header -->   
            <sc:Placeholder runat="server" ID="phContentHeader" Key="phcontentheader" />
            <!-- END Content Header -->

            <!-- START Content Body -->
            <div id="contentBody">
                <sc:Placeholder runat="server" ID="phContentBody" Key="phcontentbody" />
            </div>
            <!-- END Content Body -->

        </div>
        <!-- END Content Column -->

        <!-- START Aside -->
        <div id="aside" role="complimentary">
            <!-- START Sidebar Entry -->
            <ul>
                <sc:Placeholder runat="server" ID="phAside" Key="phaside" />    
            </ul>
            <!-- END Aside -->
        </div>
        <!-- END Sidebar -->

    </div>         
</div>
<!-- END Content Container -->
    
