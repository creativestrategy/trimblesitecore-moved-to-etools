﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OneColumn.ascx.cs" Inherits="Deloitte.BaseSite.UI.Sublayouts.BaseSite.Containers.OneColumn" %>

<!-- START Content Container -->
<div id="main" class="one-column" role="main">
    <div class="margins">
                
        <!-- START Content Column -->
        <div id="contentColumn">
        
            <!-- START Content Header -->   
            <sc:Placeholder runat="server" ID="phContentHeader" Key="phcontentheader" />
            <!-- END Content Header -->
                        
            <!-- START Content -->
            <sc:Placeholder runat="server" ID="phContentBody" Key="phcontentbody" />
            <!-- END Content -->
                
        </div>
        <!-- END Content Column -->

    </div>
</div>
<!-- END Content Container -->
