﻿using System;
using System.Web.UI;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Extensions;

namespace Deloitte.BaseSite.UI.Sublayouts.BaseSite
{
    public partial class Accordion : SublayoutBaseExtended
    {
        public virtual void Page_Load(object sender, EventArgs e)
        {
            Item sourceItem = this.DataSourceItem;
            if (sourceItem != null)
            {
                // Set datasources
                efActions.DataSource = sourceItem.Paths.Path;
                frTitle.DataSource = sourceItem.Paths.Path;
                frText.DataSource = sourceItem.Paths.Path;

                // Set title. Can customise based on state; expanded or collapsed.
                title.Attributes.Add("data-ec-collapse", sourceItem.GetFieldValue("AccordionTitle"));
                title.Attributes.Add("data-ec-expand", sourceItem.GetFieldValue("AccordionTitle"));

                // Setup anchor
                text.ClientIDMode = ClientIDMode.Static;
                var id = sourceItem.ID.ToString().Replace("{", "").Replace("}", "");
                text.ID = id;
                title.Attributes.Add("href", "#" + id);
            }
        }
    }
}