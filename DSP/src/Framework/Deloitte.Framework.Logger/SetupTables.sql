﻿/* Script

	Author: James Galliers
	Date: 20/06/12
	Target Platform: SQL Server 2008 R2

	Description: This script creates the tables needed for the logging framework.
				 The tables are as follows -
											Logger_Status:  List of possible Status Id codes with descriptions
											Logger_Service: List of Service Ids with descriptions
											Logger_Group:	List of application 'groups' - used for filtering various application log types
											Logger_WSLog:   List of logged web service events, which the log4net wrapper will write to using an AdoNetAppender.
														    This table has both Logger_Status and Logger_Service tables as foreign keys to
														    enforce self-documenting behaviour. These tables should then be populated according to
														    project needs, and mapped to enums in your project to enforce consistency.
											Logger_AppLog:  List of logged application events, which the log4net wrapper will write to using an AdoNetAppender.
*/

-- Create Logger_Status table

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Logger_Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusDescription] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Logger_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [Logger_Status]
           ([StatusDescription])
     VALUES
           ('Success')
GO

-- Create Logger_Service

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Logger_Service](
	[ServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceDescription] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Logger_Service] PRIMARY KEY CLUSTERED 
(
	[ServiceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [Logger_Service]
           ([ServiceDescription])
     VALUES
           ('Sample Service')
GO

-- Create Logger_Group table

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Logger_Group](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Logger_Group] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [Logger_Group]
           ([Description])
     VALUES
           ('General')
GO

-- Create Logger_WSLog table

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Logger_WSLog](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceId] [int] NOT NULL,
	[ServiceType] [varchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[ReceiveDate] [datetime] NULL,
	[ConversationId] [uniqueidentifier] NULL,
	[StatusId] [int] NOT NULL,
	[Notes] [varchar](100) NULL,
	[SendPacket] [varchar](max) NULL,
	[ReceivePacket] [varchar](max) NULL,
	[RequestedBy] [varchar](50) NULL,
	[Server] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Logger_Log] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Logger_WSLog]  WITH CHECK ADD  CONSTRAINT [FK_Logger_Log_Logger_Service] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Logger_Service] ([ServiceId])
GO

ALTER TABLE [dbo].[Logger_WSLog] CHECK CONSTRAINT [FK_Logger_Log_Logger_Service]
GO

ALTER TABLE [dbo].[Logger_WSLog]  WITH CHECK ADD  CONSTRAINT [FK_Logger_Log_Logger_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Logger_Status] ([StatusId])
GO

ALTER TABLE [dbo].[Logger_WSLog] CHECK CONSTRAINT [FK_Logger_Log_Logger_Status]
GO

-- Create Logger_AppLog table

USE [PortalData]
GO

/****** Object:  Table [dbo].[Logger_AppLog]    Script Date: 06/25/2012 14:56:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Logger_AppLog](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](4000) NOT NULL,
	[Exception] [varchar](2000) NULL,
	[Server] [varchar](50) NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_Logger_AppLog] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Logger_AppLog]  WITH CHECK ADD  CONSTRAINT [FK_Logger_AppLog_Logger_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Logger_Group] ([GroupId])
GO

ALTER TABLE [dbo].[Logger_AppLog] CHECK CONSTRAINT [FK_Logger_AppLog_Logger_Group]
GO




