﻿using System;
using System.ComponentModel;
using Deloitte.Framework.Extensions;
using log4net.Core;
using System.Reflection;
using log4net.Repository;

namespace Deloitte.Framework.Logger.Loggers
{
    public class ApplicationLogger : BaseLogger
    {
        private static readonly ApplicationLoggerReference Logger = LogManager.GetLogger("ApplicationLogger");
        
        public void Debug(string message)
        {
            Logger.Debug(message);
        }

        public void Debug(string message, Exception ex)
        {
            Logger.Debug(message, ex);
        }

        public void Debug(string message, Exception ex, int groupId = 1)
        {
            Logger.Debug(message, ex, groupId);
        }

        public void Info(string message)
        {
            Logger.Info(message);
        }

        public void Info(string message, Exception ex)
        {
            Logger.Info(message, ex);
        }

        public void Info(string message, Exception ex, int groupId = 1)
        {
            Logger.Debug(message, ex, groupId);
        }

        public void Warn(string message)
        {
            Logger.Warn(message);
        }

        public void Warn(string message, Exception ex)
        {
            Logger.Warn(message, ex);
        }

        public void Warn(string message, Exception ex, int groupId = 1)
        {
            Logger.Debug(message, ex, groupId);
        }

        public void Error(string message)
        {
            Logger.Error(message);
        }

        public void Error(string message, Exception ex)
        {
            Logger.Error(message, ex);
        }

        public void Error(string message, Exception ex, int groupId = 1)
        {
            Logger.Debug(message, ex, groupId);
        }

        public void Fatal(string message)
        {
            Logger.Fatal(message);
        }

        public void Fatal(string message, Exception ex)
        {
            Logger.Fatal(message, ex);
        }

        public void Fatal(string message, Exception ex, int groupId = 1)
        {
            Logger.Debug(message, ex, groupId);
        }

        /// <summary>
        /// Local override of the log4net LogManager so we can wrap the logger returned from the
        /// web.config with our own ApplicationLoggerReference type - to populate custom attributes
        /// such as server, etc
        /// </summary>
        private static class LogManager
        {
            private static readonly WrapperMap logWrapper = new WrapperMap(new WrapperCreationHandler(WrapperCreationHandler));

            private static ILoggerWrapper WrapperCreationHandler(ILogger logger)
            {
                return new ApplicationLoggerReference(logger);
            }


            public static ApplicationLoggerReference GetLogger(string name)
            {
                return (ApplicationLoggerReference)logWrapper.GetWrapper(
                 LoggerManager.GetLogger(Assembly.GetCallingAssembly(), name));
            }
        }

        public sealed class ApplicationLoggerReference : LoggerWrapperImpl
        {
            private static readonly Type declaringType = typeof (ApplicationLoggerReference);

            public ApplicationLoggerReference(ILogger logger) : base(logger)
            {
                //empty ctor as it's just a wrapper for the base logger - this only 
                //exists to override the implementation of the debug / info / warn etc functions
            }

            #region custom logging methods

            public void Debug(string message)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Debug,
                    message,
                    null
                    );

                Logger.Log(loggingEvent);
            }

            public void Debug(string message, Exception ex)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Debug,
                    message,
                    ex
                    );

                Logger.Log(loggingEvent);
            }

            public void Debug(string message, Exception ex, int groupId)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Debug,
                    message,
                    ex
                    );

                loggingEvent.Properties[LoggingField.GroupIdField.ToDescriptionString()] = groupId;

                Logger.Log(loggingEvent);
            }

            public void Info(string message)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Info,
                    message,
                    null
                    );

                Logger.Log(loggingEvent);
            }

            public void Info(string message, Exception ex)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Info,
                    message,
                    ex
                    );

                Logger.Log(loggingEvent);
            }

            public void Info(string message, Exception ex, int groupId)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Info,
                    message,
                    ex
                    );

                loggingEvent.Properties[LoggingField.GroupIdField.ToDescriptionString()] = groupId;

                Logger.Log(loggingEvent);
            }

            public void Warn(string message)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Warn,
                    message,
                    null
                    );

                Logger.Log(loggingEvent);
            }

            public void Warn(string message, Exception ex)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Warn,
                    message,
                    ex
                    );

                Logger.Log(loggingEvent);
            }

            public void Warn(string message, Exception ex, int groupId)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Warn,
                    message,
                    ex
                    );

                loggingEvent.Properties[LoggingField.GroupIdField.ToDescriptionString()] = groupId;

                Logger.Log(loggingEvent);
            }

            public void Error(string message)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Error,
                    message,
                    null
                    );

                Logger.Log(loggingEvent);
            }

            public void Error(string message, Exception ex)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Error,
                    message,
                    ex
                    );

                Logger.Log(loggingEvent);
            }

            public void Error(string message, Exception ex, int groupId)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Error,
                    message,
                    ex
                    );

                loggingEvent.Properties[LoggingField.GroupIdField.ToDescriptionString()] = groupId;

                Logger.Log(loggingEvent);
            }

            public void Fatal(string message)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Fatal,
                    message,
                    null
                    );

                Logger.Log(loggingEvent);
            }

            public void Fatal(string message, Exception ex)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Fatal,
                    message,
                    ex
                    );

                Logger.Log(loggingEvent);
            }

            public void Fatal(string message, Exception ex, int groupId)
            {
                var loggingEvent = new ApplicationLoggingEvent
                    (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Fatal,
                    message,
                    ex
                    );

                loggingEvent.Properties[LoggingField.GroupIdField.ToDescriptionString()] = groupId;

                Logger.Log(loggingEvent);
            }

            #endregion

            #region custom types

            private class ApplicationLoggingEvent : LoggingEvent
            {
                //force creation through this constructor to populate properties on every exception

                public ApplicationLoggingEvent(Type declaringType, ILoggerRepository repository, string loggerName, Level level, string message, Exception ex) 
                    : base(declaringType, repository, loggerName, level, message, ex)
                {
                    Properties[LoggingField.ServerField.ToDescriptionString()] = ServerName;
                }
            }

            #endregion

        }

        private enum LoggingField
        {
            [Description("Server")]
            ServerField,

            [Description("GroupId")]
            GroupIdField
        }

    }
}