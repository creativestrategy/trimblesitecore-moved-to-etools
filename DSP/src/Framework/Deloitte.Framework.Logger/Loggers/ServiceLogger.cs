﻿using System;
using System.ComponentModel;
using System.Reflection;
using Deloitte.Framework.Extensions;
using log4net.Core;
using log4net.Repository;

namespace Deloitte.Framework.Logger.Loggers
{
    public class ServiceLogger : BaseLogger
    {
        private static readonly ServiceLoggerReference Logger = LogManager.GetLogger("ServiceLogger");

        public void Debug(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
        {
            Logger.Debug(serviceId, statusId, serviceType, conversationId, notes, sendPacket, receivePacket, requestedBy);
        }

        public void Info(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
        {
            Logger.Info(serviceId, statusId, serviceType, conversationId, notes, sendPacket, receivePacket, requestedBy);
        }

        public void Warn(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
        {
            Logger.Warn(serviceId, statusId, serviceType, conversationId, notes, sendPacket, receivePacket, requestedBy);
        }

        public void Error(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
        {
            Logger.Error(serviceId, statusId, serviceType, conversationId, notes, sendPacket, receivePacket, requestedBy);
        }

        public void Fatal(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
        {
            Logger.Fatal(serviceId, statusId, serviceType, conversationId, notes, sendPacket, receivePacket, requestedBy);
        }   

        /// <summary>
        /// This function should be overridden with your own individual implementation to update the appropriate logging row
        /// with a receivedDate (generally set to DateTime.Now) and any received data (passed in via receivePacket parameter)
        /// </summary>
        /// <param name="receivedDate">Date the callback was received</param>
        /// <param name="receivePacket">Received data</param>
        public virtual void LogCallbackReceived(DateTime receivedDate, string receivePacket = null)
        {
            throw new NotImplementedException("You must override the LogCallbackReceived function with your own implementation.");
        }

        private static class LogManager
        {
            private static readonly WrapperMap logWrapper = new WrapperMap(new WrapperCreationHandler(WrapperCreationHandler));

            private static ILoggerWrapper WrapperCreationHandler(ILogger logger)
            {
                return new ServiceLoggerReference(logger);
            }


            public static ServiceLoggerReference GetLogger(string name)
            {
                return (ServiceLoggerReference)logWrapper.GetWrapper(
                 LoggerManager.GetLogger(Assembly.GetCallingAssembly(), name));
            }
        }

        public sealed class ServiceLoggerReference : LoggerWrapperImpl
        {
            private static readonly Type declaringType = typeof(ServiceLoggerReference);

            public ServiceLoggerReference(ILogger logger)
                : base(logger)
            {
                //empty ctor as it's just a wrapper for the base logger - this only 
                //exists to override the implementation of the debug / info / warn etc functions
            }

            #region custom logging methods

            public void Debug(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
            {
                 var loggingEvent = new ServiceLoggingEvent
                 (
                    declaringType,
                    Logger.Repository,
                    Logger.Name,
                    Level.Debug,
                    string.Empty,
                    null
                    );

                loggingEvent.Properties[LoggingField.ServiceIdField.ToDescriptionString()] = serviceId;
                loggingEvent.Properties[LoggingField.StatusIdField.ToDescriptionString()] = statusId;
                loggingEvent.Properties[LoggingField.ServiceTypeField.ToDescriptionString()] = serviceType;

                if (conversationId != new Guid()) //else leave null
                    loggingEvent.Properties[LoggingField.ConversationIdField.ToDescriptionString()] = conversationId;
                
                loggingEvent.Properties[LoggingField.NotesField.ToDescriptionString()] = notes;
                loggingEvent.Properties[LoggingField.SendPacketField.ToDescriptionString()] = sendPacket;
                loggingEvent.Properties[LoggingField.ReceivePacketField.ToDescriptionString()] = receivePacket;
                loggingEvent.Properties[LoggingField.RequestedByField.ToDescriptionString()] = requestedBy;

                Logger.Log(loggingEvent);
            }

            public void Info(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
            {
                var loggingEvent = new ServiceLoggingEvent
                (
                   declaringType,
                   Logger.Repository,
                   Logger.Name,
                   Level.Info,
                   string.Empty,
                   null
                   );

                loggingEvent.Properties[LoggingField.ServiceIdField.ToDescriptionString()] = serviceId;
                loggingEvent.Properties[LoggingField.StatusIdField.ToDescriptionString()] = statusId;
                loggingEvent.Properties[LoggingField.ServiceTypeField.ToDescriptionString()] = serviceType;
                
                if (conversationId != new Guid()) //else leave null
                    loggingEvent.Properties[LoggingField.ConversationIdField.ToDescriptionString()] = conversationId;

                loggingEvent.Properties[LoggingField.NotesField.ToDescriptionString()] = notes;
                loggingEvent.Properties[LoggingField.SendPacketField.ToDescriptionString()] = sendPacket;
                loggingEvent.Properties[LoggingField.ReceivePacketField.ToDescriptionString()] = receivePacket;
                loggingEvent.Properties[LoggingField.RequestedByField.ToDescriptionString()] = requestedBy;

                Logger.Log(loggingEvent);
            }

            public void Warn(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
            {
                var loggingEvent = new ServiceLoggingEvent
                (
                   declaringType,
                   Logger.Repository,
                   Logger.Name,
                   Level.Warn,
                   string.Empty,
                   null
                   );

                loggingEvent.Properties[LoggingField.ServiceIdField.ToDescriptionString()] = serviceId;
                loggingEvent.Properties[LoggingField.StatusIdField.ToDescriptionString()] = statusId;
                loggingEvent.Properties[LoggingField.ServiceTypeField.ToDescriptionString()] = serviceType;

                if (conversationId != new Guid()) //else leave null
                    loggingEvent.Properties[LoggingField.ConversationIdField.ToDescriptionString()] = conversationId;

                loggingEvent.Properties[LoggingField.NotesField.ToDescriptionString()] = notes;
                loggingEvent.Properties[LoggingField.SendPacketField.ToDescriptionString()] = sendPacket;
                loggingEvent.Properties[LoggingField.ReceivePacketField.ToDescriptionString()] = receivePacket;
                loggingEvent.Properties[LoggingField.RequestedByField.ToDescriptionString()] = requestedBy;

                Logger.Log(loggingEvent);
            }

            public void Error(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
            {
                var loggingEvent = new ServiceLoggingEvent
                (
                   declaringType,
                   Logger.Repository,
                   Logger.Name,
                   Level.Error,
                   string.Empty,
                   null
                   );

                loggingEvent.Properties[LoggingField.ServiceIdField.ToDescriptionString()] = serviceId;
                loggingEvent.Properties[LoggingField.StatusIdField.ToDescriptionString()] = statusId;
                loggingEvent.Properties[LoggingField.ServiceTypeField.ToDescriptionString()] = serviceType;

                if (conversationId != new Guid()) //else leave null
                    loggingEvent.Properties[LoggingField.ConversationIdField.ToDescriptionString()] = conversationId;

                loggingEvent.Properties[LoggingField.NotesField.ToDescriptionString()] = notes;
                loggingEvent.Properties[LoggingField.SendPacketField.ToDescriptionString()] = sendPacket;
                loggingEvent.Properties[LoggingField.ReceivePacketField.ToDescriptionString()] = receivePacket;
                loggingEvent.Properties[LoggingField.RequestedByField.ToDescriptionString()] = requestedBy;

                Logger.Log(loggingEvent);
            }

            public void Fatal(int serviceId, int statusId, string serviceType = null, Guid conversationId = new Guid(), string notes = null, string sendPacket = null, string receivePacket = null, string requestedBy = null)
            {
                var loggingEvent = new ServiceLoggingEvent
                (
                   declaringType,
                   Logger.Repository,
                   Logger.Name,
                   Level.Fatal,
                   string.Empty,
                   null
                   );

                loggingEvent.Properties[LoggingField.ServiceIdField.ToDescriptionString()] = serviceId;
                loggingEvent.Properties[LoggingField.StatusIdField.ToDescriptionString()] = statusId;
                loggingEvent.Properties[LoggingField.ServiceTypeField.ToDescriptionString()] = serviceType;

                if (conversationId != new Guid()) //else leave null
                    loggingEvent.Properties[LoggingField.ConversationIdField.ToDescriptionString()] = conversationId;

                loggingEvent.Properties[LoggingField.NotesField.ToDescriptionString()] = notes;
                loggingEvent.Properties[LoggingField.SendPacketField.ToDescriptionString()] = sendPacket;
                loggingEvent.Properties[LoggingField.ReceivePacketField.ToDescriptionString()] = receivePacket;
                loggingEvent.Properties[LoggingField.RequestedByField.ToDescriptionString()] = requestedBy;

                Logger.Log(loggingEvent);
            }

            #endregion

            #region custom types

            private class ServiceLoggingEvent : LoggingEvent
            {
                //force creation through this constructor to populate properties on every exception

                public ServiceLoggingEvent(Type declaringType, ILoggerRepository repository, string loggerName, Level level, string message, Exception ex)
                    : base(declaringType, repository, loggerName, level, message, ex)
                {
                    Properties[LoggingField.ServerField.ToDescriptionString()] = ServerName;
                }
            }

            #endregion

        }

        public enum LoggingField
        {
            [Description("ServiceId")]
            ServiceIdField,

            [Description("ServiceType")]
            ServiceTypeField,

            [Description("ConversationId")]
            ConversationIdField,

            [Description("StatusId")]
            StatusIdField,

            [Description("Notes")]
            NotesField,

            [Description("SendPacket")]
            SendPacketField,

            [Description("ReceivePacket")]
            ReceivePacketField,

            [Description("RequestedBy")]
            RequestedByField,

            [Description("Server")]
            ServerField
        }

        // Make sure this is inserted into the Post Deployment SQL Script
        public enum ServiceStatusEnum
        {
            [Description("Not Sent")]
            NotSent = 0,

            [Description("Sending Request")]
            SendingRequest = 1,

            [Description("Request Timeout")]
            RequestTimeout = 2,

            [Description("Awaiting Response")]
            AwaitingResponse = 3,

            [Description("Response Timeout")]
            ResponseTimeout = 4,
            
            [Description("Completed")]
            Completed = 5,
            
            [Description("Error")]
            Error = 6,
            
            [Description("From Cache")]
            FromCache = 7,
            
            [Description("Disabled")]
            Disabled = 8
        }
    }
}