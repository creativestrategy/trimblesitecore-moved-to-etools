﻿using System.Web;
using log4net.Core;

namespace Deloitte.Framework.Logger.Loggers
{
    public class BaseLogger
    {
        protected readonly static string ServerName = HttpContext.Current.Server.MachineName;
    }
}