﻿<?xml version="1.0"?>
<configuration>
    <!-- TO INSTALL: 1. Add a reference to log4net.dll to your web project (from the Dep folder in the framework project)
                     2. Right click on the log4net.dll in the reference and select 'Properties'
                     3. Change the value in the Aliases field from 'global' to 'log4net' - this is because Sitecore defines a log4net namespace also
                     4. Add the following line *as the first line* in your global.asax file
                        extern alias log4net;
                     2. Copy this file into your App_Config\Include folder and rename to log4net.config
                     3. Update your global.asax Application_Start function with the following line
                        log4net::log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(@Server.MapPath("~/App_Config/log4net.config")));
                     4. Add a using statement in global.asax to System.IO to use the Server.MapPath call
                     5. Update your file (now under App_Config/log4net.config) with your own connection strings / email addresses etc
                     6. Build / Deploy - You should now have your custom loggers operational. See classes for syntax examples.
                     
            Sample usage:
      
            For service logging
                Define service id(s) and status id(s) in db tables first (otherwise foreign key constraints will cause your service logging to fail)
      
            Define at top of global.asax
                public static ApplicationLogger ApplicationLogger;
                public static ServiceLogger ServiceLogger;
          
            Calling functions across entire application
          
                eg Global.ApplicationLogger.Fatal("This is the message", new Exception("This is the test exception"));
                    This will also trigger the smtp appender to send an email as the level is FATAL (once you have properly configured server etc in log4net.config)
          
                eg Global.ServiceLogger.Debug("Service message", 1, 1, "Service type", "Notes go here", "This is a send packet", "This a receive packet", "Requested by me..");

    -->

  <log4net>

  <!-- INSERT YOUR LOG4NET CONFIGURATION CONTENT FROM WEB.CONFIG HERE -->

  <!-- END LOG4NET CONFIGURATION CONTENT FROM WEB.CONFIG -->

  <!-- Start custom logging sections -->
  
    <!-- Custom Loggers -->
    
    <!-- Application logger appends to the ApplicationLogAppender and (fatals) to SmtpAppender -->
    <!-- Filter what level of information gets logged by changing the LEVEL attribute -->
    <logger name="ApplicationLogger" >
      <level value="DEBUG" />
      <appender-ref ref="ApplicationLogAppender" />
      <appender-ref ref="SmtpAppender" />
    </logger>

    <!-- Application logger appends to the ServiceLogAppender and (fatals) to SmtpAppender -->
    <!-- Filter what level of information gets logged by changing the LEVEL attribute -->
    <logger name="ServiceLogger" >
      <level value="DEBUG" />
      <appender-ref ref="ServiceLogAppender" />
      <appender-ref ref="SmtpAppender" />
    </logger>
    
    <!-- Appenders -->
    
    <!-- Appender for service logging -->
    <appender name="ServiceLogAppender" type="log4net.Appender.AdoNetAppender">

      <!-- Using a buffer size of one forces synchronous writes -->
      <bufferSize value="1" />
      <connectionType value="System.Data.SqlClient.SqlConnection, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      
      <!-- Set this connection string to your system-specific value or use transforms -->
      <connectionString value="data source=[database server];initial catalog=[database name];integrated security=false;persist security info=True;User ID=[user];Password=[password]" />
      
      <!-- IMPORTANT-->
      <!-- The CommandText must align with the table structure defined in SetupTables.sql 
           - if you change your tables you MUST update this -->
      <commandText value="INSERT INTO [PortalData].[dbo].[Logger_WSLog] ([ServiceId] ,[ServiceType] ,[Date] ,[ConversationId] ,[StatusId] ,[Notes] ,[SendPacket] ,[ReceivePacket] ,[RequestedBy] ,[Server])
                   VALUES (@ServiceId, @ServiceType, @Date, @ConversationId, @StatusId, @Notes, @SendPacket, @ReceivePacket, @RequestedBy, @Server)" />

      <!-- custom parameters -->
      <parameter>
        <parameterName value="@ServiceId" />
        <dbType value="Int32" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="ServiceId" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@ServiceType" />
        <dbType value="String" />
        <size value="50" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="ServiceType" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@Date" />
        <dbType value="DateTime" />
        <layout type="log4net.Layout.RawTimeStampLayout" />
      </parameter>

      <parameter>
        <parameterName value="@ConversationId" />
        <dbType value="Guid" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="ConversationId" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@StatusId" />
        <dbType value="Int32" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="StatusId" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@Notes" />
        <dbType value="String" />
        <size value="100" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="Notes" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@SendPacket" />
        <dbType value="String" />
        <!-- Using -1 is equivalent to size MAX -->
        <!-- see https://issues.apache.org/jira/browse/LOG4NET-202 -->
        <size value="-1" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="SendPacket" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@ReceivePacket" />
        <dbType value="String" />
        <!-- Using -1 is equivalent to size MAX -->
        <!-- see https://issues.apache.org/jira/browse/LOG4NET-202 -->
        <size value="-1" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="ReceivePacket" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@RequestedBy" />
        <dbType value="String" />
        <size value="50" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="RequestedBy" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@Server" />
        <dbType value="String" />
        <size value="50" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="Server" />
        </layout>
      </parameter>

      <!-- There are no standard parameters for logging to the Services table (exceptions etc go in the Application Log) -->
      
    </appender>

    <!-- Appender for application logging -->
    <appender name="ApplicationLogAppender" type="log4net.Appender.AdoNetAppender">

      <!-- Using a buffer size of one forces synchronous writes -->
      <bufferSize value="1" />

      <connectionType value="System.Data.SqlClient.SqlConnection, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />

      <!-- Set this connection string to your system-specific value or use transforms -->
      <connectionString value="data source=[database server];initial catalog=[database name];integrated security=false;persist security info=True;User ID=[user];Password=[password]" />

      <!-- IMPORTANT-->
      <!-- The CommandText must align with the table structure defined in SetupTables.sql 
           - if you change your tables you MUST update this -->
      <commandText value="INSERT INTO [PortalData].[dbo].[Logger_AppLog] ([Date], [Thread] ,[Level] ,[Logger] ,[Message] ,[Exception], [Server], [GroupId])
                   VALUES (@Date, @Thread, @Level, @Logger, @Message, @Exception, @Server, @GroupId)" />

      <!-- custom parameters -->
      <parameter>
        <parameterName value="@Server" />
        <dbType value="String" />
        <size value="50" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="Server" />
        </layout>
      </parameter>

      <parameter>
        <parameterName value="@GroupId" />
        <dbType value="Int32" />
        <layout type="log4net.Layout.RawPropertyLayout">
          <key value="GroupId" />
        </layout>
      </parameter>

      <!-- standard log4net parameters -->
      <parameter>
        <parameterName value="@Date" />
        <dbType value="DateTime" />
        <layout type="log4net.Layout.RawTimeStampLayout" />
      </parameter>
      <parameter>
        <parameterName value="@Thread" />
        <dbType value="String" />
        <size value="255" />
        <layout type="log4net.Layout.PatternLayout">
          <conversionPattern value="%thread" />
        </layout>
      </parameter>
      <parameter>
        <parameterName value="@Level" />
        <dbType value="String" />
        <size value="50" />
        <layout type="log4net.Layout.PatternLayout">
          <conversionPattern value="%level" />
        </layout>
      </parameter>
      <parameter>
        <parameterName value="@Logger" />
        <dbType value="String" />
        <size value="255" />
        <layout type="log4net.Layout.PatternLayout">
          <conversionPattern value="%logger" />
        </layout>
      </parameter>
      <parameter>
        <parameterName value="@Message" />
        <dbType value="String" />
        <size value="4000" />
        <layout type="log4net.Layout.PatternLayout">
          <conversionPattern value="%message" />
        </layout>
      </parameter>
      <parameter>
        <parameterName value="@Exception" />
        <dbType value="String" />
        <size value="2000" />
        <layout type="log4net.Layout.ExceptionLayout" />
      </parameter>

    </appender>
    
    <!-- Appender for critical errors - using an SmtpAppender for email alerts -->
    <!-- Customise this for your particular application, possibly using transforms -->
    <appender name="SmtpAppender" type="log4net.Appender.SmtpAppender">
      <threshold value="FATAL" />
      <to value="to@domain.com" />
      <from value="from@domain.com" />
      <subject value="test logging message" />
      <smtpHost value="SMTPServer.domain.com" />
      
      <!-- Buffer size less than zero forces synchronous sending -->
      <bufferSize value="-1" />
     
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%property{log4net:HostName} :: %level :: %message %newlineLogger: %logger%newlineThread: %thread%newlineDate: %date%newlineNDC: %property{NDC}%newline%newline" />
      </layout>
    </appender>
  
  <!-- End custom logging sections -->
  </log4net>
</configuration>
