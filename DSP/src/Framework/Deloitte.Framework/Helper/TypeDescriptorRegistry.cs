﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Deloitte.Framework.Helper
{
    //lifted from FUBUMVC 
    //used to load types into a cache 
    /// <summary>
    /// TypeDescriptor Registry acts as a cache storage for Reflected types
    ///  /// Retrieved from FUBUMVC framework http://code.google.com/p/fubumvc/
    /// http://code.google.com/p/fubumvc/source/browse/branches/reboot/src/FubuMVC.Core/Models/TypeDescriptorRegistry.cs?spec=svn260&r=260
    /// </summary>
    public static class TypeDescriptorRegistry
    {

        private static readonly Cache<Type, IDictionary<string, PropertyInfo>> _cache;

        static TypeDescriptorRegistry()
        {
            _cache = new Cache<Type, IDictionary<string, PropertyInfo>>( type => new Dictionary<string, PropertyInfo>( ) );
        }

        public static IDictionary<string, PropertyInfo> GetPropertiesFor<TYPE>()
        {
            return GetPropertiesFor( typeof( TYPE ) );
        }

        public static IDictionary<string, PropertyInfo> GetPropertiesFor( Type itemType )
        {
            var map = _cache.Retrieve( itemType );

            if ( map.Count == 0 )
            {
                foreach ( var propertyInfo in itemType.GetProperties( BindingFlags.Public | BindingFlags.Instance ) )
                {
                    map.Add( propertyInfo.Name, propertyInfo );
                }
            }

            return map;
        }

        public static void ClearAll()
        {
            _cache.ClearAll( );
        }
    }
}
