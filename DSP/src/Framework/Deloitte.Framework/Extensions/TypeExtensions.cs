﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deloitte.Framework.Extensions
{
    /// <summary>
    /// Extension methods for all objects
    /// </summary>
    public static class TypeExtensions
    {
        public static bool IsNullableOfT( this Type theType )
        {
            return theType.IsGenericType && theType.GetGenericTypeDefinition( ) == typeof( Nullable<> );
        }

        public static bool SetValue( this object theObject, string propertyName, object val )
        {
            try
            {
                var property = theObject.GetType( ).GetProperty( propertyName );
                if ( property == null )
                    return false;
                // convert the value to the expected type
                val = Convert.ChangeType( val, property.PropertyType );
                // attempt the assignment
                property.SetValue( theObject, val, null );
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static object GetPropertyValue( this object theObject, string propertyName )
        {
            try
            {
                var property = theObject.GetType( ).GetProperty( propertyName );
                return property == null ? null : property.GetValue( theObject, null );
            }
            catch
            {
                return null;
            }
        }
    }
}
