using System.Diagnostics;
using System.Globalization;
using Deloitte.Framework.Helper;

namespace Deloitte.Framework.Extensions
{
    public static class StringExtensions
    {
        [DebuggerStepThrough]
        public static string FormatWith( this string target, params object [ ] args )
        {
            Check.Argument.IsNotEmpty( target, "target" );

            return string.Format( CultureInfo.CurrentCulture, target, args );
        }

        /// <summary>
        /// Determines whether the specified string has value.
        /// </summary>
        /// <param name="str">string</param>
        /// <returns>
        ///   <c>true</c> if the specified string has value; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasValue(this string str)
        {
            return (!string.IsNullOrEmpty(str) && str != "");
        }

    }
}
