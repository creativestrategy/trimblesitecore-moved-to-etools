﻿using System.Text.RegularExpressions;
using Sitecore;

namespace Deloitte.SC.UI.Extensions
{
    public static class StringExtenstions
    {
        /// <summary>
        /// Gets the summary.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <param name="length">The length.</param>
        /// <param name="addString">The add string.</param>
        /// <returns></returns>
        public static string GetSummary(this string str, int length, string addString)
        {
            // rid of characters which will break stuff
            str = str.Replace("\n", "");
            str = str.Replace("\r", "");
            str = str.Replace("\t", "");

            // clean up any leading or whitespace.
            str = StringUtil.RemoveTags(str.Trim());

            if (str.Length > length)
            {
                // if string is null or empty return empty string.
                if (string.IsNullOrEmpty(str)) return "";

                // otherwise generate the summary.
                str = Regex.Match(str, @"^.{1," + length + @"}\b(?<!\s)").Value;
            }

            // throw the append string on no matter what... looks better being consistant.
            if (!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(addString))
                str = str.TrimEnd('.') + addString;

            return str;
        }

        /// <summary>
        /// Gets the summary.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <param name="length">The length.</param>
        /// <param name="addString">The add string.</param>
        /// <param name="forceAppendString">if set to <c>true</c> [force append string].</param>
        /// <returns></returns>
        public static string GetSummary(this string str, int length, string addString, bool forceAppendString)
        {
            string summary = str.GetSummary(length, addString);

            // throw the append string on no matter what... looks better being consistant.
            if (!string.IsNullOrEmpty(summary) && !string.IsNullOrEmpty(addString) && forceAppendString)
                summary = summary.TrimEnd('.') + addString;
            else if (summary.Length < str.Length)
                summary = summary.TrimEnd('.') + addString;

            return summary;
        }
    }
}