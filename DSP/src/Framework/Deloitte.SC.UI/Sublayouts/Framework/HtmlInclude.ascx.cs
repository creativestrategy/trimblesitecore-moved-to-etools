﻿using System;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;
using Deloitte.SC.UI.SharedSource.Web.UI.Sublayouts;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public partial class HtmlInclude : SublayoutBase
    {
        public virtual void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                litHtml.Text =SitecoreItem.WebsiteItem.GetFieldValue(this.FieldName);
        }

        private string fieldName;
        public virtual string FieldName
        {
            get { return fieldName; }
            set
            {
                fieldName = value ?? GetParameter("FieldName");
            }

        }

    }
}