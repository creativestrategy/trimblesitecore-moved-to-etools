﻿using System.Web;
using Deloitte.SC.Framework.Helpers;
using Deloitte.SC.UI.SharedSource.Web.UI.Sublayouts;
using Sitecore.Data.Items;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public class SublayoutBaseExtended : SublayoutBase
    {
        /// <summary>
        /// Gets the current item.
        /// </summary>
        public Item CurrentItem
        {
            get { return SitecoreItem.CurrentItem; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is page editor editing.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is page editor editing; otherwise, <c>false</c>.
        /// </value>
        public bool IsPageEditorEditing
        {
            get { return SitecoreItem.IsPageEditorEditing; }
        }


        /// <summary>
        /// Gets the website item.
        /// </summary>
        public Item WebsiteItem
        {
            get { return SitecoreItem.WebsiteItem;  }
        }

        /// <summary>
        /// Gets the home item.
        /// </summary>
        public Item HomeItem
        {
            get { return SitecoreItem.HomeItem; }
        }

        public string HomeItemPath
        {
            get { return SitecoreItem.HomeItemPath;  }
        }

        public string GetTranslateText(string key)
        {
            return SitecoreItem.GetTranslateText(key);
        }

        /// <summary>
        /// Gets the query string value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetQueryStringValue(string key)
        {
            return HttpUtility.HtmlEncode(HttpContext.Current.Request.QueryString[key] ?? string.Empty);
        }

        /// <summary>
        /// Gets the query string value as int.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public int GetQueryStringValueAsInt(string key)
        {
            // default return value
            int retValue = 0;
            int.TryParse(GetQueryStringValue(key), out retValue);

            // return value
            return retValue;
        }

    }
}