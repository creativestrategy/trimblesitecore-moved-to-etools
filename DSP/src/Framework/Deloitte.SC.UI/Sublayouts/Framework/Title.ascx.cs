﻿using System;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public partial class Title : SublayoutBaseExtended
    {
        public virtual void Page_Load(object sender, EventArgs e)
        {
            frTitle.Item = DataSourceItem;
        }
    }
}