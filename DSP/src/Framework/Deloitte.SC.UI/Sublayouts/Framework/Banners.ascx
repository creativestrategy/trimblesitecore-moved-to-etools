﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Banners.ascx.cs" Inherits="Deloitte.SC.UI.Sublayouts.Framework.Banners" %>

<ul runat="server" id="ul">
    <asp:Repeater runat="server" ID="rptData" onitemdatabound="rptData_ItemDataBound">
        <ItemTemplate>
            <!-- Banner START -->
            <li>
                <div class="text">
                    <h2>
                        <sc:Link ID="lnkBannerLinkNotEditable" runat="server" Field="BannerLink" DisableWebEditing="true">
                            <sc:FieldRenderer ID="frBannerTitle" runat="server" FieldName="BannerTitle" DisableWebEditing="false"/>
                        </sc:Link>
                    </h2>
                    <!-- Banner Copy -->
                </div>
                <div class="feature">
                    <sc:Link ID="lnkBannerLink" runat="server" Field="BannerLink" DisableWebEditing="false">
                        <sc:FieldRenderer ID="frBannerImage" runat="server" FieldName="BannerImage" DisableWebEditing="false"/>
                    </sc:Link>
                    <!-- Banner Feature -->
                </div>
            </li>
            <!-- Banner END -->
        </ItemTemplate>
    </asp:Repeater>
</ul>
