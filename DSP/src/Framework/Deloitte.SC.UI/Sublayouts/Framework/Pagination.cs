﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Extensions;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public class Pagination : WebControl
    {
        #region private member variables

        private int _currentPage;
        private int _defaultPageSize = 10; // Default page size
        private string _pageNumberQueryStringKey = "pn";
        private string _pageSizeQuerystringKey = "ps";
        private string _currentPageNumberCssClass = "active";

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets the record count.
        /// </summary>
        /// <value>
        /// The record count.
        /// </value>
        public int RecordCount { get; set; }

        /// <summary>
        /// Gets or sets the page number CSS class.
        /// </summary>
        /// <value>
        /// The page number CSS class.
        /// </value>
        public string PageNumberCssClass { get; set; }

        /// <summary>
        /// Gets or sets the current page number CSS class.
        /// </summary>
        /// <value>
        /// The current page number CSS class.
        /// </value>
        public string CurrentPageNumberCssClass
        {
            get { return string.Format("{0} {1}", PageNumberCssClass, _currentPageNumberCssClass).Trim(); }
            set { _currentPageNumberCssClass = value; }
        }

        /// <summary>
        /// Gets or sets the default size of the page.
        /// </summary>
        /// <value>
        /// The default size of the page.
        /// </value>
        public int DefaultPageSize
        {
            get { return _defaultPageSize; }
            set { _defaultPageSize = value; }
        }

        /// <summary>
        /// Gets the current page
        /// </summary>
        public int CurrentPage
        {
            get
            {
                int pageNo = 1;

                if (HttpContext.Current.Request.QueryString[_pageNumberQueryStringKey] != null &&
                    !String.IsNullOrEmpty(HttpContext.Current.Request.QueryString[_pageNumberQueryStringKey]))
                    Int32.TryParse(HttpContext.Current.Request.QueryString[_pageNumberQueryStringKey], out pageNo);

                return pageNo;
            }
        }


        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>
        /// The size of the page.
        /// </value>
        public int PageSize
        {
            get
            {
                int pageSize = _defaultPageSize;

                if (HttpContext.Current.Request.QueryString[_pageSizeQuerystringKey] != null && !String.IsNullOrEmpty(HttpContext.Current.Request.QueryString[_pageSizeQuerystringKey]))
                    Int32.TryParse(HttpContext.Current.Request.QueryString[_pageSizeQuerystringKey], out pageSize);

                return pageSize;
            }
        }

        /// <summary>
        /// Set value for the querystring key used for current page number (default "pn")
        /// </summary>
        /// <value>
        /// The page number query string key.
        /// </value>
        public string PageNumberQueryStringKey
        {
            get { return _pageNumberQueryStringKey; }
            set { _pageNumberQueryStringKey = value; }
        }

        /// <summary>
        /// Set a custom value for the querystring key used for page size (default "ps")
        /// </summary>
        /// <value>
        /// The page size querystring key.
        /// </value>
        public string PageSizeQuerystringKey
        {
            get { return _pageSizeQuerystringKey; }
            set { _pageSizeQuerystringKey = value; }
        }

        /// <summary>
        /// Gets the total pages.
        /// </summary>
        public int TotalPages
        {
            get { return _totalPages; }
        }

        public string FirstButtonText { get; set; }
        public string LastButtonText { get; set; }
        public string NextButtonText { get; set; }
        public string PreviousButtonText { get; set; }

        #endregion

        #region Private Member Variables

        private int _totalPages;
        private int _startIndex;

        #endregion

        #region Page Events

        protected override void OnLoad(EventArgs e)
        {
            Visible = true;

            // calculate total pages
            _totalPages = (int) Math.Ceiling((double) RecordCount/PageSize);
            if (_totalPages == 0) _totalPages = 1;

            // convert the current page to an index value to be used
            if (CurrentPage > 0)
                _currentPage = CurrentPage - 1;

            // get the start index needed for logic calculations used later
            CalculateStartIndex(_currentPage);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            // write out the css class for the surrounding container element if supplied
            if (!String.IsNullOrEmpty(CssClass))
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass);

            // <ul>
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            // render the start paging buttons
            RenderStartButtons(writer);
            // render the actual page number buttons
            RenderPages(writer);
            // render the final paging buttons
            RenderEndButtons(writer);

            // </ul>
            writer.RenderEndTag();
        }

        #endregion

        #region private methods/ helpers

        /// <summary>
        /// Calculate the start index to build page number from dependant on the current page user is requesting.
        /// </summary>
        /// <param name="topPage">The current page index.</param>
        private void CalculateStartIndex(int topPage)
        {
            var mid = PageSize/2;
            _startIndex = 0;
            if (topPage >= 0 && topPage < _totalPages)
            {
                if (topPage >= mid)
                {
                    if (_totalPages - topPage > mid)
                        _startIndex = topPage - (mid - 1);
                    else if (_totalPages > PageSize)
                        _startIndex = _totalPages - PageSize;
                }
            }
        }

        /// <summary>
        /// Renders the pages.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void RenderPages(HtmlTextWriter writer)
        {
            for (int i = _startIndex; i < _startIndex + PageSize; i++)
            {
                if (i == _totalPages) break;

                if (i == _currentPage)
                    RenderPage(writer, i, CurrentPageNumberCssClass);
                else
                    RenderPage(writer, i, PageNumberCssClass);
            }
        }

        /// <summary>
        /// Renders the page.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="pageText">The page text.</param>
        private void RenderPage(HtmlTextWriter writer, int pageIndex, string cssClass, string pageText = null)
        {
            if (!String.IsNullOrEmpty(cssClass))
                writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            // <li>
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            // build the page url with pagination querystring values
            string url = GetPageUrl(pageIndex);

            // <a>
            writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, (pageIndex + 1).ToString());
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            if (string.IsNullOrEmpty(pageText)) // if page text is not specified then render page number
            {
                pageText = (pageIndex + 1).ToString();
            }
            writer.Write(pageText);
            // </a>
            writer.RenderEndTag();

            // </li>
            writer.RenderEndTag();
        }

        /// <summary>
        /// Render an li element with only text.
        /// </summary>
        /// <param name="writer">The HtmlTextWriter to render to.</param>
        /// <param name="text">The value to write inside the element.</param>
        private static void RenderTextPage(HtmlTextWriter writer, string text)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(text);
            writer.RenderEndTag();
        }

        /// <summary>
        /// Renders the start buttons.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void RenderStartButtons(HtmlTextWriter writer)
        {
            if (_totalPages > PageSize)
            {
                // check if we need to show the first buttons
                if (CurrentPage != 1 && !string.IsNullOrEmpty(FirstButtonText))
                    RenderPage(writer, 0, "first", FirstButtonText);

                // show prev button only if we aren't on first page
                if (CurrentPage > 1 && !string.IsNullOrEmpty(PreviousButtonText))
                    RenderPage(writer, CurrentPage - 2, "prev", PreviousButtonText);
            }
        }

        /// <summary>
        /// Renders the end buttons.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void RenderEndButtons(HtmlTextWriter writer)
        {
            // render supporting text and last page number button
            //RenderTextPage(writer, "of");
            //RenderPage(writer, _totalPages - 1, "");

            if (_totalPages > PageSize)
            {
                if (CurrentPage < _totalPages && !string.IsNullOrEmpty(NextButtonText))
                {
                    // show next button only if we aren't on last page
                    RenderPage(writer, CurrentPage, "next", NextButtonText);

                    // check if we need to show the last buttons
                    if (_totalPages != CurrentPage && !string.IsNullOrEmpty(LastButtonText))
                        RenderPage(writer, 0, "last", LastButtonText);
                }
            }
        }

        /// <summary>
        /// Get the url used for pagination links with the supporting querystring values appended. Retains existing querystring values.
        /// </summary>
        /// <param name="index">The page index.</param>
        /// <returns>The full url.</returns>
        private string GetPageUrl(int index)
        {
            var parameters = HttpContext.Current.Request.GetWriteableQuerystringCollection();
            if (parameters != null)
                parameters[_pageNumberQueryStringKey] = (index + 1).ToString();

            return HttpContext.Current.Request.BuildUrl(parameters);
        }

        #endregion
    }
}