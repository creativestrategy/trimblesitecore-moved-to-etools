﻿using System;
using System.Linq;
using System.Web.UI;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    /// <summary>
    /// Sitemap control (please wrap with UL tag on rendering layout/sublayout)
    /// </summary>
    public class Sitemap : WebControl
    {
        #region private variables

        private string _firstElementClass = "first";
        private string _lastElementClass = "last";
        private string _fieldNameCssClass = "CssClass";

        #endregion

        #region Public Properties

        public virtual string FirstElementClass
        {
            get { return _firstElementClass; }
            set { _firstElementClass = value; }
        }

        public virtual string LastElementClass
        {
            get { return _lastElementClass; }
            set { _lastElementClass = value; }
        }

        public virtual string FieldNameCssClass
        {
            get { return _fieldNameCssClass; }
            set { _fieldNameCssClass = value; }
        }

        public virtual int Depth { get; set; }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Set menu item
            SitemapItem = SitecoreItem.HomeItem;
        }

        protected virtual Item SitemapItem { get; set; }

        public static string AddCssClass(string currentCssClass, string addCssClass)
        {
            if (!string.IsNullOrEmpty(currentCssClass))
            {
                return string.Format("{0} {1}", currentCssClass, addCssClass);
            }

            return addCssClass;
        }

        protected override void DoRender(HtmlTextWriter output)
        {
            if (SitemapItem != null)
            {
                Item[] children = SitemapItem.GetChildrenForSitemap();

                if (children != null && children.Count() > 0)
                {
                    // Rendering specified levels of Sitemap (start from level 1)
                    output.RenderBeginTag(HtmlTextWriterTag.Ul);
                    RenderMenu(output, children, 1, children.FirstOrDefault(), children.LastOrDefault());
                    output.RenderEndTag();
                }
            }
        }

        /// <summary>
        /// Renders the menu.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="children">The children.</param>
        /// <param name="iLevel">The i level.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        private void RenderMenu(HtmlTextWriter output, Item[] children, int iLevel, Item firstItem, Item lastItem)
        {
            if (children != null && children.Count() > 0)
            {
                // process children items
                foreach (Item child in children)
                {
                    Item linkItem = child;

                    // Apply CSS class
                    AddCSSClass(output, child, firstItem, lastItem, FieldNameCssClass);

                    output.RenderBeginTag(HtmlTextWriterTag.Li);

                    RenderLink(output, child, linkItem);

                    // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                    if (iLevel < Depth && linkItem.HasChildren)
                    {
                        Item[] grandChildren = linkItem.GetChildrenForSitemap();
                        if (grandChildren.Count() > 0)
                        {
                            output.RenderBeginTag(HtmlTextWriterTag.Ul);
                            RenderMenu(output, grandChildren, iLevel + 1, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                            output.RenderEndTag();
                        }
                    }

                    // close rendering LI tag
                    output.RenderEndTag();
                }
            }
        }

        /// <summary>
        /// Renders the link.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="child">The child.</param>
        /// <param name="linkItem">The link item.</param>
        public virtual void RenderLink(HtmlTextWriter output, Item child, Item linkItem)
        {
            // begin rendering A tag
            output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
            output.RenderBeginTag(HtmlTextWriterTag.A);

            // A title/ description
            output.Write(SitecoreItem.GetNavigationTitle(child));

            // close rendering A tag
            output.RenderEndTag();
        }

        /// <summary>
        /// Adds the CSS class.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="item">The item.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        /// <param name="cssClassFieldName">Name of the CSS class field.</param>
        private void AddCSSClass(HtmlTextWriter output, Item item, Item firstItem, Item lastItem, string cssClassFieldName = null)
        {
            string cssClass = string.Empty;

            // apply first class
            if (item == firstItem)
            {
                cssClass = AddCssClass(cssClass, FirstElementClass);
            }
            else if (item == lastItem)
            {
                cssClass = AddCssClass(cssClass, LastElementClass);
            }

            // Add CSS Class if specified
            if (!string.IsNullOrEmpty(cssClassFieldName) && !string.IsNullOrEmpty(item.GetFieldValue(cssClassFieldName)))
            {
                cssClass = AddCssClass(cssClass, item.GetFieldValue(cssClassFieldName));
            }

            // CSS Class attribute 
            if (!string.IsNullOrEmpty(cssClass))
                output.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
        }

        /// <summary>
        /// Need to override the GetCachingID for Sitecore caching
        /// </summary>
        /// <returns>CacheId</returns>
        protected override string GetCachingID()
        {
            return GetType().ToString();
        }
    }
}