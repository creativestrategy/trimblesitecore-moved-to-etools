﻿using System;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public partial class GeneralText : SublayoutBaseExtended
    {
        public virtual void Page_Load(object sender, EventArgs e)
        {
            frGeneralText.Item = DataSourceItem;
        }
    }
}