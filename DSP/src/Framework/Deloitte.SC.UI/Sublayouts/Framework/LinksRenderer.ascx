﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="LinksRenderer.ascx.cs" Inherits="Deloitte.SC.UI.Sublayouts.Framework.LinksRenderer" %>

<asp:Literal runat="server" ID="litWrapperHtmlBegin" />

<!-- START: Link Rendering -->
<asp:Repeater runat="server" ID="rptData" onitemdatabound="rptData_ItemDataBound">
    <ItemTemplate>
        <li id="li" runat="server">
            <sc:EditFrame runat="server" ID="efActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Framework/LinkRendererActions">
                <sc:Link ID="lnkLinkTarget" runat="server" Field="LinkTarget" DisableWebEditing="true"><sc:FieldRenderer ID="frLinkTitle" runat="server" FieldName="LinkTitle" DisableWebEditing="true"/></sc:Link>
            </sc:EditFrame>
        </li>
    </ItemTemplate>
</asp:Repeater>
<!-- END: Link Rendering -->

<asp:Literal runat="server" ID="litWrapperHtmlEnd" />

<!-- CSS guys need to fix this layout -->
<sc:EditFrame  runat="server" ID="efAddLink" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Framework/LinkRendererAddLink">
    <div style="border: 1px solid black; height:20px; color: Black;">
        Add a Link
    </div>
</sc:EditFrame>
