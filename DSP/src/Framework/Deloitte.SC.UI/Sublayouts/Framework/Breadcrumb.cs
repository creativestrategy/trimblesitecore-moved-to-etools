﻿using System.Collections.Generic;
using System.Web.UI;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI;
using System.Web.Caching;

// NOTE: code copied from QSuper and done some minor change
namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public class Breadcrumb : WebControl
    {
        protected override void DoRender(HtmlTextWriter output)
        {
            //write out id/class/ul tag
            output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumbs");
            output.RenderBeginTag(HtmlTextWriterTag.Ul);

            RenderBreadcrumb(SitecoreItem.CurrentItem.GetBreadcrumbItems(), output);

            //close ul tag
            output.RenderEndTag();
        }

        private void RenderBreadcrumb(List<Item> source, HtmlTextWriter output)
        {
            if (source != null)
            {
                foreach (Item item in source)
                {
                    //open LI tag
                    output.RenderBeginTag(HtmlTextWriterTag.Li);

                    // begin rendering A tag
                    output.AddAttribute(HtmlTextWriterAttribute.Href, item.GetFriendlyUrl());
                    output.RenderBeginTag(HtmlTextWriterTag.A);

                    // A title/ description
                    output.Write(SitecoreItem.GetBreadcrumbTitle(item));

                    // close rendering A tag
                    output.RenderEndTag();

                    //close rendering li tag
                    output.RenderEndTag();
                }

                //write out the current page

                //open LI tag
                output.RenderBeginTag(HtmlTextWriterTag.Li);

                //write current page nav title
                output.Write(SitecoreItem.GetBreadcrumbTitle(SitecoreItem.CurrentItem));

                //close rendering li tag
                output.RenderEndTag();
            }
        }
    }
}