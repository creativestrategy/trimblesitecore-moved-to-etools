﻿using System;
using System.Linq;
using System.Web.UI;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    /// <summary>
    /// Navigation control (please wrap with UL tag on rendering layout/sublayout)
    /// </summary>
    public class Navigation : WebControl
    {
        #region private variables

        private string _firstElementClass = "first";
        private string _lastElementClass = "last";
        private string _activeElementClass = "active";
        private string _fieldNameLiId = "LiId";
        private string _fieldNameLinkItem = "LinkItem";
        private string _fieldNameCssClass = "CssClass";

        #endregion

        #region Public Properties

        public virtual string FirstElementClass
        {
            get { return _firstElementClass; }
            set { _firstElementClass = value; }
        }

        public virtual string LastElementClass
        {
            get { return _lastElementClass; }
            set { _lastElementClass = value; }
        }

        public virtual string ActiveElementClass
        {
            get { return _activeElementClass; }
            set { _activeElementClass = value; }
        }

        public virtual string FieldNameLiId
        {
            get { return _fieldNameLiId; }
            set { _fieldNameLiId = value; }
        }

        public virtual string FieldNameLinkItem
        {
            get { return _fieldNameLinkItem; }
            set { _fieldNameLinkItem = value; }
        }

        public virtual string FieldNameCssClass
        {
            get { return _fieldNameCssClass; }
            set { _fieldNameCssClass = value; }
        }

        public virtual int LevelFrom { get; set; }

        public virtual int LevelTo { get; set; }

        /// <summary>
        /// Gets or sets the type of the menu.
        ///     Main & Sub
        /// </summary>
        /// <value>
        /// The type of the menu.
        /// </value>
        public virtual string MenuType { get; set; }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Set homepage item
            HomePageItem = SitecoreItem.HomeItem;

            // Set current item
            CurrentMenuItem = SitecoreItem.CurrentItem;

            // Set menu item
            SetMenuItem();
        }

        /// <summary>
        /// Sets the menu item.
        /// </summary>
        private void SetMenuItem()
        {
            if (LevelFrom == 1) // Top navigation must created within Configuration and assigned as datasouce
            {
                MenuItem = SitecoreItem.GetItem(DataSource);
            }
            else // for second level navigation onwards
            {
                // assign current tiem to MenuItem
                MenuItem = SitecoreItem.CurrentItem;

                // if MenuType != "sub" 
                if (!string.IsNullOrEmpty(MenuType))
                {
                    int moveLevels = 0;

                    if (MenuType.ToLower().Trim() != "sub")
                    {
                        // calculate levels and reassigne menuitem (e.g. current item is on level 3 and only display level 2 menu)
                        moveLevels = (MenuItem.Axes.Level - (LevelFrom + (SitecoreItem.HomeItem.Axes.Level)));

                        // change menu items if moveLevels is more then 0
                        if (moveLevels > 0)
                        {
                            // assign menu item as per level specified
                            ChangeMenuItem(moveLevels);

                            // Move one level up and then get children
                            MenuItem = MenuItem.Parent;
                        }
                    }
                    else if (MenuType.ToLower().Trim() == "sub")
                    {
                        // calculate levels and reassigne menuitem (e.g. current item is on level 3 and only display level 2 menu)
                        moveLevels = (MenuItem.Axes.Level - (LevelFrom + (SitecoreItem.HomeItem.Axes.Level) -1));

                        // change menu items if moveLevels is more then 0
                        if (moveLevels > 0)
                        {
                            // assign menu item as per level specified
                            ChangeMenuItem(moveLevels);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Changes the menu item.
        /// </summary>
        /// <param name="moveLevels">The move levels.</param>
        private void ChangeMenuItem(int moveLevels)
        {
            if (moveLevels > 0)
            {
                for (int iCnt = 0; iCnt < moveLevels; iCnt++)
                {
                    MenuItem = MenuItem.Parent;
                }
            }
        }


        /// <summary>
        /// Determines whether [is active item] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [is active item] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool IsActiveItem(Item item)
        {
            bool isActive = false;
            if (item.ID == CurrentMenuItem.ID)
                isActive = true;
            else if (CurrentMenuItem.Axes.IsDescendantOf(item))
                isActive = true;
            return isActive;
        }


        protected virtual Item CurrentMenuItem { get; set; }
        protected virtual Item HomePageItem { get; set; }
        protected virtual Item MenuItem { get; set; }

        public static string AddCssClass(string currentCssClass, string addCssClass)
        {
            if (!string.IsNullOrEmpty(currentCssClass))
            {
                return string.Format("{0} {1}", currentCssClass, addCssClass);
            }

            return addCssClass;
        }

        protected override void DoRender(HtmlTextWriter output)
        {
            if (MenuItem != null)
            {
                Item[] children = MenuItem.GetChildrenForNavigation();

                if (children != null && children.Count() > 0)
                {
                    // Rendering specified levels of menu
                    RenderMenu(output, children, LevelFrom, children.FirstOrDefault(), children.LastOrDefault());
                }
            }
        }

        /// <summary>
        /// Renders the menu.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="children">The children.</param>
        /// <param name="iLevel">The i level.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        private void RenderMenu(HtmlTextWriter output, Item[] children, int iLevel, Item firstItem, Item lastItem)
        {
            if (children != null && children.Count() > 0)
            {
                // process children items
                foreach (Item child in children)
                {
                    Item linkItem = child.GetFieldValueAsItem(_fieldNameLinkItem);

                    // if linkitem is not null then use linkitem
                    if (linkItem == null)
                    {
                        linkItem = child;
                    }

                    // begin rendering LI tag
                    // Apply Id if specified
                    ApplyId(output, child, FieldNameLiId);

                    // Apply CSS class
                    AddCSSClass(output, child, firstItem, lastItem, linkItem, FieldNameCssClass);

                    output.RenderBeginTag(HtmlTextWriterTag.Li);

                    // begin rendering A tag
                    output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                    output.RenderBeginTag(HtmlTextWriterTag.A);

                    // A title/ description
                    output.Write(SitecoreItem.GetNavigationTitle(child));

                    // close rendering A tag
                    output.RenderEndTag();

                    // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                    if (iLevel < LevelTo && linkItem.HasChildren)
                    {
                        Item[] grandChildren = linkItem.GetChildrenForNavigation();
                        if (grandChildren.Count() > 0)
                        {
                            output.RenderBeginTag(HtmlTextWriterTag.Ul);
                            RenderMenu(output, grandChildren, iLevel + 1, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                            output.RenderEndTag();
                        }
                    }

                    // close rendering LI tag
                    output.RenderEndTag();
                }
            }
        }

        /// <summary>
        /// Applies the id.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="child">The child.</param>
        /// <param name="fieldName">Name of the field.</param>
        private static void ApplyId(HtmlTextWriter output, Item child, string fieldName)
        {
            if (!string.IsNullOrEmpty(child.GetFieldValue(fieldName)))
            {
                output.AddAttribute(HtmlTextWriterAttribute.Id, child.GetFieldValue(fieldName));
            }
        }

        /// <summary>
        /// Adds the CSS class.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="item">The item.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        /// <param name="linkItem">The link item.</param>
        /// <param name="cssClassFieldName">Name of the CSS class field.</param>
        private void AddCSSClass(HtmlTextWriter output, Item item, Item firstItem, Item lastItem, Item linkItem = null, string cssClassFieldName = null)
        {
            string cssClass = string.Empty;

            // apply first class
            if (item == firstItem)
            {
                cssClass = AddCssClass(cssClass, FirstElementClass);
            }
            else if (item == lastItem)
            {
                cssClass = AddCssClass(cssClass, LastElementClass);
            }

            // Add CSS Class if specified
            if (!string.IsNullOrEmpty(cssClassFieldName) && !string.IsNullOrEmpty(item.GetFieldValue(cssClassFieldName)))
            {
                cssClass = AddCssClass(cssClass, item.GetFieldValue(cssClassFieldName));
            }

            
            // apply active item class
            if (IsActiveItem(linkItem) || IsActiveItem(item))
            {
                cssClass = AddCssClass(cssClass, ActiveElementClass);
            }

            // CSS Class attribute 
            if (!string.IsNullOrEmpty(cssClass))
                output.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
        }

        /// <summary>
        /// Need to override the GetCachingID for Sitecore caching
        /// </summary>
        /// <returns>CacheId</returns>
        protected override string GetCachingID()
        {
            return GetType().ToString();
        }
    }
}