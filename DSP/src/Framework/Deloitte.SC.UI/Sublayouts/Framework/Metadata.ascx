﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Metadata.ascx.cs" Inherits="Deloitte.SC.UI.Sublayouts.Framework.Metadata" %>
<%@ Import Namespace="Deloitte.SC.Framework.Helpers" %>

<meta charset="utf-8" />
<meta name="viewport" content="width=1000,maximum-scale=1" />
<meta name="title" content='<%=SitecoreItem.MetaTitle%>'/>
<meta name="description" content='<%=Sitecore.Context.Item.Fields["MetaDescription"]%>' /> 
<meta name="keywords" content='<%=Sitecore.Context.Item.Fields["MetaKeywords"]%>' />
<meta name="modified" content='<%=Sitecore.Context.Item.Statistics.Updated.Year%>-<%=Sitecore.Context.Item.Statistics.Updated.Month%>-<%=Sitecore.Context.Item.Statistics.Updated.Day%>' />
<meta name="contenttype" content='<%=Sitecore.Context.Item.TemplateName%>' />
