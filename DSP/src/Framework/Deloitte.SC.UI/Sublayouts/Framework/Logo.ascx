﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Logo.ascx.cs" Inherits="Deloitte.SC.UI.Sublayouts.Framework.Logo" %>

<a class="logo" href="/">
    <sc:FieldRenderer runat="server" id="frLogo" FieldName="SiteLogo" />
</a>