﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageTile.ascx.cs" Inherits="Deloitte.SC.UI.Sublayouts.Framework.ImageTile" %>
<a id="link" runat="server">
    <sc:Image ID="image" Field="Image" runat="server" />
    <br />
    <sc:FieldRenderer ID="text" FieldName="Text" runat="server" />
</a>
<sc:FieldRenderer ID="linkField" FieldName="GeneralLink" runat="server" EnclosingTag="p" />