﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public partial class ImageTile : SublayoutBaseExtended
    {
        //Standard Image Tile fields:
        // Image (image supplied)
        // Text (text rendered below image)
        // ImageDimensionHeight (optional override for max image height)
        // ImageDimensionWidth (optional override for max image width)
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string maxHeightParameter = DataSourceItem.GetFieldValue("ImageDimensionHeight");
            string maxWidthParameter = DataSourceItem.GetFieldValue("ImageDimensionWidth");

            int maxHeight;
            if(!string.IsNullOrEmpty(maxHeightParameter) && int.TryParse(maxHeightParameter, out maxHeight))
                image.MaxHeight = maxHeight;

            int maxWidth;
            if (!string.IsNullOrEmpty(maxWidthParameter) && int.TryParse(maxWidthParameter, out maxWidth))
                image.MaxWidth = maxWidth;


            linkField.Visible = SitecoreItem.IsPageEditorEditing;
            link.HRef = DataSourceItem.GetFieldValueAsTargetItem("GeneralLink").GetFriendlyUrl();

            text.Item = image.Item = linkField.Item = DataSourceItem;

        }
    }
}