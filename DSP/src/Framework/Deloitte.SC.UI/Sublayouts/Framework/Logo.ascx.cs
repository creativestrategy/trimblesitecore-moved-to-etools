﻿using System;
using System.Web.UI;
using Deloitte.SC.Framework.Helpers;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public partial class Logo : UserControl
    {
        public virtual void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                frLogo.Item = SitecoreItem.WebsiteItem;
            }
        }
    }
}