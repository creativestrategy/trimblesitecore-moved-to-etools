﻿using System;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;

namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public partial class Banners : SublayoutBaseExtended
    {
        public virtual void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Populate();
            }
        }

        public virtual void Populate()
        {
            // Get banners folder item
            Item bannersFolderItem = CurrentItem.GetFieldValueAsItem("BannersFolder");
            if (bannersFolderItem != null)
            {
                // get banner css for UL and if not null then apply CSS
                string bannerCssUl = bannersFolderItem.GetFieldValue("BannerCssUl");
                if (!string.IsNullOrEmpty(bannerCssUl))
                    ul.Attributes.Add("class", bannerCssUl);

                // bind items
                rptData.DataSource = bannersFolderItem.Children;
                rptData.DataBind();
            }
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();
                e.FindAndSetFieldRenderer("frBannerTitle", item);
                e.FindAndSetFieldRenderer("frBannerImage", item);
                e.FindAndSetLink("lnkBannerLink", item);
                e.FindAndSetLink("lnkBannerLinkNotEditable", item);
            }
        }
    }
}