﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;


namespace Deloitte.SC.UI.Sublayouts.Framework
{
    public partial class LinksRenderer : SublayoutBaseExtended
    {
        public virtual void Page_Load(object sender, EventArgs e)
        {
            Populate();
        }

        // can be override to change this logic
        public virtual Item GetDataSourceItem()
        {
            // update property for populate data
            if (this.DataSourceItem != null)
            {
                DataItems = this.DataSourceItem.Children.ToArray();
                DataItemsCount = DataItems.Count();
                DataSourceItemPath = DataSourceItem.Paths.Path;
            }

            // return source item
            return this.DataSourceItem;
        }

        private Item[] DataItems { get; set; }
        private int DataItemsCount { get; set; }
        private string DataSourceItemPath { get; set; }
        private string _activeCssClass { get; set; }

        // can be override when required
        public virtual void Populate()
        {
            // Get folder item
            Item sourceItem = GetDataSourceItem();
            _activeCssClass = sourceItem.GetFieldValue("ActiveCssClass");
            if (sourceItem != null)
            {

                // for page editor mode visible insert link edit frame
                if (this.IsPageEditorEditing)
                {
                    efAddLink.Visible = true;
                    efAddLink.DataSource = sourceItem.Paths.FullPath;
                }

                // Only render wrapping DIV and UL if has links or in edit mode
                if (sourceItem.Children.Count > 0 || this.IsPageEditorEditing)
                {
                    var includeDiv = sourceItem.IsChecked("IncludeDiv");
                    if (includeDiv)
                    {
                        // start DIV html
                        litWrapperHtmlBegin.Text += "<div";

                        // apply CSS for DIV
                        string divCss = sourceItem.GetFieldValue("DivCss");
                        if (!string.IsNullOrEmpty(divCss))
                            litWrapperHtmlBegin.Text += string.Format(" class='{0}'", divCss);

                        // appl ID for DIV
                        string divId = sourceItem.GetFieldValue("DivId");
                        if (!string.IsNullOrEmpty(divId))
                            litWrapperHtmlBegin.Text += string.Format(" id='{0}'", divId);

                        litWrapperHtmlBegin.Text += ">";
                    }

                    // start UL html
                    litWrapperHtmlBegin.Text += "<ul";

                    // apply CSS for UL
                    string ulCss = sourceItem.GetFieldValue("UlCss");
                    if (!string.IsNullOrEmpty(ulCss))
                        litWrapperHtmlBegin.Text += string.Format(" class='{0}'", ulCss);

                    // appl ID for UL
                    string ulId = sourceItem.GetFieldValue("UlId");
                    if (!string.IsNullOrEmpty(ulId))
                        litWrapperHtmlBegin.Text += string.Format(" id='{0}'", ulId);

                    // close UL html
                    litWrapperHtmlBegin.Text += ">";
                    litWrapperHtmlEnd.Text = "</ul>";

                    if (includeDiv)
                    {
                        // close DIV html
                        litWrapperHtmlEnd.Text += "</div>";
                    }
                }

                // bind items
                rptData.DataSource = sourceItem.Children;
                rptData.DataBind();
            }
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();
                if (item != null)
                {
                    //get li element
                    var li = e.FindControl<HtmlGenericControl>("li");

                    // apply path for datasource
                    e.FindControl<EditFrame>("efActions").DataSource = item.Paths.Path;

                    // apply css class to LI if any (TODO: change this logic and apply first an last CSS class as well)
                    string linkCssClass = item.GetFieldValue("LinkCssClass");
                    if (!string.IsNullOrEmpty(linkCssClass) && li != null)
                    {
                        li.Attributes.Add("class", linkCssClass);
                    }

                    //apply active class if this is the active link
                    if (item.GetFieldValueAsTargetItem("LinkTarget") != null &&
                        item.GetFieldValueAsTargetItem("LinkTarget").ID == Sitecore.Context.Item.ID &&
                        li != null)
                    {
                        if (!string.IsNullOrEmpty(_activeCssClass))
                        {
                            setActiveCss(li, _activeCssClass);
                        }
                    }


                    // apply id to LI if any
                    string LiId = item.GetFieldValue("LinkId");
                    if (!string.IsNullOrEmpty(LiId) && li != null)
                    {
                        li.ClientIDMode = ClientIDMode.Static;
                        li.ID = LiId;
                    }

                    // apply sitecore item
                    e.FindAndSetLink("lnkLinkTarget", item);
                    e.FindAndSetFieldRenderer("frLinkTitle", item);
                }
            }
        }

        private void setActiveCss(HtmlGenericControl control, string activeClass)
        {
            if (control.Attributes["class"] != null && control.Attributes["class"].Length > 0)
            {
                control.Attributes["class"] = string.Format("{0} {1}", control.Attributes["class"], activeClass);
            }
            else
            {
                control.Attributes.Add("class", activeClass);
            }
        }
    }
}