﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BaseLayout.aspx.cs" Inherits="Deloitte.SC.UI.Layouts.Framework.BaseLayout" %><%@ Import Namespace="Deloitte.SC.Framework.Helpers" %><%@ Register src="/Sublayouts/Framework/Metadata.ascx" tagname="Metadata" tagprefix="uc" %><%@ Register src="/Sublayouts/Framework/HtmlInclude.ascx" tagname="HtmlInclude" tagprefix="uc" %><!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head runat="server" id="headMain">
    <title><%= SitecoreItem.Title %></title>
    <uc:Metadata ID="ucMetadata" runat="server" />
    <uc:HtmlInclude ID="ucHtmlInclude" runat="server" FieldName="HeadHtml" />
</head>
<body runat="server" id="bodyBaseLayout">
    <form id="frmMain" runat="server">
        <sc:Placeholder ID="phBody" runat="server" Key="phbody" />
    </form>
    <sc:Placeholder ID="phBodyBottom" runat="server" Key="phbodybottom" />

    <uc:HtmlInclude ID="ucHtmlIncludeBody" runat="server" FieldName="BodyHtml" />
</body>
</html>


