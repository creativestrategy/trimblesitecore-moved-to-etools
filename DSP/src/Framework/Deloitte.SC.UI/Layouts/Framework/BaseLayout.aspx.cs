﻿using System;
using System.Web.UI;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;

namespace Deloitte.SC.UI.Layouts.Framework
{
    public partial class BaseLayout : Page
    {
        
        public virtual void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // apply data controller attribute 
                string dataController = SitecoreItem.CurrentItem.GetFieldValue("DataController");
                if (!string.IsNullOrEmpty(dataController))
                    bodyBaseLayout.Attributes.Add("data-controller", dataController);

                // applu data action attribute
                string dataAction = SitecoreItem.CurrentItem.GetFieldValue("DataAction");
                if (!string.IsNullOrEmpty(dataAction))
                    bodyBaseLayout.Attributes.Add("data-action", dataAction);

                // apply css class to the form
                string formCssClass = SitecoreItem.CurrentItem.GetFieldValue("CssClass");
                if (!string.IsNullOrEmpty(formCssClass))
                    frmMain.Attributes.Add("class", formCssClass);
            }
        }
    }
}