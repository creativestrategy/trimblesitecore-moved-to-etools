﻿using System;
using Deloitte.SC.Framework.Extensions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.SecurityModel;

namespace Deloitte.SC.Framework.Helpers
{
    public static class SitecoreItem
    {

        /// <summary>
        /// Gets the current sitecore database.
        /// </summary>
        public static Database Db
        {
            get { return Context.Database; }
        }

        /// <summary>
        /// Gets the single item.
        /// </summary>
        /// <param name="xPath">The x path.</param>
        /// <returns></returns>
        public static Item GetSingleItem(string xPath)
        {
            using (new SecurityDisabler())
            {
                return Db.SelectSingleItem(xPath);
            }
        }

        /// <summary>
        /// Gets the current item.
        /// </summary>
        public static Item CurrentItem
        {
            get { return Context.Item; }
        }

        /// <summary>
        /// Gets the home item.
        /// </summary>
        public static Item HomeItem
        {
            get { return GetSingleItem(Context.Site.StartPath); }
        }


        /// <summary>
        /// Gets the home item path.
        /// </summary>
        public static string HomeItemPath
        {
            get { return Context.Site.StartPath; }
        }

        /// <summary>
        /// Gets the context start item.
        /// </summary>
        public static Item WebsiteItem
        {
            get { return GetSingleItem(Context.Site.ContentStartPath); }
        }

        /// <summary>
        /// Gets the website item path.
        /// </summary>
        public static string WebsiteItemPath
        {
            get { return Context.Site.ContentStartPath; }
        }

        /// <summary>
        /// Gets the page title.
        /// </summary>
        public static string Title
        {
            get { return CurrentItem.GetFieldValue("Title", CurrentItem.DisplayName); }
        }

        /// <summary>
        /// Gets the meta title.
        /// </summary>
        public static string MetaTitle
        {
            get { return CurrentItem.GetFieldValue("MetaTitle", Title); }
        }

        public static bool IsPageEditorEditing
        {
            get { return Context.PageMode.IsPageEditorEditing; }
        }

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static Item GetItem(string id)
        {
            using (new SecurityDisabler())
            {
                return Db.GetItem(id);
            }
        }

        /// <summary>
        /// Gets the navigation title.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static string GetNavigationTitle(Item item, string fieldName = "NavigationTitle")
        {
            using (new SecurityDisabler())
            {
                return item.GetFieldValue(fieldName, item.GetItemNameForDisplay());
            }
        }

        /// <summary>
        /// Gets the breadcrumb title.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static string GetBreadcrumbTitle(Item item, string fieldName = "BreadcrumbTitle")
        {
            using (new SecurityDisabler())
            {
                return item.GetFieldValue(fieldName, GetNavigationTitle(item));
            }
        }

        /// <summary>
        /// Get translated text from dictionary item
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetTranslateText(string key)
        {
            string itemValue = Translate.Text(key);
            if (!String.IsNullOrEmpty(itemValue))
                return itemValue;

            return String.Empty;
        }

    }
}