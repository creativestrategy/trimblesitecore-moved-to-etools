REM The line below checks to see whether a specific profile has been created for
REM Selenium and if so, uses that proxy setting instead. if not, it uses the standard one.
REM !!!  Note that if you do use a custom profile, you need to update the path below to use it !!!

IF EXIST C:\Users\au_svc_sitecoreUser\AppData\Roaming\Mozilla\Firefox\Profiles\rg4b8r6b.Bamboo GOTO RUNPERSONALISED

C:\Windows\System32\java.exe -jar %~dp0selenium-server-standalone-2.24.1.jar

GOTO END

:RUNPERSONALISED

C:\Windows\System32\java.exe -jar %~dp0selenium-server-standalone-2.24.1.jar -firefoxProfileTemplate "C:\Users\au_svc_sitecoreUser\AppData\Roaming\Mozilla\Firefox\Profiles\rg4b8r6b.Bamboo"

:END

pause