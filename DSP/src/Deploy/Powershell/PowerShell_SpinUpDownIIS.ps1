# PowerShell Remoting 
# Requires -version 2.0

param(
[Parameter(Position=0,Mandatory=$true)]
[string]$ServerName,

[Parameter(Position=1,Mandatory=$true)]
[string]$AppPoolName,

[Parameter(Position=2,Mandatory=$true)]
[string]$SpinUpDown
)

"ServerName: {0}" -f $ServerName
"AppPoolName: {0}" -f $AppPoolName
"Spin Up/Down: {0}" -f $SpinUpDown

$s = New-PSSession -computerName $ServerName

Invoke-Command -Session $s -scriptblock { Set-ExecutionPolicy Unrestricted -Force }
Invoke-Command -Session $s -scriptblock { import-module WebAdministration }

if ($SpinUpDown -eq "down")
{
  try
  {
    Invoke-Command -Session $s -scriptblock { param($PassedAppPoolName) Stop-WebAppPool -Name $PassedAppPoolName } -ArgumentList $AppPoolName
  }
  catch [Net.InvalidOperationException]
  {
    "App Pool is already stopped"
  }
}
else
{
  Invoke-Command -Session $s -scriptblock { 
    param($PassedAppPoolName) Do{Start-WebAppPool -name $PassedAppPoolName; $WAP = Get-WebAppPoolState -Name $PassedAppPoolName; "State: {0}" -f $WAP.Value; Start-Sleep -s 10}
                              While($WAP.Value -ne 'Started') } -ArgumentList $AppPoolName
}

Invoke-Command -Session $s -scriptblock { param($PassedAppPoolName) Get-WebAppPoolState -Name $PassedAppPoolName } -ArgumentList $AppPoolName

Remove-PsSession $s