echo off

set ServerName=%1
set AppPoolName=%2
set SpinUpDown=%3

if not defined ServerName GOTO MISSINGSERVERNAME
if not defined AppPoolName GOTO MISSINGAPPPOOL
if not defined SpinUpDown GOTO MISSINGSPINUPDOWN

echo on

C:\Windows\System32\cmd.exe /c "echo aaa | %SystemRoot%\system32\WindowsPowerShell\v1.0\Powershell.exe .\PowerShell_SpinUpDownIIS.ps1 -ServerName %1 -AppPoolName %2 -SpinUpDown %3"

echo off

IF ERRORLEVEL 1 GOTO ENDPSFAIL

GOTO ENDOK

:MISSINGSERVERNAME
echo Usage: [batch file name] -ServerName [ServerName] -AppPoolName [AppPoolName] -SpinUpDown [Up/Down]
echo You must supply a server name
GOTO ENDFAIL

:MISSINGAPPPOOL
echo Usage: [batch file name] -ServerName [ServerName] -AppPoolName [AppPoolName] -SpinUpDown [Up/Down]
echo You must supply an app pool name
GOTO ENDFAIL


:MISSINGSPINUPDOWN
echo Usage: [batch file name] -ServerName [ServerName] -AppPoolName [AppPoolName] -SpinUpDown [Up/Down]
echo You must specify either "up" or "down" for SpinUpDown
GOTO ENDFAIL

:ENDFAIL
exit /b -1

:ENDPSFAIL
echo PowerShell errors detected, the operation did not complete successfully
exit /b -1

:ENDOK
exit /b 0