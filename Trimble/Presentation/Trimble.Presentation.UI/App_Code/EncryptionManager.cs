﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;
using System.Threading;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for PromotionManager
/// </summary>
public class EncryptionManager
{
   
	public string MaskString(string eMail){
		int mask = 192;
		int i;
		string cryptedkey = "";
		
		// Crypt the email
		// For each charecter (lets call it c) of the email adress,
		// apply the following formula:
		// c = (255 - c) ^ 192
		// the symbol ^ means Bitwise exluse OR operator;
		// Then reconstruct the new crypted email;
		for(i = 0; i < eMail.Length; i++)
		{
				string c = "";
				string cResult = "";
				c = ((255 - (byte)eMail[i]) ^ mask).ToString();
				cResult = string.Format("{0,2:X}", (Convert.ToInt32(c) & 0x00FF)); //each character of the crypted email we convert it into an hexabyte code using the mask 0x00FF
				cryptedkey += cResult;
		}
		return cryptedkey;
	}

	public string MD5(string str)
	{
		MD5 x = new MD5CryptoServiceProvider();
		byte[] bs = Encoding.UTF8.GetBytes(str);
		bs = x.ComputeHash(bs);
		StringBuilder s = new StringBuilder();
		foreach (byte b in bs)
		{
			s.Append(b.ToString("x2").ToLower());
		}
		return s.ToString();
	}
	
	
	public string EncodeMD5(string str)
	
	{	
		byte[] originalBytes;
		byte[] encodedBytes;
		MD5 md5;
	
		// Conver the original password to bytes; then create the hash
		md5 = new MD5CryptoServiceProvider();
		originalBytes = ASCIIEncoding.Default.GetBytes(str);
		encodedBytes = md5.ComputeHash(originalBytes);
	 
		// Bytes to string
		return System.Text.RegularExpressions.Regex.Replace(BitConverter.ToString(encodedBytes), "-", "").ToLower();
	
	}
		

}
