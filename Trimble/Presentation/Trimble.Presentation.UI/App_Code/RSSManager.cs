﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

/// <summary>
/// Summary description for RSSManager
/// </summary>
public class RSSManager
{
	public RSSManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public XmlDocument getLatestNews(string divList, string rowCount)
    {

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/xml";



        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;

        xtw.WriteStartDocument();
        /* --un-comment codes below to enable xsl stylesheet
        string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        xtw.WriteProcessingInstruction("xml-stylesheet", processtext);
        */ 
        xtw.WriteStartElement("rss");
        xtw.WriteAttributeString("version", "2.0");
        xtw.WriteStartElement("channel");
        xtw.WriteElementString("title", "Trimble Navigation Ltd., News Release");
        xtw.WriteElementString("link", "http://www.trimble.com/");
        xtw.WriteElementString("description", "Latest News Release From Trimble.");

        //added by JLM 08-12-2014
		DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
        //string EST = DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date] FROM NewsManagerArticles WHERE Status=1 ";

        if (divList != "")
        {
            string[] arrDiv = divList.Split(',');
            int tCount = arrDiv.Length;
            string logicOp = "AND";
            for (int x = 0; x < tCount; x++)
            {
                if (x != 0) logicOp = "OR";
                sqlQuery += logicOp + " CategoryID Like '%%" + arrDiv[x] + "%%' ";
            }
        }

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";


        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("item");
                xtw.WriteElementString("title", sqlReader.GetString(2));
                //xtw.WriteElementString("description", sqlReader.GetString(2));
                xtw.WriteElementString("description", "");
                xtw.WriteElementString("link", "http://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
                xtw.WriteElementString("pubDate", sqlReader.GetDateTime(4).ToString("R"));
                xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        xtw.WriteEndElement();
        xtw.WriteEndDocument();




        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
        return xmlDocument;


    }
}
