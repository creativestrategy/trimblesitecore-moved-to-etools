using System.Collections.Generic;
using System.Xml.Serialization;
 
namespace Sitemap.Models
{
    ///
 
    /// Class to generate urlset for sitemap
    /// 
 
    [XmlRoot("urlset")]
    public class Urlset
    {
        ///
 
        /// Constructor to initialize Url Object
        /// 
 
        public Urlset() { Url = new List(); }
 
        ///
 
        /// Urls collection
        /// 
 
        [XmlElement("url")]
        public List Url { get; set; }
 
    }
 
    ///
 
    /// Class to generate url with its parameters for sitemap
    /// 
 
    public class Url
    {
        ///
 
        /// Location Parameter
        /// 
 
        [XmlElement("loc")]
        public string Loc { get; set; }
 
        ///
 
        /// Last modified on
        /// 
 
        [XmlElement("lastmod")]
        public string Lastmod { get; set; }
 
        //Add required properties here like changefreq, priority
    }
}