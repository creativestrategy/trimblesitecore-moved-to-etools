using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace wrapp
{
	/// <summary>
	/// Summary description for SessionManager.
	/// </summary>
	public class SessionManager
	{
		public SessionManager()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		////////////////////////////////////////////////////////////////
		// Set/Create Session using a Name/Value pair
		// args: sname=session name; svalue=value for the given session namme
		////////////////////////////////////////////////////////////////
		public void setValue(string sname, string svalue)
		{
			HttpContext.Current.Session[sname] = svalue;
		}

		public string getValue(string sname)
		{
			string val = "";
			if(HttpContext.Current.Session[sname]!= null)
			{
				val =  HttpContext.Current.Session[sname].ToString();
			}
			return val;
		}

		////////////////////////////////////////////////////////////////
		// Validates the user if it has all the Sessions and QueryStrings 
		// args: none
		////////////////////////////////////////////////////////////////
		public bool Validate(string session_email)
		{
			bool val = true;
			//Check if there are QueryString values
			if(HttpContext.Current.Request.QueryString["sid"] != null || HttpContext.Current.Request.QueryString["at"] != null)
			{
				if(HttpContext.Current.Request.QueryString["sid"].ToString()=="" || HttpContext.Current.Request.QueryString["at"].ToString()=="")
				{
					val = false;
				}
			}
			
			//Check if there is kuki_email
			CookieManager kuki = new CookieManager();
			string kuki_email = kuki.getValue("kuki_email");
			if (kuki_email != session_email)
			{
				val = false;
			}

			return val;
		}

		public bool isAdmin(string session_email)
		{
			string strAdmin = ConfigurationSettings.AppSettings["WebAdmin"];

			if(session_email!="")
			{
				if(strAdmin.IndexOf(session_email) == -1)
				{
					return false;
				}
				else
				{
					return true;
				}
			}else
			{
				if(strAdmin.IndexOf(HttpContext.Current.Session["Email_ID"].ToString()) == -1)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}

		public void LogOut()
		{
			CookieManager kuki = new CookieManager();
			kuki.clrValue("RememberMe");
			HttpContext.Current.Session.Clear();
			HttpContext.Current.Session.Abandon();
			HttpContext.Current.Response.Redirect("login.aspx");
		}

	}
}
