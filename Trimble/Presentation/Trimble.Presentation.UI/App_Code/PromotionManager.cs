﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;
using System.Threading;

/// <summary>
/// Summary description for PromotionManager
/// </summary>
public class PromotionManager
{
    SqlConnection cn;
	public PromotionManager(string sqlString)
	{
        cn = new SqlConnection(sqlString);
	}


    //************************************************
    // Function:	RetrievePromoCode
    // Purpose:		Validate Costumer if Promo Code hasa been assigned
    // Args: 		strEmail = Email Address; aID = Account ID
    // Returns:		PromoCode value
    //***********************************************
    public string RetrievePromoCode(string aID, string strEmail, string dbTable)
    {//
        string sql = "";
        string val = "";
        SqlCommand myCommand;
        SqlDataReader myDataReader;

        //sql = "SELECT * FROM " + dbTable + " WHERE Account_ID='" + aID + "' and (Date_Used<>'' OR Date_Used IS NOT NULL) ORDER BY promocode DESC" ;
        sql = "SELECT * FROM " + dbTable + " WHERE Registered_Email='" + strEmail + "' ORDER BY PromoCode DESC";

        myCommand = new SqlCommand(sql, cn);
        if (cn.State != ConnectionState.Open) cn.Open();
        myDataReader = myCommand.ExecuteReader();

        if (myDataReader.Read())
        {
            val = myDataReader["PromoCode"].ToString();
        }
        myDataReader.Close();
        cn.Close();
        return val;
    }


    //************************************************
    // Function:	AssignPromoCode
    // Purpose:		Update LoyaltyProgram_Dimensions2007PromoCodes table by Assigning Promo code 
    // Args: 		pCode = PromoCode; aID = account ID; strEmail = registered Email
    //***********************************************
    public void AssignPromoCode(string pCode, string aID, string strEmail, string dbTable)
    {//
        string sqlUpdatePromo = "";
        SqlCommand myCommand;
        SqlDataReader myDataReader;

        sqlUpdatePromo = "UPDATE " + dbTable + " SET " +
                         "Account_ID = '" + aID + "', " +
                         "Registered_Email = '" + strEmail + "', " +
                         "Date_Used = '" + System.DateTime.Now.ToString() + "' " +
                         "WHERE PromoCode = '" + pCode + "'";

        if (cn.State != ConnectionState.Open) cn.Open();
        myCommand = new SqlCommand(sqlUpdatePromo, cn);

        if (cn.State != ConnectionState.Open) cn.Open();
        myCommand.ExecuteNonQuery();
        myCommand.Connection.Close();
        cn.Close();
    }

    //************************************************
    // Function:	InsertAssignPromoCode
    // Purpose:		Insert User details and Promocode to DB Table
    // Args: 		pCode = PromoCode; aID = account ID; strEmail = registered Email
    //***********************************************
    public void InsertAssignedPromoCode(string pCode, string aID, string strEmail, string dbTable)
    {//
        
        string sql = "INSERT INTO " + dbTable + " ([PromoCode], [Account_ID], [Registered_Email], [Date_Used]) " +
			         "VALUES(@PromoCode, @Account_ID, @Registered_Email, @Date_Used)";

		SqlCommand sqlCmd = new SqlCommand(sql, cn);
		sqlCmd.Parameters.Add(new SqlParameter("@PromoCode", pCode));
		sqlCmd.Parameters.Add(new SqlParameter("@Account_ID", aID));
		sqlCmd.Parameters.Add(new SqlParameter("@Registered_Email", strEmail));
		sqlCmd.Parameters.Add(new SqlParameter("@Date_Used", System.DateTime.Now.ToString()));
		
		sqlCmd.Connection.Open();
		sqlCmd.ExecuteNonQuery();
		sqlCmd.Connection.Close();				

    }

    //************************************************
    // Function:	ValidateAssignedPromoCode
    // Purpose:		Validate Costumer if Promo Code hasa been queued
    // Args: 		strEmail = Email Address; aID = Account ID
    // Returns:		PromoCode value
    //***********************************************
    public bool ValidateAssignedPromoCode(string pCode, string aID, string dbTable)
    {//
        string sql = "";
        bool val = false;
        SqlCommand myCommand;
        SqlDataReader myDataReader;

        sql = "SELECT * FROM " + dbTable + " WHERE Account_ID='" + aID + "' AND PromoCode='" + pCode + "'"; // AND Queue=1" ;

        myCommand = new SqlCommand(sql, cn);

        if (cn.State != ConnectionState.Open) cn.Open();
        myDataReader = myCommand.ExecuteReader();

        if (myDataReader.Read())
        {
            //val = myDataReader["PromoCode"].ToString();
            val = true;
        }
        myDataReader.Close();
        cn.Close();
        return val;
    }


    //*******************************************************************//
    //*******************************************************************//
    //********************** QUEUE PROMO CODES **************************//
    //*******************************************************************//
    //*******************************************************************//

    //************************************************
    // Function:	ValidateQuePromoCode
    // Purpose:		Validate Costumer if Promo Code hasa been queued
    // Args: 		strEmail = Email Address; aID = Account ID
    // Returns:		PromoCode value
    //***********************************************
    public string ValidateQuePromoCode(string aID, string strEmail, string dbTable)
    {//
        string sql = "";
        string val = "";
        SqlCommand myCommand;
        SqlDataReader myDataReader;

        sql = "SELECT * FROM " + dbTable + " WHERE Account_ID='" + aID + "'"; // AND Queue=1" ;

        myCommand = new SqlCommand(sql, cn);

        if (cn.State != ConnectionState.Open) cn.Open();
        myDataReader = myCommand.ExecuteReader();

        if (myDataReader.Read())
        {
            val = myDataReader["PromoCode"].ToString();
        }
        myDataReader.Close();
        cn.Close();
        return val;
    }

    //************************************************
    // Function:	QuePromoCode
    // Purpose:		Update LoyaltyProgram_Dimensions2007PromoCodes table by Assigning Promo code 
    // Args: 		aID = account ID; strEmail = registered Email
    //***********************************************
    public string QuePromoCode(string aID, string strEmail, string dbTable)
    {//
        string val = "";
        SqlCommand myCommand;
        SqlDataReader myDataReader;

        string sqlGetPromo = "SELECT TOP 1 * FROM " + dbTable + " WHERE " +
                             "(Account_ID IS NULL OR account_id='') " +
                             "OR " +
                             "(Registered_Email IS NULL OR Registered_Email='') " +
                             "ORDER BY PromoCode ASC";

        myCommand = new SqlCommand(sqlGetPromo, cn);
        if (cn.State != ConnectionState.Open) cn.Open();
        myDataReader = myCommand.ExecuteReader();
        if (myDataReader.Read())
        {
            val = myDataReader["PromoCode"].ToString();
        }
        myDataReader.Close();
		//HttpContext.Current.Response.Write("quePromo= " + sqlGetPromo);
        //AssignQuePromoCode(val, aID, strEmail, dbTable);
        AssignPromoCode(val, aID, strEmail, dbTable);
        cn.Close();
        return val;
    }


    //************************************************
    // Function:	AssignQuePromoCode
    // Purpose:		Update LoyaltyProgram_Dimensions2007PromoCodes table by Assigning Promo code 
    // Args: 		pCode = PromoCode; aID = account ID; strEmail = registered Email
    //***********************************************
    public void AssignQuePromoCode(string pCode, string aID, string strEmail, string dbTable)
    {//
        string sqlUpdatePromo = "";
        SqlCommand myCommand;
        SqlDataReader myDataReader;

        sqlUpdatePromo = "UPDATE " + dbTable + " SET " +
                         "Queue = 1, " +
                         "Account_ID = '" + aID + "', " +
                         "Registered_Email = '" + strEmail + "' " +
                         "WHERE PromoCode = '" + pCode + "'";

        myCommand = new SqlCommand(sqlUpdatePromo, cn);
        if (cn.State != ConnectionState.Open) cn.Open();
        myCommand.ExecuteNonQuery();
        myCommand.Connection.Close();
        cn.Close();
    }


    //************************************************
    // Function:	GetQuePromoCode
    // Purpose:		Get previously queued promo.
    // Args: 		aID = account ID; strEmail = registered Email
    // Returns:		PromoCode value
    //***********************************************
    public string GetQuePromoCode(string aID, string strEmail, string dbTable)
    {//
        string sqlQuePromo = "";
        string val = "";
        SqlCommand myCommand;
        SqlDataReader myDataReader;

        sqlQuePromo = "SELECT * FROM " + dbTable + " WHERE " +
                         "Account_ID = '" + aID + "', " +
                         "AND " +
                         "Registered_Email = '" + strEmail + "'";

        myCommand = new SqlCommand(sqlQuePromo, cn);
        if (cn.State != ConnectionState.Open) cn.Open();
        myDataReader = myCommand.ExecuteReader();
        if (myDataReader.Read())
        {
            val = myDataReader["PromoCode"].ToString();
        }
        myDataReader.Close();
        cn.Close();
        return val;
    }

}
