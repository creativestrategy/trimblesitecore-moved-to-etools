using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;


	/// <summary>
	/// Summary description for kuki.
	/// </summary>
	public class VisitorManager
	{
		public static string userAgentText = HttpContext.Current.Request.UserAgent;
		public HttpBrowserCapabilities bc = default(HttpBrowserCapabilities);
		

		public string Referrer {			
			get{
				return HttpContext.Current.Request.ServerVariables["HTTP_REFERER"];
			}
		}
		public string IP {			
			get{
				return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
			}
		}
		
		public string OS {			
			get{
				string os = "Unknown Operating System";
				bc = HttpContext.Current.Request.Browser;
				if (userAgentText != null){
					if(bc.IsMobileDevice==true){
						os = getMobileOS(userAgentText);
					}else{
						os = getDesktopOS(userAgentText);
					}
				}
				return os;
			}
			
		}
		
		public string Device {			
			get{
				bc = HttpContext.Current.Request.Browser;
				if(bc.IsMobileDevice==true){
					return "Mobile";
				}else{
					return "Desktop/Laptop";
				}
			}
		}
		
		public string BrowserName{			
			get{
				bc = HttpContext.Current.Request.Browser;
				return bc.Browser;
			}
		}
		
		public string BrowserVersion{			
			get{
				bc = HttpContext.Current.Request.Browser;
				return bc.Version;
			}
		}
	
		public bool JavaScript{			
			get{					
				bc = HttpContext.Current.Request.Browser;
				return bc.JavaScript;
			}
		}
		
		public bool Cookies{			
			get{					
				bc = HttpContext.Current.Request.Browser;
				return bc.Cookies;
			}
		}
		
		public bool Frames{			
			get{			
				bc = HttpContext.Current.Request.Browser;		
				return bc.Frames;
			}
		}
		
		public bool JavaApplets{			
			get{					
				bc = HttpContext.Current.Request.Browser;
				return bc.JavaApplets;
			}
		}
		
		/*
		public static class Browser {
			
			public static string Name{			
				get{
					return bc.Browser;
				}
			}
			
			public static string Version{			
				get{
					return bc.Version;
				}
			}
		
			public static bool Javscript{			
				get{					
					return bc.JavaScript;
				}
			}
			
			public static bool Cookies{			
				get{					
					return bc.Cookies;
				}
			}
			
			public static bool Frames{			
				get{					
					return bc.Frames;
				}
			}
			
			public static bool JavaApplets{			
				get{					
					return bc.JavaApplets;
				}
			}
		}
		*/
		
		
		string getDesktopOS(string strUA){
			string val = "Unknown Machine";
			
			if (strUA.Contains("PPC Mac OS X"))
				val =  "Mac OS X";
				
			if (strUA.Contains("Intel Mac OS X 10_9"))
				val =  "Mac OS X - Mavericks";
			
			if (strUA.Contains("Intel Mac OS X 10_8"))
				val =  "Mac OS X - Mountain Lion";
			
			if (strUA.Contains("Intel Mac OS X 10_7"))
				val =  "Mac OS X - Lion";
				
			if (strUA.Contains("Intel Mac OS X 10_6"))
				val =  "Mac OS X - Snow Leopard";
				
			if (strUA.Contains("Intel Mac OS X 10_5"))
				val =  "Mac OS X - Leopard";
				
			if (strUA.Contains("Windows NT 6.3")) {
				if (strUA.Contains("ARM"))
					val =  "Surface RT";
				else
					val =  "Windows 8.1";
			}
			
			if (strUA.Contains("Windows NT 6.2"))
				val =  "Windows 8";
	
			if (strUA.Contains("Windows NT 6.1"))
				val =  "Windows 7";
				
			if (strUA.Contains("Windows NT 6.0"))
				val =  "Windows Vista";
	
			if (strUA.Contains("Windows NT 5.2"))
				val =  "Windows Server 2003";
			
			if (strUA.Contains("Windows NT 5.1"))
				val =  "Windows XP";
				
			if (strUA.Contains("Windows NT 5.0"))
				val =  "Windows 2000";
	
			if (strUA.Contains("Linux"))
				val =  "Linux";
			
			return val;
		}
		
		
		
		
		
		string getMobileOS(string strUA){
			string val = "Unknown Device";
			if (strUA.Contains("Blackberry"))
				val =  "Blackberry";
				
			if (strUA.Contains("Windows Phone"))
				val =  "Windows Phone";
			
			if (strUA.Contains("Windows CE"))
				val =  "Windows CE";
	
			if (strUA.Contains("iPhone"))
				val =  "iPhone";
	
			if (strUA.Contains("iPad"))
				val =  "iPad";
	
			if (strUA.Contains("Android")) {
				if (strUA.Contains("Mobile"))
					val =  "Android Phone";
				else
					val =  "Android Tablet";
			}
			return val;
		}
		
		
	}
