using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Sublayouts.Framework;
using Trimble.Business.Model.News.Repository;
using Trimble.Business.Model.News;
using Sitecore.Data.Items;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Web.UI.WebControls;
using System.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Publishing;
using Sitecore.Layouts;
using Sitecore.Sites;

public class sitecoreTool 
{
	/*
	#region Properties

	Item dataSourceItem = null;

	#endregion
	
	protected Item GetDataSourceItem()
	{
		const string path = "/sitecore/content/Website/Content/Home/NewsAJAX";
		dataSourceItem = Sitecore.Context.Database.GetItem(path);
		return dataSourceItem;
	}

	private string _newsArticleLink;

	protected void Page_Load(object sender, EventArgs e)
	{
		GetDataSourceItem();
		txtTrimbleNews.Item = dataSourceItem;
		lnkviewAllLink.Item = dataSourceItem;
		txtTrimbleNewsContent.Item = dataSourceItem;

	}
	*/
	Sitecore.Data.Database db = Sitecore.Configuration.Factory.GetDatabase("master");
	string MainNavigation = "{117CB34A-73C2-4447-AC17-E6A20073F9B8}";
	string MainNavigation2020 = "{F737C4EC-1F0A-41C3-A450-9B36E6EBE8C4}";
	string globalNavigation3 = "{3A71A29E-5E0B-4431-A5A2-C9A90473344F}";
	
	public sitecoreTool()
	{
		//
		// TODO: Add constructor logic here
		//
		
	}
	
	public void getItems(TextBox container, string ItemPath){
		string itemID = "{D11EEA37-4691-4379-8624-916067FB5A62}";
		//Sitecore.Data.ID.IsID("{D11EEA37-4691-4379-8624-916067FB5A62}")
		//var FirstItem = Sitecore.Context.Database.GetItem(new ID("{D11EEA37-4691-4379-8624-916067FB5A62}")) ;
		
		Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
		Sitecore.Data.Items.Item home = master.GetItem(ItemPath);
		
		//HttpContext.Current.Response.Write(home);
		PrintName(home, 1, container);

	}
	
	private Item PrintName(Item mainItem, int icounter, TextBox txtBox)
	{
		string output = "";
		string f = new string('-', icounter);
		
				
		txtBox.Text += f + mainItem.DisplayName + " (id: " + mainItem.ID + " ) " + System.Environment.NewLine;
		//HttpContext.Current.TextBox1.Text += f + mainItem.DisplayName + System.Environment.NewLine;
		//output += f + mainItem.DisplayName + "<br/>";
		
		if (mainItem.HasChildren)
		{
			icounter++;
			foreach (var myInnerItem in mainItem.Children.ToList())
			{
				PrintName(myInnerItem, icounter, txtBox);
			}
		}
	
		icounter--;
		//HttpContext.Current.Response.Write(output);
		return null;
	}
	
	public Sitecore.Layouts.RenderingReference[] GetListOfSublayouts(string itemId)
	{
		
		Sitecore.Layouts.RenderingReference[] renderings = null;
		//HttpContext.Current.Response.Write(Sitecore.Context.ContentDatabase.GetItem(Sitecore.Data.ID.Parse(itemId)));
		//HttpContext.Current.Response.Write(targetSiteContext.TargetHostName);
		
		if (Sitecore.Data.ID.IsID(itemId))
		{
			/*
			Item item = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(itemId));
			if (item != null)
			{
				renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, true);
			}
			*/
			
			/*
			SiteContext targetSiteContext = SiteContext.GetSite("website");
			using (var context = new SiteContextSwitcher(targetSiteContext))
			{
				Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
				Item item = context.Database.GetItem(Sitecore.Data.ID.Parse(itemId));	
				if (item != null)
				{
					renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, true);
				}
			}
			
			*/
			
			Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
			Item item =  master.GetItem(new ID(itemId)) ;
			if (item != null)
			{
				//renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, true);
				
				string deviceName = "Default".ToLower();
				var deviceItem = item.Database.Resources.Devices.GetAll().Where(d => d.Name.ToLower() == deviceName).First();
				if (deviceItem != null)
				{
					renderings = item.Visualization.GetRenderings(deviceItem, true);
	
				}
			}
		}
		//HttpContext.Current.Response.Write(renderings[0]);
		return renderings;
	}


	public void printSublayouts(Sitecore.Layouts.RenderingReference[] r){
		foreach(var i in r){
			//HttpContext.Current.Response.Write(i.Placeholder + "<br/>");
			//HttpContext.Current.Response.Write(i.RenderingItem.Name + " ( datasource: " +  i.Settings.DataSource + " )<br/>");
			
			HttpContext.Current.Response.Write(i.RenderingItem.Name + " ( id: " +  i.RenderingItem.ID +" ; datasource: " +  i.Settings.DataSource + " )<br/>");
		}
		
	}
	
	public void testupdate(string itemId){
		Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
		Item item = master.GetItem(new ID(itemId));
		string presentationXml = item["__renderings"];
		/*
		itm.Editing.BeginEdit();
		presentationXml.Replace("what you're looking for", "what you want to replace it with");
		itm.Editing.EndEdit();
		*/
		string globalNavigation3 = "{3A71A29E-5E0B-4431-A5A2-C9A90473344F}";
		string deviceName = "Default".ToLower();
		var deviceItem = item.Database.Resources.Devices.GetAll().Where(d => d.Name.ToLower() == deviceName).First();
		
		//Sitecore.Layouts.RenderingReference[] renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, true).Where(r => r.RenderingID == Sitecore.Data.ID.Parse(globalNavigation3)).ToArray();
		Sitecore.Layouts.RenderingReference[] renderings = item.Visualization.GetRenderings(deviceItem, true).Where(r => r.RenderingID == Sitecore.Data.ID.Parse(globalNavigation3)).ToArray();
		
		
		
		LayoutField layoutField = new LayoutField(item.Fields[Sitecore.FieldIDs.LayoutField]);
		LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layoutField.Value);
		DeviceDefinition deviceDefinition = layoutDefinition.GetDevice(deviceItem.ID.ToString());
		
		//printSublayouts(renderings); 
		HttpContext.Current.Response.Write(deviceItem.Name + "<br/>");
		using (new Sitecore.SecurityModel.SecurityDisabler())
		{
			
			foreach (RenderingReference rendering in renderings)
			{
				
				// Update the renderings datasource value accordingly 
				deviceDefinition.GetRendering(rendering.RenderingID.ToString()).Datasource = MainNavigation;
				
				// Save the layout changes
				item.Editing.BeginEdit();
				layoutField.Value = layoutDefinition.ToXml();
				item.Editing.EndEdit();
				
				//HttpContext.Current.Response.Write("deviceDefinition.GetRendering(rendering.RenderingID.ToString()) = " + deviceDefinition.GetRendering(rendering.RenderingID.ToString()).Datasource + "<br/>");
				
			}
		}
			
	}
	
	
	public void UpdateRenderingDatasource(string itemId)
	{
		using (new Sitecore.SecurityModel.SecurityDisabler())
		{
			if (Sitecore.Data.ID.IsID(itemId))
			{
				Item item = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(itemId));
				if (item != null)
				{
					//Get all added renderings
					Sitecore.Layouts.RenderingReference[] renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, true);
					
					//Get specific added rendering
					//Sitecore.Layouts.RenderingReference[] renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, true).Where(r => r.RenderingID == Sitecore.Data.ID.Parse(sublayoutId)).ToArray();
			   
					// Get the layout definitions and the device
					LayoutField layoutField = new LayoutField(item.Fields[Sitecore.FieldIDs.LayoutField]);
					LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layoutField.Value);
					DeviceDefinition deviceDefinition = layoutDefinition.GetDevice(Sitecore.Context.Device.ID.ToString());
					foreach (RenderingReference rendering in renderings)
					{
						/*
						// Update the renderings datasource value accordingly 
						deviceDefinition.GetRendering(rendering.RenderingID.ToString()).Datasource = MainNavigation2020;
						
						// Save the layout changes
						item.Editing.BeginEdit();
						layoutField.Value = layoutDefinition.ToXml();
						item.Editing.EndEdit();
						*/
						HttpContext.Current.Response.Write(rendering.RenderingItem.Name + "<br/>");
						
					}
				}
			}
		}
	}
	
	
	////New Codes
	
	
	///////////GETTING ITEMS////////////////////
	public void getSitecoreItems(TextBox container, string ItemPath, string sublayoutFilter){
		
		//Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
		Sitecore.Data.Items.Item parentNode = db.GetItem(ItemPath);
		
		//HttpContext.Current.Response.Write(home);
		getItems(parentNode, 1, container, sublayoutFilter);

	}
	
	private Item getItems(Item mainItem, int icounter, TextBox txtBox, string sublayoutFilter)
	{
		string output = "";
		string f = new string('-', icounter);
		
		string itemPath = null;
		string itemDatasource  = getDatasource(getSublayouts(mainItem.ID.ToString()), sublayoutFilter);
		if(itemDatasource != null){
			itemPath = getPath(itemDatasource);
		}
		
		txtBox.Text += f + mainItem.DisplayName + " (id: " + mainItem.ID + " ) " + itemPath + System.Environment.NewLine;
		
		if (mainItem.HasChildren)
		{
			icounter++;
			foreach (var myInnerItem in mainItem.Children.ToList())
			{
				getItems(myInnerItem, icounter, txtBox, sublayoutFilter);
			}
		}
	
		icounter--;
		return null;
	}
	
	
	public Sitecore.Layouts.RenderingReference[] getSublayouts(string itemId)
	{
		
		Sitecore.Layouts.RenderingReference[] renderings = null;	
		if (Sitecore.Data.ID.IsID(itemId))
		{
			//Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
			Item item =  db.GetItem(new ID(itemId)) ;
			if (item != null)
			{
				string deviceName = "Default".ToLower();
				var deviceItem = item.Database.Resources.Devices.GetAll().Where(d => d.Name.ToLower() == deviceName).First();
				if (deviceItem != null)
				{
					renderings = item.Visualization.GetRenderings(deviceItem, true);
	
				}
			}
		}
		//HttpContext.Current.Response.Write(renderings[0]);
		return renderings;
	}

	
	public string getDatasource(Sitecore.Layouts.RenderingReference[] r, string sublayoutFilter ){
		string output = null;
		foreach(var i in r){
			if(i.RenderingItem.Name==sublayoutFilter) {
				//output = i.RenderingItem.Name + " (" + i.RenderingItem.ID +") ==> datasource: " +  i.Settings.DataSource ;
				output = i.Settings.DataSource;
			}
			//HttpContext.Current.Response.Write(i.RenderingItem.Name + " ( datasource: " +  i.Settings.DataSource + " )<br/>");
			//HttpContext.Current.Response.Write(i.RenderingItem.Name + " ( id: " +  i.RenderingItem.ID +" ; datasource: " +  i.Settings.DataSource + " )<br/>");
		}
		return output;
	}
	
	
	
	////////////////////////UPDATING ITEMS////////////////////////////
	
	
	public void updateGlobalNavigation3(TextBox container, string ItemPath, string sublayoutFilter){
		
		Sitecore.Data.Items.Item parentNode = db.GetItem(ItemPath);
		
		//HttpContext.Current.Response.Write(home);
		updateChildItems(parentNode, 1, container, sublayoutFilter);

	}
	
	
	private Item updateChildItems(Item mainItem, int icounter, TextBox txtBox, string sublayoutFilter)
	{
		string output = "";
		string f = new string('-', icounter);
		
		string itemPath_old = null;
		string itemPath_new = null;
		string itemDatasource  = getDatasource(getSublayouts(mainItem.ID.ToString()), sublayoutFilter);
		if(itemDatasource != null){
			itemPath_old = getPath(itemDatasource);
			updateDatasource(mainItem.ID.ToString(), globalNavigation3, MainNavigation2020);
			itemPath_new = getPath(itemDatasource);
		}
		
		//txtBox.Text += f + mainItem.DisplayName + " (id: " + mainItem.ID + " ) " + itemPath + System.Environment.NewLine;
		txtBox.Text += f + mainItem.DisplayName + " --> " + sublayoutFilter + " --> Data Source --> (FROM: " + itemPath_old + " TO: " + itemPath_new + System.Environment.NewLine;
		
		if (mainItem.HasChildren)
		{
			icounter++;
			foreach (var myInnerItem in mainItem.Children.ToList())
			{
				updateChildItems(myInnerItem, icounter, txtBox, sublayoutFilter);
			}
		}
	
		icounter--;
		return null;
	}

	public void updateDatasource(string pageItemID, string subLayoutID, string newDatasource ){
		//pageItemID is the Item ID of the page (e.g {7AE0E253-73C0-4DB6-9322-CCB734071D25} for /sitecore/content/Website/Home/TestArea/jm_test )
		//sublayoutID is the sublayout control Item ID to be updated (e.g {3A71A29E-5E0B-4431-A5A2-C9A90473344F} for globalNavigation3 control in Presentation Layer )
		//newDatasource is the Item ID of the new datasource (e.g {F737C4EC-1F0A-41C3-A450-9B36E6EBE8C4} for /sitecore/content/Website/Global/MainNavigation2020)
		
		HttpContext.Current.Response.Write("<br/>Updating " + pageItemID);
		//Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
		Item item = db.GetItem(new ID(pageItemID));
		string presentationXml = item["__renderings"];

		
		string deviceName = "Default".ToLower();
		var deviceItem = item.Database.Resources.Devices.GetAll().Where(d => d.Name.ToLower() == deviceName).First();
		
		//Sitecore.Layouts.RenderingReference[] renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, true).Where(r => r.RenderingID == Sitecore.Data.ID.Parse(globalNavigation3)).ToArray();
		Sitecore.Layouts.RenderingReference[] renderings = item.Visualization.GetRenderings(deviceItem, true).Where(r => r.RenderingID == Sitecore.Data.ID.Parse(subLayoutID)).ToArray();
		
		
		
		LayoutField layoutField = new LayoutField(item.Fields[Sitecore.FieldIDs.LayoutField]);
		LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layoutField.Value);
		DeviceDefinition deviceDefinition = layoutDefinition.GetDevice(deviceItem.ID.ToString());
		
		using (new Sitecore.SecurityModel.SecurityDisabler())
		{
			
			foreach (RenderingReference rendering in renderings)
			{
				
				// Update the renderings datasource value accordingly 
				deviceDefinition.GetRendering(rendering.RenderingID.ToString()).Datasource = newDatasource;
				
				// Save the layout changes
				item.Editing.BeginEdit();
				layoutField.Value = layoutDefinition.ToXml();
				item.Editing.EndEdit();
				
				//HttpContext.Current.Response.Write("deviceDefinition.GetRendering(rendering.RenderingID.ToString()) = " + deviceDefinition.GetRendering(rendering.RenderingID.ToString()).Datasource + "<br/>");
				
			}
		}
	}
	

	public string getPath(string itemID){
	
		Item item = db.GetItem(new ID(itemID));
		return item.Paths.Path.ToString();
	}
	

}
