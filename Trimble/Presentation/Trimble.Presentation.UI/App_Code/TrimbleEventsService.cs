﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.ServiceModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for NewsReleaseService
/// </summary>
[WebService(Namespace = "http://trimble.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class TrimbleEventsService : System.Web.Services.WebService {

    public TrimbleEventsService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
	
	public static string server = HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToUpper();

    	
	
	[WebMethod]
	private XmlDocument getFullStoryList(string divisionID, string region, string language, string market, string product, string xmonth, string xyear, string RowCount){
		
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
		
		StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		string quickLink = "";		
		string firstDate;
		int rCount = 0;
		string stat = "0";
		//if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) stat="1";		
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		string sqlQuery;
		
		sqlQuery = "SELECT "; 
		if(RowCount!="") sqlQuery += "TOP " + RowCount + " ";
		sqlQuery += "* FROM Customer_Stories WHERE StoryID<>''";
		
		if (divisionID!="") sqlQuery += " AND DivisionID ='" + divisionID + "'";
		if (region!="" && region!=null) sqlQuery += " AND Region='" + region +"'";
		if (market!="" && market!=null) sqlQuery += " AND Market_Segments LIKE '%%" + region + "%%'";
		if (language!="" && language!=null) sqlQuery += " AND Language='" + language +"'";
		if (product!="" && product!=null) sqlQuery += " AND Products LIKE '%%" + product + "%%'";
		
		/*
		if (divisionID!="") sqlQuery += " AND DivisionID Like @divisionID";
		if (region!="") sqlQuery += " AND Region=@region";
		if (market!="") sqlQuery += " AND Market_Segments Like @market";
		if (language!="") sqlQuery += " AND Language=@language";
		if (product!="") sqlQuery += " AND Products Like @product";
		*/
		//if(xmonth!="0")  sqlQuery += " AND DATEPART(MONTH, DateReleased) = '" + xmonth + "'";
        //if(xyear!="0")   sqlQuery += " AND DATEPART(YEAR, DateReleased) = '" + xyear + "'";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) sqlQuery += " AND Status='1'";
		else sqlQuery += " AND (Status='1' OR Status='0')";
		sqlQuery += " ORDER BY DateReleased DESC";
				
		//Response.Write(SqlQuery);
		SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB2);        		
		/*
		sqlCommand.Parameters.Add(new SqlParameter("@divisionID", divisionID));
		sqlCommand.Parameters.AddWithValue("@region", region);
		sqlCommand.Parameters.AddWithValue("@market", "%%" + market + "%%");
		sqlCommand.Parameters.AddWithValue("@language", language);
		sqlCommand.Parameters.AddWithValue("@product", "%%" + product + "%%");
		*/
		
		SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		
		
		if(sqlReader.HasRows){
			rCount++;
			xtw.WriteStartElement("table");
			xtw.WriteAttributeString("id", "tblCustomerStory");
			xtw.WriteAttributeString("class", "csContent");
			while (sqlReader.Read()){
				xtw.WriteStartElement("tr");
				
					xtw.WriteStartElement("td");
						xtw.WriteAttributeString("class", "tdThumb");
						xtw.WriteStartElement("img");
							xtw.WriteAttributeString("src", sqlReader["Thumbnail"].ToString());
							xtw.WriteAttributeString("class", "csThumb");
							xtw.WriteAttributeString("alt", sqlReader["Title"].ToString());			
						xtw.WriteEndElement();	
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("td");
						xtw.WriteAttributeString("valign", "top");
						xtw.WriteAttributeString("class", "tdDetails");
						
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("href", sqlReader["URL"].ToString());
							xtw.WriteAttributeString("class", "csTitle");
							xtw.WriteAttributeString("target", "_blank");
							xtw.WriteString(sqlReader["Title"].ToString());							
						xtw.WriteEndElement();
						
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "csDescription");
							xtw.WriteString(HttpUtility.HtmlDecode(sqlReader["Description"].ToString()));							
						xtw.WriteEndElement();
												
						
					xtw.WriteEndElement();
				xtw.WriteEndElement();
				rCount++;			
			}		
			xtw.WriteEndElement();
		}
		
		if (rCount == 0){
			xtw.WriteStartElement("p");
				xtw.WriteAttributeString("class", "noItems");
				xtw.WriteAttributeString("align", "center");
				xtw.WriteString("No Items Found. Please check back later.");					
			xtw.WriteEndElement();
		}
		
		/*
		xtw.Flush();
		xtw.WriteStartElement("p");
			xtw.WriteAttributeString("class", "noItems");
			xtw.WriteAttributeString("align", "center");
			xtw.WriteString(sqlQuery);					
		xtw.WriteEndElement();
		*/
		
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
	}
	
	
	private XmlDocument TableStyle(string title, string description, string url, string thumbnail){
		string output="";
		
		StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		xtw.WriteStartElement("table");
		xtw.WriteAttributeString("id", "tblCustomerStory");
		xtw.WriteAttributeString("class", "csResult");
		
			xtw.WriteStartElement("tr");
				xtw.WriteStartElement("td");
					xtw.WriteAttributeString("class", "tdThumb");
					xtw.WriteString("<img src='" + thumbnail + "' border='0' class='csThumb' alt='" + title + "'>");					
				xtw.WriteEndElement();
				xtw.WriteStartElement("td");
					xtw.WriteAttributeString("valign", "top");
					xtw.WriteAttributeString("class", "tdDetails");
					xtw.WriteString("<a target='_blank' href='" + url + "' class='csTitle'>" + title + "</a> <div class='csDescription' >" + description + "</div>");					
				xtw.WriteEndElement();
			xtw.WriteEndElement();
			
		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
	}
	
	
	
	[WebMethod]
	public XmlDocument LoadDivisionDropDown()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpIndustry");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateNews()");
		
        string output = "";
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT * FROM NewsManagerNewsCategories Order By CategoryID";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
			sqlReader.Read();
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
				xtw.WriteString(sqlReader["CategoryName"].ToString());					
			xtw.WriteEndElement();
			
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
					xtw.WriteString(sqlReader["CategoryName"].ToString());					
				xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "00");
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString("[View All Industry]");					
		xtw.WriteEndElement();

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	[WebMethod]
	public XmlDocument LoadYearDropDown()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpYear");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateList()");
		

		int currMonth = (int)DateTime.Now.Month;
		int currYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
		
		string prevYear = Convert.ToString(currYear - 1);
		string nextYear = Convert.ToString(currYear + 1);
		
		if(currMonth<=6){			
			
			xtw.WriteStartElement("option");		
				xtw.WriteAttributeString("value", currYear.ToString());
				xtw.WriteString(currYear.ToString());					
			xtw.WriteEndElement();
			
			xtw.WriteStartElement("option");		
				xtw.WriteAttributeString("value", prevYear.ToString());
				xtw.WriteString(prevYear.ToString());					
			xtw.WriteEndElement();
			
			//drpYear.SelectedValue = currYear.ToString();
		}else{
			xtw.WriteStartElement("option");		
				xtw.WriteAttributeString("value", nextYear.ToString());
				xtw.WriteString(nextYear.ToString());					
			xtw.WriteEndElement();
			
			xtw.WriteStartElement("option");		
				xtw.WriteAttributeString("value", currYear.ToString());
				xtw.WriteString(currYear.ToString());					
			xtw.WriteEndElement();	
				
			//if(currMonth==12) drpYear.SelectedValue = nextYear;
			//else drpYear.SelectedValue = currYear.ToString();
		}
		 //defaultYear = drpYear.SelectedValue;
		

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;

    }
		
	
	
	

    private string getDivisionName(string divID)
    {
        string output = "";
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT * FROM NewsManagerNewsCategories WHERE CategoryID=" + divID;
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                output = sqlReader["CategoryName"].ToString();
            }
        }
        sqlReader.Close();
        cnDB4.Close();
        return output;
    }
	
	
	
	[WebMethod]
	public XmlDocument createDropdown(string ddlID, string ddlType, string Table, string DataField, string ValueField, string divCode, string strFilter, string defaultValue)
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", ddlID);
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateStoryList(this)");
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "");
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString("Choose a " + ddlType);					
		xtw.WriteEndElement();
		
        SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		string sqlQuery = "";
		sqlQuery = "SELECT DISTINCT [" + DataField + "] FROM " + Table + " WHERE EventID!=''";		
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) sqlQuery += " AND Status='1'";
		else sqlQuery += " AND (Status='1' OR Status='0')";	
		if(divCode!="") sqlQuery += " AND Division='" + divCode + "'";	
		if(strFilter!="") sqlQuery += " AND " + strFilter;
		sqlQuery += " Order By " + DataField + " ASC";
		
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB2);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		ArrayList output = new ArrayList();
		
		if(sqlReader.HasRows){
			while (sqlReader.Read()){
				string[] arr = sqlReader[DataField].ToString().Split(',');
				
				for(int j=0; j<arr.Length; j++){
					output.Add(arr[j]);
				}				
			}		
		}
		sqlReader.Close();
        cnDB2.Close();
		
		output = RemoveDuplicates(output);
		bool emptyItem = false;
		for(int i=0; i<output.Count; i++){
			if(output[i].ToString()==""){
				if(ddlID=="drpMarket"){
					emptyItem = true;
				}
			}else{
				xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", output[i].ToString());
					xtw.WriteString(output[i].ToString());					
				xtw.WriteEndElement();
			}
			
		}
		if(emptyItem){
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", "");
				xtw.WriteString("- Uncategorized -");					
			xtw.WriteEndElement();
		}
		
		//ddl.Items.Add(new ListItem("United States", "US"));	
		
		//ddl.SelectedValue = defaultValue;	
		
		xtw.WriteEndElement();
		
		//Response.Write(sqlQuery);
		/*
		xtw.WriteStartElement("p");
			xtw.WriteString(sqlQuery);					
		xtw.WriteEndElement();
		*/
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	[WebMethod]
	public XmlDocument createDivisionDropdown(string ddlID, string ddlType, string Table, string DataField, string ValueField, string defaultValue)
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", ddlID);
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateList(this)");
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "");
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString("Choose a " + ddlType);					
		xtw.WriteEndElement();
		
        SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		string sqlQuery = "";
		sqlQuery = "SELECT DISTINCT [" + DataField + "] FROM " + Table;		
		sqlQuery += " Order By " + DataField + " ASC";
		
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB2);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		ArrayList output = new ArrayList();
		
		if(sqlReader.HasRows){
			while (sqlReader.Read()){
				string[] arr = sqlReader[DataField].ToString().Split(',');
				
				for(int j=0; j<arr.Length; j++){
					output.Add(arr[j]);
				}				
			}		
		}
		sqlReader.Close();
        cnDB2.Close();
		
		output = RemoveDuplicates(output);
		bool emptyItem = false;
		for(int i=0; i<output.Count; i++){
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", output[i].ToString());
				xtw.WriteString(output[i].ToString());					
			xtw.WriteEndElement();
			
		}
		
		//ddl.Items.Add(new ListItem("United States", "US"));	
		
		//ddl.SelectedValue = defaultValue;	
		
		xtw.WriteEndElement();
		
		//Response.Write(sqlQuery);
		/*
		xtw.WriteStartElement("p");
			xtw.WriteString(sqlQuery);					
		xtw.WriteEndElement();
		*/
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	
	public static ArrayList RemoveDuplicates(ArrayList list) {
		ArrayList ret=new ArrayList();
		foreach (object obj in list) {
			if (!ret.Contains(obj)) ret.Add(obj);
		}
		ret.Sort();
		return ret;
	}
	
	
	
	
	
	[WebMethod]
	public XmlDocument LoadFullEventsList(string divisionID, string region, string yr)
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		string output = "";
		output = getFullEventsList(divisionID, region, yr);

        xtw.WriteStartElement("div");
			xtw.WriteString(output);					
		xtw.WriteEndElement();

		
	 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	[WebMethod]
	[ScriptMethod(ResponseFormat = ResponseFormat.Json)] 	
	public String getFullEventsListJSON(string divisionID, string region, string Yr){

		string quickLink = "";
		
		string output = "";
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string stat = "0";
		string EST = DateTime.UtcNow.Subtract(new TimeSpan(0,5,0,0)).ToString();
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        
		
		string SqlQuery = "SELECT * FROM Trimble_Events_Calendar WHERE EventID<>''";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) SqlQuery += " AND Status='1'";
		if(server.IndexOf("ALPHA.TRIMBLE.COM") >= 0) SqlQuery += " AND Status<>'2'";
		if(server.IndexOf("LOCALHOST") >= 0) SqlQuery += " AND Status='1'";
		if (divisionID!="") SqlQuery += " AND Division Like @divisionID";
		if (region!="") SqlQuery += " AND Region=@region";
		if (Yr!="0") SqlQuery += " AND DATEPART(YEAR, StartDate)=@Yr";
		SqlQuery += " AND StartDate >='" + EST + "'";
		SqlQuery += " ORDER BY StartDate ASC";
				
		//Response.Write(server.IndexOf("LOCALHOST") + ": " + SqlQuery);
		SqlCommand myCommand;
		SqlDataReader myDataReader;
		myCommand = new SqlCommand(SqlQuery,cnDB2);
		
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		//myCommand.Parameters.Add(new SqlParameter("@divisionID", divisionID));
		myCommand.Parameters.AddWithValue("@divisionID", "%" + divisionID + "%");
        myCommand.Parameters.Add(new SqlParameter("@region", region));
		myCommand.Parameters.Add(new SqlParameter("@Yr", Yr));
		
		myDataReader = myCommand.ExecuteReader();
		
		JObject obj1 = new JObject();
		JObject obj2 = new JObject();
		obj1["name"] = "jim";
		obj1["address"] = "123";
		
		obj2["name"] = "zel";
		obj2["address"] = "321";
		
		JArray array = new JArray();
		array.Add(obj1);
		array.Add(obj2);		
		
		
			
			
		if(myDataReader.HasRows){
			//xtw.WriteStartDocument(); //uncomment for XML Declaration
			
			
			
			
			
			
			rCount++;
			myDataReader.Read();
			firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
			
			quickLink = "<a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
			
			output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
			output += "<ul>";
			output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
			output += myDataReader["EventName"].ToString()+ "</a><br>";
			output += myDataReader["City"].ToString()+ " ";
			output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
			output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
			output += " - ";
			output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
			output += "</ul>";
			output += "";
								
			while (myDataReader.Read()){
				if(firstDate == Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy")){
					//output += "<p><strong>" + Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy") +"</strong></p>";
					newMonth = false;
				}else{
					//firstDate = Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy");
					newMonth = true;
				}
				
				if(newMonth){
					quickLink += " | <a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
					output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><br><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
					firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
					newMonth = false;
				}
				output += "<ul>";
				output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
				output += myDataReader["EventName"].ToString()+ "</a><br>";
				output += myDataReader["City"].ToString()+ " ";
				output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
				output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
				output += " - ";
				output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
				output += "</ul>";
				output += "";
				rCount++;			
			}		
		}
		
		if (rCount == 0){
			output += "<p><br><center>No events found.<br><br>Please check back later.</b></center></p>";
		}
		//quickLinks.Text = quickLink;
		myDataReader.Close();
		cnDB2.Close();
		
		
		return JsonConvert.SerializeObject(array);
		//Context.Response.Write(JsonConvert.SerializeObject(array));
		
		//WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
		//return new MemoryStream(Encoding.UTF8.GetBytes(array));

	}
	
	private string getFullEventsList(string divisionID, string region, string Yr){
		string quickLink = "";
		
		string output = "";
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string stat = "0";
		string EST = DateTime.UtcNow.Subtract(new TimeSpan(0,5,0,0)).ToString();
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        
		
		string SqlQuery = "SELECT * FROM Trimble_Events_Calendar WHERE EventID<>''";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) SqlQuery += " AND Status='1'";
		if(server.IndexOf("ALPHA.TRIMBLE.COM") >= 0) SqlQuery += " AND Status<>'2'";
		if(server.IndexOf("LOCALHOST") >= 0) SqlQuery += " AND Status='1'";
		if (divisionID!="") SqlQuery += " AND Division Like @divisionID";
		if (region!="") SqlQuery += " AND Region=@region";
		if (Yr!="0") SqlQuery += " AND DATEPART(YEAR, StartDate)=@Yr";
		SqlQuery += " AND StartDate >='" + EST + "'";
		SqlQuery += " ORDER BY StartDate ASC";
				
		//Response.Write(server.IndexOf("LOCALHOST") + ": " + SqlQuery);
		SqlCommand myCommand;
		SqlDataReader myDataReader;
		myCommand = new SqlCommand(SqlQuery,cnDB2);
		
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		//myCommand.Parameters.Add(new SqlParameter("@divisionID", divisionID));
		myCommand.Parameters.AddWithValue("@divisionID", "%" + divisionID + "%");
        myCommand.Parameters.Add(new SqlParameter("@region", region));
		myCommand.Parameters.Add(new SqlParameter("@Yr", Yr));
		
		myDataReader = myCommand.ExecuteReader();
				
		if(myDataReader.HasRows){
			//xtw.WriteStartDocument(); //uncomment for XML Declaration
	
			rCount++;
			myDataReader.Read();
			firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
			
			quickLink = "<a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
			
			output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
			output += "<ul>";
			output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
			output += myDataReader["EventName"].ToString()+ "</a><br>";
			output += myDataReader["City"].ToString()+ " ";
			output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
			output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
			output += " - ";
			output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
			output += "</ul>";
			output += "";
			
			while (myDataReader.Read()){
				if(firstDate == Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy")){
					//output += "<p><strong>" + Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy") +"</strong></p>";
					newMonth = false;
				}else{
					//firstDate = Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy");
					newMonth = true;
				}
				
				if(newMonth){
					quickLink += " | <a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
					output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><br><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
					firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
					newMonth = false;
				}
				output += "<ul>";
				output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
				output += myDataReader["EventName"].ToString()+ "</a><br>";
				output += myDataReader["City"].ToString()+ " ";
				output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
				output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
				output += " - ";
				output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
				output += "</ul>";
				output += "";
				rCount++;			
			}		
		}
		
		if (rCount == 0){
			output += "<p><br><center>No events found.<br><br>Please check back later.</b></center></p>";
		}
		//quickLinks.Text = quickLink;
		myDataReader.Close();
		cnDB2.Close();
		return output;
	}
	
	
	private string getFullEventsList_BAK(string divisionID, string region, string Yr){
		string quickLink = "";
		
		string output = "";
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string stat = "0";
		string EST = DateTime.UtcNow.Subtract(new TimeSpan(0,5,0,0)).ToString();
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        
		
		string SqlQuery = "SELECT * FROM Trimble_Events_Calendar WHERE EventID<>''";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) SqlQuery += " AND Status='1'";
		if(server.IndexOf("ALPHA.TRIMBLE.COM") >= 0) SqlQuery += " AND Status<>'2'";
		if(server.IndexOf("LOCALHOST") >= 0) SqlQuery += " AND Status='1'";
		if (divisionID!="") SqlQuery += " AND Division Like @divisionID";
		if (region!="") SqlQuery += " AND Region=@region";
		if (Yr!="0") SqlQuery += " AND DATEPART(YEAR, StartDate)=@Yr";
		SqlQuery += " AND StartDate >='" + EST + "'";
		SqlQuery += " ORDER BY StartDate ASC";
				
		//Response.Write(server.IndexOf("LOCALHOST") + ": " + SqlQuery);
		SqlCommand myCommand;
		SqlDataReader myDataReader;
		myCommand = new SqlCommand(SqlQuery,cnDB2);
		
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		//myCommand.Parameters.Add(new SqlParameter("@divisionID", divisionID));
		myCommand.Parameters.AddWithValue("@divisionID", "%" + divisionID + "%");
        myCommand.Parameters.Add(new SqlParameter("@region", region));
		myCommand.Parameters.Add(new SqlParameter("@Yr", Yr));
		
		myDataReader = myCommand.ExecuteReader();
		
		if(myDataReader.HasRows){
			//output = "<strong>" + Title + "</strong>";
			rCount++;
			myDataReader.Read();
			firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
			
			quickLink = "<a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
			
			output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
			output += "<ul>";
			output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
			output += myDataReader["EventName"].ToString()+ "</a><br>";
			output += myDataReader["City"].ToString()+ " ";
			output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
			output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
			output += " - ";
			output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
			output += "</ul>";
			output += "";

			
			while (myDataReader.Read()){
				if(firstDate == Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy")){
					//output += "<p><strong>" + Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy") +"</strong></p>";
					newMonth = false;
				}else{
					//firstDate = Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy");
					newMonth = true;
				}
				
				if(newMonth){
					quickLink += " | <a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
					output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><br><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
					firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
					newMonth = false;
				}
				output += "<ul>";
				output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
				output += myDataReader["EventName"].ToString()+ "</a><br>";
				output += myDataReader["City"].ToString()+ " ";
				output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
				output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
				output += " - ";
				output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
				output += "</ul>";
				output += "";
				rCount++;			
			}		
		}
		
		if (rCount == 0){
			output += "<p><br><center>No events found.<br><br>Please check back later.</b></center></p>";
		}
		//quickLinks.Text = quickLink;
		myDataReader.Close();
		cnDB2.Close();
		return output;
	}
	
	//Convert String to ProperCase
	private static string PCase(String strParam){
		string strProper=strParam.Substring(0,1).ToUpper();
		strParam = strParam.Substring(1).ToLower();
		string strPrev="";
	
		for(int iIndex=0;iIndex < strParam.Length;iIndex++){
			if(iIndex > 1){
				strPrev=strParam.Substring(iIndex-1,1);
			}
			if( strPrev.Equals(" ") ||
			  strPrev.Equals("\t") ||
			  strPrev.Equals("\n") ||
			  strPrev.Equals(".") ||
			  strPrev.Equals("(")){
			  strProper+=strParam.Substring(iIndex,1).ToUpper();
			}
			else
			{
				strProper+=strParam.Substring(iIndex,1);
			}
		}
		return strProper;
	}
	
}




