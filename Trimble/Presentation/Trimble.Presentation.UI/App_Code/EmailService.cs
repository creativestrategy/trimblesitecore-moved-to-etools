﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.Mail;
using System.Web.Script.Serialization;


/// <summary>
/// Summary description for Mailer
/// </summary>
[WebService(Namespace = "https://trimble.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class EmailService : System.Web.Services.WebService
{
	public EmailService()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  //************************************************
  // Function:	SendEmail
  // Purpose:		Generic Mailer 
  // Args: 		string sTo = Recipient
  //				string sSubject = Email Subject
  //				string sFrom = Sender
  //				string sBCC
  //***********************************************
  [WebMethod]
  public string SendEmail(string sSubject, string sBody, string sFrom, string sTo, string sCC, string sBCC, string format)
  {
    MailMessage MyMail = new MailMessage();
    string sPriority = "HIGH";
    switch (format.ToUpper())
    {
      case "HTML":
        MyMail.BodyFormat = MailFormat.Html;
        break;
      case "TEXT":
        MyMail.BodyFormat = MailFormat.Text;
        break;
      default:
        MyMail.BodyFormat = MailFormat.Text;
        break;
    }

    switch (sPriority.ToUpper())
    {
      case "HIGH":
        MyMail.Priority = MailPriority.High;
        break;
      case "LOW":
        MyMail.Priority = MailPriority.Low;
        break;
      default:
        MyMail.Priority = MailPriority.Normal;
        break;
    }

    if (format.ToUpper() == "HTML")
    {
      string header = "";
      header += "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
      header += "<html xmlns='http://www.w3.org/1999/xhtml'>";
      header += "<head>";
      header += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
      header += "<link rel='stylesheet' type='text/css' href='http://www.trimble.com/theme/global_table_form.css' media='all' />";
      header += "</head>";
      sBody = header + sBody + "</html>";
    }

    
    //JavaScriptSerializer js = new JavaScriptSerializer();
    try{
      MyMail.From = sFrom;
      MyMail.To = sTo;
      MyMail.Subject = sSubject;
      MyMail.Body = sBody;
      MyMail.Bcc = sBCC;
      MyMail.Cc = sCC;
      MyMail.BodyEncoding = Encoding.UTF8;

      string sMailServer = "10.90.65.139";
      SmtpMail.SmtpServer = sMailServer;
      SmtpMail.Send(MyMail);
      //status = "success";
      //public string Message {get{return status;}};
      //return "success:true";
      
      return new JavaScriptSerializer().Serialize(new { errMsg = "pass" });
    }catch(Exception ex){
      //return "success:false";
      return new JavaScriptSerializer().Serialize(new { errMsg = "fail" });
    }
    
  


  }
}
