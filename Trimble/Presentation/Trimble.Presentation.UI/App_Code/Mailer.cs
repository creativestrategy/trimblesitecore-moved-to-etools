﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.ComponentModel;
using System.Collections;
using System.Web.Mail;

/// <summary>
/// Summary description for Mailer
/// </summary>
public class Mailer
{
	public Mailer()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    //************************************************
    // Function:	SendEmail
    // Purpose:		Generic Mailer 
    // Args: 		string sTo = Recipient
    //				string sSubject = Email Subject
    //				string sFrom = Sender
    //				string sBCC
    //***********************************************
    public void Send(string sTo, string sSubject, string sBody, string sFrom, string sBCC)
    {
        string sBodyFormat = "HTML";
        string sPriority = "HIGH";

        MailMessage MyMail = new MailMessage();
        MyMail.From = sFrom;
        MyMail.To = sTo; //"shelly_nooner@trimble.com; jimmy_mercado@trimble.com";
        MyMail.Subject = sSubject;
        MyMail.Body = sBody;

        MyMail.Bcc = sBCC; // "shelly_nooner@trimble.com; jimmy_mercado@trimble.com";//sBCC;

        //MyMail.UrlContentBase = sUrlContentBase;
        //MyMail.UrlContentLocation = sUrlContentLocation;
        /*
        if (txtBodyEncoding.Text == Encoding.UTF7.EncodingName)
            MyMail.BodyEncoding = Encoding.UTF7;
        else if (txtBodyEncoding.Text == Encoding.UTF8.EncodingName)
            MyMail.BodyEncoding = Encoding.UTF8;
        else
            MyMail.BodyEncoding = Encoding.ASCII;
        */

        switch (sBodyFormat.ToUpper())
        {
            case "HTML":
                MyMail.BodyFormat = MailFormat.Html;
                break;
            default:
                MyMail.BodyFormat = MailFormat.Text;
                break;
        }

        switch (sPriority.ToUpper())
        {
            case "HIGH":
                MyMail.Priority = MailPriority.High;
                break;
            case "LOW":
                MyMail.Priority = MailPriority.Low;
                break;
            default:
                MyMail.Priority = MailPriority.Normal;
                break;
        }

        //SmtpMail.SmtpServer = sMailServer;
        SmtpMail.Send(MyMail);
    }
}
