﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Web.UI;
using Newtonsoft.Json;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Collections.Generic;



/// <summary>
/// Summary description for NewsReleaseService
/// </summary>
[WebService(Namespace = "https://trimble.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class DealerLocatorService : System.Web.Services.WebService {

    public DealerLocatorService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

	
	[WebMethod]
	[ScriptMethod(UseHttpGet=true ,ResponseFormat = ResponseFormat.Json)]
	public void HelloWorld()
	{
		JavaScriptSerializer js = new JavaScriptSerializer();
		Context.Response.Clear();
		Context.Response.ContentType = "application/json";           
		JSONData data = new JSONData();
		data.Message = "HelloWorld";
		Context.Response.Write(js.Serialize(data));
	
	
	}
	
	[WebMethod]
    public String Example()
    {
        //return new MyClass();
		MyClass mc = new MyClass();
		return mc.Message;
    }


	[WebMethod]
	[ScriptMethod(UseHttpGet=true ,ResponseFormat = ResponseFormat.Json)]
	public void Test(String Token, String Id)
	{
		Dictionary<string, object> features = new Dictionary<string, object>();
		Dictionary<string, object> featuresItems = new Dictionary<string, object>();
		
		//protected Dictionary<string, object> properties = new Dictionary<string, object>();
		Dictionary<string, string> propertiesItems = new Dictionary<string, string>();
		
		//protected Dictionary<string, object> geometry = new Dictionary<string, object>();
		Dictionary<string, string> geometryItems = new Dictionary<string, string>();
		
		propertiesItems.Add("name", "Adam's Apple Crown Point");
		propertiesItems.Add("amenity", "Fruit Store");
		propertiesItems.Add("popupContent", "<h1>Adam's Apple Crown Point</h1><p>45 Nith Street<br />GLASSHOUSES<br />HG3 3XD<br />tel: 070 3442 7551</p>");	
		//properties.add("properties", propertiesItem);
		
		geometryItems.Add("type", "Point");
		geometryItems.Add("coordinates", "1.3074276,52.6128844");
		//geometry.add("geometry", geometryItems);
		
		features.Add("type", "Feature");
		features.Add("properties", propertiesItems);
		features.Add("geometry", geometryItems);
	
		JavaScriptSerializer serializer = new JavaScriptSerializer();
		string json = serializer.Serialize(features);
		
		Context.Response.Clear();
		Context.Response.ContentType = "application/json";
		Context.Response.Write(json);
	}
	
	
	[WebMethod]
	[ScriptMethod(UseHttpGet=true ,ResponseFormat = ResponseFormat.Json)]
	public void Test2(String Token, String Id)
	{
		Dictionary<string, object> features = new Dictionary<string, object>();
		List<object> Locations = new List<object>();
		
		
		//crate new Location
		Location L1 = new Location();
		L1.Name = "Adam's Apple Crown Point";
		L1.Amenity = "Fruit Store";
		L1.popupContent = "<h1>Adam's Apple Crown Point</h1><p>45 Nith Street<br />GLASSHOUSES<br />HG3 3XD<br />tel: 070 3442 7551";
		L1.Type = "Point";
		//L1.Coordinates = "1.3074276,52.6128844";
		L1.Coordinates = new List<double>() {1.3074276 , 52.6128844};
		
		Location L2 = new Location();
		L2.Name = "Adam's Apple Thorpe Hamlet";
		L2.Amenity = "Fruit Store";
		L2.popupContent = "<h1>Adam's Apple Thorpe Hamlet</h1><p>50 Wood Lane<br />MONACHTY<br />SY23 2ZW<br />077 7895 5460</p>";
		L2.Type = "Point";
		L2.Coordinates = new List<double>() {1.3129529 , 52.6280232};
		
		Locations.Add(L1.getData());
		Locations.Add(L2.getData());
		
		features.Add("type", "FeatureCollection");
		features.Add("features", Locations);
		
	
		JavaScriptSerializer serializer = new JavaScriptSerializer();
		string json = serializer.Serialize(features);
		
		Context.Response.Clear();
		Context.Response.ContentType = "application/json";
		Context.Response.Write(json);
	}

	[WebMethod]
	[ScriptMethod(UseHttpGet=true ,ResponseFormat = ResponseFormat.Json)]
	public void Test3(String Token, String Id, String Country)
	{
		Dictionary<string, object> features = new Dictionary<string, object>();
		List<object> Locations = new List<object>();
		
		List<object> locations = new List<object>();
		locations = getLocations(Country);
		
		features.Add("type", "FeatureCollection");
		features.Add("features", locations);
		
	
		JavaScriptSerializer serializer = new JavaScriptSerializer();
		string json = serializer.Serialize(features);
		
		Context.Response.Clear();
		Context.Response.ContentType = "application/json";
		Context.Response.Write(json);
	}

	[WebMethod]
	[ScriptMethod(UseHttpGet=true ,ResponseFormat = ResponseFormat.Json)]
	public void FindNearBy(int type, String lat, String lon, int industry, int application, int product )
	{
		switch (type){
			case 1: //industry
				break;
				
			case 2: //application
				
				break;
				
			case 3: // product
				List<object> param = ProductToChanel(product);
				
				break;				
		}
		
		Dictionary<string, object> features = new Dictionary<string, object>();
		List<object> Locations = new List<object>();
		
		List<object> locations = new List<object>();
		locations = getLocations(Country);
		
		features.Add("type", "FeatureCollection");
		features.Add("features", locations);
		
	
		JavaScriptSerializer serializer = new JavaScriptSerializer();
		string json = serializer.Serialize(features);
		
		Context.Response.Clear();
		Context.Response.ContentType = "application/json";
		Context.Response.Write(json);
	}

	private List<object> getLocations(string country, string param )
    {
		List<object> Locations = new List<object>();
		
        SqlConnection cnDL = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDealerLocator"]);
        if (cnDL.State != ConnectionState.Open) cnDL.Open();

        //string sqlQuery = "Select * FROM TrimbleLocations WHERE Country_Name='" + country +"' AND Dealer=1";
		string sqlQuery = "Select * FROM TrimbleLocations";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDL);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                //output = sqlReader["CategoryName"].ToString();
				Location location = new Location();
				location.Name = sqlReader["Location Name"].ToString();
				location.Amenity = sqlReader["Product_Line"].ToString();
				location.popupContent = "<h1>" + sqlReader["Location Name"].ToString() + "</h1><p>" + sqlReader["ADDRESSLINE"].ToString() + " " + sqlReader["ADDRESSLINE2"].ToString() + "<br>" + sqlReader["PRIMARYCITY"].ToString() + ", " + sqlReader["POSTALCODE"].ToString() + "<br/>" + sqlReader["Country_Name"].ToString() + "<br/>Phone: " + sqlReader["Business Phone"].ToString() + "<br/>Fax: " + sqlReader["Fax"].ToString() + "<br/>Web: " + sqlReader["URL"].ToString() + "</p>"; //"<h1>Adam's Apple Thorpe Hamlet</h1><p>50 Wood Lane<br />MONACHTY<br />SY23 2ZW<br />077 7895 5460</p>";
				location.Type = "Point";
				location.Coordinates = new List<double>() { Convert.ToDouble(sqlReader["LATITUDE"]) , Convert.ToDouble(sqlReader["LONGITUDE"])};
				
				Locations.Add(location.getData());
            }
        }
        sqlReader.Close();
        cnDL.Close();
        return Locations;
    }
	
	private List<object> getProductToChannel(string search)
    {
		List<object> productInfo = new List<object>();
		string strSQL = "SELECT * From qryWebProductToChannel WHERE AutoID=" + search ;
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while(reader.Read()){
						for(int i=1;i<reader.FieldCount;i++){
							
							//Dictionary<string, object> info = new Dictionary<string, object>();		
							string colName = reader.GetName(i).ToString();
							if(Convert.ToBoolean(reader[colName])==true){
								//info.Add(colName, reader[colName]);
								//productInfo.Add(info);
								productInfo.Add(colName);
							}
						}	
					}
 
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		return productInfo;
    }
	
	private List<object> getChanels(string search)
    {
		List<object> productInfo = new List<object>();
		string strSQL = "SELECT * From [Product To Channel] WHERE AutoID=" + search ;
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while(reader.Read()){
						for(int i=1;i<reader.FieldCount;i++){
							
							//Dictionary<string, object> info = new Dictionary<string, object>();		
							string colName = reader.GetName(i).ToString();
							if(Convert.ToBoolean(reader[colName])==true){
								//info.Add(colName, reader[colName]);
								//productInfo.Add(info);
								productInfo.Add(colName);
							}
						}	
					}
 
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		return productInfo;
    }
	
	
	[WebMethod]
	public void ProductToChanel(string search)
    {
		List<object> productInfo = new List<object>();
		string strSQL = "SELECT * From qryWebProductToChannel WHERE AutoID=" + search ;
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while(reader.Read()){
						for(int i=1;i<reader.FieldCount;i++){
							
							Dictionary<string, object> info = new Dictionary<string, object>();		
							string colName = reader.GetName(i).ToString();
							if(Convert.ToBoolean(reader[colName])==true){
								info.Add(colName, reader[colName]);
								productInfo.Add(info);
							}
						}	
					}
 
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		
		JavaScriptSerializer serializer = new JavaScriptSerializer();  
        Context.Response.Write(serializer.Serialize(productInfo)); 
    }
	
	
	
	[WebMethod]
	public XmlDocument LoadCountries()
    {
		HttpContext.Current.Response.Clear();
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpCountry");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateStates()");
		
        string output = "";
        SqlConnection cnDB = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cnDB.State != ConnectionState.Open) cnDB.Open();

        string sqlQuery = "SELECT * FROM FormCountryList Order By Country_Name";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "");
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString("");					
		xtw.WriteEndElement();
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "US");
			xtw.WriteString("United States");					
		xtw.WriteEndElement();
		
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", sqlReader["Country_ID"].ToString());

					xtw.WriteString(sqlReader["Country_Name"].ToString());					
				xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB.Close();
		
				
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	[WebMethod]
	public XmlDocument LoadStates(String countryID)
    {
		HttpContext.Current.Response.Clear();
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = System.Xml.Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        
		
        string output = "";
        SqlConnection cnDB = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cnDB.State != ConnectionState.Open) cnDB.Open();

        string sqlQuery = "SELECT * FROM FormStateList_New WHERE Country_ID='"+ countryID.ToUpper() +"' Order By State_Name";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
        if (sqlReader.HasRows)
        {
			xtw.WriteStartElement("select");
			xtw.WriteAttributeString("id", "drpState");
			xtw.WriteAttributeString("class", "dropList");
			
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", "");
				xtw.WriteAttributeString("selected", "selected");
				xtw.WriteString("");					
			xtw.WriteEndElement();
		
			while (sqlReader.Read())
            {
                xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", sqlReader["State_ID"].ToString());
					xtw.WriteString(sqlReader["State_Name"].ToString());					
				xtw.WriteEndElement();
            }
        }else{
			xtw.WriteStartElement("input");
			xtw.WriteAttributeString("id", "txtState");
			xtw.WriteAttributeString("class", "txtBox");
		}
		
        sqlReader.Close();
        cnDB.Close();
		
				
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	[WebMethod]
	public void LoadProducts()
    {
		List<object> products = new List<object>();
		string strSQL = "SELECT * From Product Order By Product";
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while (reader.Read())  
					{  
						Dictionary<string, string> product = new Dictionary<string, string>();			
						product.Add("id", reader["AutoID"].ToString());
						product.Add("product", reader["Product"].ToString());
						product.Add("businessArea", reader["Business Area"].ToString());
						products.Add(product);
					}    
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		JavaScriptSerializer serializer = new JavaScriptSerializer();  
        Context.Response.Write(serializer.Serialize(products));  
    }
	
	[WebMethod]
	public void SearchProducts(string search)
    {
		List<object> productList = new List<object>();
		string strSQL = "SELECT TOP 15 * From Product WHERE Product like '%" + search + "%' Order By Product ASC ";
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while (reader.Read())  
					{  
						Dictionary<string, string> products = new Dictionary<string, string>();			
						products.Add("id", reader["AutoID"].ToString());
						products.Add("name", reader["Product"].ToString());
						products.Add("businesArea", reader["Business Area"].ToString());
						productList.Add(products);
					}  
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		
		JavaScriptSerializer serializer = new JavaScriptSerializer();  
        Context.Response.Write(serializer.Serialize(productList)); 
    }
	
	
	[WebMethod]
	public void LoadIndustries()
    {
		List<object> industries = new List<object>();
		string strSQL = "SELECT * FROM Industry WHERE enIndustry <> '' ORDER BY enIndustry ASC ";
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while (reader.Read())  
					{  
						Dictionary<string, string> industry = new Dictionary<string, string>();			
						industry.Add("id", reader["AutoID"].ToString());
						industry.Add("industry", reader["enIndustry"].ToString());
						industries.Add(industry);
					}  
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		
		JavaScriptSerializer serializer = new JavaScriptSerializer();  
        Context.Response.Write(serializer.Serialize(industries)); 
    }
	
	[WebMethod]
	public void LoadSolutions()
    {
		List<object> solutions = new List<object>();
		string strSQL = "SELECT * FROM Applications ORDER BY enApplications ASC";
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while (reader.Read())  
					{  
						Dictionary<string, string> solution = new Dictionary<string, string>();			
						solution.Add("id", reader["AutoID"].ToString());
						solution.Add("solution", reader["enApplications"].ToString());
						solution.Add("industry_id", reader["IndustryID"].ToString());
						solutions.Add(solution);
					}  
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		
		JavaScriptSerializer serializer = new JavaScriptSerializer();  
        Context.Response.Write(serializer.Serialize(solutions)); 
    }
	
	[WebMethod]
	public void LoadSolutionsByIndusty(string industry)
    {
		List<object> solutions = new List<object>();
		string strSQL = "SELECT * From Applications WHERE IndustryID = " + industry + " Order By enApplications ASC ";
		using (OleDbConnection connection = new OleDbConnection(ConfigurationSettings.AppSettings["SqlConnStrAccessDB"]))  
		{  
			// Create a command and set its connection  
			OleDbCommand command = new OleDbCommand(strSQL, connection);  
			// Open the connection and execute the select command.  
			try  
			{  
				// Open connecton  
				connection.Open();  
				// Execute command  
				using (OleDbDataReader reader = command.ExecuteReader())  
				{  
					while (reader.Read())  
					{  
						Dictionary<string, string> solution = new Dictionary<string, string>();			
						solution.Add("id", reader["AutoID"].ToString());
						solution.Add("name", reader["enApplications"].ToString());
						solutions.Add(solution);
					}  
				}  
			}  
			catch (Exception ex)  
			{  
				//Console.WriteLine(ex.Message);  
			}  
			// The connection is automatically closed becasuse of using block.  
		}
		
		JavaScriptSerializer serializer = new JavaScriptSerializer();  
        Context.Response.Write(serializer.Serialize(solutions)); 
    }
	
}

//END DealerLocatorService

public class MyClass
{
	public string Message { get { return "Hi"; } }
	public int Number { get { return 123; } }
	public List<string> List { get { return new List<string> { "Item1", "Item2", "Item3" }; } }
}

public class JSONData
{
   public String Message;
}


public class Location
{
	public string Name { get; set;}
	public string Amenity { get; set;}
	public string popupContent { get; set;}
	public string Type { get; set;}
	public List<double> Coordinates { get; set;}
	
	public Dictionary<string, object> data;
	private Dictionary<string, string> propertiesItems;
	private Dictionary<string, object> geometryItems;
	
	/*	
	public Location(string _name){
		this.Name = _name;
		data = new Dictionary<string, object>();
	 	propertiesItems = new Dictionary<string, string>();
		geometryItems = new Dictionary<string, string>();
	
		propertiesItems.Add("name", _name);
		propertiesItems.Add("amenity", "Fruit Store");
		propertiesItems.Add("popupContent", "<h1>Adam's Apple Crown Point</h1><p>45 Nith Street<br />GLASSHOUSES<br />HG3 3XD<br />tel: 070 3442 7551</p>");	
		
		geometryItems.Add("type", "Point");
		geometryItems.Add("coordinates", "1.3074276,52.6128844");
		
		data.Add("type", "Feature");
		data.Add("properties", propertiesItems);
		data.Add("geometry", geometryItems);		
	}
	*/	
	
	public IReadOnlyDictionary<string, object> getData(){
		data = new Dictionary<string, object>();
	 	propertiesItems = new Dictionary<string, string>();
		geometryItems = new Dictionary<string, object>();
	
		propertiesItems.Add("name", this.Name);
		propertiesItems.Add("amenity", this.Amenity);
		propertiesItems.Add("popupContent", this.popupContent);	
		
		geometryItems.Add("type", this.Type);
		geometryItems.Add("coordinates", this.Coordinates);
		
		data.Add("type", "Feature");
		data.Add("properties", propertiesItems);
		data.Add("geometry", geometryItems);
		return data;
	}	

}
