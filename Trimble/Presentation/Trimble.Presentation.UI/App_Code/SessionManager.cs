using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


	public class SessionManager
	{
		public SessionManager()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		////////////////////////////////////////////////////////////////
		// Set/Create Session using a Name/Value pair
		// args: sname=session name; svalue=value for the given session namme
		////////////////////////////////////////////////////////////////
		public void setValue(string sname, string svalue)
		{
			HttpContext.Current.Session[sname] = svalue;
		}

		public string getValue(string sname)
		{
			string val = "";
			if(HttpContext.Current.Session[sname]!= null)
			{
				val =  HttpContext.Current.Session[sname].ToString();
			}
			return val;
		}

		

		public void LogOut()
		{
			HttpContext.Current.Session.Clear();
			HttpContext.Current.Session.Abandon();
		}

	}