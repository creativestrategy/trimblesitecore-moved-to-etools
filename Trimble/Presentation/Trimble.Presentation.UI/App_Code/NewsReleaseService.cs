﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Globalization;




/// <summary>
/// Summary description for NewsReleaseService
/// </summary>
[WebService(Namespace = "https://trimble.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class NewsReleaseService : System.Web.Services.WebService {

    public NewsReleaseService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    [WebMethod]
    public XmlDocument getLatestNews(string divList, string rowCount)
    {

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/xml";

        
        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;

        

        xtw.WriteStartDocument();
        string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("rss");
        xtw.WriteAttributeString("version", "2.0");
        xtw.WriteStartElement("channel");
        xtw.WriteElementString("title", "Trimble News");
        xtw.WriteElementString("link",  "https://www.trimble.com/news");
        xtw.WriteElementString("description", "Latest News Release From Trimble.");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date] FROM NewsManagerArticles WHERE Status=1 ";

        if (divList!=""){
            string[] arrDiv = divList.Split(',');
            int tCount = arrDiv.Length;
            string logicOp = "AND";
            for (int x = 0; x < tCount; x++)
            {
                if (x != 0) logicOp = "OR";
                sqlQuery += logicOp + " CategoryID Like '%%" + arrDiv[x] + "%%' ";
            }
        }

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";
        
        /* 
        SqlDataAdapter da = new SqlDataAdapter(sqlQuery, cnDB4);        
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                xtw.WriteStartElement("item");
                xtw.WriteElementString("title", dt.Rows[i]["Title"].ToString());
                xtw.WriteElementString("description", dt.Rows[i]["Title"].ToString());
                xtw.WriteElementString("link", "http://www.trimble.com/news/release.aspx?id=" + dt.Rows[i]["FileName"].ToString());
                xtw.WriteElementString("pubDate", XmlConvert.ToString(Convert.ToDateTime(dt.Rows[i]["Publish_Date"].ToString())));
                xtw.WriteEndElement();
            }
        }
       */
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("item");
                xtw.WriteElementString("title", sqlReader.GetString(2));
                //xtw.WriteElementString("description", sqlReader.GetString(2));
                xtw.WriteElementString("description", "");
                xtw.WriteElementString("link", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
                xtw.WriteElementString("pubDate", sqlReader.GetDateTime(4).ToString("R"));
                xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        xtw.WriteEndElement();
        xtw.WriteEndDocument();


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
        return xmlDocument;


    }

    [WebMethod]
    public XmlDocument getLatestNewsByDivision(string divID, string rowCount)
    {
       
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/xml";

        
        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		string divName = "";
		if(divID!="0" && divID!=""){
       		divName = getDivisionName(divID);
		}

        xtw.WriteStartDocument();
        string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("rss");
        xtw.WriteAttributeString("version", "2.0");
        xtw.WriteStartElement("channel");
        xtw.WriteElementString("title", "Trimble - " + divName + " News Release");
        xtw.WriteElementString("link",  "https://www.trimble.com/news");
        xtw.WriteElementString("description", "Latest news release from Trimble - " + divName + ".");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date] FROM NewsManagerArticles WHERE Status=1";
		if(divID!="0" || divID!="") sqlQuery += " AND CategoryID Like '%%" + divID + "%%'";


        sqlQuery += " AND Publish_Date <='" + EST + "'";
        sqlQuery += " ORDER BY Publish_Date DESC";
        
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("item");
                xtw.WriteElementString("title", sqlReader.GetString(2));
                //xtw.WriteElementString("description", sqlReader.GetString(2));
                xtw.WriteElementString("description", sqlReader.GetDateTime(4).ToString("D"));
				xtw.WriteElementString("content", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
                xtw.WriteElementString("link", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
                xtw.WriteElementString("pubDate", sqlReader.GetDateTime(4).ToString("MM/dd/yyyy HH:mm tt"));
                xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();

        //Adding XML Footer 
        xtw.WriteStartElement("item");
        xtw.WriteElementString("title", "News Release for Trimble - " + divName);
        //xtw.WriteElementString("description", sqlReader.GetString(2));
        //xtw.WriteElementString("description", "Visit https://www.trimble.com/news/news_release.aspx?divID=" + divID);
		xtw.WriteElementString("description", "View all latest news release from Trimble - " + divName + ".");
        xtw.WriteElementString("link", "https://www.trimble.com/corporate/news_release.aspx?div_id=" + divID);
        xtw.WriteElementString("pubDate", "");
        xtw.WriteEndElement();

        //end Footer

        xtw.WriteEndElement();
        xtw.WriteEndElement();
        xtw.WriteEndDocument();


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
        return xmlDocument;


    }
	
	
	
	/* Added by JM 05-03-2012 */
	
	[WebMethod]
	public XmlDocument getLatestNewsHTMLPop(string divList, string rowCount, string year)
    {
		int currYear = (int)DateTime.Now.Year;
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;


        //xtw.WriteStartDocument();
        //string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        //xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("div");
        xtw.WriteAttributeString("class", "newslist");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date], [Never_Expire] FROM NewsManagerArticles WHERE Status=1 ";
		
		if (year=="" || year==null) {
			year = currYear.ToString();	
		}
		sqlQuery += " AND DATEPART(YEAR, Publish_Date) = '" + year + "'";
		
        
		if (divList!=""){
            string[] arrDiv = divList.Split(',');
            int tCount = arrDiv.Length;
            string logicOp = "AND";
            for (int x = 0; x < tCount; x++)
            {
                if (x != 0) logicOp = "OR";
                sqlQuery += logicOp + " CategoryID Like '%%" + arrDiv[x] + "%%' ";
            }
        }

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";
        

        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		
        if (sqlReader.HasRows)
        {
			xtw.WriteStartElement("div");
			/*-----------------------------------------*/	
			rCount++;
			sqlReader.Read();
			firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
			if(Convert.ToBoolean(sqlReader["Never_Expire"])){
				xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
				
				xtw.WriteStartElement("span");
					xtw.WriteAttributeString("class", "newsDate");
					xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
				xtw.WriteEndElement();
				
				xtw.WriteStartElement("a");
					xtw.WriteAttributeString("href", "#");
					xtw.WriteAttributeString("onclick", "window.open(\"https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3) + "\",\"NewsWindow\",\"status=1,left=100,width=800,height=650,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,location=0\");popupWindow.focus();popupWindow.opener.blur();");
					xtw.WriteString(sqlReader.GetString(2));
				xtw.WriteEndElement();

			}else{
				if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
					xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
					
					xtw.WriteStartElement("span");
						xtw.WriteAttributeString("class", "newsDate");
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", "#");
						xtw.WriteAttributeString("onclick", "window.open(\"https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3) + "\",\"NewsWindow\",\"status=1,left=100,width=800,height=650,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,location=0\");popupWindow.focus();popupWindow.opener.blur();");
						xtw.WriteString(sqlReader.GetString(2));
					xtw.WriteEndElement();
				}
			}
			
			while (sqlReader.Read()){
				if(Convert.ToBoolean(sqlReader["Never_Expire"])){
					if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
						//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
						newMonth = false;
					}else{
						//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = true;
					}
					
					if(newMonth){
						xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
						firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = false;
					}
					xtw.WriteStartElement("span");
						xtw.WriteAttributeString("class", "newsDate");
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", "#");
						xtw.WriteAttributeString("onclick", "window.open(\"https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3) + "\",\"NewsWindow\",\"status=1,left=100,width=800,height=650,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,location=0\");popupWindow.focus();popupWindow.opener.blur();");
						xtw.WriteString(sqlReader.GetString(2));
					xtw.WriteEndElement();
					
					rCount++;
				}else{
					if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
						if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
							//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
							newMonth = false;
						}else{
							//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = true;
						}
						
						if(newMonth){
							xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
							firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = false;
						}
						xtw.WriteStartElement("span");
							xtw.WriteAttributeString("class", "newsDate");
							xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
						xtw.WriteEndElement();
						
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("href", "#");
							xtw.WriteAttributeString("onclick", "window.open(\"https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3) + "\",\"NewsWindow\",\"status=1,left=100,width=800,height=650,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,location=0\");popupWindow.focus();popupWindow.opener.blur();");
							xtw.WriteString(sqlReader.GetString(2));
						xtw.WriteEndElement();
						
						rCount++;
					}
				}
				
			}	
			
			
			xtw.WriteEndElement();
			
			
			
			
			
			
			
			
			
			/*-------------------------------------------*/
			
			/*
			xtw.WriteStartElement("div");
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("span");
					xtw.WriteAttributeString("class", "newsDate");
					xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
				xtw.WriteEndElement();
				
				xtw.WriteStartElement("a");
					xtw.WriteAttributeString("href", "http://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
					xtw.WriteAttributeString("onclick", "window.open(\"http://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3) + "\",\"NewsWindow\",\"status=1,left=100,width=800,height=650,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,location=0\");popupWindow.focus();popupWindow.opener.blur();");
					xtw.WriteString(sqlReader.GetString(2));
				xtw.WriteEndElement();
				
            }
			xtw.WriteEndElement();
			*/
        }else{
			xtw.WriteStartElement("div");
				xtw.WriteAttributeString("style", "text-align:center; padding:32px;");
				xtw.WriteString("No News Release Found!");					
			xtw.WriteEndElement();
		}
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        //xtw.WriteEndDocument();


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
        return xmlDocument;


    }
	
	
	
	
		/* Added by JM 05-03-2012 */
	
	[WebMethod]
	 public XmlDocument getLatestNewsHTML_EN(string divList, string rowCount, string year)
    {
		int currYear = (int)DateTime.Now.Year;
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string newsURL = "/news/release.aspx";
		
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        //string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        //xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("div");
        xtw.WriteAttributeString("class", "newslist");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date], [Never_Expire] FROM NewsManagerArticles WHERE Status=1 ";
		
		if (year=="" || year==null) {
			year = currYear.ToString();	
		}
		sqlQuery += " AND DATEPART(YEAR, Publish_Date) = '" + year + "'";
		
        
		if (divList!=""){
            string[] arrDiv = divList.Split(',');
            int tCount = arrDiv.Length;
            string logicOp = "AND";
            for (int x = 0; x < tCount; x++)
            {
                if (x != 0) logicOp = "OR";
                sqlQuery += logicOp + " CategoryID Like '%%" + arrDiv[x] + "%%' ";
            }
        }

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";
        

        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		
        if (sqlReader.HasRows)
        {
			xtw.WriteStartElement("div");
			/*-----------------------------------------*/	
			rCount++;
			sqlReader.Read();
			firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
			if(Convert.ToBoolean(sqlReader["Never_Expire"])){
				xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
				
				xtw.WriteStartElement("div");
					xtw.WriteAttributeString("class", "newsDate");
					xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
				xtw.WriteEndElement();
				
				xtw.WriteStartElement("a");
					xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
					xtw.WriteAttributeString("target", "_blank");
					xtw.WriteString(sqlReader.GetString(2));
				xtw.WriteEndElement();

			}else{
				if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
					xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
						xtw.WriteAttributeString("target", "_blank");
						xtw.WriteRaw(sqlReader.GetString(2));
					xtw.WriteEndElement();
				}
			}
			
			while (sqlReader.Read()){
				if(Convert.ToBoolean(sqlReader["Never_Expire"])){
					if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
						//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
						newMonth = false;
					}else{
						//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = true;
					}
					
					if(newMonth){
						xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
						firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = false;
					}
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
						xtw.WriteAttributeString("target", "_blank");
						xtw.WriteRaw(sqlReader.GetString(2));
					xtw.WriteEndElement();
					
					rCount++;
				}else{
					if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
						if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
							//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
							newMonth = false;
						}else{
							//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = true;
						}
						
						if(newMonth){
							xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
							firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = false;
						}
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "newsDate");
							xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
						xtw.WriteEndElement();
						
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("href", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
							xtw.WriteAttributeString("target", "_blank");
							xtw.WriteRaw(sqlReader.GetString(2));
						xtw.WriteEndElement();
						
						rCount++;
					}
				}
				
			}			
			xtw.WriteEndElement();			
        }else{
			xtw.WriteStartElement("div");
				xtw.WriteAttributeString("style", "text-align:center; padding:32px;");
				xtw.WriteString("No News Release Found!");					
			xtw.WriteEndElement();
		}
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        //xtw.WriteEndDocument(); //uncomment for closing XML Declaration


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		//XmlDocumentType XDType = xmlDocument.DocumentType;
		//xmlDocument.RemoveChild(XDType);
		/*
		if (xmlDocument.FirstChild.NodeType == XmlNodeType.XmlDeclaration) {
			xmlDocument.RemoveChild(xmlDocument.FirstChild);
		}
		*/

        return xmlDocument;


    }
	/*End*/
	
	/* Added by JM 03-31-2018 */	
	[WebMethod]
	 public XmlDocument getLatestNewsHTML_CN(string divList, string rowCount, string year)
    {
		int currYear = (int)DateTime.Now.Year;
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string newsURL = "/news/CN/release.aspx";
		
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        //string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        //xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("div");
        xtw.WriteAttributeString("class", "newslist");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date], [Never_Expire] FROM NewsManagerArticles_CN WHERE Status=1 ";
		
		/*
		if (year=="" || year==null) {
			year = currYear.ToString();	
		}
		*/
		if(year!=""){
			sqlQuery += " AND DATEPART(YEAR, Publish_Date) = '" + year + "'";
		}
        
		if (divList!=""){
            string[] arrDiv = divList.Split(',');
            int tCount = arrDiv.Length;
            string logicOp = "AND";
            for (int x = 0; x < tCount; x++)
            {
                if (x != 0) logicOp = "OR";
                sqlQuery += logicOp + " CategoryID Like '%%" + arrDiv[x] + "%%' ";
            }
        }

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";
        

        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		
        if (sqlReader.HasRows)
        {
			xtw.WriteStartElement("div");
			/*-----------------------------------------*/	
			rCount++;
			sqlReader.Read();
			firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
			if(Convert.ToBoolean(sqlReader["Never_Expire"])){
				//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
				xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
				
				xtw.WriteStartElement("div");
					xtw.WriteAttributeString("class", "newsDate");
					//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));						
					xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));			
				xtw.WriteEndElement();
				
				xtw.WriteStartElement("a");
					xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
					xtw.WriteAttributeString("target", "_blank");
					xtw.WriteString(sqlReader.GetString(2));
				xtw.WriteEndElement();

			}else{
				if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
					//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
					xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));	
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));					
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
						xtw.WriteAttributeString("target", "_blank");
						xtw.WriteRaw(sqlReader.GetString(2));
					xtw.WriteEndElement();
				}
			}
			
			while (sqlReader.Read()){
				if(Convert.ToBoolean(sqlReader["Never_Expire"])){
					if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
						//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
						newMonth = false;
					}else{
						//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = true;
					}
					
					if(newMonth){
						//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
						xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
						firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = false;
					}
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));		
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));			
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
						xtw.WriteAttributeString("target", "_blank");
						xtw.WriteRaw(sqlReader.GetString(2));
					xtw.WriteEndElement();
					
					rCount++;
				}else{
					if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
						if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
							//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
							newMonth = false;
						}else{
							//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = true;
						}
						
						if(newMonth){
							//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
							xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
							firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = false;
						}
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "newsDate");
							//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));
							xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));						
						xtw.WriteEndElement();
						
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
							xtw.WriteAttributeString("target", "_blank");
							xtw.WriteRaw(sqlReader.GetString(2));
						xtw.WriteEndElement();
						
						rCount++;
					}
				}
				
			}			
			xtw.WriteEndElement();			
        }else{
			xtw.WriteStartElement("div");
				xtw.WriteAttributeString("style", "text-align:center; padding:32px;");
				xtw.WriteString("No News Release Found!");			
			xtw.WriteEndElement();
		}
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        //xtw.WriteEndDocument(); //uncomment for closing XML Declaration


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		//XmlDocumentType XDType = xmlDocument.DocumentType;
		//xmlDocument.RemoveChild(XDType);
		/*
		if (xmlDocument.FirstChild.NodeType == XmlNodeType.XmlDeclaration) {
			xmlDocument.RemoveChild(xmlDocument.FirstChild);
		}
		*/

        return xmlDocument;


    }
	/*End getLatestNewsHTML_CN */
	
	
	/* Added by JM 03-31-2018 */	
	[WebMethod]
	public XmlDocument getLatestNewsHomepage(string language, string rowCount)
    {
		int currYear = (int)DateTime.Now.Year;
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string newsURL;
		string sqlTable;
		string langDateFormat;
        CultureInfo culture;
		
		switch(language.ToUpper()){
			case "CN":
				sqlTable = "NewsManagerArticles_CN";
				langDateFormat = "MM月 dd日";
                culture = CultureInfo.CreateSpecificCulture("zh-CN");
				newsURL = "/news/CN/release.aspx";
				break;
            case "PT":
                sqlTable = "NewsManagerArticles";
                langDateFormat = "D";
                culture = CultureInfo.CreateSpecificCulture("pt-BR");
                newsURL = "/news/release.aspx";
                break;
			default:
				sqlTable = "NewsManagerArticles";
				langDateFormat = "D";
                culture = CultureInfo.CreateSpecificCulture("en-US");
				newsURL = "/news/release.aspx";
				break;
				
		}
		
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        //string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        //xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("div");
        xtw.WriteAttributeString("class", "newslist");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date], [Never_Expire] FROM " + sqlTable + " WHERE Status=1 ";

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";
        

        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();

	
        if (sqlReader.HasRows)
        {
			xtw.WriteStartElement("div");
			/*-----------------------------------------*/	
			rCount++;
			sqlReader.Read();
			firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
			if(Convert.ToBoolean(sqlReader["Never_Expire"])){
				//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
				//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
				
				xtw.WriteStartElement("div");
					xtw.WriteAttributeString("class", "newsDate");
					//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));						
					//xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));		
					xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat, culture));		
				xtw.WriteEndElement();
				
				xtw.WriteStartElement("a");
					xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
					xtw.WriteAttributeString("target", "_blank");
					xtw.WriteString(sqlReader.GetString(2));
				xtw.WriteEndElement();

			}else{
				if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
					//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
					//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));	
						//xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));		
						xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat, culture));			
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
						xtw.WriteAttributeString("target", "_blank");
						xtw.WriteRaw(sqlReader.GetString(2));
					xtw.WriteEndElement();
				}
			}
			
			while (sqlReader.Read()){
				if(Convert.ToBoolean(sqlReader["Never_Expire"])){
					if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
						//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
						newMonth = false;
					}else{
						//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = true;
					}
					
					if(newMonth){
						//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
						//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
						firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = false;
					}
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));		
						//xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));	
						xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat, culture));		
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("a");
						xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
						xtw.WriteAttributeString("target", "_blank");
						xtw.WriteRaw(sqlReader.GetString(2));
					xtw.WriteEndElement();
					
					rCount++;
				}else{
					if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
						if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
							//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
							newMonth = false;
						}else{
							//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = true;
						}
						
						if(newMonth){
							//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
							//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
							firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = false;
						}
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "newsDate");
							//xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));
							//xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));	
							xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat, culture));					
						xtw.WriteEndElement();
						
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
							xtw.WriteAttributeString("target", "_blank");
							xtw.WriteRaw(sqlReader.GetString(2));
						xtw.WriteEndElement();
						
						rCount++;
					}
				}
				
			}			
			xtw.WriteEndElement();			
        }else{
			xtw.WriteStartElement("div");
				xtw.WriteAttributeString("style", "text-align:center; padding:32px;");
				xtw.WriteString("No News Release Found!");			
			xtw.WriteEndElement();
		}
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        //xtw.WriteEndDocument(); //uncomment for closing XML Declaration


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		//XmlDocumentType XDType = xmlDocument.DocumentType;
		//xmlDocument.RemoveChild(XDType);
		/*
		if (xmlDocument.FirstChild.NodeType == XmlNodeType.XmlDeclaration) {
			xmlDocument.RemoveChild(xmlDocument.FirstChild);
		}
		*/

        return xmlDocument;


    }

	/*End getLatestNewsHomepage */


	/* Added by JM 12-20-2020 */	
	[WebMethod]
	public XmlDocument getLatestNewsHomepage_Luna(string language, string rowCount)
    {
		int currYear = (int)DateTime.Now.Year;
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string newsURL;
		string sqlTable;
		string langDateFormat;
    string strFullTitle;
    string strTrimTitle;
		
		switch(language.ToUpper()){
			case "CN":
				sqlTable = "NewsManagerArticles_CN";
				langDateFormat = "MM月 dd日";
				newsURL = "/news/CN/release.aspx";
				break;
			default:
				sqlTable = "NewsManagerArticles";
				langDateFormat = "D";
				newsURL = "/news/release.aspx";
				break;
				
		}
		
    HttpContext.Current.Response.Clear();
    HttpContext.Current.Response.ContentType = "text/html";
    
    
    StringWriter stringWriter = new StringWriter();
    //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
    XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
    xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

    //string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
    //xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
    //xtw.WriteStartElement("div");
    //xtw.WriteAttributeString("class", "newslist");

    DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
    SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
    if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

    string sqlQuery = "SELECT ";
    if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
    //else sqlQuery += " * ";
    sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date], [Never_Expire] FROM " + sqlTable + " WHERE Status=1 ";

    sqlQuery += "AND Publish_Date <='" + EST + "' ";
    sqlQuery += "ORDER BY Publish_Date DESC";
    

    SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
    SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		
    if (sqlReader.HasRows)
    {

			/*-----------------------------------------*/	
			rCount++;
			sqlReader.Read();
			firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");

      strFullTitle = sqlReader.GetString(2);
      strTrimTitle = TruncateAtWord(sqlReader.GetString(2), 80);

			if(Convert.ToBoolean(sqlReader["Never_Expire"])){
				//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
				//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
        
        xtw.WriteStartElement("div");
				xtw.WriteAttributeString("class", "newsItem");

          xtw.WriteStartElement("div");
          xtw.WriteAttributeString("class", "pubDate");
            //xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));						
            //xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));		
            xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat));		
          xtw.WriteEndElement();
          
          xtw.WriteStartElement("div");
          xtw.WriteAttributeString("class", "pubTitle");
            xtw.WriteStartElement("a");
              xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
              xtw.WriteAttributeString("target", "_blank");
              xtw.WriteAttributeString("title", strFullTitle);
              xtw.WriteString(strTrimTitle);
            xtw.WriteEndElement();
          xtw.WriteEndElement();
        
        xtw.WriteEndElement();

			}else{
				if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
					//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
					//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));

        xtw.WriteStartElement("div");
				xtw.WriteAttributeString("class", "newsItem");

          xtw.WriteStartElement("div");
          xtw.WriteAttributeString("class", "pubDate");
            //xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));						
            //xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));		
            xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat));		
          xtw.WriteEndElement();
          
          xtw.WriteStartElement("div");
          xtw.WriteAttributeString("class", "pubTitle");
            xtw.WriteStartElement("a");
              xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
              xtw.WriteAttributeString("target", "_blank");
              xtw.WriteAttributeString("title", strFullTitle);
              xtw.WriteString(strTrimTitle);
            xtw.WriteEndElement();
          xtw.WriteEndElement();
        
        xtw.WriteEndElement();
				}
			}
			
			while (sqlReader.Read()){

        strFullTitle = sqlReader.GetString(2);
        strTrimTitle = TruncateAtWord(sqlReader.GetString(2), 80);

				if(Convert.ToBoolean(sqlReader["Never_Expire"])){
					if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
						//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
						newMonth = false;
					}else{
						//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = true;
					}
					
					if(newMonth){
						//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
						//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
						firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = false;
					}

          xtw.WriteStartElement("div");
          xtw.WriteAttributeString("class", "newsItem");

            xtw.WriteStartElement("div");
            xtw.WriteAttributeString("class", "pubDate");
              //xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));						
              //xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));		
              xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat));		
            xtw.WriteEndElement();
            
            xtw.WriteStartElement("div");
            xtw.WriteAttributeString("class", "pubTitle");
              xtw.WriteStartElement("a");
                xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
                xtw.WriteAttributeString("target", "_blank");
                xtw.WriteAttributeString("title", strFullTitle);
                xtw.WriteString(strTrimTitle);
              xtw.WriteEndElement();
            xtw.WriteEndElement();
          
          xtw.WriteEndElement();
					
					rCount++;
				}else{
					if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
						if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
							//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
							newMonth = false;
						}else{
							//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = true;
						}
						
						if(newMonth){
							//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
							//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("yyyy年 MM月"));
							firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = false;
						}

            xtw.WriteStartElement("div");
            xtw.WriteAttributeString("class", "newsItem");

              xtw.WriteStartElement("div");
              xtw.WriteAttributeString("class", "pubDate");
                //xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));						
                //xtw.WriteString(sqlReader.GetDateTime(4).ToString("MM月 dd日"));		
                xtw.WriteString(sqlReader.GetDateTime(4).ToString(langDateFormat));		
              xtw.WriteEndElement();
              
              xtw.WriteStartElement("div");
              xtw.WriteAttributeString("class", "pubTitle");
                xtw.WriteStartElement("a");
                  xtw.WriteAttributeString("href", newsURL + "?id=" + sqlReader.GetString(3));
                  xtw.WriteAttributeString("target", "_blank");
                  xtw.WriteAttributeString("title", strFullTitle);
                  xtw.WriteString(strTrimTitle);
                xtw.WriteEndElement();
              xtw.WriteEndElement();
            
            xtw.WriteEndElement();

						
						rCount++;
					}
				}
				
			}	



    }else{
			xtw.WriteStartElement("div");
				xtw.WriteAttributeString("style", "text-align:center; padding:32px;");
				xtw.WriteString("No News Release Found!");			
			xtw.WriteEndElement();
		}
        
    sqlReader.Close();
    cnDB4.Close();


    //xtw.WriteEndElement();
    //xtw.WriteEndDocument(); //uncomment for closing XML Declaration


        

    xtw.Flush();
    xtw.Close();
    HttpContext.Current.Response.End();


    XmlDocument xmlDocument = new XmlDocument();
    xmlDocument.LoadXml(stringWriter.ToString());
		//XmlDocumentType XDType = xmlDocument.DocumentType;
		//xmlDocument.RemoveChild(XDType);
		/*
		if (xmlDocument.FirstChild.NodeType == XmlNodeType.XmlDeclaration) {
			xmlDocument.RemoveChild(xmlDocument.FirstChild);
		}
		*/

    return xmlDocument;


    }

	/*End getLatestNewsHomepage_Luna */


	/* Added by JM 03-06-2018 */
	
	[WebMethod]
	 public XmlDocument getLatestNewsHTML2(string divList, string rowCount, string year)
    {
		int currYear = (int)DateTime.Now.Year;
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        //string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        //xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("div");
        xtw.WriteAttributeString("class", "newslist");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date], [Never_Expire] FROM NewsManagerArticles WHERE Status=1 ";
		
		if (year=="" || year==null) {
			year = currYear.ToString();	
		}
		sqlQuery += " AND DATEPART(YEAR, Publish_Date) = '" + year + "'";
		
        
		if (divList!=""){
            string[] arrDiv = divList.Split(',');
            int tCount = arrDiv.Length;
            string logicOp = "AND";
            for (int x = 0; x < tCount; x++)
            {
                if (x != 0) logicOp = "OR";
                sqlQuery += logicOp + " CategoryID Like '%%" + arrDiv[x] + "%%' ";
            }
        }

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";
        

        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		
        if (sqlReader.HasRows)
        {
			//xtw.WriteStartElement("div");
			/*-----------------------------------------*/	
			rCount++;
			sqlReader.Read();
			firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
			if(Convert.ToBoolean(sqlReader["Never_Expire"])){
				//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
				
				xtw.WriteStartElement("div");
					xtw.WriteAttributeString("class", "newsItem");
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsTitle");
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("class", "newsLink");
							xtw.WriteAttributeString("href", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
							xtw.WriteAttributeString("target", "_blank");
							xtw.WriteString(sqlReader.GetString(2));
						xtw.WriteEndElement();
					xtw.WriteEndElement();
					
				xtw.WriteEndElement();
			}else{
				if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
					//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
					
					xtw.WriteStartElement("div");
					xtw.WriteAttributeString("class", "newsItem");
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsDate");
						xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsTitle");
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("class", "newsLink");
							xtw.WriteAttributeString("href", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
							xtw.WriteAttributeString("target", "_blank");
							xtw.WriteString(sqlReader.GetString(2));
						xtw.WriteEndElement();
					xtw.WriteEndElement();
					
				xtw.WriteEndElement();
				}
			}
			
			while (sqlReader.Read()){
				if(Convert.ToBoolean(sqlReader["Never_Expire"])){
					if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
						//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
						newMonth = false;
					}else{
						//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = true;
					}
					
					if(newMonth){
						//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
						firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
						newMonth = false;
					}
					
					xtw.WriteStartElement("div");
						xtw.WriteAttributeString("class", "newsItem");
						
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "newsDate");
							xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
						xtw.WriteEndElement();
						
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "newsTitle");
							xtw.WriteStartElement("a");
								xtw.WriteAttributeString("class", "newsLink");
								xtw.WriteAttributeString("href", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
								xtw.WriteAttributeString("target", "_blank");
								xtw.WriteString(sqlReader.GetString(2));
							xtw.WriteEndElement();
						xtw.WriteEndElement();
						
					xtw.WriteEndElement();
					
					rCount++;
				}else{
					if (Convert.ToDateTime(sqlReader["Expire_Date"]) > System.DateTime.Now){
						if(firstDate == Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy")){
							//output += "<h2>" + Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy") +"</h2>";
							newMonth = false;
						}else{
							//firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = true;
						}
						
						if(newMonth){
							//xtw.WriteElementString("h2", Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy"));
							firstDate = Convert.ToDateTime(sqlReader["Publish_Date"]).ToString("MMMM yyyy");
							newMonth = false;
						}
						
						
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "newsItem");
							
							xtw.WriteStartElement("div");
								xtw.WriteAttributeString("class", "newsDate");
								xtw.WriteString(sqlReader.GetDateTime(4).ToString("D"));					
							xtw.WriteEndElement();
							
							xtw.WriteStartElement("div");
								xtw.WriteAttributeString("class", "newsTitle");
								xtw.WriteStartElement("a");
									xtw.WriteAttributeString("class", "newsLink");
									xtw.WriteAttributeString("href", "https://www.trimble.com/news/release.aspx?id=" + sqlReader.GetString(3));
									xtw.WriteAttributeString("target", "_blank");
									xtw.WriteString(sqlReader.GetString(2));
								xtw.WriteEndElement();
							xtw.WriteEndElement();
							
						xtw.WriteEndElement();
						
						rCount++;
					}
				}
				
			}			
			//xtw.WriteEndElement(); //end xtw.WriteStartElement("div")
        }else{
			xtw.WriteStartElement("div");
				xtw.WriteAttributeString("style", "text-align:center; padding:32px;");
				xtw.WriteString("No News Release Found!");					
			xtw.WriteEndElement();
		}
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        //xtw.WriteEndDocument(); //uncomment for closing XML Declaration


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		//XmlDocumentType XDType = xmlDocument.DocumentType;
		//xmlDocument.RemoveChild(XDType);
		/*
		if (xmlDocument.FirstChild.NodeType == XmlNodeType.XmlDeclaration) {
			xmlDocument.RemoveChild(xmlDocument.FirstChild);
		}
		*/

        return xmlDocument;


    }
	/*End*/

	
	[WebMethod]
	public XmlDocument LoadDivisionDropDown()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
		
		string urlParam = "00";
		if(HttpContext.Current.Request.QueryString["div_id"] != null){				
			urlParam = HttpContext.Current.Request.QueryString["div_id"];
		}
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpIndustry");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateNews()");
		
        string output = "";
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT * FROM NewsManagerNewsCategories Order By CategoryID";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
			sqlReader.Read();
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
				if(urlParam==sqlReader["CategoryID"].ToString()){
					xtw.WriteAttributeString("selected", "selected");
				}
				xtw.WriteString(sqlReader["CategoryName"].ToString());					
			xtw.WriteEndElement();
			
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
					if(urlParam==sqlReader["CategoryID"].ToString()){
						xtw.WriteAttributeString("selected", "selected");
					}
					xtw.WriteString(sqlReader["CategoryName"].ToString());					
				xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "00");
			//xtw.WriteAttributeString("selected", "selected");
			if(urlParam=="00"){
				xtw.WriteAttributeString("selected", "selected");
			}
			xtw.WriteString("[View All Industry]");					
		xtw.WriteEndElement();
		
		

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	[WebMethod]
	public XmlDocument LoadYearDropDown()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpYear");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateNews()");
		
        int sYear = 1999;
		int currYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
		
		xtw.WriteStartElement("option");		
			xtw.WriteAttributeString("value", currYear.ToString());
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString(currYear.ToString());					
		xtw.WriteEndElement();
		
		for(int i = (currYear-1); sYear<=i; i--){
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", i.ToString());
				xtw.WriteString(i.ToString());					
			xtw.WriteEndElement();
		}
		

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	
	public static string TruncateAtWord(string text, int maxCharacters, string trailingStringIfTextCut = "…")
  {
    if (text == null || (text = text.Trim()).Length <= maxCharacters) 
      return text;

    int trailLength = trailingStringIfTextCut.StartsWith("&") ? 1 
                                                              : trailingStringIfTextCut.Length; 
    maxCharacters = maxCharacters - trailLength >= 0 ? maxCharacters - trailLength 
                                                     : 0;
    int pos = text.LastIndexOf(" ", maxCharacters);
    if (pos >= 0)
        return text.Substring(0, pos) + trailingStringIfTextCut;

    return string.Empty;
  }
	

  private string getDivisionName(string divID)
  {
      string output = "";
      SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
      if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

      string sqlQuery = "SELECT * FROM NewsManagerNewsCategories WHERE CategoryID=" + divID;
      SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
      SqlDataReader sqlReader = sqlCommand.ExecuteReader();
      if (sqlReader.HasRows)
      {
          while (sqlReader.Read())
          {
              output = sqlReader["CategoryName"].ToString();
          }
      }
      sqlReader.Close();
      cnDB4.Close();
      return output;
  }
	
	
	//CN Translation
	[WebMethod]
	public XmlDocument LoadYearDropDown_CN()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpYear");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateNews_CN()");
		
        int sYear = 2009;
		int currYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
		
		xtw.WriteStartElement("option");		
			xtw.WriteAttributeString("value", currYear.ToString());
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString(currYear.ToString());					
		xtw.WriteEndElement();
		
		for(int i = (currYear-1); sYear<=i; i--){
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", i.ToString());
				xtw.WriteString(i.ToString());					
			xtw.WriteEndElement();
		}
		

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	[WebMethod]
	public XmlDocument LoadDivisionDropDown_CN()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpIndustry");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateNews_CN()");
		
        string output = "";
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT * FROM NewsManagerNewsCategories Order By CategoryID";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
			sqlReader.Read();
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
				xtw.WriteString(sqlReader["CategoryName_CN"].ToString());					
			xtw.WriteEndElement();
			
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
					xtw.WriteString(sqlReader["CategoryName_CN"].ToString());					
				xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "00");
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString("查看所有行业");					
		xtw.WriteEndElement();

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	[WebMethod]
    public XmlDocument getLatestNews_CN(string divList, string rowCount)
    {

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/xml";

        
        StringWriter stringWriter = new StringWriter();
        //XmlTextWriter xtw = new XmlTextWriter(stringWriter);
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;

        

        xtw.WriteStartDocument();
        string processtext = "type=\"text/xsl\" href=\"/webservices/NewsReleaseService.xslt\"";
        xtw.WriteProcessingInstruction("xml-stylesheet", processtext); 
        xtw.WriteStartElement("rss");
        xtw.WriteAttributeString("version", "2.0");
        xtw.WriteStartElement("channel");
        xtw.WriteElementString("title", "天宝新闻与事件");
        xtw.WriteElementString("link",  "https://www.trimble.com/news");
        xtw.WriteElementString("description", "Trimble 天宝新闻与事件");

        DateTime thisTime = DateTime.Now;
		TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
		bool isDaylight = tst.IsDaylightSavingTime(thisTime);
		string EST = isDaylight==true? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)).ToString() : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)).ToString();
		
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT ";
        if (rowCount != "") sqlQuery += "TOP " + rowCount + " ";
        //else sqlQuery += " * ";
        sqlQuery += "[Status], [CategoryID], [Title], [FileName], [Publish_Date] FROM NewsManagerArticles_CN WHERE Status=1 ";

        if (divList!=""){
            string[] arrDiv = divList.Split(',');
            int tCount = arrDiv.Length;
            string logicOp = "AND";
            for (int x = 0; x < tCount; x++)
            {
                if (x != 0) logicOp = "OR";
                sqlQuery += logicOp + " CategoryID Like '%%" + arrDiv[x] + "%%' ";
            }
        }

        sqlQuery += "AND Publish_Date <='" + EST + "' ";
        sqlQuery += "ORDER BY Publish_Date DESC";
        

        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("item");
                xtw.WriteElementString("title", sqlReader.GetString(2));
                //xtw.WriteElementString("description", sqlReader.GetString(2));
                xtw.WriteElementString("description", "");
                xtw.WriteElementString("link", "https://www.trimble.com/news/CN/release.aspx?id=" + sqlReader.GetString(3));
                xtw.WriteElementString("pubDate", sqlReader.GetDateTime(4).ToString("R"));
                xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();


        xtw.WriteEndElement();
        xtw.WriteEndElement();
        xtw.WriteEndDocument();


        

        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();


        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
        return xmlDocument;


    }
	
}




