﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


/// <summary>
/// Summary description for Polls
/// </summary>
public class Polls
{
    protected SqlConnection cn;
    public Button SubmitButton;
    public bool MultiSelect;
    public Label QuestionControl;
    public Label ResultControl;
    public CheckBoxList MultiSelectControl;
    public RadioButtonList SingleSelectControl;
    protected RadioButtonList rbl;
    protected string activePollID;

	public Polls()
	{
		//
		// TODO: Add constructor logic here
		//
        
	}

    public void OpenConnection(){
        cn = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cn.State != ConnectionState.Open) cn.Open();
    }

    public void CloseConnection()
    {
        cn.Close();
    }

    public void Load(string pollID)
    {
        string sql;
        SqlDataReader myDataReader;
        SqlCommand cmd;

        //get Active poll or pull Poll based on pollID param
        if (pollID == "0") sql = "SELECT * FROM Polls WHERE status='1'";     
        else sql = "SELECT * FROM Polls WHERE PollID='" + pollID + "'";       
        
        OpenConnection();
        cmd = new SqlCommand(sql, cn);
        myDataReader = cmd.ExecuteReader();
        if (myDataReader.Read())
        {
            activePollID = myDataReader["pollID"].ToString();
            QuestionControl.Text = myDataReader["Question"].ToString();
        }
        myDataReader.Close();


        //get Poll Options 
        sql = "SELECT * FROM Poll_Options WHERE PollID='" + activePollID + "' Order By SortOrder ASC";
        
        cmd = new SqlCommand(sql, cn);
        myDataReader = cmd.ExecuteReader();

        if (MultiSelect)
        {
            MultiSelectControl.DataSource = myDataReader;//cmd.ExecuteReader();
            MultiSelectControl.DataTextField = "OptionValue";
            MultiSelectControl.DataValueField = "PollOptionID";
            MultiSelectControl.DataBind();
            MultiSelectControl.Visible = true;
        }
        else
        {
            SingleSelectControl.DataSource = myDataReader;//cmd.ExecuteReader();
            SingleSelectControl.DataTextField = "OptionValue";
            SingleSelectControl.DataValueField = "PollOptionID";
            SingleSelectControl.DataBind();
            SingleSelectControl.Visible = true;
        }
        
        myDataReader.Close();
        CloseConnection();

        
        SubmitButton.Visible = true;
    }

    public bool vote(string optionID){
        bool output = false;
        string sql = "";
        SqlCommand cmd;
        
        string[] pids = optionID.Split(',');
        
        for (int i=0; i<pids.Length; i++){
            sql += "UPDATE Poll_Options SET Votes = (Votes + 1) WHERE PollOptionID='" + pids[i] + "';";
        }

        //sql = "UPDATE TST_Poll_Options SET Votes = (Votes + 1) WHERE PollOptionID='" + optionID + "'";
        OpenConnection();
        try
        {
            cmd = new SqlCommand(sql, cn);
            cmd.ExecuteNonQuery();
            SubmitButton.Visible = false;
            output = true;
        }
        catch
        {

        }
        CloseConnection();
        return output;
    }

    public void Results(string pollID)
    {
        string sql;
        SqlDataReader myDataReader;
        SqlCommand cmd;

        
        
        OpenConnection();
        //get Active poll or pull Poll based on pollID param
        if (pollID == "0") sql = "SELECT * FROM Polls WHERE status='1'";
        else sql = "SELECT * FROM Polls WHERE PollID='" + pollID + "'";  

        cmd = new SqlCommand(sql, cn);
        myDataReader = cmd.ExecuteReader();
        if (myDataReader.Read())
        {
            activePollID = myDataReader["pollID"].ToString();
            QuestionControl.Text = myDataReader["Question"].ToString();
        }
        myDataReader.Close();

        //get Total votes
        sql = "SELECT SUM(Votes) AS Total FROM Poll_Options WHERE PollID='" + activePollID + "'";
        cmd = new SqlCommand(sql, cn);
        myDataReader = cmd.ExecuteReader();
        int totalVotes = 0;
        if (myDataReader.Read()) totalVotes = Convert.ToInt32(myDataReader["Total"].ToString());
        myDataReader.Close();

        //display result with style
        sql = "SELECT * FROM Poll_Options WHERE PollID='" + activePollID + "'";
        cmd = new SqlCommand(sql, cn);
        myDataReader = cmd.ExecuteReader();
        string output = "";
        if (myDataReader.HasRows)
        {
            while (myDataReader.Read())
            {
                output += myDataReader["OptionValue"].ToString() + "<br/>";
                output += "<div style='width:" + ((Convert.ToDouble(myDataReader["Votes"]) / totalVotes) * 100) + "%; display:block; background:#2F83A8; height:6px; border:1px solid #000; font-size:1px;'></div>";
                //output += "<img src='/graphics/bg_grad_white_left.png' style='position:relative; left:0; top:0; height:100%; width:100%; z-index:0;'/>";
                output += "<div style='width:" + ((Convert.ToDouble(myDataReader["Votes"]) / totalVotes) * 100) + "%; display:block;height:9px; margin-bottom:5px;'>";
                if (((Convert.ToDouble(myDataReader["Votes"]) / totalVotes) * 100) < 13) output += "<span style='float:left;font:bold 9px arial;display:block;'>" + Convert.ToInt32(((Convert.ToDouble(myDataReader["Votes"]) / totalVotes) * 100)) + "%</span></div>";
                else output += "<span style='float:right;font:bold 9px arial;display:block;'>" + Convert.ToInt32(((Convert.ToDouble(myDataReader["Votes"]) / totalVotes) * 100)) + "%</span></div>";
            }
        }
        
        myDataReader.Close();
        CloseConnection();
        
        //ResultControl.Text = "<br/>" + output + "<br/>";
        ResultControl.Text = output + "<span style='font-size:1px;'>&nbsp;</span>";
        ResultControl.Visible = true;

        if (MultiSelect) MultiSelectControl.Visible = false;
        else SingleSelectControl.Visible = false;
        
    }
}
