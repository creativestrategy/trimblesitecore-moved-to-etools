using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace wrapp
{
	/// <summary>
	/// Summary description for SQLManager.
	/// </summary>
	public class SQLManager
	{
		protected System.Data.SqlClient.SqlConnection cn;

		public SQLManager()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public string NewID(string rid)
		{
			return rid;
		}
		
		public SqlConnection OpenConnection(string DB)
		{
			cn = new SqlConnection(ConfigurationSettings.AppSettings[DB]);
			if (cn.State!=ConnectionState.Open)
			{
				cn.Open();
			}
			return cn;
		}
		
		
		public void CloseConnection()
		{
			if (cn.State==ConnectionState.Open)
			{
				cn.Close();
			}			
		}

		
		public void DDLoader(DropDownList ddl, string tblName, string ddText, string ddValue, string defValue, string defMessage)
		{
			string sql = "SELECT * FROM " + tblName + " Order By " +ddText + " ASC";
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
			ddl.DataSource = myDataReader;//cmd.ExecuteReader();
		  
			ddl.DataTextField = ddText;
			ddl.DataValueField = ddValue;
			ddl.DataBind();
			if(defValue!="") ddl.SelectedValue = defValue;
			else
			{
				ddl.Items.Add(new ListItem(defMessage, ""));
				ddl.SelectedValue = "";
			}
			myDataReader.Close();
		}

		
		public string getCC(string div_ID)
		{
			string val = "";
			string sql = "SELECT CC_Email FROM WR_Divisions WHERE Division_ID='" + div_ID + "'";
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
			if (myDataReader.Read())val=myDataReader["CC_Email"].ToString();
			return val;
		}


		public void InsertNewRequest(string Requestor_ID, string Requestor_Fullname, string Requestor_Phone, string Viewers ,string Division, string SendCC, string Summary, string Priority, bool IsForm, bool IsDealerLocator, string URLs, string File_Upload, bool IsReview , string Date_Review, string Date_Release)
		{
			int Status = 1;
			string PrivacyForm_ID = "";
			string Developer_ID = "";
			string Date_Claimed = "";
			string Date_Modified = "";


			string sql = "INSERT INTO WR_Requests ([Status], [Requestor_ID], [Requestor_Fullname], [Requestor_Phone], [Viewers], [Division], [SendCC], ";
			 sql = sql + "[Summary], [Priority], [IsForm], [PrivacyForm_ID], [IsDealerLocator], [URLs], [File_Upload], ";
			 sql = sql + "[IsReview], [Date_Review], [Date_Release], [Date_Submitted], ";
			 sql = sql + "[Developer_ID], [Date_Claimed], [Date_Modified]) ";

			 sql = sql + "VALUES(@Status, @Requestor_ID, @Requestor_Fullname, @Requestor_Phone, @Viewers, @Division, @SendCC, ";
			 sql = sql + "@Summary, @Priority, @IsForm, @PrivacyForm_ID, @IsDealerLocator, @URLs, @File_Upload, ";
			 sql = sql + "@IsReview, @Date_Review, @Date_Release, @Date_Submitted, ";
			 sql = sql + "@Developer_ID, @Date_Claimed, @Date_Modified) ";

			//connection string
			SqlCommand sqlCmd = new SqlCommand(sql, cn);
			
			//create parameters
			sqlCmd.Parameters.Add(new SqlParameter("@Status", Status));
			sqlCmd.Parameters.Add(new SqlParameter("@Requestor_ID", Requestor_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@Requestor_Fullname", Requestor_Fullname));
			sqlCmd.Parameters.Add(new SqlParameter("@Requestor_Phone", Requestor_Phone));
			sqlCmd.Parameters.Add(new SqlParameter("@Viewers", Viewers));
			sqlCmd.Parameters.Add(new SqlParameter("@Division", Division));
			sqlCmd.Parameters.Add(new SqlParameter("@SendCC", SendCC));

			sqlCmd.Parameters.Add(new SqlParameter("@Summary", Summary));
			sqlCmd.Parameters.Add(new SqlParameter("@Priority", Priority));
			sqlCmd.Parameters.Add(new SqlParameter("@IsForm", IsForm));
			sqlCmd.Parameters.Add(new SqlParameter("@PrivacyForm_ID", PrivacyForm_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@IsDealerLocator", IsDealerLocator));
			sqlCmd.Parameters.Add(new SqlParameter("@URLs", URLs));
			sqlCmd.Parameters.Add(new SqlParameter("@File_Upload", File_Upload));

			sqlCmd.Parameters.Add(new SqlParameter("@IsReview", IsReview));
			if(IsReview)sqlCmd.Parameters.Add(new SqlParameter("@Date_Review", Date_Review));
			else sqlCmd.Parameters.Add(new SqlParameter("@Date_Review", Date_Release));

			sqlCmd.Parameters.Add(new SqlParameter("@Date_Release", Date_Release));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Submitted", System.DateTime.Now.ToString()));

			sqlCmd.Parameters.Add(new SqlParameter("@Developer_ID", Developer_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Claimed", Date_Claimed));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Modified", Date_Modified));

			//sqlCmd.Connection.Open();
			sqlCmd.ExecuteNonQuery();
			//sqlCmd.Connection.Close();


			//getting the Identity
			SqlCommand sqlCmdIdentity = new SqlCommand("SELECT @@IDENTITY", cn);
			HttpContext.Current.Session["newRID"] = sqlCmdIdentity.ExecuteScalar().ToString();
		}

		
		public void InsertReply(string Request_ID, string Reply_Message, string ReplyBy_Fullname, string ReplyBy_EmailID)
		{
			string sql="";
		
			sql = "INSERT INTO WR_Replies ([Request_ID], [Reply_Message], " +
				"[ReplyBy_Fullname], [ReplyBy_EmailID], [Date_Submitted]) " +
				"VALUES(@Request_ID, @Reply_Message, " +
				"@ReplyBy_Fullname, @ReplyBy_EmailID, @Date_Submitted)";
		    
		
			SqlCommand sqlCmd = new SqlCommand(sql, cn);
		
			sqlCmd.Parameters.Add(new SqlParameter("@Request_ID", Request_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@Reply_Message", Reply_Message));
			sqlCmd.Parameters.Add(new SqlParameter("@ReplyBy_Fullname", ReplyBy_Fullname));
			sqlCmd.Parameters.Add(new SqlParameter("@ReplyBy_EmailID", ReplyBy_EmailID));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Submitted", System.DateTime.Now.ToString()));

			//sqlCmd.Connection.Open();
			sqlCmd.ExecuteNonQuery();
			//sqlCmd.Connection.Close();
		}

		//Get WEB REQUESTS and populate datagrid in wrlist.aspx 
		public void LoadWRRequests(DataGrid dg, string email_ID, string filter, string sortby){
			string sql = "";
			string strAdminUsers = ConfigurationSettings.AppSettings["WebAdmin"];
			
			/*
			sql = "SELECT * FROM WR_Requests WHERE Request_ID<>'' ";
			
			if(filter!="") sql += filter;

			if(strAdminUsers.IndexOf(email_ID) == -1)
			{
				//comment out if you want to view all requests, disregarding admin and user accounts
				sql += " AND Viewers like '%%" + email_ID + "%%' ";
				//////
			}

			if(sortby!="")sql += "ORDER BY " + sortby + " ASC";
			else sql += "ORDER BY Date_Release ASC";
			*/

			sql = "select * from wr_requests " +
				  " Left join " +
				  "(" +
					"select request_id, count(request_id) as total, max(Date_submitted) as LastDate " +
					", DateDiff(minute, Max(Date_submitted), GetDate()) as minDiff " + 					
					"from wr_replies " +
					"group by request_id" +
				  ") " +
				  "wr_replies " +
				  "on wr_requests.request_id = wr_replies.request_id WHERE wr_requests.Request_ID<>'' ";
			
			if(filter!="") sql += filter;

			if(strAdminUsers.IndexOf(email_ID) == -1)
			{
				//comment out if you want to view all requests, disregarding admin and user accounts
				sql += " AND wr_requests.Viewers like '%%" + email_ID + "%%' ";
				//////
			}

			if(sortby!="")sql += "ORDER BY " + sortby + " ASC";
			else sql += "ORDER BY wr_requests.Date_Submitted ASC";
			
			
			//HttpContext.Current.Response.Write(sql);

			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter();

			da.SelectCommand = new SqlCommand(sql, cn);
			da.Fill(ds, "Requests");
			dg.DataSource = ds.Tables["Requests"].DefaultView;
			dg.DataBind();

			SessionManager pSession = new SessionManager();
			//string strAdmin = ConfigurationSettings.AppSettings["WebAdmin"];
			//if(strAdmin.IndexOf(pSession.getValue("Email_ID")) == -1)
			if(pSession.isAdmin(pSession.getValue("Email_ID")))
			{
				dg.Columns[0].Visible = true;
			}
			else
			{
				dg.Columns[0].Visible = false;
			}
		}
		
		
		
		
		//*************************************************************************
		// Function:	LoadFilteredRequests
		// Purpose:		Pull out Requests 
		// Args: 		dg = DataGrid to populate
		//				status = type of status to show
		//				email_ID = email id of current user
		//				maxCount = no. of items to show
		// Returns:		none, but will populate a datagrid control specifird in dg.
		//*************************************************************************		
		public void LoadFilteredRequests(DataList objX, string rTable, string status, string email_ID, int maxCount)
		{	
			/*
			if(objType=="DG")
			{
				DataGrid objX = new DataGrid();
				objX = ((DataGrid)obj);
			}
			else if(objType=="DL")
			{
				DataList objX = new DataList();
				objX = ((DataList)obj);
			}
			*/
				
			string strAdmin = ConfigurationSettings.AppSettings["WebAdmin"];

			string sql = "";
			
			if (maxCount==0)
			{
				sql += "SELECT * FROM WR_Requests " ; 				
			}
			else
			{
				sql += "SELECT TOP " + maxCount + " * FROM " + rTable ; 
			}

			sql += " WHERE Request_ID <> '' "; 

			if(status!="") sql += "AND Status='" + status + "'";
			
			if(strAdmin.IndexOf(email_ID) == -1)
			{
				//comment out if you want to view all requests, disregarding admin and user accounts
				sql += "AND Viewers like '%%" + reformatString(email_ID) + "%%' ";
				//////
			}

			sql += "ORDER BY Date_Submitted DESC";

			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter();

			da.SelectCommand = new SqlCommand(sql, cn);
			da.Fill(ds, "WRequests");
			objX.DataSource = ds.Tables["WRequests"].DefaultView;
			objX.DataBind();
			//HttpContext.Current.Response.Write(sql);
			SessionManager pSession = new SessionManager();
			/*
			if(pSession.isAdmin(pSession.getValue("Email_ID")))
			{
				dg.Columns[0].Visible = true;
			}
			else
			{
				dg.Columns[0].Visible = false;
			}
			*/
		}

		
		public void Claim(string reqType, string devID, string reqIDs)
		{
			string[] reqID = reqIDs.Split(',');

			for(int i = 0; i<= reqID.Length - 1; i++){
				string uSQL = "UPDATE " + reqType + "_Requests SET Developer_ID='" + devID + "', Date_Claimed='" + DateTime.Now.ToString() + "', Status='2' WHERE Request_ID=" + reqID[i];
			
				SqlCommand myCommand;
				myCommand = new SqlCommand(uSQL,cn);
				myCommand.ExecuteNonQuery();
				//HttpContext.Current.Response.Write(uSQL);
		   }
			
		}

		
		public void UpdateStatus(string reqType, string Request_ID, string status, string devID)
		{

			string[] reqID = Request_ID.Split(',');
			SqlCommand myCommand;

			for(int i = 0; i<= reqID.Length - 1; i++){
				string uSQL = "UPDATE " + reqType + "_Requests SET ";
				
				if(devID!="") uSQL += "Developer_ID='" + devID + "', ";
				
				uSQL += "Date_Modified='" + DateTime.Now.ToString() + "', Status='" + status + "' WHERE Request_ID=" + reqID[i];
				myCommand = new SqlCommand(uSQL,cn);
				myCommand.ExecuteNonQuery();
			 }
		}

		
		public bool ValidateFirstTimeUse_WR(string ID)
		{

			bool val = true;
			string sql = "SELECT * FROM WR_Requestors WHERE Email_ID='" + reformatString(ID) + "'";
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
			if (myDataReader.Read()) val=false;
			myDataReader.Close();
			return val;
		}

		public string RetrievePassword(string email)
		{
			string val = "";
			string sql = "SELECT * FROM Trimble_WebApp_Accounts WHERE Email='" + reformatString(email) + "'";
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
			if(myDataReader.Read()) val = myDataReader["UserPass"].ToString();;
			myDataReader.Close();
			return val;
		}
		
		public void InsertRequestor(string Email_ID, int WR_Requests, int PR_Requests)
		{
			string sql = "INSERT INTO WR_Requestors_Stat ([Email_ID], [WR_Requests], [PR_Requests], ";
			sql = sql + "[Date_Created], [Date_Modified]) ";

			sql = sql + "VALUES(@Email_ID, @WR_Requests, @PR_Requests, ";
			sql = sql + "@Date_Created, @Date_Modified) ";

			//connection string
			SqlCommand sqlCmd = new SqlCommand(sql, cn);
			
			//create parameters
			sqlCmd.Parameters.Add(new SqlParameter("@Email_ID", Email_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@WR_Requests", WR_Requests));
			sqlCmd.Parameters.Add(new SqlParameter("@PR_Requests", PR_Requests));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Created", System.DateTime.Now.ToString()));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Modified", System.DateTime.Now.ToString()));
			
			sqlCmd.ExecuteNonQuery();
		}


		public bool ValidateFirstTimeUse(string emailID)
		{
			bool val = true;
			string sql = "SELECT * FROM Trimble_WebApp_Accounts WHERE Email='" + reformatString(emailID) + "'";
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
			if (myDataReader.Read())
			{
				HttpContext.Current.Session["Date_Registered"] = Convert.ToDateTime(myDataReader["DateCreated"]).ToString("MMMM dd, yyyy");
				val=false;
			}
			myDataReader.Close();
			cn.Close();
			return val;
		}

		
		public void InsertAccount(string email, string username, string password, string fname, string lname, string division, string title, string phone, string appID)
		{
			string sql = "INSERT INTO Trimble_WebApp_Accounts ([Email], ";
			sql += "[Username], [Userpass], ";
			sql += "[FirstName], [LastName], ";
			sql += "[Department], [Title], ";
			sql += "[Phone], ";
			sql += "[AccountType], [Applications], ";
			sql += "[Status], [CreatedBy], ";
			sql += "[DateCreated], [DateModified]) ";

			sql = sql + "VALUES(@Email, ";
			sql += "@Username, @Userpass, ";
			sql += "@FirstName, @LastName, ";
			sql += "@Department, @Title, ";
			sql += "@Phone, ";
			sql += "@AccountType, @Applications, ";
			sql += "@Status, @CreatedBy, ";
			sql += "@DateCreated, @DateModified) ";

			//connection string
			SqlCommand sqlCmd = new SqlCommand(sql, cn);
		
			//create parameters
			sqlCmd.Parameters.Add(new SqlParameter("@Email", email));
			sqlCmd.Parameters.Add(new SqlParameter("@Username", username));
			sqlCmd.Parameters.Add(new SqlParameter("@Userpass", password));
			sqlCmd.Parameters.Add(new SqlParameter("@FirstName", fname));
			sqlCmd.Parameters.Add(new SqlParameter("@LastName", lname));
			sqlCmd.Parameters.Add(new SqlParameter("@Department", division));
			sqlCmd.Parameters.Add(new SqlParameter("@Title", title));
			sqlCmd.Parameters.Add(new SqlParameter("@Phone", phone));
			sqlCmd.Parameters.Add(new SqlParameter("@AccountType", "2"));
			sqlCmd.Parameters.Add(new SqlParameter("@Applications", appID));
			sqlCmd.Parameters.Add(new SqlParameter("@Status", "1"));
			sqlCmd.Parameters.Add(new SqlParameter("@CreatedBy", email));
			sqlCmd.Parameters.Add(new SqlParameter("@DateCreated", System.DateTime.Now.ToString()));
			sqlCmd.Parameters.Add(new SqlParameter("@DateModified", System.DateTime.Now.ToString()));
		
			sqlCmd.ExecuteNonQuery();
		}

		
		string MorphTag(string strText,  string strFind, string strReplace)
		{
			int iPos=strText.IndexOf(strFind);
			String strReturn="";
			while(iPos!=-1)
			{
				strReturn+=strText.Substring(0,iPos) + strReplace;
				strText=strText.Substring(iPos+strFind.Length);
				iPos=strText.IndexOf(strFind);
			}
			if(strText.Length>0)
				strReturn+=strText;
			return strReturn;
		}


		////////////////////////////////////////////////////////
		//  PROJECT REQUESTS
		////////////////////////////////////////////////////////


		//Get PROJECT REQUESTS and populate datagrid in prlist.aspx 
		public void LoadRequests_PR(DataGrid dg, string email_ID, string filter, string sortby)
		{
			string sql = "";
			string strAdminUsers = ConfigurationSettings.AppSettings["WebAdmin"];

			//sql = "SELECT * FROM PR_Requests WHERE Request_ID<>'' ";

			sql = "select * from PR_Requests " +
				" Left join " +
				"(" +
				"select request_id, count(request_id) as total, max(Date_submitted) as LastDate " +
				", DateDiff(minute, Max(Date_submitted), GetDate()) as minDiff " + 					
				"from pr_replies " +
				"group by request_id" +
				") " +
				"pr_replies " +
				"on PR_Requests.request_id = pr_replies.request_id WHERE pr_requests.Request_ID<>'' ";
			
			
			if(filter!="") sql += filter;

			if(strAdminUsers.IndexOf(email_ID) == -1)
			{
				//comment out if you want to view all requests, disregarding admin and user accounts
				sql += " AND Viewers like '%%" + reformatString(email_ID) + "%%' ";
				//////
			}

			if(sortby!="")sql += "ORDER BY " + sortby + " ASC";
			else sql += "ORDER BY Date_Release ASC";

			//HttpContext.Current.Response.Write(sql);

			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter();

			da.SelectCommand = new SqlCommand(sql, cn);
			da.Fill(ds, "Requests");
			dg.DataSource = ds.Tables["Requests"].DefaultView;
			dg.DataBind();

			SessionManager pSession = new SessionManager();
			//string strAdmin = ConfigurationSettings.AppSettings["WebAdmin"];
			//if(strAdmin.IndexOf(pSession.getValue("Email_ID")) == -1)
			if(pSession.isAdmin(pSession.getValue("Email_ID")))
			{
				dg.Columns[0].Visible = true;
			}
			else
			{
				dg.Columns[0].Visible = false;
			}
		}
		
		public void InsertNewRequest_PR(string Requestor_ID, string Requestor_Fullname, string Requestor_Phone, string Viewers, string Division, string SendCC, string ProjectName, string PO_Name, string PO_Email, string SOA_Name, string SOA_Email, string  Summary, string BusinessCase, string ProjectType, string Priority, bool IsForm, string URLs, string WhoAreUsers, string WhatWillSiteDo, string HowManyPages, string AdditionalDetails, string File_Upload, string  Date_Review, string Date_Release)
		{
			int Status = 1;
			string PrivacyForm_ID = "";
			string Developer_ID = "";
			string Date_Claimed = "";
			string Date_Modified = "";

			string sql = "INSERT INTO PR_Requests ([Status], [Requestor_ID], [Requestor_Fullname], [Requestor_Phone], [Viewers], [Division], [SendCC], ";
			sql = sql + "[ProjectName], [PO_Name], [PO_Email], [SOA_Name], [SOA_Email], [Summary], [BusinessCase], [ProjectType], ";
			sql = sql + "[Priority], [IsForm], [URLs], ";
			sql = sql + "[WhoAreUsers], [WhatWillSiteDo], [HowManyPages], [AdditionalDetails], [File_Upload], ";
			sql = sql + "[Date_Review], [Date_Release], [Date_Submitted], ";
			sql = sql + "[Developer_ID], [Date_Claimed], [Date_Modified]) ";

			sql = sql + "VALUES(@Status, @Requestor_ID, @Requestor_Fullname, @Requestor_Phone, @Viewers, @Division, @SendCC, ";
			sql = sql + "@ProjectName, @PO_Name, @PO_Email, @SOA_Name, @SOA_Email, @Summary, @BusinessCase, @ProjectType, ";
			sql = sql + "@Priority, @IsForm, @URLs, ";
			sql = sql + "@WhoAreUsers, @WhatWillSiteDo, @HowManyPages, @AdditionalDetails, @File_Upload, ";
			sql = sql + "@Date_Review, @Date_Release, @Date_Submitted, ";
			sql = sql + "@Developer_ID, @Date_Claimed, @Date_Modified) ";

			//connection string
			SqlCommand sqlCmd = new SqlCommand(sql, cn);
			
			//create parameters
			sqlCmd.Parameters.Add(new SqlParameter("@Status", Status));
			sqlCmd.Parameters.Add(new SqlParameter("@Requestor_ID", Requestor_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@Requestor_Fullname", Requestor_Fullname));
			sqlCmd.Parameters.Add(new SqlParameter("@Requestor_Phone", Requestor_Phone));
			sqlCmd.Parameters.Add(new SqlParameter("@Viewers", Viewers));
			sqlCmd.Parameters.Add(new SqlParameter("@Division", Division));
			sqlCmd.Parameters.Add(new SqlParameter("@SendCC", SendCC));

			sqlCmd.Parameters.Add(new SqlParameter("@ProjectName", ProjectName));
			sqlCmd.Parameters.Add(new SqlParameter("@PO_Name", PO_Name));
			sqlCmd.Parameters.Add(new SqlParameter("@PO_Email", PO_Email));
			sqlCmd.Parameters.Add(new SqlParameter("@SOA_Name", SOA_Name));
			sqlCmd.Parameters.Add(new SqlParameter("@SOA_Email", SOA_Email));
			sqlCmd.Parameters.Add(new SqlParameter("@Summary", Summary));
			sqlCmd.Parameters.Add(new SqlParameter("@BusinessCase", BusinessCase));
			sqlCmd.Parameters.Add(new SqlParameter("@ProjectType", ProjectType));

			sqlCmd.Parameters.Add(new SqlParameter("@Priority", Priority));
			sqlCmd.Parameters.Add(new SqlParameter("@IsForm", IsForm));
			sqlCmd.Parameters.Add(new SqlParameter("@PrivacyForm_ID", PrivacyForm_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@URLs", URLs));

			sqlCmd.Parameters.Add(new SqlParameter("@WhoAreUsers", WhoAreUsers));
			sqlCmd.Parameters.Add(new SqlParameter("@WhatWillSiteDo", WhatWillSiteDo));
			sqlCmd.Parameters.Add(new SqlParameter("@HowManyPages", HowManyPages));
			sqlCmd.Parameters.Add(new SqlParameter("@AdditionalDetails", AdditionalDetails));
			sqlCmd.Parameters.Add(new SqlParameter("@File_Upload", File_Upload));

			sqlCmd.Parameters.Add(new SqlParameter("@Date_Review", Date_Review));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Release", Date_Release));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Submitted", System.DateTime.Now.ToString()));

			sqlCmd.Parameters.Add(new SqlParameter("@Developer_ID", Developer_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Claimed", Date_Claimed));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Modified", Date_Modified));

			sqlCmd.ExecuteNonQuery();

			//getting the Identity
			SqlCommand sqlCmdIdentity = new SqlCommand("SELECT @@IDENTITY", cn);
			HttpContext.Current.Session["newRID"] = sqlCmdIdentity.ExecuteScalar().ToString();
		}

		public void InsertReply_PR(string Request_ID, string Reply_Message, string ReplyBy_Fullname, string ReplyBy_EmailID)
		{
			string sql="";
		
			sql = "INSERT INTO PR_Replies ([Request_ID], [Reply_Message], " +
				"[ReplyBy_Fullname], [ReplyBy_EmailID], [Date_Submitted]) " +
				"VALUES(@Request_ID, @Reply_Message, " +
				"@ReplyBy_Fullname, @ReplyBy_EmailID, @Date_Submitted)";
		    
		
			SqlCommand sqlCmd = new SqlCommand(sql, cn);
		
			sqlCmd.Parameters.Add(new SqlParameter("@Request_ID", Request_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@Reply_Message", Reply_Message));
			sqlCmd.Parameters.Add(new SqlParameter("@ReplyBy_Fullname", ReplyBy_Fullname));
			sqlCmd.Parameters.Add(new SqlParameter("@ReplyBy_EmailID", ReplyBy_EmailID));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Submitted", System.DateTime.Now.ToString()));

			//sqlCmd.Connection.Open();
			sqlCmd.ExecuteNonQuery();
			//sqlCmd.Connection.Close();
		}

		/////////////////////////////////////////////////////////////////////////
		//////////////////////////////PRIVACY FORM///////////////////////////////
		

		public void Insert_PrivacyForm(string Request_Type, string Request_ID, string Owner_Name, string Owner_Email, string Manager_Name, string Manager_Email, string Form_Purpose, string Form_Type, string Form_StdFields, string Form_AddFields, string HostedInTrimble, string HIT_Database, string HIT_Database_Type, string HIT_Database_Specific, string HIT_Email, string HIT_Email_Address, string NHIT_BusinessCase, string NHIT_Database, string NHIT_Email, string File_Upload)
		{
			//HttpContext.Current.Response.Write(Request_Type + "<br>" + Request_ID + "<br>" + Owner_Name + "<br>" + Owner_Email + "<br>" + Manager_Name + "<br>" + Manager_Email + "<br>" + Form_Purpose + "<br>" + Form_Type + "<br>" + Form_StdFields + "<br>" + Form_AddFields + "<br>" + HostedInTrimble + "<br>" + HIT_Database + "<br>" + HIT_Database_Type + "<br>" + HIT_Database_Specific + "<br>" + HIT_Email + "<br>" + HIT_Email_Address + "<br>" + NHIT_BusinessCase + "<br>" + NHIT_Database + "<br>" + NHIT_Email+ "<br>" + File_Upload);
			string sql = "";
			sql = sql + "SET NOCOUNT ON; ";
			sql = sql + "INSERT INTO ";
			sql = sql + "WR_PrivacyForm ([Request_Type], [Request_ID], [Owner_Name], [Owner_Email], [Manager_Name], [Manager_Email], [Form_Purpose], [Form_Type], [Form_StdFields], [Form_AddFields], [HostedInTrimble], [HIT_Database], [HIT_Database_Type], [HIT_Database_Specific], [HIT_Email], [HIT_Email_Address], [NHIT_BusinessCase], [NHIT_Database], [NHIT_Email], [File_Upload], [Date_Submitted]) ";
			sql = sql + "VALUES(@Request_Type, @Request_ID, @Owner_Name, @Owner_Email, @Manager_Name, @Manager_Email, @Form_Purpose, @Form_Type, @Form_StdFields, @Form_AddFields, @HostedInTrimble, @HIT_Database, @HIT_Database_Type, @HIT_Database_Specific, @HIT_Email, @HIT_Email_Address, @NHIT_BusinessCase, @NHIT_Database, @NHIT_Email, @File_Upload, @Date_Submitted) ";
			
			//connection string
			SqlCommand sqlCmd = new SqlCommand(sql, cn);
			
			//create parameters
			sqlCmd.Parameters.Add(new SqlParameter("@Request_Type", Request_Type));
			sqlCmd.Parameters.Add(new SqlParameter("@Request_ID", Request_ID));
			sqlCmd.Parameters.Add(new SqlParameter("@Owner_Name", Owner_Name));
			sqlCmd.Parameters.Add(new SqlParameter("@Owner_Email", Owner_Email));
			sqlCmd.Parameters.Add(new SqlParameter("@Manager_Name", Manager_Name));
			sqlCmd.Parameters.Add(new SqlParameter("@Manager_Email", Manager_Email));
			sqlCmd.Parameters.Add(new SqlParameter("@Form_Purpose", Form_Purpose));
			sqlCmd.Parameters.Add(new SqlParameter("@Form_Type", Form_Type));
			sqlCmd.Parameters.Add(new SqlParameter("@Form_StdFields", Form_StdFields));
			sqlCmd.Parameters.Add(new SqlParameter("@Form_AddFields", Form_AddFields));
			sqlCmd.Parameters.Add(new SqlParameter("@HostedInTrimble", HostedInTrimble));
			sqlCmd.Parameters.Add(new SqlParameter("@HIT_Database", HIT_Database));
			sqlCmd.Parameters.Add(new SqlParameter("@HIT_Database_Type", HIT_Database_Type));
			sqlCmd.Parameters.Add(new SqlParameter("@HIT_Database_Specific", HIT_Database_Specific));
			sqlCmd.Parameters.Add(new SqlParameter("@HIT_Email", HIT_Email));
			sqlCmd.Parameters.Add(new SqlParameter("@HIT_Email_Address", HIT_Email_Address));
			sqlCmd.Parameters.Add(new SqlParameter("@NHIT_BusinessCase", NHIT_BusinessCase));
			sqlCmd.Parameters.Add(new SqlParameter("@NHIT_Database", NHIT_Database));
			sqlCmd.Parameters.Add(new SqlParameter("@NHIT_Email", NHIT_Email));
			sqlCmd.Parameters.Add(new SqlParameter("@File_Upload", File_Upload));
			sqlCmd.Parameters.Add(new SqlParameter("@Date_Submitted", System.DateTime.Now.ToString()));
			
			sqlCmd.ExecuteNonQuery();
	
			//getting the Identity
			string pid = "";
            SqlCommand sqlCmdIdentity = new SqlCommand("SELECT @@IDENTITY", cn);
			pid = sqlCmdIdentity.ExecuteScalar().ToString();

			if(pid!="") InsertPFID(pid, Request_ID, Request_Type);
			//HttpContext.Current.Response.Write("<br>" + sql);

		}

		public void InsertPFID(string Privacy_ID, string rid, string rt)
		{
			string rTable = (rt=="WR")? "WR_Requests":"PR_Requests"; 

			string uSQL = "UPDATE " + rTable + " SET ";
			uSQL += "PrivacyForm_ID='" + Privacy_ID + "' WHERE Request_ID=" + rid;

			SqlCommand myCommand;
			myCommand = new SqlCommand(uSQL,cn);
			myCommand.ExecuteNonQuery();
			
		}

		public string getTopRequestor(int rowCount)
		{
			
			string output = "";
			string sql = "SELECT Top " + rowCount +"  Requestor_FullName, Requestor_ID, COUNT(Requestor_FullName) as Total " +
							  "FROM WR_Requests GROUP BY Requestor_FullName, Requestor_ID ORDER BY total DESC";
				
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
		
			if(myDataReader.HasRows)
			{
				output = "<span style='padding-left:10px; font-weight:bold;'>Top Requestors </span>";
				output += "<ol class='TopList'>";
				while (myDataReader.Read())
				{
					output += "<li>" + myDataReader["Requestor_FullName"].ToString() + " (" + myDataReader["Total"].ToString() + ")";
					
				}
			
				output += "</ol>";
			}
			myDataReader.Close();
			return output;
		}

		public string getTopDivisions(int rowCount)
		{
			
			string output = "";
			string sql = "SELECT Top " + rowCount + " WR_Divisions.Division_Name as DivName, COUNT(WR_Requests.Division) as Total FROM WR_Requests " +
						 "INNER JOIN WR_Divisions ON WR_Requests.Division = WR_Divisions.Division_ID " +
						 "GROUP BY Division_Name " +
						 "ORDER BY total DESC";
				
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
		
			if(myDataReader.HasRows)
			{
				output = "<span style='padding-left:10px; font-weight:bold;'>Top Divisions</span>";
				output += "<ol class='TopList'>";
				while (myDataReader.Read())
				{
					output += "<li>" + myDataReader["DivName"].ToString() + " (" + myDataReader["Total"].ToString() + ")";
					
				}
			
				output += "</ol>";
			}
			myDataReader.Close();
			return output;
		}
		
		public string getReplyDetails(string rid, string rt)
		{
			
			string output = "";
			string sql = "select top 1 replyby_fullname, request_id  from " + rt + "_replies " +
				"WHERE Request_ID='" + rid + "' " +
				"ORDER BY Date_Submitted DESC";
				
			SqlDataReader myDataReader;
			SqlCommand cmd = new SqlCommand(sql,cn);
			myDataReader = cmd.ExecuteReader();
		
			if(myDataReader.HasRows)
			{

				while (myDataReader.Read())
				{
					output += myDataReader["ReplyBy_Fullname"].ToString();
					
				}
			
			}
			myDataReader.Close();
			return output;
		}

		string reformatString(string str)
		{
			string val = "";

			val = str.Replace("'", "''");
			return val;
		}
		
		public string getAppURL()	{
			string baseURL = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
			
			string appID = "TEM";
			if(HttpContext.Current.Request.QueryString["appID"] != null && HttpContext.Current.Request.QueryString["appID"] != null){
				appID = HttpContext.Current.Request.QueryString["appID"];
			}
				
				
			string AppName = "";
			string AppURL = "";
			string SqlQuery = "SELECT * FROM Trimble_Applications WHERE " +
							"Application_CODE='" + appID + "'";
	
			SqlCommand myCommand;
			SqlDataReader myDataReader;
			myCommand = new SqlCommand(SqlQuery,cn);
			
			if (cn.State!=ConnectionState.Open){
				cn.Open();
			}
			myDataReader = myCommand.ExecuteReader();
			
			if (myDataReader.Read()) 
			{
				AppName = myDataReader["Application_Name"].ToString();
				AppURL = myDataReader["URL"].ToString();
			}
			myDataReader.Close();
			cn.Close();
			return "<a href='" + baseURL + AppURL + "' >" + AppName + "</a>" ;
		}
				
	}
}
