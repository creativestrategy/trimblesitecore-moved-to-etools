using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


	/// <summary>
	/// Summary description for kuki.
	/// </summary>
	public class CookieManager
	{
		public CookieManager()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public bool setValue(string cookiename, string cookievalue ,int iDaysToExpire)
		{
			try
			{
				HttpContext.Current.Response.Cookies[cookiename].Value = cookievalue;
				DateTime dtExpiry = DateTime.Now.AddDays(iDaysToExpire);
				HttpContext.Current.Response.Cookies[cookiename].Expires = dtExpiry; 
			}
			catch(Exception e)
			{
				string errMsg = "Error Found:<br>" + e.ToString();
				return false;
			}
			return true;
		}

		public string getValue(string cookieName)
		{
			string val = null;
			
			if (HttpContext.Current.Request.Cookies[cookieName] != null )
			{
				val = HttpContext.Current.Request.Cookies[cookieName].Value;
				//val = HttpContext.Current.Request.Cookies[cookieName].Value.ToString();
			}
			return val;
		}

		public void clrValue(string cookieName)
		{
			HttpContext.Current.Response.Cookies[cookieName].Expires = DateTime.Now.AddYears(-30);
		}

		
	}
