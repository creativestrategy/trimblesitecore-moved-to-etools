using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.UI;
using System.Web.Mail;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;


namespace wrapp
{
	/// <summary>
	/// Summary description for EmailManager.
	/// </summary>
	public class EmailManager
	{
		public EmailManager()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		string sMailServer = "10.90.65.139";



		//************************************************
		// Function:	SendEmail
		// Purpose:		Generic Mailer 
		// Args: 		string sTo = Recipient
		//				string sSubject = Email Subject
		//				string sFrom = Sender
		//				string sBCC
		//***********************************************
		public void SendEmail(string sTo, string sSubject, string sBody, string sFrom, string sCC)
		{
			string sBodyFormat = "HTML";
			string sPriority = "HIGH";
		
			MailMessage MyMail = new MailMessage();
		
			//MyMail.UrlContentBase = sUrlContentBase;
			//MyMail.UrlContentLocation = sUrlContentLocation;
			/*
			if (txtBodyEncoding.Text == Encoding.UTF7.EncodingName)
				MyMail.BodyEncoding = Encoding.UTF7;
			else if (txtBodyEncoding.Text == Encoding.UTF8.EncodingName)
				MyMail.BodyEncoding = Encoding.UTF8;
			else
				MyMail.BodyEncoding = Encoding.ASCII;
			*/
		
			switch (sBodyFormat.ToUpper())
			{
				case "HTML": 
					MyMail.BodyFormat = MailFormat.Html;
					break;
				default: 
					MyMail.BodyFormat = MailFormat.Text;
					break;
			}
		
			switch (sPriority.ToUpper())
			{
				case "HIGH": 
					MyMail.Priority = MailPriority.High;
					break;
				case "LOW": 
					MyMail.Priority = MailPriority.Low;
					break;
				default: 
					MyMail.Priority = MailPriority.Normal;
					break;
			}

			MyMail.From = sFrom;
			MyMail.To = sTo;
			MyMail.Subject = sSubject;
			MyMail.Body = sBody;
			MyMail.Cc = sCC;
			MyMail.BodyEncoding = Encoding.UTF8;
			
			SmtpMail.SmtpServer = sMailServer;
			SmtpMail.Send(MyMail);
		}

		//************************************************
		// Function:	SendPassword
		// Purpose:		Gets the email address from remedy and send out to the user 
		// Args: 		string sTo = Registered Email
		//				string sPpass = generated Password
		//***********************************************
		public void SendPassword(string sPass, string sTo, string appURL)
		{
			string sSubject = "< WRaPP /> - Password Request";
			string sFrom = "webserver@trimble.com";

			string sBody = "<font face='Verdana, Arial, Helvetica, sans-serif' size=2>";
			sBody = sBody + 
				"<p>Thank you for using <strong>&lt;WRaPP/&gt;</strong>!</p>" +
				"<p>Your Password is:&nbsp; <b>" + sPass + "</b></p>" +
				//"<br>Please go to <strong>&lt;WRaPP/&gt;</strong> <a href='/wrapp/login.aspx'>login page</a> " + 
				"<br>Please go to the <strong>" + appURL + " App to continue.</strong>  " + 
				"<p><br><br>Please remember your password and always keep your password secured" +
				"";
					
			sBody = sBody + "</font>";
					
			SendEmail(sTo, sSubject, sBody, sFrom, "");
		}


		//************************************************
		// Function:	SendReplyNotice
		// Purpose:		Gets the email address from remedy and send out to the user 
		// Args: 		string sTo = Registered Email
		//				string rid = Request ID
		//***********************************************
		public void SendReplyNotice(string rid, string rt, string sTo, string sCC, string sFrom, string summary)
		{
			string baseURL = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + "/wrapp/";
			string sSubject = "<WRaPP/> - Reply Alert [" + rt + "#" + Convert.ToUInt32(rid).ToString("D5")  + "]";
			sSubject += " By " + HttpContext.Current.Session["First_Name"] + " " + HttpContext.Current.Session["Last_Name"];
			//sFrom = "webserver@trimble.com";

			string sBody = "<font face='Verdana, Arial, Helvetica, sans-serif' size=2>";
			sBody =  "<html>" +
				"<head>" +
				"<meta http-equiv'Content-Type' content='text/html; charset=utf-8' />" +
				"<title><WRapp/> Reply Alert</title>" +
				"<style type='text/css'>" +
				"<!--" +
				"body {font-family: Arial, Helvetica, sans-serif; font-size:10px; margin:10px 0px 0px 0px; padding:0;}" +
				".fontstyle {font-family: Arial, Helvetica, sans-serif; font-size:10px}" +
				".blueBorder {border: #3366ff 1px solid; background:#ffffff;}" +
				"-->" +
				"</style>" +
				"</head>" +
				"<body>" +
				"<table class='blueBorder' id='tblSuccess' style='width:500px;' cellspacing='0' align='center'>" +
				"<tr>" +
				"	<td style='padding:5px; background:#C14652;' valign='middle' align='center' colspan='2'>" +
				"		<span style='font-weight:bold; font-size:16px; color:#fff;'>&lt;&nbsp;A New Reply has been Posted By " + HttpContext.Current.Session["First_Name"] + " " + HttpContext.Current.Session["Last_Name"] + "&nbsp;/&gt;</span>" +
				"	</td>" +
				"</tr>" +
				"<tr>" +
				"	<td valign='middle' align='center' width='130'><img src='" + baseURL + "images/icon_RE_Success.jpg' border='0' width='70px' height='70px'></td>" +
				"	<td valign='middle' align='left'>" +
				"		<br>" +
				"		<br>" +
				"		Request ID: <a href='" + baseURL + "view" + rt.ToUpper() + "details.aspx?rid=" + rid + "'>" + rt.ToUpper() + "#" + Convert.ToUInt32(rid).ToString("D5") + "</a>" +
				"		<br><strong>Summary:</strong><br>" + summary +
				"		<br><br>" +
				"		Login to <a href='" + baseURL + "workspace.aspx'>&lt;WRaPP/&gt;</a> Workspace" +
				"		<br>" +
				"		&nbsp;" +
				"	</td>" +
				"</tr>" +
				"</table>" +
				"<table style='width:500px;' cellspacing='0' align='center'>" +
				"<tr>" +
				"	<td align='center'>" +
				"		Please <strong>DO NOT REPLY</strong> to this message.<br>Please use the links above to see the details of this Request." +
				"	</td>" +
				"</tr>" +
				"</table>" +
				"</body>" +
				"</html>";
					
			sBody = sBody + "</font>";
					
			SendEmail(sTo, sSubject, sBody, sFrom, sCC);
		}

	}
}
