﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;




/// <summary>
/// Summary description for NewsReleaseService
/// </summary>
[WebService(Namespace = "http://trimble.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class CustomerStoriesService : System.Web.Services.WebService {

    public CustomerStoriesService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
	
	public static string server = HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToUpper();

    	
	
	[WebMethod]
	public XmlDocument getFullStoryList(string divisionID, string region, string language, string market, string product, string xmonth, string xyear, string RowCount){
		
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
		
		StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		string quickLink = "";		
		string firstDate;
		int rCount = 0;
		string stat = "0";
		//if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) stat="1";		
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		string sqlQuery;
		
		sqlQuery = "SELECT "; 
		if(RowCount!="") sqlQuery += "TOP " + RowCount + " ";
		sqlQuery += "* FROM Customer_Stories WHERE StoryID<>''";
		
		if (divisionID!="") sqlQuery += " AND DivisionID ='" + divisionID + "'";
		if (region!="" && region!=null) sqlQuery += " AND Region='" + region +"'";
		if (market!="" && market!=null) sqlQuery += " AND Market_Segments LIKE '%%" + region + "%%'";
		if (language!="" && language!=null) sqlQuery += " AND Language='" + language +"'";
		if (product!="" && product!=null) sqlQuery += " AND Products LIKE '%%" + product + "%%'";
		
		/*
		if (divisionID!="") sqlQuery += " AND DivisionID Like @divisionID";
		if (region!="") sqlQuery += " AND Region=@region";
		if (market!="") sqlQuery += " AND Market_Segments Like @market";
		if (language!="") sqlQuery += " AND Language=@language";
		if (product!="") sqlQuery += " AND Products Like @product";
		*/
		//if(xmonth!="0")  sqlQuery += " AND DATEPART(MONTH, DateReleased) = '" + xmonth + "'";
        //if(xyear!="0")   sqlQuery += " AND DATEPART(YEAR, DateReleased) = '" + xyear + "'";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) sqlQuery += " AND Status='1'";
		else sqlQuery += " AND (Status='1' OR Status='0')";
		sqlQuery += " ORDER BY DateReleased DESC";
				
		//Response.Write(SqlQuery);
		SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB2);        		
		/*
		sqlCommand.Parameters.Add(new SqlParameter("@divisionID", divisionID));
		sqlCommand.Parameters.AddWithValue("@region", region);
		sqlCommand.Parameters.AddWithValue("@market", "%%" + market + "%%");
		sqlCommand.Parameters.AddWithValue("@language", language);
		sqlCommand.Parameters.AddWithValue("@product", "%%" + product + "%%");
		*/
		
		SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		
		
		if(sqlReader.HasRows){
			rCount++;
			xtw.WriteStartElement("table");
			xtw.WriteAttributeString("id", "tblCustomerStory");
			xtw.WriteAttributeString("class", "csContent");
			while (sqlReader.Read()){
				xtw.WriteStartElement("tr");
				
					xtw.WriteStartElement("td");
						xtw.WriteAttributeString("class", "tdThumb");
						xtw.WriteStartElement("img");
							xtw.WriteAttributeString("src", sqlReader["Thumbnail"].ToString());
							xtw.WriteAttributeString("class", "csThumb");
							xtw.WriteAttributeString("alt", sqlReader["Title"].ToString());			
						xtw.WriteEndElement();	
					xtw.WriteEndElement();
					
					xtw.WriteStartElement("td");
						xtw.WriteAttributeString("valign", "top");
						xtw.WriteAttributeString("class", "tdDetails");
						
						xtw.WriteStartElement("a");
							xtw.WriteAttributeString("href", sqlReader["URL"].ToString());
							xtw.WriteAttributeString("class", "csTitle");
							xtw.WriteAttributeString("target", "_blank");
							xtw.WriteString(sqlReader["Title"].ToString());							
						xtw.WriteEndElement();
						
						xtw.WriteStartElement("div");
							xtw.WriteAttributeString("class", "csDescription");
							xtw.WriteString(HttpUtility.HtmlDecode(sqlReader["Description"].ToString()));							
						xtw.WriteEndElement();
												
						
					xtw.WriteEndElement();
				xtw.WriteEndElement();
				rCount++;			
			}		
			xtw.WriteEndElement();
		}
		
		if (rCount == 0){
			xtw.WriteStartElement("p");
				xtw.WriteAttributeString("class", "noItems");
				xtw.WriteAttributeString("align", "center");
				xtw.WriteString("No Items Found. Please check back later.");					
			xtw.WriteEndElement();
		}
		
		/*
		xtw.Flush();
		xtw.WriteStartElement("p");
			xtw.WriteAttributeString("class", "noItems");
			xtw.WriteAttributeString("align", "center");
			xtw.WriteString(sqlQuery);					
		xtw.WriteEndElement();
		*/
		
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
	}
	
	
	private XmlDocument TableStyle(string title, string description, string url, string thumbnail){
		string output="";
		
		StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		xtw.WriteStartElement("table");
		xtw.WriteAttributeString("id", "tblCustomerStory");
		xtw.WriteAttributeString("class", "csResult");
		
			xtw.WriteStartElement("tr");
				xtw.WriteStartElement("td");
					xtw.WriteAttributeString("class", "tdThumb");
					xtw.WriteString("<img src='" + thumbnail + "' border='0' class='csThumb' alt='" + title + "'>");					
				xtw.WriteEndElement();
				xtw.WriteStartElement("td");
					xtw.WriteAttributeString("valign", "top");
					xtw.WriteAttributeString("class", "tdDetails");
					xtw.WriteString("<a target='_blank' href='" + url + "' class='csTitle'>" + title + "</a> <div class='csDescription' >" + description + "</div>");					
				xtw.WriteEndElement();
			xtw.WriteEndElement();
			
		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
	}
	
	
	
	[WebMethod]
	public XmlDocument LoadDivisionDropDown()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpIndustry");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateNews()");
		
        string output = "";
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT * FROM NewsManagerNewsCategories Order By CategoryID";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
			sqlReader.Read();
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
				xtw.WriteString(sqlReader["CategoryName"].ToString());					
			xtw.WriteEndElement();
			
            while (sqlReader.Read())
            {
                xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", sqlReader["CategoryID"].ToString());
					xtw.WriteString(sqlReader["CategoryName"].ToString());					
				xtw.WriteEndElement();
            }
        }
        sqlReader.Close();
        cnDB4.Close();
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "00");
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString("[View All Industry]");					
		xtw.WriteEndElement();

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	[WebMethod]
	public XmlDocument LoadYearDropDown()
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", "drpYear");
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateNews()");
		
        int sYear = 1999;
		int currYear = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
		
		xtw.WriteStartElement("option");		
			xtw.WriteAttributeString("value", currYear.ToString());
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString(currYear.ToString());					
		xtw.WriteEndElement();
		
		for(int i = (currYear-1); sYear<=i; i--){
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", i.ToString());
				xtw.WriteString(i.ToString());					
			xtw.WriteEndElement();
		}
		

		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	
	
	

    private string getDivisionName(string divID)
    {
        string output = "";
        SqlConnection cnDB4 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB4"]);
        if (cnDB4.State != ConnectionState.Open) cnDB4.Open();

        string sqlQuery = "SELECT * FROM NewsManagerNewsCategories WHERE CategoryID=" + divID;
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB4);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
        if (sqlReader.HasRows)
        {
            while (sqlReader.Read())
            {
                output = sqlReader["CategoryName"].ToString();
            }
        }
        sqlReader.Close();
        cnDB4.Close();
        return output;
    }
	
	
	
	[WebMethod]
	public XmlDocument createDropdown(string ddlID, string ddlType, string Table, string DataField, string ValueField, string divCode, string strFilter, string defaultValue)
    {
		HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/html";
        
        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xtw = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);
        xtw.Formatting = Formatting.Indented;
		
		//xtw.WriteStartDocument(); //uncomment for XML Declaration

        xtw.WriteStartElement("select");
		xtw.WriteAttributeString("id", ddlID);
		xtw.WriteAttributeString("class", "dropList");
		xtw.WriteAttributeString("onchange", "updateStoryList(this)");
		
		xtw.WriteStartElement("option");
			xtw.WriteAttributeString("value", "");
			xtw.WriteAttributeString("selected", "selected");
			xtw.WriteString("Choose a " + ddlType);					
		xtw.WriteEndElement();
		
        SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		string sqlQuery = "";
		sqlQuery = "SELECT DISTINCT [" + DataField + "] FROM " + Table + " WHERE DivisionID='" + divCode + "'";		
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) sqlQuery += " AND Status='1'";
		else sqlQuery += " AND (Status='1' OR Status='0')";	
		if(strFilter!="") sqlQuery += " AND " + strFilter;
		sqlQuery += " Order By " + DataField + " ASC";
		
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB2);
        SqlDataReader sqlReader = sqlCommand.ExecuteReader();
		
		ArrayList output = new ArrayList();
		
		if(sqlReader.HasRows){
			while (sqlReader.Read()){
				string[] arr = sqlReader[DataField].ToString().Split(',');
				
				for(int j=0; j<arr.Length; j++){
					output.Add(arr[j]);
				}				
			}		
		}
		sqlReader.Close();
        cnDB2.Close();
		
		output = RemoveDuplicates(output);
		bool emptyItem = false;
		for(int i=0; i<output.Count; i++){
			if(output[i].ToString()==""){
				if(ddlID=="drpMarket"){
					emptyItem = true;
				}
			}else{
				xtw.WriteStartElement("option");
					xtw.WriteAttributeString("value", output[i].ToString());
					xtw.WriteString(output[i].ToString());					
				xtw.WriteEndElement();
			}
			
		}
		if(emptyItem){
			xtw.WriteStartElement("option");
				xtw.WriteAttributeString("value", "");
				xtw.WriteString("- Uncategorized -");					
			xtw.WriteEndElement();
		}
		
		//ddl.Items.Add(new ListItem("United States", "US"));	
		
		//ddl.SelectedValue = defaultValue;	
		
		
		xtw.WriteEndElement();
		 
        xtw.Flush();
        xtw.Close();
        HttpContext.Current.Response.End();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(stringWriter.ToString());
		return xmlDocument;
    }
	
	
	public static ArrayList RemoveDuplicates(ArrayList list) {
		ArrayList ret=new ArrayList();
		foreach (object obj in list) {
			if (!ret.Contains(obj)) ret.Add(obj);
		}
		ret.Sort();
		return ret;
	}
	
	
}




