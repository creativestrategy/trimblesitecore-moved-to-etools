﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.ServiceModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for NewsReleaseService
/// </summary>
[WebService(Namespace = "https://trimble.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class JSONTrimbleEvents : System.Web.Services.WebService {

    public JSONTrimbleEvents () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
	
	public static string server = HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToUpper();

    public Dictionary<string, string> divNames = new Dictionary<string, string>();
	public Dictionary<string, string> divURLs= new Dictionary<string, string>();

	
	[WebMethod]
	[ScriptMethod(ResponseFormat = ResponseFormat.Json)] 	
	private void getEvents_OLD(){

		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string stat = "0";
		string EST = DateTime.UtcNow.Subtract(new TimeSpan(0,5,0,0)).ToString();
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        
		
		string SqlQuery = "SELECT * FROM Trimble_Events_Calendar";
		SqlQuery += " JOIN Trimble_Divisions ON Trimble_Events_Calendar.Division=Trimble_Divisions.DivisionCode";
		SqlQuery += " WHERE Trimble_Events_Calendar.EventID<>''";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status='1'";
		if(server.IndexOf("ALPHA.TRIMBLE.COM") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status<>'2'";
		if(server.IndexOf("LOCALHOST") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status='1'";
		SqlQuery += " AND DATEPART(YEAR, Trimble_Events_Calendar.StartDate)=@Yr";
		SqlQuery += " AND Trimble_Events_Calendar.StartDate >='" + EST + "'";
		SqlQuery += " ORDER BY Trimble_Events_Calendar.StartDate ASC";
				
		//Response.Write(server.IndexOf("LOCALHOST") + ": " + SqlQuery);
		SqlCommand myCommand;
		SqlDataReader myDataReader;
		myCommand = new SqlCommand(SqlQuery,cnDB2);
		
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		myCommand.Parameters.Add(new SqlParameter("@Yr", DateTime.Parse(EST).Year.ToString()));
		
		myDataReader = myCommand.ExecuteReader();
		
		JArray Events = new JArray();	
		while (myDataReader.Read()){			
			rCount++;
			Events.Add(new JObject{
				{"event", new JObject {
					{"name", myDataReader["EventName"].ToString()},
					{"url", myDataReader["URL"].ToString()}
				}},
				{"date", new JObject {
					{"start", Convert.ToDateTime(myDataReader["StartDate"]).ToString("MM/dd/yyyy")},
					{"end", Convert.ToDateTime(myDataReader["EndDate"]).ToString("MM/dd/yyyy")}
				}},
				{"location", new JObject {
					{"venue", myDataReader["LocationName"].ToString()},
					{"address", myDataReader["Address"].ToString()},
					{"city", myDataReader["City"].ToString()},
					{"country", myDataReader["Country"].ToString()},
					{"region", myDataReader["REgion"].ToString()}
				}},
				{"reseller", new JObject {
					{"name", myDataReader["ResellerName"].ToString()},
					{"url", myDataReader["ResellerURL"].ToString()}
				}},
				{"trimbleinfo", new JObject {
					{"division", myDataReader["DivisionName"].ToString()},
					{"url", "https://www.trimble.com" + myDataReader["DivisionSite"].ToString()}
				}}
				
			});
		}
		
	
		myDataReader.Close();
		cnDB2.Close();
				
		//return JsonConvert.SerializeObject(array);
		//WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
		HttpContext.Current.Response.ContentType = "application/json";
		Context.Response.Write(JsonConvert.SerializeObject(Events));
		
	}
	
	
	[WebMethod]
	[ScriptMethod(ResponseFormat = ResponseFormat.Json)] 	
	private void getEvents_OLD2(){

		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string stat = "0";
		string EST = DateTime.UtcNow.Subtract(new TimeSpan(0,5,0,0)).ToString();
		string[] divArr;
		string divName;
		string divURL;
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        getDivisions();
		
		
		string SqlQuery = "SELECT * FROM Trimble_Events_Calendar";
		//SqlQuery += " JOIN Trimble_Divisions ON Trimble_Events_Calendar.Division=Trimble_Divisions.DivisionCode";
		SqlQuery += " WHERE Trimble_Events_Calendar.EventID<>''";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status='1'";
		if(server.IndexOf("ALPHA.TRIMBLE.COM") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status<>'2'";
		if(server.IndexOf("LOCALHOST") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status='1'";
		SqlQuery += " AND DATEPART(YEAR, Trimble_Events_Calendar.StartDate)=@Yr";
		SqlQuery += " AND Trimble_Events_Calendar.StartDate >='" + EST + "'";
		SqlQuery += " ORDER BY Trimble_Events_Calendar.StartDate ASC";
				
		//Response.Write(server.IndexOf("LOCALHOST") + ": " + SqlQuery);
		SqlCommand myCommand;
		SqlDataReader myDataReader;
		myCommand = new SqlCommand(SqlQuery,cnDB2);
		
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		myCommand.Parameters.Add(new SqlParameter("@Yr", DateTime.Parse(EST).Year.ToString()));
		
		myDataReader = myCommand.ExecuteReader();
		
		JArray Events = new JArray();	
		while (myDataReader.Read()){
			divArr = myDataReader["Division"].ToString().Split(',');
			if(divArr.Count()==1){
				//divName = myDataReader["Division"].ToString().Trim();				
				//divURL = divURL + myDataReader["DivisionSite"].ToString();
				//HttpContext.Current.Response.Write("<h2>" + myDataReader["Division"].ToString().Trim() + "</h2>");
				divNames.TryGetValue(myDataReader["Division"].ToString().Trim(), out divName);
				divURLs.TryGetValue(myDataReader["Division"].ToString().Trim(), out divURL);
				divURL = "https://www.trimble.com" + divURL;
			}else{
				divName = "Trimble Inc.";
				divURL = "https://www.trimble.com";
			}
				
			rCount++;
			Events.Add(new JObject{
				{"event", new JObject {
					{"name", myDataReader["EventName"].ToString()},
					{"url", myDataReader["URL"].ToString()}
				}},
				{"date", new JObject {
					{"start", Convert.ToDateTime(myDataReader["StartDate"]).ToString("MM/dd/yyyy")},
					{"end", Convert.ToDateTime(myDataReader["EndDate"]).ToString("MM/dd/yyyy")}
				}},
				{"location", new JObject {
					{"venue", myDataReader["LocationName"].ToString()},
					{"address", myDataReader["Address"].ToString()},
					{"city", myDataReader["City"].ToString()},
					{"country", myDataReader["Country"].ToString()},
					{"region", myDataReader["REgion"].ToString()}
				}},
				{"reseller", new JObject {
					{"name", myDataReader["ResellerName"].ToString()},
					{"url", myDataReader["ResellerURL"].ToString()}
				}},
				{"trimbleinfo", new JObject {
					{"division", divName},
					{"url", divURL}
				}}
				
			});
		}
		
	
		myDataReader.Close();
		cnDB2.Close();
				
		//return JsonConvert.SerializeObject(array);
		//WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
		HttpContext.Current.Response.ContentType = "application/json";
		Context.Response.Write(JsonConvert.SerializeObject(Events));
		
	}
	
	[WebMethod]
	[ScriptMethod(ResponseFormat = ResponseFormat.Json)] 	
	public void getEvents(){

		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string stat = "0";
		string EST = DateTime.UtcNow.Subtract(new TimeSpan(0,5,0,0)).ToString();
		string[] divArr;
		string divName;
		string divURL;
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        getDivisions();
		
		
		string SqlQuery = "SELECT * FROM Trimble_Events_Calendar";
		//SqlQuery += " JOIN Trimble_Divisions ON Trimble_Events_Calendar.Division=Trimble_Divisions.DivisionCode";
		SqlQuery += " WHERE Trimble_Events_Calendar.EventID<>''";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status='1'";
		if(server.IndexOf("CMS.TRIMBLE.COM") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status<>'2'";
		if(server.IndexOf("LOCALHOST") >= 0) SqlQuery += " AND Trimble_Events_Calendar.Status='1'";
		SqlQuery += " AND DATEPART(YEAR, Trimble_Events_Calendar.StartDate)=@Yr";
		SqlQuery += " AND Trimble_Events_Calendar.StartDate >='" + EST + "'";
		SqlQuery += " ORDER BY Trimble_Events_Calendar.StartDate ASC";
				
		//Response.Write(server.IndexOf("LOCALHOST") + ": " + SqlQuery);
		SqlCommand myCommand;
		SqlDataReader myDataReader;
		myCommand = new SqlCommand(SqlQuery,cnDB2);
		
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		myCommand.Parameters.Add(new SqlParameter("@Yr", DateTime.Parse(EST).Year.ToString()));
		
		myDataReader = myCommand.ExecuteReader();
		
		JArray Events = new JArray();	
		JArray divJArray = new JArray();
		JArray urlJArray = new JArray();
		
		while (myDataReader.Read()){
			divArr = myDataReader["Division"].ToString().Split(',');
			divName = "";
			divURL = "";
			divJArray.Clear();
			urlJArray.Clear();
			
			foreach (string s in divArr){
					divNames.TryGetValue(s.Trim(), out divName);
					divURLs.TryGetValue(s.Trim(), out divURL);
					divURL = "https://www.trimble.com" + divURL;
					divJArray.Add(divName);
					urlJArray.Add(divURL);
				}
				
			rCount++;
			Events.Add(new JObject{
				{"event", new JObject {
					{"name", myDataReader["EventName"].ToString()},
					{"url", myDataReader["URL"].ToString()}
				}},
				{"date", new JObject {
					{"start", Convert.ToDateTime(myDataReader["StartDate"]).ToString("MM/dd/yyyy")},
					{"end", Convert.ToDateTime(myDataReader["EndDate"]).ToString("MM/dd/yyyy")}
				}},
				{"location", new JObject {
					{"venue", myDataReader["LocationName"].ToString()},
					{"address", myDataReader["Address"].ToString()},
					{"city", myDataReader["City"].ToString()},
					{"country", myDataReader["Country"].ToString()},
					{"region", myDataReader["REgion"].ToString()}
				}},
				{"reseller", new JObject {
					{"name", myDataReader["ResellerName"].ToString()},
					{"url", myDataReader["ResellerURL"].ToString()}
				}},
				{"trimbleinfo", new JObject {
					{"division", divJArray},
					{"url", urlJArray}
				}}
				
			});
		}
		
	
		myDataReader.Close();
		cnDB2.Close();
				
		//return JsonConvert.SerializeObject(array);
		//WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
		HttpContext.Current.Response.ContentType = "application/json";
		Context.Response.Write(JsonConvert.SerializeObject(Events));
		
	}
	

	
	
	[ScriptMethod(ResponseFormat = ResponseFormat.Json)] 	
	public void queryEvents(string divisionID, string region, string Yr){

		string quickLink = "";
		
		string output = "";
		bool newMonth = false;
		string firstDate;
		int rCount = 0;
		string stat = "0";
		string EST = DateTime.UtcNow.Subtract(new TimeSpan(0,5,0,0)).ToString();
		
		SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        
		
		string SqlQuery = "SELECT * FROM Trimble_Events_Calendar WHERE EventID<>''";
		if(server.IndexOf("WWW.TRIMBLE.COM") >= 0) SqlQuery += " AND Status='1'";
		if(server.IndexOf("ALPHA.TRIMBLE.COM") >= 0) SqlQuery += " AND Status<>'2'";
		if(server.IndexOf("LOCALHOST") >= 0) SqlQuery += " AND Status='1'";
		if (divisionID!="") SqlQuery += " AND Division Like @divisionID";
		if (region!="") SqlQuery += " AND Region=@region";
		if (Yr!="0") SqlQuery += " AND DATEPART(YEAR, StartDate)=@Yr";
		SqlQuery += " AND StartDate >='" + EST + "'";
		SqlQuery += " ORDER BY StartDate ASC";
				
		//Response.Write(server.IndexOf("LOCALHOST") + ": " + SqlQuery);
		SqlCommand myCommand;
		SqlDataReader myDataReader;
		myCommand = new SqlCommand(SqlQuery,cnDB2);
		
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
		
		//myCommand.Parameters.Add(new SqlParameter("@divisionID", divisionID));
		myCommand.Parameters.AddWithValue("@divisionID", "%" + divisionID + "%");
        myCommand.Parameters.Add(new SqlParameter("@region", region));
		myCommand.Parameters.Add(new SqlParameter("@Yr", Yr));
		
		myDataReader = myCommand.ExecuteReader();
		
		JObject obj1 = new JObject();
		JObject obj2 = new JObject();
		obj1["name"] = "jim";
		obj1["address"] = "123";
		
		obj2["name"] = "zel";
		obj2["address"] = "321";
		
		JArray array = new JArray();
		array.Add(obj1);
		array.Add(obj2);	
		
		JArray Events = new JArray();	
		
		
			
			
		if(myDataReader.HasRows){
			rCount++;
			myDataReader.Read();
			firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
			
			quickLink = "<a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
			
			output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
			output += "<ul>";
			output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
			output += myDataReader["EventName"].ToString()+ "</a><br>";
			output += myDataReader["City"].ToString()+ " ";
			output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
			output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
			output += " - ";
			output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
			output += "</ul>";
			output += "";
			
			Events.Add(new JObject{
				{"event", myDataReader["EventName"].ToString()},	
				{"venue", myDataReader["LocationName"].ToString()},	
				{"city", myDataReader["city"].ToString()}
			});
								
			while (myDataReader.Read()){
				if(firstDate == Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy")){
					//output += "<p><strong>" + Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy") +"</strong></p>";
					newMonth = false;
				}else{
					//firstDate = Convert.ToDateTime(myDataReader["Publish_Date"]).ToString("MMMM yyyy");
					newMonth = true;
				}
				
				if(newMonth){
					quickLink += " | <a href='#"  + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "'>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM") + "</a>";
					output += "<a name=" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM") + "></a><p><br><strong>" + Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy") +"</strong></p>";
					firstDate = Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMMM yyyy");
					newMonth = false;
				}
				output += "<ul>";
				output += " <a target='_blank' href='" + myDataReader["URL"].ToString() + "'>";
				output += myDataReader["EventName"].ToString()+ "</a><br>";
				output += myDataReader["City"].ToString()+ " ";
				output += PCase(myDataReader["Country"].ToString())+ "</a><br>";
				output += Convert.ToDateTime(myDataReader["StartDate"]).ToString("MMM dd");
				output += " - ";
				output += Convert.ToDateTime(myDataReader["EndDate"]).ToString("MMM dd");
				output += "</ul>";
				output += "";
				rCount++;			
			}		
		}
		
		if (rCount == 0){
			output += "<p><br><center>No events found.<br><br>Please check back later.</b></center></p>";
		}
		//quickLinks.Text = quickLink;
		myDataReader.Close();
		cnDB2.Close();
		
		
		//return JsonConvert.SerializeObject(array);
		//WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
		HttpContext.Current.Response.ContentType = "application/json";
		Context.Response.Write(JsonConvert.SerializeObject(Events));
		
		//WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
		//return new MemoryStream(Encoding.UTF8.GetBytes(array));

	}
	
	
	//Convert String to ProperCase
	private static string PCase(String strParam){
		string strProper=strParam.Substring(0,1).ToUpper();
		strParam = strParam.Substring(1).ToLower();
		string strPrev="";
	
		for(int iIndex=0;iIndex < strParam.Length;iIndex++){
			if(iIndex > 1){
				strPrev=strParam.Substring(iIndex-1,1);
			}
			if( strPrev.Equals(" ") ||
			  strPrev.Equals("\t") ||
			  strPrev.Equals("\n") ||
			  strPrev.Equals(".") ||
			  strPrev.Equals("(")){
			  strProper+=strParam.Substring(iIndex,1).ToUpper();
			}
			else
			{
				strProper+=strParam.Substring(iIndex,1);
			}
		}
		return strProper;
	}
	
	
	private void getDivisions()
    {
		//Dictionary<string, string> divNames = new Dictionary<string, string>();
		//Dictionary<string, string> divURLs= new Dictionary<string, string>();
        SqlConnection cnDB2 = new SqlConnection(ConfigurationSettings.AppSettings["SqlConnStrDB2"]);
        
        string sqlQuery = "SELECT * FROM Trimble_Divisions Order By DivisionName";
        SqlCommand sqlCommand = new SqlCommand(sqlQuery, cnDB2);
		if (cnDB2.State != ConnectionState.Open) cnDB2.Open();
        SqlDataReader myDataReader = sqlCommand.ExecuteReader();
			
		while (myDataReader.Read())
		{
			divNames.Add(myDataReader["DivisionCode"].ToString(), myDataReader["DivisionName"].ToString());
			divURLs.Add(myDataReader["DivisionCode"].ToString(), myDataReader["DivisionSite"].ToString());
		}

        myDataReader.Close();
        cnDB2.Close();
		
		
    }
	
	
}




