﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Includes.ascx.cs" Inherits="Trimble.Presentation.UI.Includes.Includes" %>
<meta name="viewport" content="target-densitydpi=device-dpi, width=1060" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
<meta content="text/html; charset=utf-8" http-equiv=Content-Type />

	<!-- for sli searh added by VJ 10-02-2013 -->
	<link rel="stylesheet" type="text/css" href="//assets.resultspage.com/js/rac/sli-rac.1.5.css"/>
	<link rel="stylesheet" type="text/css" href="//trimble.resultspage.com/rac/sli-rac.css"/>

	<!-- end for sli searh -->

<link rel="shortcut icon" href="../html/images/favicon.ico" />
<link rel="apple-touch-icon" href="iphone_favicon.png" />
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Titillium+Web:400,900,700italic,700,600italic,600,400italic,300italic,300,200italic,200&subset=latin,latin-ext' />
<link href="/html/styles/styles.css" rel="stylesheet" type="text/css" />
<link href="/html/styles/main_menu.css" rel="stylesheet" type="text/css" />
<link href="/html/styles/home_banner1.css" rel="stylesheet" type="text/css" />
<link href="/html/styles/colorbox.css" rel="stylesheet" type="text/css" />
<link href="/html/styles/dcverticalmegamenu.css" rel="stylesheet" type="text/css" />
<link href="/html/styles/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="/html/styles/inner_slideshow.css" rel="stylesheet" type="text/css" />
<link href="/default.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/html/styles/lightbox.css" type="text/css" media="screen" />

<script type='text/javascript' src='/js/munchkin_Geospatial.js'></script>
<script type='text/javascript' src='/js/munchkin_Agriculture.js'></script>

<!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
  <![endif]-->
<!--[if (lte IE 7)]>
   <link href="/html/styles/ie7Tabs.css" rel="stylesheet" type="text/css" />
   <!--<![endif]-->
<script type="text/javascript" src="/html/iepngfix/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="/html/js/ga.js"></script>
<script type="text/javascript" src="/html/js/jquery_002.js"></script>
<script type="text/javascript" src="/html/js/jquery-ui.js"></script>
<script type="text/javascript" src="/html/js/jquery_003.js"></script>
<script type="text/javascript" src="/html/js/allinone_thumbnailsBanner.js"></script>
<script type="text/javascript" src="/html/js/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript" src='/html/js/jquery.dcverticalmegamenu.1.3.js'></script>
<script type="text/javascript" src="/html/js/SpryTabbedPanels.js"></script>
<script type="text/javascript" src="/html/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="/html/js/jQuery_Slideshow_fade_with_menu.js"></script>
<script type="text/javascript" src="/html/js/jquery.galleryview-3.0-dev.js"></script>
<script type="text/javascript" src="/html/js/common.js"></script>
<script type="text/javascript" src="/html/js/jquery.masonry.min.js">
</script><script type="text/javascript" src="/html/js/Jssor.Slider.Min.js"></script>
<script type="text/javascript" src="/html/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="/html/js/jquery.smooth-scroll.min.js"></script>
<script type="text/javascript" src="/html/js/lightbox.js"></script>
<script type="text/javascript" src="/html/js/popup.js"></script>

<script type="text/javascript">
    $(function () {
        $('.mega-1').dcVerticalMegaMenu({
            rowItems: '3',
            speed: 'fast',
            effect: 'show',
            direction: 'right'
        });
        // $('.drop').masonry({ itemSelector: 'div.drop div.left' });      
        $("#click").click(function () {
            $('#click').css({ "background-color": "#f00", "color": "#fff", "cursor": "inherit" }).text("Open this window again and this message will still be here.");
            return false;
        });
        setTimeout(setMinHeightForContainer, 1000);
        if (typeof Sitecore == 'object') {
            var fileref = document.createElement("link")
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", "/html/styles/custom_sitecore.css");
            document.getElementsByTagName("head")[0].appendChild(fileref);
        }

        if ($(".mainHeadContainer span").html() != null) {
            if ($.trim($(".mainHeadContainer span").html()) == "")
                $(".custLoginContainer").css("margin-top", "-15px");
        } else if ($(".mainHeadContainer span").html() == null && !$(".mainHeadContainer div").hasClass("productfamily_title")) {
            $(".custLoginContainer").css("margin-top", "-15px");
        }
    });
    function checkSideBarLoaded() {
        if ($('.sidebar-container').length == 0) {
            console.log('not ready');
            $jquery.error('not ready');
        }
    }
    function setMinHeightForContainer() {
        try {
            checkSideBarLoaded();
            console.log($('.sidebar-container').css("height"));
            $('#container_body').css('min-height', $('.sidebar-container').css("height"));
            console.log($('.sidebar-container').css("height"));
            $(".container_footer").css('display', "block");
        } catch (err) {
            // debugger;
            setTimeout(setMinHeightForContainer, 500);
            $(".container_footer").css('display', "block");
        }

        $(".fsmAboutVideo").colorbox({ iframe: true, innerWidth: 750, innerHeight: 510 });
    }
  
</script>

<script type="text/javascript">
//<![CDATA[
    function fnTrapKD(event, caller) {
        if (event.keyCode == 13) {
            event.returnValue = false;
            event.cancel = true;
            SliSearch(caller);
            return false;
        }
        return true;
    }
    function SliSearchById(id) {
        SliSearch(document.getElementById(id));
    }
    function SliSearch(caller) {
        var lsQueryString;
        if (caller.value != 'enter keywords') {
            lsQueryString = "w=" + encodeURIComponent(caller.value);
        }
        if (caller.value != '') {
            document.location.href = 'http://trimble.resultspage.com/search?' + lsQueryString;
        }
    }
//]]>
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".colorboxImage").colorbox();
        $(".colorboxVideo").colorbox({ iframe: true, innerWidth: 730, innerHeight: 430 });
        $(".colorboxPDF").colorbox({ iframe: true, innerWidth: 800, innerHeight: 600 });
        $(".colorboxDocushare").colorbox({ iframe: true, innerWidth: 750, innerHeight: 510 });
    });
</script>
