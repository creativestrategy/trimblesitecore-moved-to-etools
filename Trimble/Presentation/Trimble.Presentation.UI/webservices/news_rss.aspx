﻿<%@ Page Language="C#" %>

<script runat="server">
	string maxCount;
	string defDivision;
    
    private void Page_Load(object sender, System.EventArgs e)
    {
		maxCount = Request.QueryString["max"]; //uncomment if querystring is used
		defDivision = Request.QueryString["divID"]; //uncomment if querystring is used
		
		if(maxCount==null || maxCount=="") maxCount = "";
		if(defDivision==null || defDivision=="") defDivision = "";
		
		RSSManager newsRSS = new RSSManager();
		newsRSS.getLatestNews(defDivision, maxCount); 
       
    }    
</script>