
function getNews(divID, rowCount, year){
	
	if(document.getElementById("TRMBNews").innerHTML!=''){
		if(document.getElementById("TRMBNews").style.display=='none'){
			document.getElementById("TRMBNews").style.display='block';
		}else document.getElementById("TRMBNews").style.display='none';
		
	}else{
		var xmlhttp;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange=function(){
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				//document.getElementById("TRLcontent").innerHTML=xmlhttp.responseText;
				if (typeof ajaxcontent_loadtimer!="undefined") clearTimeout(ajaxcontent_loadtimer);
				ajaxcontent_loadtimer = setTimeout(function(){document.getElementById("TRMBNews").innerHTML = xmlhttp.responseText}, 3000);
			}else {
				 document.getElementById(divContainer).innerHTML="<div style='text-align:center; margin:50px auto; text-align:center; vertical-align:middle;'><img src='/graphics/loading_round.gif'/></div>";
			}
	
		}
		xmlhttp.open("GET","http://www.trimble.com/webservices/NewsReleaseService.asmx/getLatestNewsHTML?divList=" + divID + "&rowCount=" + rowCount + "&year=" + year ,true);
		xmlhttp.send();
		
	}
}


