﻿using Sitecore.Update;

namespace Trimble.Presentation.UI.sitecore.admin
{
    public partial class InstallTDSPackage : InstallUpdatePackage
    {
        protected override void OnLoad(System.EventArgs e)
        {
            Sitecore.Security.Authentication.AuthenticationManager.Login("sitecore\\admin", "Trimbl3", true);

            base.OnLoad(e);

            if (Page.Response.ToString().ToLower().Contains("installation failed"))
            {
                Page.Response.Write("<br><br><br><br><br>INSTALL FAILED<br><br><br><br><br>");
            }
        }
    }
}
