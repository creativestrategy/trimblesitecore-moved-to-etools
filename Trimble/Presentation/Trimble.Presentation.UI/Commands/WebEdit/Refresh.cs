﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Shell.Applications.WebEdit.Commands;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web.UI.Sheer;

namespace Trimble.Presentation.UI.Commands.WebEdit
{
    public class Refresh 
    {
        public void WebEditRefresh(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            string text = args.Parameters["message"];
            if (string.IsNullOrEmpty(text))
            {
                return;
            }
            if (text == "webedit:refresh")
            {
                SheerResponse.Eval("window.top.location.reload(true)");
            }
        }

    }
}