﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web.UI.Sheer;
using Sitecore.Diagnostics;
using System.Collections.Specialized;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Shell.Framework;
using Sitecore.Shell.Applications.WebEdit.Commands;

namespace Trimble.Presentation.UI.Commands.WebEdit
{
    public class DeleteAndRefresh : WebEditCommand
    {
        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection["id"] = context.Parameters["id"];
            Context.ClientPage.Start(this, "Run", nameValueCollection);
        }
        /// <summary>
        /// Runs the pipeline.
        /// </summary>
        /// <param name="args">The arguments.</param>
        protected void Run(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            if (!SheerResponse.CheckModified())
            {
                return;
            }
            Item item = Client.ContentDatabase.GetItem(args.Parameters["id"]);
            if (item != null)
            {
                Items.Delete(new Item[]
				{
					item
				}, "webedit:refresh");
                
                return;
            }
            SheerResponse.Alert("The item could not be found.\n\nYou may not have read access or it may have been deleted by another user.", new string[0]);
        }
    }
}