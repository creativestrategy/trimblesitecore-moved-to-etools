var cbpHorizontalMenu = (function() {
    var b = $("#cbp-hrmenu > ul > li"),
        g = b.children("a"),
		ic = g.children("span"),
        c = $("body"),
        d = -1;

    function f() {
        g.on("click", a);
		ic.on("click", a);
        b.on("click", function(h) {
            h.stopPropagation()
        })
    }

    function a(j) {
        if (d !== -1) {
            b.eq(d).removeClass("cbp-hropen");
			b.eq(d).children("a").children("span").removeClass("minus").addClass("plus");
        }
        var i = $(j.currentTarget).parent("li"),
            h = i.index();
        if (d === h) {
            i.removeClass("cbp-hropen");
			i.children("a").children("span").removeClass("minus").addClass("plus");
            d = -1
        } else {
            i.addClass("cbp-hropen");
			i.children("a").children("span").removeClass("plus").addClass("minus");
            d = h;
            c.off("click").on("click", e)
        }
        return false
    }

    function e(h) {
        b.eq(d).removeClass("cbp-hropen");
		b.children("a").children("span").removeClass("minus").addClass("plus");
        d = -1
    }
    return {
        init: f
    }
})();