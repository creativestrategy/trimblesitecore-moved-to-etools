﻿var navbarTitle = "Menu";
var navSocialIcons = "";
var mmenuNavTitle = "Home";
var searchText = "Enter your search here...";
var mmenu_locator = "Dealer Locator";
var mmenu_service = "Service Centers";
var mmenu_worldwide = "Trimble Worldwide";
var mmenu_locator = "Trimble Partners";

//init Top-Menu
$(function () {

    switch (sc_lang) {
        case "PT-BR":
            mmenuNavTitle = "Casa";
            searchText = "Digite sua pesquisa aqui";
            mmenu_locator = "Localizar Revendedor";
            mmenu_service = "Centros de Serviço";
            mmenu_worldwide = "Trimble no Mundo";
            mmenu_partner = "Parceiros Trimble";
            break;
        case "ZH-CN":
            mmenuNavTitle = "家";
            searchText = "在这里输入您的搜索";
            mmenu_locator = "经销商定位器";
            mmenu_service = "服务中心";
            mmenu_worldwide = "Trimble全球";
            mmenu_partner = "Trimble合作伙伴";
            break;
        default:
            mmenuNavTitle = "Home";
    }

    $('nav#menu-right').mmenu({
        extensions: ['fx-menu-slide', 'shadow-page', 'shadow-panels', 'listview-large', 'pagedim-black', 'border-full', 'fx-menu-fade', 'multiline', 'theme-dark'],
        backButton: { close: true },
        setSelected: { hover: true, parent: true },
        iconPanels: false,
        counters: false,
        keyboardNavigation: {
            enable: true,
            enhance: true
        },
        offCanvas: {
            position: 'left',
            zposition: 'front'
        },
        navbar: {
            title: mmenuNavTitle
        },
        navbars: [
		{
		    position: 'top',
		    content: [
				"<a id='subnavIconDealerLocator' href='http://dealerlocator.trimble.com/' title='"+ mmenu_locator +"' target='_blank' class='fa3 fa-map-marker fa-2x slideMenuSplitter'></a>",
				"<a id='subnavIconServiceCenters' href='/SPLocator/' title='"+ mmenu_service +"' class='fa3 fa-cog fa-2x slideMenuSplitter'></a>",
				"<a id='subnavIconTrimbleWorldWide' href='/Worldwide/Index.aspx' title='"+ mmenu_worldwide +"' class='fa3 fa-globe fa-2x slideMenuSplitter'></a>",
				"<a id='subnavIconPartners' href='/Partners/Index.aspx' title='"+ mmenu_partner +"' class='fa3 fa-handshake-o fa-2x slideMenuSplitter'></a>"
					]
		},
		{
		    position: 'top',
		    content: ["<div class='formmenu'><input type='text' placeholder='" + searchText + "' class='input search' name='w'  onfocus='this.value=&quot;&quot;' id='mmenu_sli_search' autocomplete='off' onkeydown='javascript:return fnTrapKD(event,this);'/></div>"]

		},
			{
			    position: "bottom",
			    content: [
            "<a id='subnavIconFacebook' class='fa fa-facebook' href='https://www.facebook.com/TrimbleCorporate'></a>",
			"<a id='subnavIconTwitter' class='fa fa-twitter' href='https://twitter.com/TrimbleCorpNews'></a>",
			"<a id='subnavIconLinkedIn' class='fa fa-linkedin' href='http://www.linkedin.com/company/trimble'></a>",
			"<a id='subnavIconYoutube' class='fa fa-youtube' href='https://www.youtube.com/channel/UCD5r7hBRwI6NFc4izfm-ocg'></a>",
			"<a id='subnavIconContacts' class='fa fa-envelope' href='https://www.trimble.com/Corporate/Contacts.aspx'></a>"
            ]
			}
		]
    });
});

//init Body Left-Menu
$(function () {
    if (typeof lhmSocialLinks !== 'undefined' && lhmSocialLinks.length > 0) {
        navSocialIcons = lhmSocialLinks;
    } else {
        navSocialIcons = defaultSocialLinks;
    }


    $('nav#menu-left').mmenu({
        extensions: ['shadow-page', 'shadow-panels', 'listview-large', 'pagedim-black', 'border-full', 'multiline', 'popup'],
        autoHeight: false,
        setSelected: { hover: true, parent: true },
        iconPanels: true,
        counters: false,
        keyboardNavigation: {
            enable: true,
            enhance: true
        },
        searchfield: {
            placeholder: 'Search menu items'
        },
        offCanvas: {
            position: 'left'
        },
        navbar: {
            title: navbarTitle
        },
        navbars: [
			{
			    position: 'top',
			    content: ['searchfield']
			}, {
			    position: 'top',
			    content: ['breadcrumbs', 'close']
			},
			{
			    position: "bottom",
			    content: navSocialIcons
			}
		]
    }, {
        searchfield: {
            clear: true
        }
    });
});

var defaultSocialLinks = [
            "<a id='leftnavIconFacebook' class='fa fa-facebook' href='https://www.facebook.com/TrimbleCorporate'></a>",
			"<a id='leftnavIconTwitter' class='fa fa-twitter' href='https://twitter.com/TrimbleCorpNews'></a>",
			"<a id='leftnavIconLinkedIn' class='fa fa-linkedin' href='http://www.linkedin.com/company/trimble'></a>",
			"<a id='leftnavIconYoutube' class='fa fa-youtube' href='https://www.youtube.com/channel/UCD5r7hBRwI6NFc4izfm-ocg'></a>",
			"<a id='leftnavIconContacts' class='fa fa-envelope' href='https://www.trimble.com/Corporate/Contacts.aspx'></a>"
            ];

