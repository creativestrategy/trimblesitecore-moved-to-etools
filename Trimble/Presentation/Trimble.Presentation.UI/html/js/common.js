 (function ($) {
     $(document).ready(function () {
         var uagent = navigator.userAgent.toLowerCase();
         if (uagent.search("android") > -1) {
             $(".tabcontent_img > img, .tabcontent_img a img").css("border-left", "2px solid #000");
             $(".tabcontent_img > img, .tabcontent_img a img").css("border-bottom", "2px solid #000");
             $(".tabcontent_img > img, .tabcontent_img a img").css("border-top", "2px solid #000");
             $(".featured-solutions .item img").css("border-top", "2px solid #000");
             $(".featured-solutions .item img").css("border-left", "2px solid #000");
             $(".featured-solutions .item img").css("border-right", "2px solid #000");
             $(".leftmenu li ").css("border-bottom", "#acbecd 2px solid");
             $(".sidebarmenu ul li a").css("border-bottom", "#aabfcf 2px solid");
             $(".sidebarmenu ul li a").css("border-right", "#aabfcf 2px solid");
             $(".categoryProducts").css("border", "2px solid #F6F7F9;");
             $(".mega-menu li a, .mega-menu li.heading span").css("border-bottom", "2px solid #aabfcf");
         }

         if ($("#container_banner").css("height") == "0px") {

             $(".sidebar-container").css("margin-top", "42px");

         }

         // Top Search default text
         // $('.topsearch .search').focus(function () {
         // if (this.value == 'Search...') {
         // this.value = '';
         // }
         // });
         // $('.topsearch .search').blur(function () {
         // if (this.value == '') {
         // this.value = 'Search...';
         // }
         // });
         // $('.topsearch .search').on('keydown',function(e){
         // if( e.which == 13)
         // startSearch();
         // });

         $('.close_flyout').live('click', function () {
             $('.mega-menu .sub-container').hide();
         });
         $('.topsearch input[name="Submit"]').click(function () {
             startSearch();
         });

         // End of Top Search default text

         $('.socialicons-container ul.socialicons li a img').hover(swapImage, restoreImage);

         $(document).click(function () {
             $('.mega-menu .sub-container').hide();
         });

     });
 })(jQuery);
function startSearch(){
  window.location.href = 'http://trimble.resultspage.com/search?p=Q&ts=custom&Submit=Submit+Query&w='+ $('.topsearch .search').val();
}
function swapImage(){
  $(this).attr('src', $(this).parent().data('hover-icon'));
}

function restoreImage(){
  $(this).attr('src', $(this).parent().data('icon'));
}

function MM_swapImgRestore() { //v3.0
  var i,x,a = document.MM_sr;
  for(i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) 
    x.src = x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document;
  if(d.images){ 
    if(!d.MM_p) 
      d.MM_p = new Array();
    var i,j=d.MM_p.length, a=MM_preloadImages.arguments; 
//    for(i=0; i<a.length; i++){
//      if (a[i].indexOf("#")!=0){ 
//        d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];
//      }
//    }
  }
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;
  if(!d) 
    d=document; 

  if( (p = n.indexOf("?") ) > 0 && parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; 
    n=n.substring(0,p);
  }
  if(!(x=d[n]) && d.all) 
    x=d.all[n];
  for (i=0;!x&&i<d.forms.length;i++) 
    x=d.forms[i][n];
  
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
    if(!x && d.getElementById)
      x=d.getElementById(n); 
  return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments;
  document.MM_sr=new Array; 
  for(i=0;i<(a.length-2);i+=3) {
    if ((x=MM_findObj(a[i]))!=null){
      document.MM_sr[j++]=x;
      if(!x.oSrc)
        x.oSrc=x.src; 
      x.src=a[i+2];
    }
  }
}
jQuery.fn.doesExist = function(){
  return jQuery(this).length > 0;
};

function productImageSlider(identifier){
  if(! $('.prod_slides a.light-box img').doesExist()) {
    $('.prod_slides').prepend('<a class="light-box"><img/><span>Click to Zoom</span></a>');
  }
  $('.prod_slides a.light-box').attr('href', $('.prod_slides .active a').attr('href'));
  $('.prod_slides a.light-box img').attr('src', $('.prod_slides .active a img').attr('src'));
}
