//Added by Jimmy Mercado - 11-15-2013
(function() {
 
  var url = window.location.href.toString().toLowerCase();
  if((url.indexOf('/imaging/')!= -1 ) || (url.indexOf('/3D-laser-scanning/')!= -1) || (url.indexOf('/survey/')!= -1) || (url.indexOf('/mappinggis/')!= -1)){
  var didInit = false;
  function initMunchkin() {
    if(didInit === false) {
      didInit = true;
      Munchkin.init('846-FAE-776');
	  //alert('yes');
    }
  }
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = '//munchkin.marketo.net/munchkin.js';
  s.onreadystatechange = function() {
    if (this.readyState == 'complete' || this.readyState == 'loaded') {
      initMunchkin();
    }
  };
  s.onload = initMunchkin;
  document.getElementsByTagName('head')[0].appendChild(s);
  }
})();
