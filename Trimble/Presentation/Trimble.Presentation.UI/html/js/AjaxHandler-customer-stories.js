﻿function updateStoryList(obj){
	var dropObject = obj.id;
	var language = (document.getElementById("drpLanguage").options[document.getElementById("drpLanguage").selectedIndex].value);
	var region = (document.getElementById("drpRegion").options[document.getElementById("drpRegion").selectedIndex].value);
	var market = (document.getElementById("drpMarket").options[document.getElementById("drpMarket").selectedIndex].value);
	var product = (document.getElementById("drpProduct").options[document.getElementById("drpProduct").selectedIndex].value);
	
	var filter;
	//alert(obj.value);
	//alert("/webservices/CustomerStoriesService.asmx/getFullStoryList?divisionID=" + csDivCode + "&region=" + region + "&language=" + language + "&market=" + market + "&product=" + product + "&xmonth=&xyear=&RowCount=");
	if(dropObject=="drpRegion"){
		filter = "Region%3D'" + obj.value + "'";
		clearDropDown("drpMarket");
		clearDropDown("drpProduct");
		clearDropDown("drpLanguage");
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/createDropdown?ddlID=drpMarket&ddlType=Market Segment&Table=Customer_Stories&DataField=Market_Segments&ValueField=Market_Segments&DivCode=" + csDivCode + "&strFilter=" + filter + "&defaultValue=","divMarket" );
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/createDropdown?ddlID=drpProduct&ddlType=Product&Table=Customer_Stories&DataField=Products&ValueField=Products&DivCode=" + csDivCode + "&strFilter=" + filter + "&defaultValue=","divProduct" )
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/createDropdown?ddlID=drpLanguage&ddlType=Language&Table=Customer_Stories&DataField=Language&ValueField=Language&DivCode=" + csDivCode + "&strFilter=" + filter + "&defaultValue=","divLanguage" )
	
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/getFullStoryList?divisionID=" + csDivCode + "&region=" + region + "&language=" + language + "&market=" + market + "&product=" + product + "&xmonth=&xyear=&RowCount=", "divCS");
	
	}else if(obj.id=="drpMarket"){
		filter = "Market_Segments%3D'" + obj.value + "'";
		clearDropDown("drpProduct");
		clearDropDown("drpLanguage");
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/createDropdown?ddlID=drpProduct&ddlType=Product&Table=Customer_Stories&DataField=Products&ValueField=Products&DivCode=" + csDivCode + "&strFilter=" + filter + "&defaultValue=","divProduct" )
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/createDropdown?ddlID=drpLanguage&ddlType=Language&Table=Customer_Stories&DataField=Language&ValueField=Language&DivCode=" + csDivCode + "&strFilter=" + filter + "&defaultValue=","divLanguage" )
	
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/getFullStoryList?divisionID=" + csDivCode + "&region=" + region + "&language=" + language + "&market=" + market + "&product=" + product + "&xmonth=&xyear=&RowCount=", "divCS");
	
	}else if(obj.id=="drpProduct"){
		filter = "Products%3D'" + obj.value + "'";
		clearDropDown("drpLanguage");
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/createDropdown?ddlID=drpLanguage&ddlType=Language&Table=Customer_Stories&DataField=Language&ValueField=Language&DivCode=" + csDivCode + "&strFilter=" + filter + "&defaultValue=","divLanguage" )
	
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/getFullStoryList?divisionID=" + csDivCode + "&region=" + region + "&language=" + language + "&market=" + market + "&product=" + product + "&xmonth=&xyear=&RowCount=", "divCS");
	
	}else if(obj.id=="drpLanguage"){
		filter = "Language%3D'" + obj.value + "'";
		
		HttpRequestHandler("/webservices/CustomerStoriesService.asmx/getFullStoryList?divisionID=" + csDivCode + "&region=" + region + "&language=" + language + "&market=" + market + "&product=" + product + "&xmonth=&xyear=&RowCount=", "divCS");
	}
	/*
	var y = document.getElementById("drpYear");
	var strYear = y.options[y.selectedIndex].value;
	
	var i = document.getElementById("drpIndustry");
	var strIndustry = i.options[i.selectedIndex].value;

	getNews(strYear, strIndustry, 'newsContainer');
	*/
}	

function clearDropDown(drpID){
	var select = document.getElementById(drpID);
	select.options.length = 0;
}