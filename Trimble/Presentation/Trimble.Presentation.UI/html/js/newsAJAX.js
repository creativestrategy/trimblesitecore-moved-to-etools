
function getNews(strYear, strIndustry, divContainer){
	

		var xmlhttp;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange=function(){
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				//document.getElementById("TRLcontent").innerHTML=xmlhttp.responseText;
				if (typeof ajaxcontent_loadtimer!="undefined") clearTimeout(ajaxcontent_loadtimer);
				ajaxcontent_loadtimer = setTimeout(function(){document.getElementById(divContainer).innerHTML = xmlhttp.responseText}, 2000);
			}else {
				 document.getElementById(divContainer).innerHTML="<div style='text-align:center; margin:50px auto; text-align:center; vertical-align:middle;'><img src='/theme/images/loading_round.gif'/></div>";
			}
	
		}
		xmlhttp.open("GET","/webservices/NewsReleaseService.asmx/getLatestNewsHTML_EN?divList=" + strIndustry + "&year=" + strYear + "&rowCount=", true);
		//xmlhttp.open("GET","newsAJAX.aspx?yr=" + strYear + "&cat=" + strIndustry, true);
		xmlhttp.send();

}

function getNews_CN(strYear, strIndustry, divContainer){
	

		var xmlhttp;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange=function(){
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				//document.getElementById("TRLcontent").innerHTML=xmlhttp.responseText;
				if (typeof ajaxcontent_loadtimer!="undefined") clearTimeout(ajaxcontent_loadtimer);
				ajaxcontent_loadtimer = setTimeout(function(){document.getElementById(divContainer).innerHTML = xmlhttp.responseText}, 2000);
			}else {
				 document.getElementById(divContainer).innerHTML="<div style='text-align:center; margin:50px auto; text-align:center; vertical-align:middle;'><img src='/theme/images/loading_round.gif'/></div>";
			}
	
		}
		xmlhttp.open("GET","/webservices/NewsReleaseService.asmx/getLatestNewsHTML_CN?divList=" + strIndustry + "&year=" + strYear + "&rowCount=", true);
		//xmlhttp.open("GET","newsAJAX.aspx?yr=" + strYear + "&cat=" + strIndustry, true);
		xmlhttp.send();

}

function getNewsHomepage(strLanguage, strCount, divContainer){
		var xmlhttp;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange=function(){
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById(divContainer).innerHTML = xmlhttp.responseText
			}else {
				 document.getElementById(divContainer).innerHTML="<div style='text-align:center; margin:50px auto; text-align:center; vertical-align:middle;'><img src='/theme/images/loading_round.gif'/></div>";
			}
	
		}
		xmlhttp.open("GET","/webservices/NewsReleaseService.asmx/getLatestNewsHomepage?language=" + strLanguage + "&rowCount=" + strCount, true);
		//xmlhttp.open("GET","newsAJAX.aspx?yr=" + strYear + "&cat=" + strIndustry, true);
		xmlhttp.send();

}

function getNewsHomepage_Luna(strLanguage, strCount, divContainer) {
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById(divContainer).innerHTML = xmlhttp.responseText
        } else {
            document.getElementById(divContainer).innerHTML = "<div style='text-align:center; margin:50px auto; text-align:center; vertical-align:middle;'><img src='/theme/images/loading_round.gif'/></div>";
        }

    }
    xmlhttp.open("GET", "/webservices/NewsReleaseService.asmx/getLatestNewsHomepage_Luna?language=" + strLanguage + "&rowCount=" + strCount, true);
    //xmlhttp.open("GET","newsAJAX.aspx?yr=" + strYear + "&cat=" + strIndustry, true); 
    xmlhttp.send();

}

function getNewsHomepage_STO(strLanguage, strCount, divContainer){
		var xmlhttp;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange=function(){
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				//document.getElementById("TRLcontent").innerHTML=xmlhttp.responseText;
				if (typeof ajaxcontent_loadtimer!="undefined") clearTimeout(ajaxcontent_loadtimer);
				ajaxcontent_loadtimer = setTimeout(function(){document.getElementById(divContainer).innerHTML = xmlhttp.responseText}, 2000);
			}else {
				 document.getElementById(divContainer).innerHTML="<div style='text-align:center; margin:50px auto; text-align:center; vertical-align:middle;'><img src='/theme/images/loading_round.gif'/></div>";
			}
	
		}
		xmlhttp.open("GET","/webservices/NewsReleaseService.asmx/getLatestNewsHomepage?language=" + strLanguage + "&rowCount=" + strCount, true);
		//xmlhttp.open("GET","newsAJAX.aspx?yr=" + strYear + "&cat=" + strIndustry, true);
		xmlhttp.send();

}




function updateNews(divContainer){
	var y = document.getElementById("drpYear");
	var strYear = y.options[y.selectedIndex].value;
	
	var i = document.getElementById("drpIndustry");
	var strIndustry = i.options[i.selectedIndex].value;

	getNews(strYear, strIndustry, 'newsContainer');	
}


function updateNews_CN(divContainer){
	var y = document.getElementById("drpYear");
	var strYear = y.options[y.selectedIndex].value;
	
	var i = document.getElementById("drpIndustry");
	var strIndustry = i.options[i.selectedIndex].value;

	getNews_CN(strYear, strIndustry, 'newsContainer');	
}


if (document.images) {
	imgLoading = new Image();
	imgLoading.src = "/theme/images/loading_round.gif";
}



if (document.images) {
	imgLoading = new Image();
	imgLoading.src = "/theme/images/loading_round.gif";
}


function getXMLObject()  //XML OBJECT
{	
   var gobject = false;
   try {
     gobject = new ActiveXObject("Msxml2.XMLHTTP")  // For Old Microsoft Browsers
   }
   catch (e) {
     try {
       gobject = new ActiveXObject("Microsoft.XMLHTTP")  // For Microsoft IE 6.0+
     }
     catch (e2) {
       gobject = false   // No Browser accepts the XMLHTTP Object then false
     }
   }
   if (!gobject && typeof XMLHttpRequest != 'undefined') {
     gobject = new XMLHttpRequest();        //For Mozilla, Opera Browsers
   }
   return gobject;  // Mandatory Statement returning the ajax object created
}
	

function HttpRequestHandler(url, divContainer) {

	var xobject = new getXMLObject();
	var randint = Math.random();
	var getdate = new Date();
		
		xobject.open("GET", url, xobject.onreadystatechange  = function() {
			if (xobject.readyState==4 && xobject.status==200){
				setTimeout(function(){document.getElementById(divContainer).innerHTML = xobject.responseText}, 1000);
			}else {
				 document.getElementById(divContainer).innerHTML="<img src='/theme/images/loading_round.gif' align='center' style='margin:0px auto;'/>";
			}
		});
		xobject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xobject.send();
	} 


