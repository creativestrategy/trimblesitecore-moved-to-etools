

if (document.images) {
	imgLoading = new Image();

	imgLoading.src = "/theme/images/loading_round.gif";
}


function getXMLObject()  //XML OBJECT
{
	
   var gobject = false;
   try {
     gobject = new ActiveXObject("Msxml2.XMLHTTP")  // For Old Microsoft Browsers
   }
   catch (e) {
     try {
       gobject = new ActiveXObject("Microsoft.XMLHTTP")  // For Microsoft IE 6.0+
     }
     catch (e2) {
       gobject = false   // No Browser accepts the XMLHTTP Object then false
     }
   }
   if (!gobject && typeof XMLHttpRequest != 'undefined') {
     gobject = new XMLHttpRequest();        //For Mozilla, Opera Browsers
   }
   return gobject;  // Mandatory Statement returning the ajax object created
}
	

function HttpRequestHandler(url, divContainer) {

	var xobject = new getXMLObject();
	var randint = Math.random();
	var getdate = new Date();
		
		xobject.open("GET", url, xobject.onreadystatechange  = function() {
			if (xobject.readyState==4 && xobject.status==200){
				setTimeout(function(){document.getElementById(divContainer).innerHTML = xobject.responseText}, 1000);
			}else {
				 document.getElementById(divContainer).innerHTML="<img src='/theme/images/loading_round.gif' align='center' style='margin:0px auto;'/>";
			}
		});
		xobject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xobject.send();
	} 


