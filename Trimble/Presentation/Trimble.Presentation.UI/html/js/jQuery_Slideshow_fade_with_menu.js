var $$ = $.fn;
var $maxLoop = 100;
var $currLoop = 0;
var $bannerCount;

$$.extend({
  SplitID : function()
  {
    return this.attr('id').split('-').pop();
  },

  Slideshow : {
    Ready : function()
    {
      $('div.tmpSlideshowControl')
        .hover(
          function() {
            $(this).addClass('tmpSlideshowControlOn');
          },
          function() {
            $(this).removeClass('tmpSlideshowControlOn');
          }
        )
        .click(
          function() {
		        $(this).addClass('tmpSlideshowControlOn');
            $$.Slideshow.Interrupted = false; //set this to true if you want to stop slideshow when buttons are clicked
            $('div.tmpSlide').hide();
            $('div.tmpSlideshowControl').removeClass('tmpSlideshowControlActive');				
            $('div.tmpSlide-' + $(this).SplitID()).fadeIn('slow');
            $(this).addClass('tmpSlideshowControlActive');

			$$.Slideshow.Counter = $(this).SplitID(); //Comment this out if you want to stop slideshow when buttons are clicked
          }
        );

      this.Counter = 1;
      this.Interrupted = false;

      this.Transition();
    },

    Transition : function()
    {
      if (this.Interrupted) {
        return;
      }

      this.Last = this.Counter - 1;

      if (this.Last < 1) {
        this.Last = $bannerCount;
      }

      $('div.tmpSlide-' + this.Last).fadeOut(
        'slow',
        function() {
          $('div#tmpSlideshowControl-' + $$.Slideshow.Last).removeClass('tmpSlideshowControlActive');
          $('div#tmpSlideshowControl-' + $$.Slideshow.Counter).addClass('tmpSlideshowControlActive');
          $('div.tmpSlide-' + $$.Slideshow.Counter).fadeIn('slow');

          $$.Slideshow.Counter++;

          if ($$.Slideshow.Counter > $bannerCount) {			
            $$.Slideshow.Counter = 1;
			$currLoop++;
          }
		  
		  if($currLoop==$maxLoop){
			if($$.Slideshow.Counter == ($bannerCount)){
				$$.Slideshow.Interrupted = true;	
			}
		  }
		
          setTimeout('$$.Slideshow.Transition();', 5000);
        }
      );
    }
  }
});

$(document).ready(
  function () {
      $bannerCount = $(".tmpSlide").length;
      if (typeof Sitecore == 'undefined') {
          $(".tmpSlide").css("display", "none");
          $$.Slideshow.Ready();
      } else {
          if ($("#scWebEditRibbon").attr("src").indexOf("mode=edit") > -1) {
              $(".tmpSlide").css("display", "block");
          } else {
              $(".tmpSlide").css("display", "none");
              $$.Slideshow.Ready();
          }
      }
  }
);


