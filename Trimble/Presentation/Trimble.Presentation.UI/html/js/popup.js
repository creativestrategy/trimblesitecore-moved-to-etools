/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;

//loading popup with jQuery magic!
function loadPopup(){
	//loads popup only if it is disabled
	if(popupStatus==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		$("#backgroundPopup").fadeIn("slow");
		var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#customerlogin_box").height();
	var popupWidth = $("#customerlogin_box").width();
	//centering
	$("#customerlogin_box").css({
		"display": "block",
		"position": "absolute",
		"right": (windowWidth -1000)/2,
		// "top": windowHeight/2-popupHeight/2,
		// "left": windowWidth/2-popupWidth/2
	});
		//$("#customerlogin_box").css("display","block");
		
		$("#customerlogin_box").fadeIn("slow");
		popupStatus = 1;
	}
}

//disabling popup with jQuery magic!
function disablePopup(){
	//disables popup only if it is enabled
	if(popupStatus==1){
		debugger;
		$("#backgroundPopup").fadeOut("slow");
		$("#customerlogin_box").fadeOut("slow");
		popupStatus = 0;
	}
}

//centering popup
function centerPopup(){
	//request data for centering
	
	//only need force for IE6
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	$("#backgroundPopup").css({
		 "height": "100%",
		 "width" : windowWidth
	});
	
}


//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){
	//alert("clicked");
	//LOADING POPUP
	//Click the button event!
	$(".loginbutton").click(function(){
		//centering with css
		//centerPopup();
		loadPopup();
		
	});
				
	//CLOSING POPUP
	//Click the x event!
	$("#popupContactClose").click(function(){
		disablePopup();
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disablePopup();
	});
	//Press Escape event!
	// $(document).keypress(function(e){
		// if(e.keyCode==27 && popupStatus==1){
			// disablePopup();
		// }
	// });

});