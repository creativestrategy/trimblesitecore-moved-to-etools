// getSpryTab.js - version 1.0 
// Created by Jimmy Mercado

////////////////////////////////////////
/*
 This script enables selection of specific tab in SpryTabbedPanels script.
 This script processes the URL parameter "tab" value (tab=TabTitle) aand matches the value to the exisitng TabbedPanelsTab innerHTML value.
*/
//////////////////////////////////////
function getParams(ucStr, paramSeparator, nameValueSeparator)
{	
	var o = new Object;
	if (ucStr)
	{
		if (!paramSeparator) paramSeparator = "&";
		if (!nameValueSeparator) nameValueSeparator = "=";
		var params = ucStr.split(paramSeparator);
		
		for (var i = 0; i < params.length; i++)
		{
			var a = params[i].split(nameValueSeparator);
			var n = decodeURIComponent(a[0]?a[0]:"");
			var v = decodeURIComponent(a[1]?a[1]:"");
			if (v.match(/^0$|^[1-9]\d*$/))
				v = parseInt(v);
			if (typeof o[n] == "undefined")
				o[n] = v;
			else			
			{
				if (typeof o[n] != "object")
				{
					var t = o[n];
					o[n] = new Array;
					o[n].push(t);
				}
				o[n].push(v);
			}
			//alert(o);
		}
	}
	return o;
};


var params = getParams(window.location.search.replace(/^\?/, ""));

function getIndex(){
    var tp1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
    var tabCount = tp1.getTabbedPanelCount();
	var tab = tp1.getTabs();
	var tabTitle = "";
	var tabIndex = 0;
	var tabParam = params.tab.toString();
	
	
	for (var i = 0; i < tabCount; i++)
	{
		
		tabTitle = tab[i].innerHTML.toString().replace(/(<([^>]+)>)/ig, "").trim();
		//alert(tabTitle);
		//alert(tabTitle + " ? " + params.tab);
		//alert(params.tab.replace(" ", "_").toLowerCase());
		tabTitle = tabTitle.replace(/\s/g, "_");
		tabTitle = tabTitle.replace(/&amp;/g, "~");
		
		tabParam = tabParam.replace(/\s/g, "_");
		tabParam = tabParam.replace(/&amp;/g, "~");
		
		if(params.tab!=undefined){			
			if (tabTitle.toLowerCase()==tabParam.toLowerCase()){
				tabIndex = i;
			}
		}

	}
	//alert(tabIndex);
	//alert(tp1.showPanel);
	//tp1.showPanel(tabIndex);
	var tabButtonsArray = tp1.getTabs();
	var tabButton = tabButtonsArray[tabIndex];
	//alert(tabButton.innerHTML);
	tp1.showPanel(tabIndex);

	if(i>=1) scrollToElement(".TabbedPanels");//window.location.hash="tabs";

}

function scrollToElement(element){
	$('html, body').animate({
	   'scrollTop':   $(element).offset().top
	   }, 2000);
	   
	
}

$(document).ready(function () {
    //alert(document.getElementById("TabbedPanels1"));
    if (document.getElementById("TabbedPanels1") != null & params.tab!=undefined) {
        getIndex();
    }
});