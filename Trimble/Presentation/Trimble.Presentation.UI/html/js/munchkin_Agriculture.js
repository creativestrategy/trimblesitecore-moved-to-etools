//Added by Jimmy Mercado - 01-07-2014
(function() {
 
  var url = window.location.href.toString().toLowerCase();
  if((url.indexOf('/agriculture/')!= -1 )){
  var didInit = false;
  function initMunchkin() {
    if(didInit === false) {
      didInit = true;
      Munchkin.init('862-EAP-456');
	  //alert('yes');
    }
  }
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = '//munchkin.marketo.net/munchkin.js';
  s.onreadystatechange = function() {
    if (this.readyState == 'complete' || this.readyState == 'loaded') {
      initMunchkin();
    }
  };
  s.onload = initMunchkin;
  document.getElementsByTagName('head')[0].appendChild(s);
  }
})();
