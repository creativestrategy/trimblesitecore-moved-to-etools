//Added by 		Jimmy Mercado - 03-25-2014
//Modified by 	Jimmy Mercado - 04-08-2015


var url = window.location.href.toString().toLowerCase();
if((url.indexOf('/fsm/')!= -1 )){
	
	(function(i,s,o,g,r,a,m){
		i['GoogleAnalyticsObject']=r;
		i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();
		a=s.createElement(o), m=s.getElementsByTagName(o)[0];
		a.async=1;
		a.src=g;
		m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-38428637-1', 'auto');
	ga('require', 'displayfeatures');
	ga('send', 'pageview');
}


