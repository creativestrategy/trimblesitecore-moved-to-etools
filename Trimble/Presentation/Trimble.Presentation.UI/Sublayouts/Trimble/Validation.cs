﻿using System.Runtime.Serialization;
using Sitecore.Data.Validators;
using System;
using Sitecore.Data.Items;
using System.Text;

namespace Trimble.Presentation.UI.Sublayouts.Trimble
{
    [Serializable]
    public class Validation : Sitecore.Data.Validators.StandardValidator
    {
        public Validation() { }
        public Validation(SerializationInfo info, StreamingContext context)
: base(info, context) { }

        protected override ValidatorResult Evaluate()
        {
            // set initial return value
            ValidatorResult result = ValidatorResult.Valid;
            // grab the target ImageField
            Sitecore.Data.Fields.LinkField link = GetField();
            /* ignore blank fields
             * (use the Sitecore-supplied Required validator if you want blank fields to fail) */
            if (link.Value == "") return result;
            // use a StringBuilder for constructing the result Text
            StringBuilder txt = new StringBuilder("URL should not be empty");

            // are we checking width?
            if (Parameters.ContainsKey("url"))
                // mismatch

                if (String.IsNullOrEmpty(link.Url))
                {
                    result = ValidatorResult.FatalError;
                    Text = "URL should not be empty for the links";
                }

            return result;
        }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return (GetFailedResult(ValidatorResult.FatalError));
        }

        public override string Name
        {
            get
            {
                return ("URLValidator");
            }
        }
    }
}









