﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SocialMedia.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.SocialMedia" %>
    <script type="text/javascript">
        var lhmSocialLinks = Array();
        <asp:Repeater runat="server" ID="rptLink">
            <ItemTemplate>lhmSocialLinks.push("<asp:Literal runat="server" ID="linkSource" Text="<%# Container.DataItem %>" />");</ItemTemplate>
        </asp:Repeater>
    </script>
    <div id="divTitle" class="heading" runat="server" visible="false">
        <span>
        <sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" DisableWebEditing="false" visible="false"/>
        </span>
    </div>

   <div class="socialicons-container">
       <ul class="socialicons marginLeft10">             
            <asp:Repeater runat="server" ID="rptData" onitemdatabound="rptData_ItemDataBound">
             <ItemTemplate>   
              <li>                        
                    <sc:EditFrame runat="server" ID="efActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/LinkRendererActions">
                         <sc:Link ID="lnkSocialMediaTarget" runat="server" Field="SocialMediaTarget" DisableWebEditing="false" >                   
                            <sc:FieldRenderer ID="frSocialMediaLogo" runat="server" FieldName="SocialMediaLogo" DisableWebEditing="true"/>                   
                        </sc:Link>
                    </sc:EditFrame>   
                     </li>                
                </ItemTemplate>
            </asp:Repeater>
       
        <asp:Literal runat="server" ID="litWrapperHtmlEnd" />

        <!-- CSS guys need to fix this layout -->
     <sc:EditFrame  runat="server" ID="efAddLink" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/LinkRendererAddLink">
        <div id="AddNewLink" runat="server">
            Add Social Media
        </div>
      </sc:EditFrame>
    </ul>
</div>







