﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePageSimplePlaceholderRight.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.HomePageSimplePlaceholderRight" %>
<div class="content1">
    <div class="headerContainer">
        <h1 class="trimbleNewsHeader completeWidth">
            <sc:FieldRenderer ID="frHomepageSimpleTitle" runat="server" FieldName="Title" />
        </h1>
    </div>
    <div class="front_full_image" id="divImage" runat="server">
        <sc:FieldRenderer ID="frHomepageSimpleImage" Width="300px" Height="200px" runat="server"
            FieldName="Image" />
    </div>
    <div class="indexPlaceholderType" id="divFreeText" runat="server">
        <p>
            <sc:FieldRenderer ID="frHomepageSimpleFreeText" runat="server" FieldName="FreeText" />
        </p>
    </div>
    <span class="productHomeQuote" id="spanQuote" runat="server">
        <sc:FieldRenderer ID="frHomepageSimpleQuote" runat="server" FieldName="Quote" />
    </span>
</div>
