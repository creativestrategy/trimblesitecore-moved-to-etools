﻿#region System
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;
#endregion

#region Sitecore
using Sitecore.Data.Items;
using Sitecore.Web.UI;
#endregion

#region Deloitte Framework
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;
using Deloitte.SC.UI.Sublayouts.Framework;
#endregion

#region Namespace

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public class TrimbleBreadCrumb : WebControl
    {

        #region Properties
        protected int breadcrumbItem;
        protected int quickNavigation;
        private string _fieldNameLinkItem = "LinkItem";
        #endregion

        protected virtual Item RootItem { get; set; }

        #region Public class

        #region Protected Methods

        //Method to render breadcrumb items
        protected override void DoRender(System.Web.UI.HtmlTextWriter output)
        {
           
            string cacheValue = SitecoreItem.CurrentItem.ID.ToString();

            if (System.Web.HttpContext.Current.Application[cacheValue] != null)
            {
                RootItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(cacheValue).ToString());
            }

            else
                RootItem = SitecoreItem.CurrentItem;
            if (RootItem != null)
            {
                RenderBreadcrumb(GetBreadcrumbNavigationItems(RootItem), output);
            }
        }

        #endregion

        #region Private Methods

        private void RenderBreadcrumb(List<Item> source, HtmlTextWriter output)
        {
            try
            {
                //Condition if breadcrumb item source is not null               
                if (source != null)
                {

                    if (source.Count < 6)
                    {
                        // Add breadcrumb css from stylesheet
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumb_link");

                        //getting friendly URL for item
                        output.AddAttribute(HtmlTextWriterAttribute.Href, SitecoreItem.HomeItem.GetFriendlyUrl());

                        //Add anchor teg to display navigation link
                        output.RenderBeginTag(HtmlTextWriterTag.A);

                        // A title/ description
                        output.Write(SitecoreItem.GetBreadcrumbTitle(SitecoreItem.HomeItem));

                        // close rendering A tag
                        output.RenderEndTag();
                        int ItemCount = 0;
                        foreach (Item item in source)
                        {                    
                           
                            Item linkItem = item.GetFieldValueAsItem(_fieldNameLinkItem);
                            if (linkItem != null)
                            {
                                if (SitecoreItem.CurrentItem.Parent.Name == "Agriculture" )
                                {
                                    
                                }
                                else if(SitecoreItem.CurrentItem.Parent.Name == "fsm" || SitecoreItem.CurrentItem.Parent.Name == "Survey" || SitecoreItem.CurrentItem.Parent.Name == "MGIS")
                                {
                                   if (linkItem.Name == "ListTemplate")
                                    {
                                        if (ItemCount == 0)
                                        {
                                            ItemCount++;
                                            continue;
                                        }
                                    }
                                }
                                // Add breadcrumb css from stylesheet
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumb_link");

                                //getting friendly URL for item
                                output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());

                                //Add anchor teg to display navigation link
                                output.RenderBeginTag(HtmlTextWriterTag.A);

                                // A title/ description
                                output.Write(SitecoreItem.GetBreadcrumbTitle(linkItem));

                                // close rendering A tag
                                output.RenderEndTag();
                               
                            }
                          
                        }
                    }
                    if (source.Count > 5)
                    {

                        // Add breadcrumb css from stylesheet
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumb_link");

                        //getting friendly URL for item
                        output.AddAttribute(HtmlTextWriterAttribute.Href, SitecoreItem.HomeItem.GetFriendlyUrl());

                        //Add anchor teg to display navigation link
                        output.RenderBeginTag(HtmlTextWriterTag.A);

                        // A title/ description
                        output.Write(SitecoreItem.HomeItem.Name);

                        // close rendering A tag
                        output.RenderEndTag();

                        int i = 0;
                        breadcrumbItem = (source.Count) - 3;
                        quickNavigation = (source.Count) - 3;
                        foreach (Item item in source)
                        {

                            Item linkItem = item.GetFieldValueAsItem(_fieldNameLinkItem);
                            if (linkItem != null)
                            {
                                if (i == quickNavigation)
                                {
                                    // Add breadcrumb css from stylesheet
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumb_link");

                                    //getting friendly URL for item
                                    output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());

                                    //Add anchor teg to display navigation link
                                    output.RenderBeginTag(HtmlTextWriterTag.A);

                                    // A title/ description
                                    output.Write("...");

                                    // close rendering A tag
                                    output.RenderEndTag();

                                }
                                if (i > breadcrumbItem)
                                {
                                    // Add breadcrumb css from stylesheet
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumb_link");

                                    //getting friendly URL for item
                                    output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());

                                    //Add anchor teg to display navigation link
                                    output.RenderBeginTag(HtmlTextWriterTag.A);

                                    // A title/ description
                                    output.Write(SitecoreItem.GetBreadcrumbTitle(linkItem));

                                    // close rendering A tag
                                    output.RenderEndTag();
                                }

                                i++;
                            }
                        }
                       
                    }

                    output.Write(SitecoreItem.GetBreadcrumbTitle(SitecoreItem.CurrentItem));
                  
                }

            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("TrimbleBreadCrumb:rptData_ItemDataBound" + ex.Message);
            }

        }

        #endregion



        /// <summary>
        /// Gets the breadcrumb items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public List<Item> GetBreadcrumbNavigationItems(Item item)
        {           
            // HideFromAll and HideFromBreadcrumb must not selected
            return item.Axes.GetAncestors().Where(c => c.Axes.Level >= SitecoreItem.CurrentItem.Axes.Level).ToList();
          
        }
    }

        #endregion
}

#endregion

