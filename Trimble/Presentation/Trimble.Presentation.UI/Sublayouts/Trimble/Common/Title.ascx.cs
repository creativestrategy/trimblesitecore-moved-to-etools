﻿using System;
using System.Web.UI;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public partial class Title : SublayoutBaseExtended
    {
        #region Properties

        Item dataSourceItem = null;

        #endregion

        protected Item GetDataSourceItem()
        {
           
                const string path = "/sitecore/content/Website/common/Titles/HomeTitle";
                dataSourceItem = Sitecore.Context.Database.GetItem(path);
                return dataSourceItem;
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Condition to check is page postback
            if (!Page.IsPostBack)
            {

                if (SitecoreItem.CurrentItem.Name == CommonFields.Home)
                {
                    GetDataSourceItem();
                }
                else
                {
                    dataSourceItem = DataSourceItem;
                }


                frTitle.Item = dataSourceItem;
            }
        }
    }
}