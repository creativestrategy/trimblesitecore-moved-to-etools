﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterRibbonDisplay_ORIG.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer.FooterRibbonDisplay_ORIG" %>
<div class="footer_menu">
    <div class="links">
        <sc:Link ID="lnkcorporatelink1" runat="server" Field="CorporateLink1" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink2" runat="server" Field="CorporateLink2" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink3" runat="server" Field="CorporateLink3" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink4" runat="server" Field="CorporateLink4" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink5" runat="server" Field="CorporateLink5" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink6" runat="server" Field="CorporateLink6" CssClass="sitemap"/>
       </div>
</div>
