﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer
{
    public partial class FooterDisplay : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
              AssigningData();   
        }

        public void AssigningData()
        {
            try
            {
                if (DataSourceItem != null)
                {
                    //lnkCopyrights.Item = DataSourceItem;
                    //lblCopyright.Text = DateTime.Now.Year.ToString();
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("FooterDisplay:AssigningData" + ex.Message);
            }
        }
    }
}