﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterRibbonDisplay.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer.FooterRibbonDisplay" %>

<%
    string sc_curLang = Sitecore.Context.Language.ToString().ToUpper();
    if (sc_curLang == "PT-BR"){
        lblCopyright.Text = "Copyright";         
    }else if (sc_curLang == "ZH-CN"){
        lblCopyright.Text = "版权";      
    }else{
        lblCopyright.Text = "Copyright";  
    }
%>

<div class="footer_menu">
    <div class="links">
        <sc:Link ID="lnkcorporatelink1" runat="server" Field="CorporateLink1" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink2" runat="server" Field="CorporateLink2" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink3" runat="server" Field="CorporateLink3" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink4" runat="server" Field="CorporateLink4" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink5" runat="server" Field="CorporateLink5" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink6" runat="server" Field="CorporateLink6" CssClass="sitemap"/>
        <sc:Link ID="lnkcorporatelink7" runat="server" Field="CorporateLink7" DisableWebEditing="true" CssClass="sitemap">© <asp:label id="lblCopyright" runat="server"/> <asp:label id="lblCopyrightYear" runat="server"/>, Trimble Inc.</sc:Link>
    </div>
</div>
