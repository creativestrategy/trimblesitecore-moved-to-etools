﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterQuickLinks.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer.FooterQuickLinks" %>

    <div>
        <div class="footer_heading margl10">
            <a id="Column1titlerenderer" runat="server"><sc:FieldRenderer ID="frcolumn1title" runat="server" FieldName="Column1Title" /></a>
            <div class="padd8"></div>
            <div class="footer_links">
                <ul class="footerli">
                    <li>
                        <sc:Link ID="lnkcolumn1categorylink" runat="server" Field="Column1CategoryLink" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn1link1" runat="server" Field="Column1Link1" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn1link2" runat="server" Field="Column1Link2" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn1link3" runat="server" Field="Column1Link3" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn1link4" runat="server" Field="Column1Link4" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn1link5" runat="server" Field="Column1Link5" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn1link6" runat="server" Field="Column1Link6" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn1link7" runat="server" Field="Column1Link7" />
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class="footer_heading">
            <a id="Column2titlerenderer" runat="server"><sc:FieldRenderer ID="frcolumn2title" runat="server" FieldName="Column2Title" /></a>
            <div class="padd8"> </div>
            <div class="footer_links">
                <ul class="footerli">
                    <li>
                        <sc:Link ID="lnkcolumn2categorylink" runat="server" Field="Column2CategoryLink" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn2link1" runat="server" Field="Column2Link1" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn2link2" runat="server" Field="Column2Link2" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn2link3" runat="server" Field="Column2Link3" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn2link4" runat="server" Field="Column2Link4" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn2link5" runat="server" Field="Column2Link5" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn2link6" runat="server" Field="Column2Link6" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn2link7" runat="server" Field="Column2Link7" />
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class="footer_heading">
            <a id="Column3titlerenderer" runat="server"><sc:FieldRenderer ID="frcolumn3title" runat="server" FieldName="Column3Title" /></a>
            <div class="padd8"> </div>
            <div class="footer_links">
                <ul class="footerli">
                    <li>
                        <sc:Link ID="lnkcolumn3categorylink" runat="server" Field="Column3CategoryLink" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn3link1" runat="server" Field="Column3Link1" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn3link2" runat="server" Field="Column3Link2" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn3link3" runat="server" Field="Column3Link3" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn3link4" runat="server" Field="Column3Link4" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn3link5" runat="server" Field="Column3Link5" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn3link6" runat="server" Field="Column3Link6" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn3link7" runat="server" Field="Column3Link7" />
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class="footer_heading">
            <a id="Column4titlerenderer" runat="server"><sc:FieldRenderer ID="frcolumn4title" runat="server" FieldName="Column4Title" /></a>
            <div class="padd8"> </div>
            <div class="footer_links">
                <ul class="footerli">
                    <li>
                        <sc:Link ID="lnkcolumn4categorylink" runat="server" Field="Column4CategoryLink" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn4link1" runat="server" Field="Column4Link1" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn4link2" runat="server" Field="Column4Link2" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn4link3" runat="server" Field="Column4Link3" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn4link4" runat="server" Field="Column4Link4" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn4link5" runat="server" Field="Column4Link5" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn4link6" runat="server" Field="Column4Link6" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn4link7" runat="server" Field="Column4Link7" />
                    </li>
                    
                </ul>
            </div>
        </div>
		<!--
        <div class="footer_heading" style="display:none;">
            <a id="Column5titlerenderer" runat="server"><sc:FieldRenderer ID="frcolumn5title" runat="server" FieldName="Column5Title" /></a>
            <div class="padd8"> </div>
            <div class="footer_links">
                <ul class="footerli">
                    <li>
                        <sc:Link ID="lnkcolumn5categorylink" runat="server" Field="Column5CategoryLink" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn5link1" runat="server" Field="Column5Link1" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn5link2" runat="server" Field="Column5Link2" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn5link3" runat="server" Field="Column5Link3" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn5link4" runat="server" Field="Column5Link4" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn5link5" runat="server" Field="Column5Link5" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn5link6" runat="server" Field="Column5Link6" />
                    </li>
                    <li>
                        <sc:Link ID="lnkcolumn5link7" runat="server" Field="Column5Link7" />
                    </li>
                    
                </ul>
            </div>
        </div>
		-->
    </div>
  