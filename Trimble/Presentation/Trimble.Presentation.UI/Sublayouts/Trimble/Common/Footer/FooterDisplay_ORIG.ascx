﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterDisplay_ORIG.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer.FooterDisplay" %>
    <div class="copyrights"><sc:Link ID="lnkCopyrights" runat="server" Field="Copyrights" DisableWebEditing="true" CssClass="sitemap">© Copyright <asp:label id="lblCopyright" runat="server"/>, Trimble Inc.
</sc:Link>
 </div>
