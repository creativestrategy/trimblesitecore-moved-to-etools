﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer
{
    public partial class FooterRibbonDisplay : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AssigningData();  
            try
            {
                lnkcorporatelink1.Item = lnkcorporatelink2.Item = lnkcorporatelink3.Item = lnkcorporatelink6.Item = DataSourceItem;
                //Removed from code as per the discussion
                //lnkcorporatelink4.Item =lnkcorporatelink5.Item
                
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("FooterRibbonDisplay:Page_Load"+ex.Message);
            }
        }

        public void AssigningData()
        {
            try
            {
                if (DataSourceItem != null)
                {
                    lnkcorporatelink7.Item = DataSourceItem;
                    lblCopyrightYear.Text = DateTime.Now.Year.ToString();
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("FooterDisplay:AssigningData" + ex.Message);
            }
        }
    }
}