﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.HtmlControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Web.UI.WebControls;
using Trimble.Presentation.UI.Sublayouts.Trimble;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer
{
    public partial class FooterQuickLinks : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            AssignLinkUrl();

            MakingLinkInvisible();

            AssigningDataSource();

            IsEditable();

        }

        #region HomepageFooterEditable
        protected void IsEditable()
        {
            try
            {
                Sublayout sublayout = this.Parent as Sublayout;
                if (sublayout != null)
                {
                    String sublayoutPath = sublayout.Path;
                    Placeholder placeholder = sublayout.Parent as Placeholder;
                    //the sitecore placeholder key
                    string urlToTrack = HttpContext.Current.Request.Url.AbsolutePath;
                    if ((placeholder != null) && !(urlToTrack == @"/") && (Sitecore.Context.PageMode.IsPageEditorEditing))
                    {
                        placeholder.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("FooterQuickLinks:IsEditable"+ex.Message);
            }
        }
        #endregion


        #region GetUrl Definition

        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {

            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    // Use LinkMananger for internal links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    // Use MediaManager for media links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    // Just return external links 
                    return lnkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty 
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link 
                    return lnkField.Url;
                case "javascript":
                    // Just return javascript 
                    return lnkField.Url;
                default:
                    // Just please the compiler, this 
                    // condition will never be met 
                    return lnkField.Url;
            }

        }

        #endregion

        #region AssignLinkUrl

        //Getting the url from the external link and assiging it to the Href Attribute
        public void AssignLinkUrl()
        {
            try
            {
                Sitecore.Data.Fields.LinkField linkFieldcolumn1 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[FooterModule.Column1CategoryLink]);
                string urlcolumn1 = GetURL(linkFieldcolumn1);

                Sitecore.Data.Fields.LinkField linkFieldcolumn2 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[FooterModule.Column2CategoryLink]);
                string urlcolumn2 = GetURL(linkFieldcolumn2);

                Sitecore.Data.Fields.LinkField linkFieldcolumn3 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[FooterModule.Column3CategoryLink]);
                string urlcolumn3 = GetURL(linkFieldcolumn3);

                Sitecore.Data.Fields.LinkField linkFieldcolumn4 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[FooterModule.Column4CategoryLink]);
                string urlcolumn4 = GetURL(linkFieldcolumn4);

                Sitecore.Data.Fields.LinkField linkFieldcolumn5 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[FooterModule.Column5CategoryLink]);
                string urlcolumn5 = GetURL(linkFieldcolumn5);


                Column1titlerenderer.HRef = urlcolumn1;

                Column2titlerenderer.HRef = urlcolumn2;

                Column3titlerenderer.HRef = urlcolumn3;

                Column4titlerenderer.HRef = urlcolumn4;

                Column5titlerenderer.HRef = urlcolumn5;

            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("FooterQuickLinks:AssignLinkUrl" + ex.Message);
            }

        }

        #endregion

        #region MakingLinkInvisible

        //Making the Category Links Invisible on the page
        public void MakingLinkInvisible()
        {

            lnkcolumn5categorylink.Visible = lnkcolumn4categorylink.Visible = lnkcolumn3categorylink.Visible =

                lnkcolumn2categorylink.Visible = lnkcolumn1categorylink.Visible = SitecoreItem.IsPageEditorEditing;

        }

        #endregion

        #region AssigningDataSource

        public void AssigningDataSource()
        {
            try
            {
                frcolumn1title.Item = lnkcolumn1categorylink.Item = lnkcolumn1link1.Item = lnkcolumn1link2.Item = lnkcolumn1link3.Item = lnkcolumn1link4.Item = frcolumn2title.Item = lnkcolumn2categorylink.Item =

               lnkcolumn2link1.Item = lnkcolumn2link2.Item = lnkcolumn2link3.Item = lnkcolumn2link4.Item = frcolumn3title.Item = lnkcolumn3categorylink.Item =

               lnkcolumn3link1.Item = lnkcolumn3link2.Item = lnkcolumn3link3.Item = lnkcolumn3link4.Item = frcolumn4title.Item = lnkcolumn4categorylink.Item =

               lnkcolumn4link1.Item = lnkcolumn4link2.Item = lnkcolumn4link3.Item = lnkcolumn4link4.Item = frcolumn5title.Item = lnkcolumn5categorylink.Item =

               lnkcolumn5link1.Item = lnkcolumn5link2.Item = lnkcolumn5link3.Item = lnkcolumn5link4.Item = DataSourceItem;

                lnkcolumn1link5.Item = lnkcolumn1link6.Item = lnkcolumn1link7.Item = lnkcolumn2link5.Item = lnkcolumn2link6.Item = lnkcolumn2link7.Item =

                lnkcolumn3link5.Item = lnkcolumn3link6.Item = lnkcolumn3link7.Item = lnkcolumn4link5.Item = lnkcolumn4link6.Item = lnkcolumn4link7.Item =

                lnkcolumn5link5.Item = lnkcolumn5link6.Item = lnkcolumn5link7.Item = DataSourceItem;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("FooterQuickLinks:AssigningData"+ex.Message);
            }
        }
        #endregion
    }
}