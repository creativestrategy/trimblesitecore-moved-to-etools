﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common.Footer {
    
    
    public partial class FooterQuickLinks {
        
        /// <summary>
        /// Column1titlerenderer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor Column1titlerenderer;
        
        /// <summary>
        /// frcolumn1title control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.FieldRenderer frcolumn1title;
        
        /// <summary>
        /// lnkcolumn1categorylink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1categorylink;
        
        /// <summary>
        /// lnkcolumn1link1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1link1;
        
        /// <summary>
        /// lnkcolumn1link2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1link2;
        
        /// <summary>
        /// lnkcolumn1link3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1link3;
        
        /// <summary>
        /// lnkcolumn1link4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1link4;
        
        /// <summary>
        /// lnkcolumn1link5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1link5;
        
        /// <summary>
        /// lnkcolumn1link6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1link6;
        
        /// <summary>
        /// lnkcolumn1link7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn1link7;
        
        /// <summary>
        /// Column2titlerenderer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor Column2titlerenderer;
        
        /// <summary>
        /// frcolumn2title control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.FieldRenderer frcolumn2title;
        
        /// <summary>
        /// lnkcolumn2categorylink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2categorylink;
        
        /// <summary>
        /// lnkcolumn2link1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2link1;
        
        /// <summary>
        /// lnkcolumn2link2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2link2;
        
        /// <summary>
        /// lnkcolumn2link3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2link3;
        
        /// <summary>
        /// lnkcolumn2link4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2link4;
        
        /// <summary>
        /// lnkcolumn2link5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2link5;
        
        /// <summary>
        /// lnkcolumn2link6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2link6;
        
        /// <summary>
        /// lnkcolumn2link7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn2link7;
        
        /// <summary>
        /// Column3titlerenderer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor Column3titlerenderer;
        
        /// <summary>
        /// frcolumn3title control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.FieldRenderer frcolumn3title;
        
        /// <summary>
        /// lnkcolumn3categorylink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3categorylink;
        
        /// <summary>
        /// lnkcolumn3link1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3link1;
        
        /// <summary>
        /// lnkcolumn3link2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3link2;
        
        /// <summary>
        /// lnkcolumn3link3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3link3;
        
        /// <summary>
        /// lnkcolumn3link4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3link4;
        
        /// <summary>
        /// lnkcolumn3link5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3link5;
        
        /// <summary>
        /// lnkcolumn3link6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3link6;
        
        /// <summary>
        /// lnkcolumn3link7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn3link7;
        
        /// <summary>
        /// Column4titlerenderer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor Column4titlerenderer;
        
        /// <summary>
        /// frcolumn4title control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.FieldRenderer frcolumn4title;
        
        /// <summary>
        /// lnkcolumn4categorylink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4categorylink;
        
        /// <summary>
        /// lnkcolumn4link1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4link1;
        
        /// <summary>
        /// lnkcolumn4link2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4link2;
        
        /// <summary>
        /// lnkcolumn4link3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4link3;
        
        /// <summary>
        /// lnkcolumn4link4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4link4;
        
        /// <summary>
        /// lnkcolumn4link5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4link5;
        
        /// <summary>
        /// lnkcolumn4link6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4link6;
        
        /// <summary>
        /// lnkcolumn4link7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn4link7;
        
        /// <summary>
        /// Column5titlerenderer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor Column5titlerenderer;
        
        /// <summary>
        /// frcolumn5title control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.FieldRenderer frcolumn5title;
        
        /// <summary>
        /// lnkcolumn5categorylink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5categorylink;
        
        /// <summary>
        /// lnkcolumn5link1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5link1;
        
        /// <summary>
        /// lnkcolumn5link2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5link2;
        
        /// <summary>
        /// lnkcolumn5link3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5link3;
        
        /// <summary>
        /// lnkcolumn5link4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5link4;
        
        /// <summary>
        /// lnkcolumn5link5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5link5;
        
        /// <summary>
        /// lnkcolumn5link6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5link6;
        
        /// <summary>
        /// lnkcolumn5link7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Link lnkcolumn5link7;
    }
}
