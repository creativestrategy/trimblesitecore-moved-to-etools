﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RandomizedPlaceHolder.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.RandomizedPlaceHolder" %>
<div class="headerContainer">
<h1 class="trimbleNewsHeader completeWidth">
    <sc:FieldRenderer ID="frRandomizedTitle" runat="server" FieldName="TItle" />
</h1>
</div>
<div id="divImage" runat="server">
    <sc:FieldRenderer ID="frRandomizedImage" Width="300px" Height="200px" runat="server"
        FieldName="Image" />
</div>
<div runat="server" id="divFreeText">
    <sc:FieldRenderer ID="frRandomizedFreeText" runat="server" FieldName="FreeText" />
</div>
<span class="productHomeQuote" runat="server" id="spanQuote">
    <sc:FieldRenderer ID="frRandomizedQuote" runat="server" FieldName="Quote" />
</span>