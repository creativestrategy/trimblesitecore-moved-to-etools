﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;


namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public partial class TrimbleBanner : SublayoutBaseExtended
    {
        int i = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (base.SupportsTemplates("{26697D39-3633-4E60-88FD-DE93C38B0127}"))
                {
                    Populate();
                }
                else
                {
                    this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                }
            }
        }

        public virtual void Populate()
        {
            try
            {
                // Get banners folder item
                Item bannersFolderItem = DataSourceItem;
                if (bannersFolderItem != null)
                {
                    List<Item> rotatingBanners = new List<Item>();

                    int index = 1;

                    foreach (Item i in bannersFolderItem.GetChildren())
                    {
                        rotatingBanners.Add(i);
                        if (index == 6)
                            break;
                        index++;

                    }



                    if (rotatingBanners.Count < 7)
                    {
                        // bind items
                        rptData.DataSource = bannersFolderItem.Children;
                        rptData.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("TrimbleBanner:Populate" + ex.Message);
            }
        
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {           
            if (e.IsListingItem())
            {
                try
                {
                    Item item = e.GetSitecoreItem();
                    e.FindAndSetFieldRenderer("frBannerTitle", item);
                    e.FindAndSetFieldRenderer("frBannerImage", item);
                    e.FindAndSetFieldRenderer("frBannerTeaser", item);


                    string bTitle = item["BannerTitle"].ToString();
                    string bTeaser = item["BannerTeaserRichText"].ToString();

                    if (bTitle == "" && bTeaser == "")
                    {
                        var div = e.FindControl<HtmlGenericControl>("divBGOnOff");
                        div.Attributes.Add("class", "allinone_thumbnailsBanner_text_line_noBackground");
                    }


                    if (item != null)
                    {

                        string divClass = "tmpSlide tmpSlide-" + i;
                        var div = e.FindControl<HtmlGenericControl>("innerBanner");
                        div.Attributes.Add("class", divClass);

                        i++;
                    }
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("TrimbleBanner:rptData_ItemDataBound" + ex.Message);
                }
            }
        }


        
    }
}