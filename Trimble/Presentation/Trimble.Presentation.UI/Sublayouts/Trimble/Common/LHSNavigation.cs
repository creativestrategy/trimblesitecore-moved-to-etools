﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;


using Sitecore.Resources;
using Sitecore.Globalization;
using Sitecore.Diagnostics;
using System.Collections.Specialized;
using Sitecore.Shell.Framework.CommandBuilders;

using Sitecore.Data.Fields;


namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public class LHSNavigation : WebControl
    {
        #region private variables

        private string _firstElementClass = "first";
        private string _lastElementClass = "last";
        private string _activeElementClass = "active";
        private string _fieldNameLiId = "LiId";
        private string _fieldNameLinkItem = "LinkItem";
        private string _fieldNameCssClass = "CssClass";
        private string _fieldNameGeneralLink = "ExistingContentPageLink";

        #endregion

        #region Public Properties

        public virtual string FirstElementClass
        {
            get { return _firstElementClass; }
            set { _firstElementClass = value; }
        }

        public virtual string LastElementClass
        {
            get { return _lastElementClass; }
            set { _lastElementClass = value; }
        }

        public virtual string ActiveElementClass
        {
            get { return _activeElementClass; }
            set { _activeElementClass = value; }
        }

        public virtual string FieldNameLiId
        {
            get { return _fieldNameLiId; }
            set { _fieldNameLiId = value; }
        }

        public virtual string FieldNameLinkItem
        {
            get { return _fieldNameLinkItem; }
            set { _fieldNameLinkItem = value; }
        }

        public virtual string FieldNameCssClass
        {
            get { return _fieldNameCssClass; }
            set { _fieldNameCssClass = value; }
        }

        public virtual string FieldNameGeneralLink
        {
            get { return _fieldNameGeneralLink; }
            set { _fieldNameGeneralLink = value; }
        }
        public virtual int LevelFrom { get; set; }

        public virtual int LevelTo { get; set; }

        /// <summary>
        /// Gets or sets the type of the menu.
        ///     Main & Sub
        /// </summary>
        /// <value>
        /// The type of the menu.
        /// </value>
        public virtual string MenuType { get; set; }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);



                // Set homepage item
                HomePageItem = SitecoreItem.HomeItem;

                // Set current item
                CurrentMenuItem = SitecoreItem.CurrentItem;

                //RootItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Cache.Get(CurrentMenuItem.ID.ToString()).ToString());

                string cacheValue = CurrentMenuItem.ID.ToString();



                if (System.Web.HttpContext.Current.Application[cacheValue] != null)
                {
                    RootItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(cacheValue).ToString());
                }

                //else
                //    RootItem = HomePageItem;


                // Set menu item
                SetMenuItem();
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:onInit" + ex.Message);
            }
        }

        public override void DataBind()
        {
            try
            {
                base.DataBind();
                Sitecore.Data.Items.Item item = Sitecore.Context.Item;
                Sitecore.Data.ItemUri uri = item.Uri;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:DataBind" + ex.Message);
            }
        }

        protected override void DataBind(bool raiseOnDataBinding)
        {
            try
            {
                base.DataBind(raiseOnDataBinding);
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:DataBind(bool)" + ex.Message);
            }
        }

        /// <summary>
        /// Traverses up the tree from the current item and returns the closest landing page. 
        /// If none found, returns null.
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        protected Item FindLandingPage(Item currentItem)
        {
            // Landing page
            Item landingPage = null;
            try
            {

                // Get ancestors of current item
                var ancestors = currentItem.Axes.GetAncestors();

                // Traverse up the tree and find the first landing page
                for (var i = ancestors.Length - 1; i >= 0; i--)
                {
                    var ancestor = ancestors[i];
                    if (SitecoreItem.HomeItem.ID == ancestor.ID)
                    {
                        if (SitecoreItem.HomeItem.ID == ancestor.ID)
                        {
                            // special case where we have a page living outside the IA.
                            // set the landing page item to the relevant child of the home page

                            landingPage = (i == ancestors.Length - 1) ? currentItem : ancestors[i + 1];
                        }
                        else
                        {

                            // Found it!
                            landingPage = ancestor;
                        }

                        break;

                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:FindLnadingPage" + ex.Message);
            }

            return landingPage;
        }

        /// <summary>
        /// Sets the menu item.
        /// </summary>
        private void SetMenuItem()
        {
            try
            {
                if (LevelFrom == 1) // Top navigation must created within Configuration and assigned as datasouce
                {
                    MenuItem = SitecoreItem.GetItem(DataSource);
                }
                else // for second level navigation onwards
                {
                    // assign current tiem to MenuItem

                    //MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Cache.Get(CurrentMenuItem.ID.ToString()).ToString());

                    MenuItem = RootItem;

                    // if MenuType != "sub" 
                    if (!string.IsNullOrEmpty(MenuType))
                    {
                        int moveLevels = 0;

                        if (MenuType.ToLower().Trim() != "sub")
                        {
                            // calculate levels and reassigne menuitem (e.g. current item is on level 3 and only display level 2 menu)
                            moveLevels = (MenuItem.Axes.Level - (LevelFrom + (SitecoreItem.HomeItem.Axes.Level) - 1));

                            // change menu items if moveLevels is more then 0
                            if (moveLevels > 0)
                            {
                                // assign menu item as per level specified
                                ChangeMenuItem(moveLevels);

                                // Move one level up and then get children
                                MenuItem = MenuItem.Parent;
                            }
                        }
                        else if (MenuType.ToLower().Trim() == "sub")
                        {
                            // calculate levels and reassigne menuitem (e.g. current item is on level 3 and only display level 2 menu)
                            //moveLevels = (MenuItem.Axes.Level - (LevelFrom + (SitecoreItem.HomeItem.Axes.Level) - 1));
                            moveLevels = (MenuItem.Axes.Level - (LevelFrom + (HomePageItem.Axes.Level) - 1));
                            //moveLevels = 2;
                            // change menu items if moveLevels is more then 0

                            /*
                            if (moveLevels > 0)
                            {
                                if (RootItem.Parent.Name == "Agriculture" || RootItem.Parent.Parent.Name == "Agriculture" || RootItem.Parent.Parent.Parent.Name == "Agriculture" || RootItem.Parent.Parent.Parent.Parent.Name == "Agriculture")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "Survey" || RootItem.Parent.Parent.Name == "Survey" || RootItem.Parent.Parent.Parent.Name == "Survey")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "FSM" || RootItem.Parent.Parent.Name == "FSM" || RootItem.Parent.Parent.Parent.Name == "FSM")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "MGIS" || RootItem.Parent.Parent.Name == "MGIS" || RootItem.Parent.Parent.Parent.Name == "MGIS")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "Geo Spatial" || RootItem.Parent.Parent.Name == "Geo Spatial" || RootItem.Parent.Parent.Parent.Name == "Geo Spatial")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (MenuItem.Name == "FSM" || MenuItem.Name == "Agriculture" || MenuItem.Name == "Survey" || MenuItem.Name == "MGIS" || MenuItem.Name == "Geo Spatial")
                                {
                                    MenuItem= MenuItem;
                                }
                                else
                                    // assign menu item as per level specified
                                    ChangeMenuItem(moveLevels);
                            }
                            */

                            if (moveLevels > 0)
                            {
                                string itemId = string.Empty;

                                foreach (Item child in CurrentMenuItem.Parent.Children)
                                {
                                    if (child.Name.ToLower() == "index")
                                    {
                                        itemId = child.ID.ToString();
                                    }
                                    //System.Web.HttpContext.Current.Response.Write("<!--child.Name=" + child.Name + "-->" + System.Environment.NewLine);
                                }

                                if (itemId != null && itemId != "")
                                {
                                    if (System.Web.HttpContext.Current.Application[itemId] != null)
                                    {
                                        MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                        //System.Web.HttpContext.Current.Response.Write("<!--itemId not Null MenuItem.Name=" + MenuItem.Name.ToUpper() + "-->" + System.Environment.NewLine);
                                    }
                                    else
                                    {
                                        MenuItem = SitecoreItem.GetItem(itemId);
                                        //System.Web.HttpContext.Current.Response.Write("<!--itemId Null MenuItem.Name=" + MenuItem.Name.ToUpper() + "-->" + System.Environment.NewLine);
                                    }
                                }

                                ChangeMenuItem(moveLevels);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:SetMenuItem" + ex.Message);
            }
        }

        /// <summary>
        /// Sets the menu item.
        /// </summary>
        private void SetMenuItemBAK()
        {
            try
            {
                if (LevelFrom == 1) // Top navigation must created within Configuration and assigned as datasouce
                {
                    MenuItem = SitecoreItem.GetItem(DataSource);
                }
                else // for second level navigation onwards
                {
                    // assign current tiem to MenuItem

                    //MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Cache.Get(CurrentMenuItem.ID.ToString()).ToString());

                    MenuItem = RootItem;

                    // if MenuType != "sub" 
                    if (!string.IsNullOrEmpty(MenuType))
                    {
                        int moveLevels = 0;

                        if (MenuType.ToLower().Trim() != "sub")
                        {
                            // calculate levels and reassigne menuitem (e.g. current item is on level 3 and only display level 2 menu)
                            moveLevels = (MenuItem.Axes.Level - (LevelFrom + (SitecoreItem.HomeItem.Axes.Level) - 1));

                            // change menu items if moveLevels is more then 0
                            if (moveLevels > 0)
                            {
                                // assign menu item as per level specified
                                ChangeMenuItem(moveLevels);

                                // Move one level up and then get children
                                MenuItem = MenuItem.Parent;
                            }
                        }
                        else if (MenuType.ToLower().Trim() == "sub")
                        {
                            // calculate levels and reassigne menuitem (e.g. current item is on level 3 and only display level 2 menu)
                            //moveLevels = (MenuItem.Axes.Level - (LevelFrom + (SitecoreItem.HomeItem.Axes.Level) - 1));
                            moveLevels = (MenuItem.Axes.Level - (LevelFrom + (HomePageItem.Axes.Level) - 1));
                            //moveLevels = 2;
                            // change menu items if moveLevels is more then 0
                            if (moveLevels > 0)
                            {
                                if (RootItem.Parent.Name == "Agriculture" || RootItem.Parent.Parent.Name == "Agriculture" || RootItem.Parent.Parent.Parent.Name == "Agriculture" || RootItem.Parent.Parent.Parent.Parent.Name == "Agriculture")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "Survey" || RootItem.Parent.Parent.Name == "Survey" || RootItem.Parent.Parent.Parent.Name == "Survey")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "FSM" || RootItem.Parent.Parent.Name == "FSM" || RootItem.Parent.Parent.Parent.Name == "FSM")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "MGIS" || RootItem.Parent.Parent.Name == "MGIS" || RootItem.Parent.Parent.Parent.Name == "MGIS")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (RootItem.Parent.Name == "Geo Spatial" || RootItem.Parent.Parent.Name == "Geo Spatial" || RootItem.Parent.Parent.Parent.Name == "Geo Spatial")
                                {
                                    string itemId = string.Empty;

                                    foreach (Item child in CurrentMenuItem.Parent.Children)
                                    {
                                        if (child.Name == "index")
                                        {
                                            itemId = child.ID.ToString();
                                        }
                                    }

                                    MenuItem = SitecoreItem.GetItem(System.Web.HttpContext.Current.Application.Get(itemId).ToString());
                                }

                                if (MenuItem.Name == "FSM" || MenuItem.Name == "Agriculture" || MenuItem.Name == "Survey" || MenuItem.Name == "MGIS" || MenuItem.Name == "Geo Spatial")
                                {
                                    MenuItem = MenuItem;
                                }
                                else
                                    // assign menu item as per level specified
                                    ChangeMenuItem(moveLevels);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:SetMenuItem" + ex.Message);
            }
        }

        /// <summary>
        /// Changes the menu item.
        /// </summary>
        /// <param name="moveLevels">The move levels.</param>
        /// 
        private void ChangeMenuItem(int moveLevels)
        {
            try
            {
                if (moveLevels > 0)
                {
                    //System.Web.HttpContext.Current.Response.Write("<!--ChangeMenuItem-->" + System.Environment.NewLine);
                    if (moveLevels == 1)
                    {
                        moveLevels = 2;
                    }
                    for (int iCnt = 1; iCnt < moveLevels; iCnt++)
                    {
                        //System.Web.HttpContext.Current.Response.Write("<!--ChangeMenuItem from " + MenuItem.Name + " to " + MenuItem.Parent.Name + "-->" + System.Environment.NewLine);
                        MenuItem = MenuItem.Parent;
                    }

                }


            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LHSNavigation3:ChangeMenuItem" + ex.Message);
            }
        }

        private void ChangeMenuItemBAK(int moveLevels)
        {
            try
            {
                if (moveLevels > 0)
                {
                    for (int iCnt = 0; iCnt < moveLevels; iCnt++)
                    {
                        MenuItem = MenuItem.Parent;
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:ChangeMenuItem" + ex.Message);
            }
        }


        /// <summary>
        /// Determines whether [is active item] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [is active item] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool IsActiveItem(Item item)
        {
            bool isActive = false;
            try
            {
                if (item.ID == CurrentMenuItem.ID)
                    isActive = true;
                else if (CurrentMenuItem.Axes.IsDescendantOf(item))
                    isActive = true;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:IsActiveItem" + ex.Message);
            }
            return isActive;
        }


        protected virtual Item CurrentMenuItem { get; set; }
        protected virtual Item HomePageItem { get; set; }
        protected virtual Item MenuItem { get; set; }
        protected virtual Item RootItem { get; set; }

        public static string AddCssClass(string currentCssClass, string addCssClass)
        {
            try
            {
                if (!string.IsNullOrEmpty(currentCssClass))
                {
                    return string.Format("{0} {1}", currentCssClass, addCssClass);
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:AddCssClass(string)" + ex.Message);
            }
            return addCssClass;
        }

        protected override void DoRender(HtmlTextWriter output)
        {
            try
            {
                if (MenuItem != null)
                {

                    Item[] children = MenuItem.GetChildrenForNavigation();

                    if (children != null && children.Count() > 0)
                    {
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "mega-menu mega-1 hideLHS");
                        //output.AddAttribute(HtmlTextWriterAttribute.Id, "mega-1");
                        output.RenderBeginTag(HtmlTextWriterTag.Ul);
                        // Rendering specified levels of menu
                        /*
                        if (MenuItem.Name == "FSM" || MenuItem.Name == "Agriculture" || MenuItem.Name == "Survey" || MenuItem.Name == "MGIS" || MenuItem.Name == "Geo Spatial")
                        {
                            if (MenuItem.Name != "Agriculture")
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "heading");
                                output.RenderBeginTag(HtmlTextWriterTag.Li);
                                //output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                                output.RenderBeginTag(HtmlTextWriterTag.Span);
                                output.Write(SitecoreItem.GetNavigationTitle(MenuItem));
                                output.RenderEndTag();
                                output.RenderEndTag();
                            }
                            if (MenuItem.Name == "Agriculture")
                            {
                                foreach (Item child in children)
                                {
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "heading");
                                    output.RenderBeginTag(HtmlTextWriterTag.Li);
                                    output.RenderBeginTag(HtmlTextWriterTag.Span);
                                    output.Write(SitecoreItem.GetNavigationTitle(child));
                                    output.RenderEndTag();
                                    output.RenderEndTag();
                                   Item[] children1 = child.GetChildrenForNavigation();
                                   if (child.HasChildren)
                                   {
                                       RenderAgriMenu(output, children1, LevelFrom, children1.FirstOrDefault(), children1.LastOrDefault());
                                   }
                                }
                               
                            }
                            else
                            {
                                RenderFSMMenu(output, children, LevelFrom, children.FirstOrDefault(), children.LastOrDefault());
                            }
                        }
                        else
                        {
                            RenderMenu(output, children, LevelFrom, children.FirstOrDefault(), children.LastOrDefault());
                        }
                        */
      
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "heading");
                        output.RenderBeginTag(HtmlTextWriterTag.Li);
                        //output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                        output.RenderBeginTag(HtmlTextWriterTag.Span);
                        output.Write(SitecoreItem.GetNavigationTitle(MenuItem));
                        output.RenderEndTag();
                        output.RenderEndTag();
                        RenderSubNavigation(output, children, LevelFrom, children.FirstOrDefault(), children.LastOrDefault());
                        output.RenderEndTag();
                    }

                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:DoRender" + ex.Message);
            }
        }

        /*
        protected override void DoRenderBAK(HtmlTextWriter output)
        {
            try
            {
                if (MenuItem != null)
                {

                    Item[] children = MenuItem.GetChildrenForNavigation();

                    if (children != null && children.Count() > 0)
                    {
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "mega-menu mega-1");
                        //output.AddAttribute(HtmlTextWriterAttribute.Id, "mega-1");
                        output.RenderBeginTag(HtmlTextWriterTag.Ul);
                        // Rendering specified levels of menu
                        if (MenuItem.Name == "FSM" || MenuItem.Name == "Agriculture" || MenuItem.Name == "Survey" || MenuItem.Name == "MGIS" || MenuItem.Name == "Geo Spatial")
                        {
                            if (MenuItem.Name != "Agriculture")
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "heading");
                                output.RenderBeginTag(HtmlTextWriterTag.Li);
                                //output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                                output.RenderBeginTag(HtmlTextWriterTag.Span);
                                output.Write(SitecoreItem.GetNavigationTitle(MenuItem));
                                output.RenderEndTag();
                                output.RenderEndTag();
                            }
                            if (MenuItem.Name == "Agriculture")
                            {
                                foreach (Item child in children)
                                {
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "heading");
                                    output.RenderBeginTag(HtmlTextWriterTag.Li);
                                    output.RenderBeginTag(HtmlTextWriterTag.Span);
                                    output.Write(SitecoreItem.GetNavigationTitle(child));
                                    output.RenderEndTag();
                                    output.RenderEndTag();
                                    Item[] children1 = child.GetChildrenForNavigation();
                                    if (child.HasChildren)
                                    {
                                        RenderAgriMenu(output, children1, LevelFrom, children1.FirstOrDefault(), children1.LastOrDefault());
                                    }
                                }

                            }
                            else
                            {
                                RenderFSMMenu(output, children, LevelFrom, children.FirstOrDefault(), children.LastOrDefault());
                            }
                        }
                        else
                        {
                            RenderMenu(output, children, LevelFrom, children.FirstOrDefault(), children.LastOrDefault());
                        }
                        output.RenderEndTag();
                    }

                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:DoRender" + ex.Message);
            }
        }
        */
        /// <summary>
        /// Renders the menu.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="children">The children.</param>
        /// <param name="iLevel">The i level.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        private void RenderMenu(HtmlTextWriter output, Item[] children, int iLevel, Item firstItem, Item lastItem)
        {
            try
            {
                if (children != null && children.Count() > 0)
                {
                    // process children items
                    foreach (Item child in children)
                    {
                        if (child.HasChildren)
                        {
                            Item[] grandChildren1 = child.GetChildrenForNavigation();

                            for (int i = 0; i < grandChildren1.Count(); i++)
                            {
                                if (grandChildren1[i].ID == RootItem.Parent.ID || grandChildren1[i].ID == RootItem.Parent.Parent.ID)
                                {
                                    Item linkItem = RootItem.Parent.GetFieldValueAsItem(_fieldNameLinkItem);

                                    // if linkitem is not null then use linkitem
                                    if (linkItem == null)
                                    {
                                        linkItem = RootItem.Parent;
                                    }

                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "heading");
                                    output.RenderBeginTag(HtmlTextWriterTag.Li);
                                    //output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                                    output.RenderBeginTag(HtmlTextWriterTag.Span);
                                    output.Write(SitecoreItem.GetNavigationTitle(RootItem.Parent));
                                    output.RenderEndTag();
                                    output.RenderEndTag();

                                    // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                                    if (iLevel < LevelTo && RootItem.Parent.HasChildren)
                                    {
                                        Item[] grandChildren = RootItem.Parent.GetChildrenForNavigation();
                                        if (grandChildren.Count() > 0)
                                        {
                                            //output.RenderBeginTag(HtmlTextWriterTag.Ul);
                                            RenderSubNavigation(output, grandChildren, iLevel, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                                            //output.RenderEndTag();
                                        }
                                    }

                                }
                            }

                        }
                        if (child.ID == RootItem.ID || child.ID == RootItem.ParentID || MenuItem==RootItem)
                        {
                            Item linkItem = child.GetFieldValueAsItem(_fieldNameLinkItem);

                            // if linkitem is not null then use linkitem
                            if (linkItem == null)
                            {
                                linkItem = child;
                            }

                            output.AddAttribute(HtmlTextWriterAttribute.Class, "heading");
                            output.RenderBeginTag(HtmlTextWriterTag.Li);
                            //output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                            output.RenderBeginTag(HtmlTextWriterTag.Span);
                            output.Write(SitecoreItem.GetNavigationTitle(child));
                            output.RenderEndTag();
                            output.RenderEndTag();

                            // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                            if (iLevel < LevelTo && child.HasChildren)
                            {
                                Item[] grandChildren = child.GetChildrenForNavigation();
                                if (grandChildren.Count() > 0)
                                {
                                    //output.RenderBeginTag(HtmlTextWriterTag.Ul);
                                    RenderSubNavigation(output, grandChildren, iLevel, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                                    //output.RenderEndTag();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:RenderMenu" + ex.Message);
            }
        }

        /// <summary>
        /// Renders the menu.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="children">The children.</param>
        /// <param name="iLevel">The i level.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        private void RenderSubNavigation(HtmlTextWriter output, Item[] children, int iLevel, Item firstItem, Item lastItem)
        {
            try
            {
                if (children != null && children.Count() > 0)
                {
                    // process children items
                    foreach (Item child in children)
                    {
                        if (HasLanguageVersion(child, Sitecore.Context.Language.ToString())) //check if there's a translated language
                        {
                            Item linkItem = child.GetFieldValueAsItem(_fieldNameLinkItem);
                            Sitecore.Data.Fields.LinkField linkField = ((Sitecore.Data.Fields.LinkField)child.Fields[_fieldNameGeneralLink]);
                            string url = GetURL(linkField);

                            List<string> intParentList = new List<string>();
                            List<string> intList = new List<string>();

                            if (child.HasChildren)
                            {
                                for (int i = 0; i < child.GetChildren().Count; i++)
                                {
                                    Item[] Children = child.GetChildrenForNavigation();
                                    Item granchildren1 = Children[i];
                                    intParentList.Add(granchildren1.Name.ToString());
                                    intList.Add(granchildren1.Name.ToString());
                                    Item[] grandChildren = granchildren1.GetChildrenForNavigation();
                                    if (grandChildren != null)
                                    {
                                        for (int j = 1; j <= grandChildren.Count(); j++)
                                        {
                                            Item granchildren2 = grandChildren[j - 1];
                                            intList.Add(granchildren2.Name.ToString());
                                        }
                                    }

                                }
                            }

                            if (intList.Count <= 8 && intParentList.Count > 0)
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "one-col");
                            }
                            else if (intList.Count >= 9 && intList.Count <= 16 && intParentList.Count > 2)
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "two-col");
                            }
                            else if (intList.Count >= 16 && intParentList.Count > 3)
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "three-col");

                            output.RenderBeginTag(HtmlTextWriterTag.Li);

                            if (child.HasChildren)
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "leftNavMenuItem");
                                output.RenderBeginTag(HtmlTextWriterTag.A);
                                // A title/ description
                                output.Write(SitecoreItem.GetNavigationTitle(child));
                                // close rendering A tag
                                output.RenderEndTag();
                            }
                            else
                            {
                                // begin rendering A tag
                                if (linkItem != null)
                                {
                                    output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                                }
                                else
                                {
                                    output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                                }
                                output.RenderBeginTag(HtmlTextWriterTag.A);
                                // A title/ description
                                output.Write(SitecoreItem.GetNavigationTitle(child));

                                // close rendering A tag
                                output.RenderEndTag();
                            }

                            // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                            if (iLevel < LevelTo && child.HasChildren)
                            {
                                Item[] grandChildren = child.GetChildrenForNavigation();
                                if (grandChildren.Count() > 0)
                                {
                                    output.RenderBeginTag(HtmlTextWriterTag.Ul);
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "flyout-popup");
                                    output.RenderBeginTag(HtmlTextWriterTag.Li);
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "close_flyout");
                                    output.RenderBeginTag(HtmlTextWriterTag.Span);
                                    output.RenderEndTag();
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "main_head");
                                    output.RenderBeginTag(HtmlTextWriterTag.Div);
                                    // begin rendering A tag
                                    if (linkItem != null || !String.IsNullOrEmpty(url))
                                    {
                                        if (linkItem != null)
                                        {
                                            output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                                        }
                                        else
                                            output.AddAttribute(HtmlTextWriterAttribute.Href, url);

                                        output.RenderBeginTag(HtmlTextWriterTag.A);
                                        // A title/ description
                                        output.Write(SitecoreItem.GetNavigationTitle(child));
                                        // close rendering A tag
                                        output.RenderEndTag();
                                    }
                                    else
                                    {
                                        output.RenderBeginTag(HtmlTextWriterTag.Span);
                                        // A title/ description
                                        output.Write(SitecoreItem.GetNavigationTitle(child));
                                        // close rendering A tag
                                        output.RenderEndTag();
                                    }


                                    output.RenderEndTag();
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "list-items");
                                    output.RenderBeginTag(HtmlTextWriterTag.Div);
                                    RenderLink(output, grandChildren, iLevel + 1, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                                    output.RenderEndTag();
                                    output.RenderEndTag();
                                    output.RenderEndTag();
                                }
                            }

                            // close rendering LI tag
                            output.RenderEndTag();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:RenderSubNavigation" + ex.Message);
            }
        }

        protected void RenderLink(HtmlTextWriter output, Item[] children, int iLevel, Item firstItem, Item lastItem)
        {
            try
            {
                if (children != null && children.Count() > 0)
                {
                    // process children items
                    foreach (Item child in children)
                    {

                        if (HasLanguageVersion(child, Sitecore.Context.Language.ToString())) //check if there's a translated language
                        {

                            Item linkItem = child.GetFieldValueAsItem(_fieldNameLinkItem);

                            Sitecore.Data.Fields.LinkField linkField = ((Sitecore.Data.Fields.LinkField)child.Fields[_fieldNameGeneralLink]);
                            string url = GetURL(linkField);

                            output.AddAttribute(HtmlTextWriterAttribute.Class, "subcategory");
                            output.RenderBeginTag(HtmlTextWriterTag.Div);
                            output.RenderBeginTag(HtmlTextWriterTag.Div);
                            // begin rendering A tag
                            if (linkItem != null)
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                            }
                            else
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                            }

                            output.RenderBeginTag(HtmlTextWriterTag.A);

                            // A title/ description
                            output.Write(SitecoreItem.GetNavigationTitle(child));

                            // close rendering A tag
                            output.RenderEndTag();
                            output.RenderEndTag();

                            // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                            if (iLevel < LevelTo && child.HasChildren)
                            {
                                Item[] grandChildren = child.GetChildrenForNavigation();
                                if (grandChildren.Count() > 0)
                                {
                                    RenderLevel3Link(output, grandChildren);
                                    //RenderSubNavigation(output, grandChildren, iLevel + 1, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                                }
                            }

                            // close rendering LI tag
                            output.RenderEndTag();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:RenderLink" + ex.Message);
            }

        }

        protected void RenderLevel3Link(HtmlTextWriter output, Item[] item)
        {
            try
            {
                if (item != null && item.Count() > 0)
                {
                    // process children items
                    foreach (Item child in item)
                    {
                        if (HasLanguageVersion(child, Sitecore.Context.Language.ToString())) //check if there's a translated language
                        {
                            Item linkItem = child.GetFieldValueAsItem(_fieldNameLinkItem);
                            Sitecore.Data.Fields.LinkField linkField = ((Sitecore.Data.Fields.LinkField)child.Fields[_fieldNameGeneralLink]);
                            string url = GetURL(linkField);
                            object obj = (object)child.ID;
                            if (linkItem != null)
                            {
                                //System.Web.HttpContext.Current.Cache.Add(linkItem.ID.ToString(), child.ID, null, DateTime.Now.AddHours(2), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                                System.Web.HttpContext.Current.Application.Add(linkItem.ID.ToString(), obj);
                                output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                            }
                            else
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                            }
                            output.AddAttribute("href", linkItem.GetFriendlyUrl());
                            output.RenderBeginTag(HtmlTextWriterTag.A);
                            output.Write(child.GetItemNameForNavigation());
                            output.RenderEndTag();
                            output.Write("</br>");

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:RenderLevel3Link" + ex.Message);
            }

        }

        /// <summary>
        /// Renders the menu.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="children">The children.</param>
        /// <param name="iLevel">The i level.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        private void RenderFSMMenu(HtmlTextWriter output, Item[] children, int iLevel, Item firstItem, Item lastItem)
        {
            try
            {
                if (children != null && children.Count() > 0)
                {
                    // process children items
                    foreach (Item child in children)
                    {
                        Item linkItem = child.GetFieldValueAsItem(_fieldNameLinkItem);
                        Sitecore.Data.Fields.LinkField linkField = ((Sitecore.Data.Fields.LinkField)child.Fields[_fieldNameGeneralLink]);
                        string url = GetURL(linkField);

                        List<string> intParentList = new List<string>();
                        List<string> intList = new List<string>();

                        if (child.HasChildren)
                        {
                            for (int i = 0; i < child.GetChildren().Count; i++)
                            {
                                Item[] Children = child.GetChildrenForNavigation();
                                Item granchildren1 = Children[i];
                                intParentList.Add(granchildren1.Name.ToString());
                                intList.Add(granchildren1.Name.ToString());
                                Item[] grandChildren = granchildren1.GetChildrenForNavigation();
                                if (grandChildren != null)
                                {
                                    for (int j = 1; j <= grandChildren.Count(); j++)
                                    {
                                        Item granchildren2 = grandChildren[j - 1];
                                        intList.Add(granchildren2.Name.ToString());
                                    }
                                }

                            }
                        }

                        if (intList.Count <= 8 && intParentList.Count > 0)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "one-col");
                        }
                        else if (intList.Count >= 9 && intList.Count <= 16 && intParentList.Count > 2)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "two-col");
                        }
                        else if (intList.Count >= 16 && intParentList.Count > 3)
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "three-col");

                        output.RenderBeginTag(HtmlTextWriterTag.Li);

                        if (child.HasChildren)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "leftNavMenuItem");
                            output.RenderBeginTag(HtmlTextWriterTag.A);
                            // A title/ description
                            output.Write(SitecoreItem.GetNavigationTitle(child));
                            // close rendering A tag
                            output.RenderEndTag();
                        }
                        else
                        {
                            // begin rendering A tag
                            if (linkItem != null)
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                            }
                            else
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                            }
                            output.RenderBeginTag(HtmlTextWriterTag.A);
                            // A title/ description
                            output.Write(SitecoreItem.GetNavigationTitle(child));

                            // close rendering A tag
                            output.RenderEndTag();
                        }

                        // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                        if (iLevel < LevelTo && child.HasChildren)
                        {
                            Item[] grandChildren = child.GetChildrenForNavigation();
                            if (grandChildren.Count() > 0)
                            {
                                output.RenderBeginTag(HtmlTextWriterTag.Ul);
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "flyout-popup");
                                output.RenderBeginTag(HtmlTextWriterTag.Li);
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "close_flyout");
                                output.RenderBeginTag(HtmlTextWriterTag.Span);
                                output.RenderEndTag();
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "main_head");
                                output.RenderBeginTag(HtmlTextWriterTag.Div);
                                // begin rendering A tag
                                if (linkItem != null || !String.IsNullOrEmpty(url))
                                {
                                    if (linkItem != null)
                                    {
                                        output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                                    }
                                    else
                                        output.AddAttribute(HtmlTextWriterAttribute.Href, url);

                                    output.RenderBeginTag(HtmlTextWriterTag.A);
                                    // A title/ description
                                    output.Write(SitecoreItem.GetNavigationTitle(child));
                                    // close rendering A tag
                                    output.RenderEndTag();
                                }
                                else
                                {
                                    output.RenderBeginTag(HtmlTextWriterTag.Span);
                                    // A title/ description
                                    output.Write(SitecoreItem.GetNavigationTitle(child));
                                    // close rendering A tag
                                    output.RenderEndTag();
                                }


                                output.RenderEndTag();
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "list-items");
                                output.RenderBeginTag(HtmlTextWriterTag.Div);
                                RenderLink(output, grandChildren, iLevel + 1, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                                output.RenderEndTag();
                                output.RenderEndTag();
                                output.RenderEndTag();
                            }
                        }

                        // close rendering LI tag
                        output.RenderEndTag();
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:RenderSubNavigation" + ex.Message);
            }
        }

        /// <summary>
        /// Renders the menu.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="children">The children.</param>
        /// <param name="iLevel">The i level.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        private void RenderAgriMenu(HtmlTextWriter output, Item[] children, int iLevel, Item firstItem, Item lastItem)
        {
            try
            {
                if (children != null && children.Count() > 0)
                {
                    // process children items
                    foreach (Item child in children)
                    {
                        Item linkItem = child.GetFieldValueAsItem(_fieldNameLinkItem);
                        Sitecore.Data.Fields.LinkField linkField = ((Sitecore.Data.Fields.LinkField)child.Fields[_fieldNameGeneralLink]);
                        string url = GetURL(linkField);

                        List<string> intParentList = new List<string>();
                        List<string> intList = new List<string>();

                        if (child.HasChildren)
                        {
                            for (int i = 0; i < child.GetChildren().Count; i++)
                            {
                                Item[] Children = child.GetChildrenForNavigation();
                                Item granchildren1 = Children[i];
                                intParentList.Add(granchildren1.Name.ToString());
                                intList.Add(granchildren1.Name.ToString());
                                Item[] grandChildren = granchildren1.GetChildrenForNavigation();
                                if (grandChildren != null)
                                {
                                    for (int j = 1; j <= grandChildren.Count(); j++)
                                    {
                                        Item granchildren2 = grandChildren[j - 1];
                                        intList.Add(granchildren2.Name.ToString());
                                    }
                                }

                            }
                        }

                        if (intList.Count <= 8 && intParentList.Count > 0)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "one-col");
                        }
                        else if (intList.Count >= 9 && intList.Count <= 16 && intParentList.Count > 2)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "two-col");
                        }
                        else if (intList.Count >= 16 && intParentList.Count > 3)
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "three-col");

                        output.RenderBeginTag(HtmlTextWriterTag.Li);

                        if (child.HasChildren)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "leftNavMenuItem");
                            output.RenderBeginTag(HtmlTextWriterTag.A);
                            // A title/ description
                            output.Write(SitecoreItem.GetNavigationTitle(child));
                            // close rendering A tag
                            output.RenderEndTag();
                        }
                        else
                        {
                            // begin rendering A tag
                            if (linkItem != null)
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                            }
                            else
                            {
                                output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                            }
                            output.RenderBeginTag(HtmlTextWriterTag.A);
                            // A title/ description
                            output.Write(SitecoreItem.GetNavigationTitle(child));

                            // close rendering A tag
                            output.RenderEndTag();
                        }

                        // if current level is higher then LevelFrom then wrap with UL tag and process childrens
                        if (iLevel < LevelTo && child.HasChildren)
                        {
                            Item[] grandChildren = child.GetChildrenForNavigation();
                            if (grandChildren.Count() > 0)
                            {
                                output.RenderBeginTag(HtmlTextWriterTag.Ul);
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "flyout-popup");
                                output.RenderBeginTag(HtmlTextWriterTag.Li);
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "close_flyout");
                                output.RenderBeginTag(HtmlTextWriterTag.Span);
                                output.RenderEndTag();
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "main_head");
                                output.RenderBeginTag(HtmlTextWriterTag.Div);
                                // begin rendering A tag
                                if (linkItem != null || !String.IsNullOrEmpty(url))
                                {
                                    if (linkItem != null)
                                    {
                                        output.AddAttribute(HtmlTextWriterAttribute.Href, linkItem.GetFriendlyUrl());
                                    }
                                    else
                                        output.AddAttribute(HtmlTextWriterAttribute.Href, url);

                                    output.RenderBeginTag(HtmlTextWriterTag.A);
                                    // A title/ description
                                    output.Write(SitecoreItem.GetNavigationTitle(child));
                                    // close rendering A tag
                                    output.RenderEndTag();
                                }
                                else
                                {
                                    output.RenderBeginTag(HtmlTextWriterTag.Span);
                                    // A title/ description
                                    output.Write(SitecoreItem.GetNavigationTitle(child));
                                    // close rendering A tag
                                    output.RenderEndTag();
                                }


                                output.RenderEndTag();
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "list-items");
                                output.RenderBeginTag(HtmlTextWriterTag.Div);
                                RenderLink(output, grandChildren, iLevel + 1, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                                output.RenderEndTag();
                                output.RenderEndTag();
                                output.RenderEndTag();
                            }
                        }

                        // close rendering LI tag
                        output.RenderEndTag();
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:RenderSubNavigation" + ex.Message);
            }
        }

        /// <summary>
        /// Get URL for General Link 
        /// </summary>
        /// <param name="lnkField"></param>
        /// <returns></returns>
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {

            switch (lnkField.LinkType.ToLower())
            {
                case "internal":
                    // Use LinkMananger for internal links, if link is not empty
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;
                case "media":
                    // Use MediaManager for media links, if link is not empty
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;
                case "external":
                    // Just return external links
                    return lnkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link
                    return lnkField.Url;
                case "javascript":
                    // Just return javascript
                    return lnkField.Url;
                default:
                    // Just please the compiler, this
                    // condition will never be met
                    return lnkField.Url;
            }

        }

        /// <summary>
        /// Applies the id.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="child">The child.</param>
        /// <param name="fieldName">Name of the field.</param>
        private static void ApplyId(HtmlTextWriter output, Item child, string fieldName)
        {
            try
            {
                if (!string.IsNullOrEmpty(child.GetFieldValue(fieldName)))
                {
                    output.AddAttribute(HtmlTextWriterAttribute.Id, child.GetFieldValue(fieldName));
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:ApplyId" + ex.Message);
            }
        }

        /// <summary>
        /// Adds the CSS class.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="item">The item.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        /// <param name="linkItem">The link item.</param>
        /// <param name="cssClassFieldName">Name of the CSS class field.</param>
        private void AddCSSClass(HtmlTextWriter output, Item item, Item firstItem, Item lastItem, Item linkItem = null, string cssClassFieldName = null)
        {
            try
            {
                string cssClass = string.Empty;

                // apply first class
                if (item == firstItem)
                {
                    cssClass = AddCssClass(cssClass, FirstElementClass);
                }
                else if (item == lastItem)
                {
                    cssClass = AddCssClass(cssClass, LastElementClass);
                }

                // Add CSS Class if specified
                if (!string.IsNullOrEmpty(cssClassFieldName) && !string.IsNullOrEmpty(item.GetFieldValue(cssClassFieldName)))
                {
                    cssClass = AddCssClass(cssClass, item.GetFieldValue(cssClassFieldName));
                }


                // apply active item class
                if (IsActiveItem(linkItem) || IsActiveItem(item))
                {
                    cssClass = AddCssClass(cssClass, ActiveElementClass);
                }

                // CSS Class attribute 
                if (!string.IsNullOrEmpty(cssClass))
                    output.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("LeftNavigation:AddCssClass" + ex.Message);
            }
        }

        /// <summary>
        /// Need to override the GetCachingID for Sitecore caching
        /// </summary>
        /// <returns>CacheId</returns>
        protected override string GetCachingID()
        {
            return GetType().ToString();
        }

        /// <summary>
        /// Check if there's Language Translation on the Menu Item
        /// Added by JM 01/26/2018
        private bool HasLanguageVersion(Sitecore.Data.Items.Item item, string languageName)
        {
            var language = item.Languages.FirstOrDefault(l => l.Name == languageName);
            if (language != null)
            {
                var languageSpecificItem = global::Sitecore.Context.Database.GetItem(item.ID, language);
                if (languageSpecificItem != null && languageSpecificItem.Versions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }


    }
}