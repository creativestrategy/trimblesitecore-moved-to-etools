﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.HtmlControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Web.UI.WebControls;
using Trimble.Presentation.UI.Sublayouts.Trimble;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public partial class CustomerLogin : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            frTitle.Item = DataSourceItem;
        }
    }
}