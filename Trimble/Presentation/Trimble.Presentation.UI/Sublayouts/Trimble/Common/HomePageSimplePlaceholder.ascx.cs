﻿using System;
using System.Web.UI;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public partial class HomePageSimplePlaceholder : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            GetHomePageSimplePlaceHolder();
        }
        /// <summary>
        /// Load Homepage Simple Placeholder (Left Component which exists in 1st column)
        /// </summary>
        private void GetHomePageSimplePlaceHolder()
        {
            try
            {
                Item HomepageSimpleitem = DataSourceItem;
                if (HomepageSimpleitem != null)
                {


                    if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[CommonFields.Title].Value))
                    {
                        Sitecore.Data.Fields.TextField quote = HomepageSimpleitem.Fields[RandomSimplePlaceHolder.Quote];
                        Sitecore.Data.Fields.TextField freetext = HomepageSimpleitem.Fields[RandomSimplePlaceHolder.FreeText];

                        frHomepageSimpleTitle.Item = HomepageSimpleitem;


                        if (quote != null && freetext != null)
                        {
                            //Show Image if all other fields are not null
                            if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.FreeText].Value) && !String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.Quote].Value))
                                renderImage(HomepageSimpleitem);
                            //show image if image and Quote are not null
                            else if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.Quote].Value))
                                renderImage(HomepageSimpleitem);
                            //show image if image and FreeText are not null
                            else if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                renderImage(HomepageSimpleitem);
                            //show Quote if FreeText and Quote are not null
                            else if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.Quote].Value) && !String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                renderQuote(HomepageSimpleitem);

                            else
                            {
                                //Show Image if other fields are nulls
                                if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[CommonFields.Image].Value))
                                    renderImage(HomepageSimpleitem);
                                //Show Quote if other fields are nulls
                                else if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.Quote].Value))
                                    renderQuote(HomepageSimpleitem);
                                //Show FreeText if other fields are nulls
                                else if (!String.IsNullOrEmpty(HomepageSimpleitem.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                    renderFreeText(HomepageSimpleitem);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomePageSimplePlaceholder:GetHomePageSimplePlaceHolder" + ex.Message);
            }
        }
        /// <summary>
        /// Render Image (remove FreeText and Quote)
        /// </summary>
        /// <param name="dtsource"></param>
        private void renderImage(Item item)
        {
            try
            {
                frHomepageSimpleImage.Item = item;
                HtmlGenericControl spanQuote = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Quote));
                spanQuote.Visible = false;
                HtmlGenericControl divFreeText = (HtmlGenericControl)(FindControl(RandomSimpleDivID.FreeText));
                divFreeText.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomePageSimpleplaceholder:renderImage" + ex.Message);
            }
        }
        /// <summary>
        /// Render Quote (remove FreeText and Image)
        /// </summary>
        /// <param name="dtsource"></param>
        private void renderQuote(Item item)
        {
            try
            {
                frHomepageSimpleQuote.Item = item;
                HtmlGenericControl divFreeText = (HtmlGenericControl)(FindControl(RandomSimpleDivID.FreeText));
                divFreeText.Visible = false;
                HtmlGenericControl divImage = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Image));
                divImage.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomePageSimpleplaceholder:renderQuote" + ex.Message);
            }
        }
        /// <summary>
        /// Render FreeText (remove Image and Quote)
        /// </summary>
        /// <param name="dtsource"></param>
        private void renderFreeText(Item item)
        {
            try
            {
                frHomepageSimpleFreeText.Item = item;
                HtmlGenericControl divImage = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Image));
                divImage.Visible = false;
                HtmlGenericControl spanQuote = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Quote));
                spanQuote.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomePageSimpleplaceholder:renderFreeText" + ex.Message);
            }
        }
    }
}
