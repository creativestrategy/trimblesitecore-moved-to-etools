﻿using System;
using System.Web.UI;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public partial class GeneralText : SublayoutBaseExtended
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Condition to check is page postback
                if (!Page.IsPostBack)
                {
                    if (base.SupportsFields(frGeneralText.FieldName))
                    {
                        frGeneralText.Item = DataSourceItem;
                    }
                    else
                    {
                        this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("GeneralText:page_Load"+ex.Message);
            }
         }
    }
}