﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrimbleBanner.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.TrimbleBanner" %>
<div id="container_innerbanner">
    <div id="tmpSlideshow">
        <asp:Repeater runat="server" ID="rptData" OnItemDataBound="rptData_ItemDataBound">
            <ItemTemplate>
                <div id="innerBanner" runat="server" clientidmode="Static">
                    <sc:FieldRenderer ID="frBannerImage" runat="server" FieldName="BannerImage" DisableWebEditing="false" />
                    <div class="bannerText">
                        <div runat="server" id="divBGOnOff" class="allinone_thumbnailsBanner_text_line textElement21_simpleFullWidth bannerTextPosition trimbleBannerSize">
                            <span class="largefont bannerTextHeading displayBlock">
                                <sc:FieldRenderer ID="frBannerTitle" runat="server" FieldName="BannerTitle" DisableWebEditing="false" />
                            </span>
                            
                            <span class="smallfont bannerTextSubHeading lineHeight"> <sc:FieldRenderer ID="frBannerTeaser" runat="server" FieldName="BannerTeaserRichText" DisableWebEditing="false" /></span><br />
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
