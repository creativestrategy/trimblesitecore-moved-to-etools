﻿#region System

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

#endregion

#region Deloitte Framework

using Deloitte.SC.UI.Sublayouts.Framework;

#endregion

#region Namespace

    namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
    {
        #region Partial class

            public partial class SLISearch : SublayoutBaseExtended
            {
                #region Protected Methods
                 
                    // Page Load Method
                    protected void Page_Load(object sender, EventArgs e)
                    {
                        try
                        {
                            //Condtition to check is page postback
                            if (!Page.IsPostBack)
                            {

                                // Added style elements for image button
                                //imgSLISearch.Style.Add(HtmlTextWriterStyle.BorderWidth, "0px");
                                //imgSLISearch.Style.Add(HtmlTextWriterStyle.Width, "20px");
                                //imgSLISearch.Style.Add(HtmlTextWriterStyle.Height, "20px");
                                // frSearch.Item = DataSourceItem;

                                //Set Deafult Search Keyword for Search text box
                                string strSearchKeyword = "Search...";
                                //txtSLISearch.Text = strSearchKeyword;
                                //Calling Javascript
                                //txtSLISearch.Attributes.Add("onfocus", "SearchFocus(this, '" + strSearchKeyword + "');");
                                //txtSLISearch.Attributes.Add("onblur", "SearchBlur(this, '" + strSearchKeyword + "');");
                            }
                        }
                      catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("Search:Page_Load" + ex.Message);
                        }
                    }

                    // Image click event
                    protected void imgSearch_Click(object sender, ImageClickEventArgs e)
                    {
                        try
                        {
                            //Redirect page to Trimble Search page with Search Keyword
                            //Response.Redirect("http://trimble.resultspage.com/search?p=Q&ts=custom&Submit=Submit+Query&w=" + txtSLISearch.Text);
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("Search:imgSearch_Click" + ex.Message);
                        }
                    }

                #endregion
                                    
            }

        #endregion       
    }

#endregion