﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public partial class RandomizedPlaceHolder : SublayoutBaseExtended
    {
        int Count = 0;
        Random rnd = new Random();
       
        #region Properties

        Item dataSourceItem = null;

        #endregion

        /// <summary>
        /// To maintain the source for compatibility renderings
        /// </summary>
        /// <returns></returns>
        //protected Item GetDataSourceItem()
        //{
        //    const string path = "/sitecore/content/Website/Content/Pages/Home/Randomized Placeholder";
        //    dataSourceItem = Sitecore.Context.Database.GetItem(path);
        //    return dataSourceItem;
        //}
        protected Item GetDataSourceItem()
        {
            this.dataSourceItem = this.DataSourceItem;
            return this.dataSourceItem;
        } 


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetDataSourceItem();
                if (base.SupportsTemplates("{08521697-9FF4-41C6-8841-78812A50A461}"))
                {
                    GetRandomizedPlaceHolder();
                }
                else
                {
                    this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                }
            }
        }
        /// <summary>
        /// Load Randomized Placeholder
        /// </summary>
        private void GetRandomizedPlaceHolder()
        {
            try
            {
                int RandomizedMaxCount = (int)MaxCount.RandomPlaceHolderMaxCount;
                Item RandomizedDataSource = dataSourceItem;
                if (RandomizedDataSource != null && Convert.ToString(RandomizedDataSource.Name) == "Randomized Placeholder")
                {

                    List<Item> listRandomizedDataSource = new List<Item>();

                    //if the child count is > 4 limit to 4
                    if (RandomizedDataSource.Children.Count() > RandomizedMaxCount)
                    {
                        Count = RandomizedMaxCount;
                    }
                    //else take the actual count of child
                    else
                    {
                        Count = RandomizedDataSource.Children.Count();
                    }

                    //Get the child items
                    for (int i = 0; i < Count; i++)
                    {
                        Item nItem = RandomizedDataSource.GetChildren()[i];
                        listRandomizedDataSource.Add(nItem);
                    }

                    //Get an item randomly on each page load
                    int randomNumber = rnd.Next(1, Count + 1);
                    Item dtsource = listRandomizedDataSource[randomNumber - 1];


                    //Rendering the HTML markup accordingly
                    if (dtsource != null)
                    {

                        if (!String.IsNullOrEmpty(dtsource.Fields[CommonFields.Title].Value))
                        {
                            frRandomizedTitle.Item = dtsource;
                            //Show image if all other fields are not null
                            if (!String.IsNullOrEmpty(dtsource.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.Quote].Value) && !String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                renderImage(dtsource);
                            //show image if image and Quote are not null
                            else if (!String.IsNullOrEmpty(dtsource.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.Quote].Value))
                                renderImage(dtsource);
                            //show image if image and FreeText are not null
                            else if (!String.IsNullOrEmpty(dtsource.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                renderImage(dtsource);
                            //show Quote if FreeText and Quote are not null
                            else if (!String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.Quote].Value) && !String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                renderQuote(dtsource);

                            else
                            {//Show Image if other fields are nulls
                                if (!String.IsNullOrEmpty(dtsource.Fields[CommonFields.Image].Value))
                                    renderImage(dtsource);
                                //Show Quote if other fields are nulls
                                else if (!String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.Quote].Value))
                                    renderQuote(dtsource);
                                //Show FreeText if other fields are nulls
                                else if (!String.IsNullOrEmpty(dtsource.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                    renderFreeText(dtsource);
                            }
                        }
                    }
                }
                else
                {
                }
            }
            catch(Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("Randomizedplaceholder:GetRandomizedPlaceHolder" + ex.Message);
            }
        }

        /// <summary>
        /// Render Image (remove FreeText and Quote)
        /// </summary>
        /// <param name="dtsource"></param>
        private void renderImage(Item dtsource)
        {
            try
            {
                frRandomizedImage.Item = dtsource;
                HtmlGenericControl divFreeText = (HtmlGenericControl)(FindControl(RandomSimpleDivID.FreeText));
                divFreeText.Visible = false;
                HtmlGenericControl spanQuote = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Quote));
                spanQuote.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("Randomizedplaceholder:renderImage" + ex.Message);
            }
        }

        /// <summary>
        /// Render Quote (remove FreeText and Image)
        /// </summary>
        /// <param name="dtsource"></param>
         private void renderQuote(Item dtsource)
        {
            try
            {
                frRandomizedQuote.Item = dtsource;
                HtmlGenericControl divFreeText = (HtmlGenericControl)(FindControl(RandomSimpleDivID.FreeText));
                divFreeText.Visible = false;
                HtmlGenericControl divImage = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Image));
                divImage.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("Randomizedplaceholder:renderQuote" + ex.Message);
            }
             
        }

        /// <summary>
         /// Render FreeText (remove Image and Quote)
        /// </summary>
        /// <param name="dtsource"></param>
         private void renderFreeText(Item dtsource)
        {
            try
            {
                frRandomizedFreeText.Item = dtsource;
                HtmlGenericControl divImage = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Image));
                divImage.Visible = false;
                HtmlGenericControl spanQuote = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Quote));
                spanQuote.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("Randomizedplaceholder:renderFreeText" + ex.Message);
            }
        }

    }
}
