﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerLogin.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.CustomerLogin" %>

<script type="text/javascript">
    $(document).ready(function () {
        $(".fsmlogin").colorbox({ inline: true });
        
    });
</script>

<div class="custLoginContainer">
   <span class="loginbutton liasd"> 
        <a href="#fsmLoginBox" class="fsmlogin" title="FSM Customer Login">
                <sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" DisableWebEditing="false" />   
        </a>  
   </span>
</div>

<div style='display:none'>
    <div class="login_boxx" id="fsmLoginBox">

	    <ul class="logbullet">
	    <li>GEOMANAGER
	    <ul>
	    <li onmouseover="this.style.backgroundColor='#e3ebee';" onmouseout="this.style.backgroundColor='#fff';" style="background-color: rgb(255, 255, 255);"><a href="https://www.road.com/application/road/geomanager/login.html"><!--GeoManager Region 1-->Region 1 - Americas</a><!--<br /><span style="font-size:10px;">(fomerly GeoManager <em>i</em>LM, PE &amp; SMB)</span>--></li>
	    <li onmouseover="this.style.backgroundColor='#e3ebee';" onmouseout="this.style.backgroundColor='#fff';" style="background-color: rgb(255, 255, 255);"><a href="https://eugm.road.com/application/signon/secured/login.html"><!--GeoManager Region 2-->Region 2 - Europe, Middle East, Africa, <br>Australia, Asia Pacific</a></li>
	    </ul>
	    </li>
        <!--
	    <li>TRIMWEB
	    <ul>
	    <li onmouseover="this.style.backgroundColor='#e3ebee';" onmouseout="this.style.backgroundColor='#fff';" style="background-color: rgb(255, 255, 255);"><a href="https://login3.trimblems.com/Supervision/OuterPages/HomeLogin.aspx">TrimWeb Region 1</a><br><span style="font-size:10px;">(fomerly Login 1 &amp; 3)</span></li>
	    <li onmouseover="this.style.backgroundColor='#e3ebee';" onmouseout="this.style.backgroundColor='#fff';" style="background-color: rgb(255, 255, 255);"><a href="https://login2.trimblems.com/Supervision/OuterPages/HomeLogin.aspx">TrimWeb Region 2</a><br><span style="font-size:10px;">(fomerly Login 2)</span></li>
	    </ul>
	    </li>
        -->
	    <li>PATHWAY
	    <ul>
	    <li onmouseover="this.style.backgroundColor='#e3ebee';" onmouseout="this.style.backgroundColor='#fff';" style="background-color: rgb(255, 255, 255);"><a href="https://www.road.com/application/road/geomanager/login.html">PathWay <em>i</em>LM</a></li>
	    <!--<li onmouseover="this.style.backgroundColor='#e3ebee';" onmouseout="this.style.backgroundColor='#fff';"><a href="http://www.pathway.road.com/login/login.pl">PathWay PE</a></li>-->
	    </ul>
	    </li>
	    <li>PORTICO
	    <ul>
	    <li onmouseover="this.style.backgroundColor='#e3ebee';" onmouseout="this.style.backgroundColor='#fff';"><a href="https://www.road.com/application/road/geomanager/login.html">Portico <em>i</em>LM</a></li>
	    </ul>
	    </li>
	    </ul>
    </div>
</div>