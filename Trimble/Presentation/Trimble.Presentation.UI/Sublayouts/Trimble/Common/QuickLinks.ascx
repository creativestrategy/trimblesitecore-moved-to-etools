﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuickLinks.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.QuickLinks" Debug="true" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".aboutvideo a").colorbox({ iframe: true, innerWidth: 750, innerHeight: 510 });
    }); 
</script>
  
  <div class="heading" runat="server" id="QuickLinksMainDiv">
            <span>
             <sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" DisableWebEditing="false" />            
            </span>
         </div>     
        <sc:Link ID="lnkQuickLinkTarget" runat="server" Field="QuickLinkTarget" />
        <ul id="ulMain" runat="server">
        <asp:Repeater runat="server" ID="rptQuickData" onitemdatabound="rptQuickData_ItemDataBound">
        <ItemTemplate>
            <li id="li_mediatype" runat="server" >
             <sc:EditFrame runat="server" ID="efActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/LinkRendererActions">
                <sc:Link ID="lnkQuickLinkTarget" runat="server" Field="QuickLinkTarget" DisableWebEditing="true">
                     <asp:Literal runat="server" ID="lnkTitle" />
                </sc:Link>                      
            </sc:EditFrame>
            </li>      
          
        </ItemTemplate>

    </asp:Repeater>

    <asp:Literal runat="server" ID="litWrapperHtmlEnd" />

        <!-- CSS guys need to fix this layout -->
     <sc:EditFrame  runat="server" ID="efAddLink" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/LinkRendererAddLink">
     <div id="AddNewLink" runat="server">
        Add Quick Link
    </div>
</sc:EditFrame>
 
   </ul>
    
