﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Search" %>
<script>
		jQuery(function() {
			$('.topsearch .search').focus(function () {
			    if (this.value == 'Search...') {
			      this.value = '';
			    }
		    });
			$('.topsearch .search').blur(function () {
			    if (this.value == '') {
			      this.value = 'Search...';
			    }
			});
			$('.topsearch .search').on('keydown',function(e){
			    if( e.which == 13)
			        startSearch();
			});
			$('.topsearch input[name="Submit"]').click(function () {
			    startSearch();
			});
			$('.menuTemplate1 .dropimg img').hover( swapImage, restoreImage);
		});
		function startSearch(){
		  window.location.href = 'http://trimble.resultspage.com/search?p=Q&ts=custom&Submit=Submit+Query&w='+ $('.topsearch .search').val();
		}
		function swapImage(){
		  $(this).attr('src', $(this).parent().data('hover-icon'));
		}
		
		function restoreImage(){
		  $(this).attr('src', $(this).parent().data('icon'));
		}
</script>



<div class="topsearch">
      <div class="form">
        <input type="text" class="input search" name="w" size="16" value="Search..." onfocus="this.value=''" id="sli_search_1" autocomplete="off" style="height:12px; font-size:11px;" onkeydown="javascript:return fnTrapKD(event,this);">
        
<input name="Submit" type="button" class="button" onclick="javascript:return fnTrapKD(event,document.getElementById('sli_search_1'));">
      </div>
    </div>
       <%-- <sc:fieldrenderer runat="server" id="frSearch" FieldName ="SearchResultUrl"></sc:fieldrenderer>   --%>
