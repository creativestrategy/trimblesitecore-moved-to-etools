﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Header.Header" %>
<%
    string[] menuItem;
    string menuTitle = "MENU";
    string sc_curLang = Sitecore.Context.Language.ToString().ToUpper();
    //Response.Write(Sitecore.Context.Language.ToString().ToUpper());
    if (sc_curLang == "PT-BR"){
        //menuItem = new string[] {"Sobre", "Carreira", "Suporte e Treinamento", "Acionistas", "Produtos", "Indústrias"};
        //menuItem = new string[] { "Sobre", "Carreira", "Suporte", "Acionistas", "Soluções", "Indústrias" };
        menuItem = new string[] { "Sobre", "Suporte", "Acionistas", "Soluções", "Indústrias" };
        menuTitle = "VISÃO";         
    }else if (sc_curLang == "ZH-CN"){
        //menuItem = new string[] { "关于我们", "工作机会", "支持和培训", "新闻", "公司地点", "行业" };
        //menuItem = new string[] { "关于我们", "工作机会", "支持", "新闻", "解决方案", "行业" };
        menuItem = new string[] { "关于我们", "支持", "新闻", "解决方案", "行业" };
        menuTitle = "菜单";      
    }else{
        //menuItem = new string[] {"About", "Careers", "Support & Training", "Investors", "Products", "Industries"};
        //menuItem = new string[] { "About", "Careers", "Support", "Investors", "Solutions", "Industries" }; //changed 03/25/2020 by JMercado
        menuItem = new string[] { "About", "Support", "Investors", "Solutions", "Industries" }; //changed 05/13/2020 by JMercado
        menuTitle = "MENU";  
    }
%>



<div id="container_header">
    <div class="header">
        <div class="franchiseTitle"><sc:Placeholder ID="scFranchiseTitle" runat="server" Key="scFranchiseTitle" /></div>
        <div class="tag-line"><sc:Placeholder ID="scTagLine" runat="server" Key="scTagLine" /></div>
        
        <div class="roundborder"> <a href="#menu-right" onClick="$(function() {var api = $('#menu-right').data('mmenu'); api.closeAllPanels();})"> <span><i class="fa2 fa-bars fa-2xb"></i></span><span class="hamburgerMenu"><%=menuTitle%><span></a></div>
            <div class="padmiddle">
            <ul class="padmenu2">
                <li><a id="gm-<%=menuItem[4]%>" href="#menu-right" onClick="$(function() { $('#subnav<%=menuItem[4]%> a[tabindex]=0').trigger('click');})" class="menupad" data-menu-trigger="<%=menuItem[4]%>"><%=menuItem[4]%></a></li>
                <li><a id="gm-<%=menuItem[3]%>" href="#menu-right" onClick="$(function() { $('#subnav<%=menuItem[3]%> a[tabindex]=0').trigger('click');})" class="menupad" data-menu-trigger="<%=menuItem[3]%>"><%=menuItem[3]%></a></li>
                <li><a id="gm-<%=menuItem[2]%>" href="#menu-right" onClick="$(function() { $('#subnav<%=menuItem[2]%> a[tabindex]=0').trigger('click');})" class="menupad" data-menu-trigger="<%=menuItem[2]%>"><%=menuItem[2]%></a></li>
                <li><a id="gm-<%=menuItem[1]%>" href="#menu-right" onClick="$(function() { $('#subnav<%=menuItem[1]%> a[tabindex]=0').trigger('click');})" class="menupad" data-menu-trigger="<%=menuItem[1]%>"><%=menuItem[1]%></a></li>
                <li><a id="gm-<%=menuItem[0]%>" href="#menu-right" onClick="$(function() { $('#subnav<%=menuItem[0]%> a[tabindex]=0').trigger('click');})" class="menupad" data-menu-trigger="<%=menuItem[0]%>"><%=menuItem[0]%></a></li>
                
            </ul>
        </div>    

    </div>
    <sc:Placeholder ID="scNavigation" runat="server" Key="scNavigation" />
</div>

