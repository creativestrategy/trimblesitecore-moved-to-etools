﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopHeader-Orig.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Header.TopHeader" %>
 <div class="container">
    <div id="partners"><sc:Link ID="lnkUtilityNavigationLink4" runat="server" Field="UtilityNavigationLink4"/></div>
    <div class="top_links"><sc:Link ID="lnkUtilityNavigationLink3" runat="server" Field="UtilityNavigationLink3"/></div>
    <div class="trimble_worldwide header_icon"></div>
    <div class="top_links"><sc:Link ID="lnkUtilityNavigationLink2" runat="server" Field="UtilityNavigationLink2"/></div>
    <div class="service_center header_icon"></div>
    <div class="top_links"><sc:Link ID="lnkUtilityNavigationLink1" runat="server" Field="UtilityNavigationLink1"/></div>
    <div class="dealer_locator header_icon"></div>
  </div>
