﻿#region System
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
#endregion

#region Deloitte Framework
    using Deloitte.SC.UI.Sublayouts.Framework;
using Sitecore.Web.UI.WebControls;
#endregion

#region Namespce

    namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common.Header
    {
        #region Partial class
            public partial class TopHeader : SublayoutBaseExtended
            {
                #region Protected Methods
                    protected void Page_Load(object sender, EventArgs e)
                    {
                        try
                        {
                            //Condition is page postback
                            if (!Page.IsPostBack)
                            {
                                //Assign datasource to custom item
                                lnkUtilityNavigationLink4.Item = lnkUtilityNavigationLink3.Item = lnkUtilityNavigationLink2.Item = lnkUtilityNavigationLink1.Item = DataSourceItem;
                            }
                            IsEditable();
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("TopHeader:Page_Load"+ex.Message);
                        }
                    }
                #endregion 
                    #region TopHeaderEditable
                    protected void IsEditable()
                    {
                        try
                        {
                            Sublayout sublayout = this.Parent as Sublayout;
                            if (sublayout != null)
                            {
                                String sublayoutPath = sublayout.Path;
                                Placeholder placeholder = sublayout.Parent as Placeholder;
                                //the sitecore placeholder key
                                string urlToTrack = HttpContext.Current.Request.Url.AbsolutePath;
                                if ((placeholder != null) && !(urlToTrack == @"/") && (Sitecore.Context.PageMode.IsPageEditorEditing))
                                {
                                    placeholder.Visible = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("TopHeader:IsEditable" + ex.Message);
                        }
                    }
                    #endregion

            }
        #endregion 
    }
#endregion 