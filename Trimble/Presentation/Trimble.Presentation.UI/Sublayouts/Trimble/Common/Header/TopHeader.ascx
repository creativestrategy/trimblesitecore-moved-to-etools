﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopHeader.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Header.TopHeader" %>
 <div class="container topNavMenu">
    <!--
    /*
    <div id="partners"><sc:Link ID="lnkUtilityNavigationLink44" runat="server" Field="UtilityNavigationLink4"/></div>
    <div class="top_links"><sc:Link ID="lnkUtilityNavigationLink33" runat="server" Field="UtilityNavigationLink3"/></div>
    <div class="trimble_worldwide_blue header_icon"></div>
    <div class="top_links"><sc:Link ID="lnkUtilityNavigationLink22" runat="server" Field="UtilityNavigationLink2"/></div>
    <div class="service_center_blue header_icon"></div>
    <div class="top_links"><sc:Link ID="lnkUtilityNavigationLink11" runat="server" Field="UtilityNavigationLink1"/></div>
    <div class="dealer_locator_blue header_icon"></div>
    */
    -->


    <div class="top_links" id="partners"><i class="fa4 fa-link fa-lg"></i><sc:Link ID="lnkUtilityNavigationLink4" runat="server" Field="UtilityNavigationLink4"/></div>
    <div class="top_links"><i class="fa4 fa-globe fa-lg"></i><sc:Link ID="lnkUtilityNavigationLink3" runat="server" Field="UtilityNavigationLink3"/></div>
    <div class="top_links"><i class="fa4 fa-wrench fa-lg"></i><sc:Link ID="lnkUtilityNavigationLink2" runat="server" Field="UtilityNavigationLink2"/></div>
    <div class="top_links"><i class="fa4 fa-map-marker fa-lg"></i><sc:Link ID="lnkUtilityNavigationLink1" runat="server" Field="UtilityNavigationLink1"/></div>

  </div>
