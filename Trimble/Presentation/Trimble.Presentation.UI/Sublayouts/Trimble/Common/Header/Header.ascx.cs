#region System
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
#endregion

#region Deloitte Framework
    using Deloitte.SC.Framework.Helpers;
    using Deloitte.SC.UI.Sublayouts.Framework;
#endregion

#region Sitecore
    using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
#endregion

#region Namespace

    namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common.Header
    {
        #region Partial Class

            public partial class Header : SublayoutBaseExtended
            {
                #region Protected Methods
                    /// <summary> 
                    /// Page Load Method
                    /// </summary>
                    /// <param name="sender"></param>
                    /// <param name="e"></param>
                    protected void Page_Load(object sender, EventArgs e)
                    {
                        try
                        {
                            //Condition for is page postback
                            if (!Page.IsPostBack)
                            {
                                //removed binding by JMercado 02-27-2017
                                //Bind logo item with site field
                                //frSiteLogo.Item = SitecoreItem.WebsiteItem;

                                //Bind the Home link to come back from other content pages
                                //lnkHomeLink.Item = SitecoreItem.WebsiteItem;

                                //Bind tagline item with site field
                                // frTagLine.Item = DataSourceItem;                

                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("Header:Page_Load"+ex.Message);
                        }
                        //IsEditable();
                    }

                #endregion

                    #region HeaderEditable
                    protected void IsEditable()
                    {
                        try
                        {
                            Sublayout sublayout = this.Parent as Sublayout;
                            if (sublayout != null)
                            {
                                String sublayoutPath = sublayout.Path;
                                Placeholder placeholder = sublayout.Parent as Placeholder;
                                //the sitecore placeholder key
                                string urlToTrack = HttpContext.Current.Request.Url.AbsolutePath;
                                if ((placeholder != null) && !(urlToTrack == @"/") && (Sitecore.Context.PageMode.IsPageEditorEditing))
                                {
                                    placeholder.Visible = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("Header:IsEditable" + ex.Message);
                        }
                    }
                    #endregion
            }

        #endregion
    }

#endregion