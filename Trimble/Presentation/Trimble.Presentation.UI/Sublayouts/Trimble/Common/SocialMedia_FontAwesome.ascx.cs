﻿#region System
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
#endregion

#region Deloitte Framework
    using Deloitte.SC.UI.Sublayouts.Framework;
    using Deloitte.SC.Framework.Extensions;
    using Deloitte.SC.UI.Extensions;
#endregion

#region Sitecore
    using Sitecore.Web.UI.WebControls;
    using Sitecore.Data.Items;
using System.Web.UI.HtmlControls;
#endregion

#region Namespace

    namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
    {
        #region Partial Class

            public partial class SocialMedia_FontAwesome : SublayoutBaseExtended
            {
                #region private variables

                private string _divCssClass = string.Empty;

                #endregion

                #region Public Properties

                public virtual string DivCssClass
                {
                    get { return _divCssClass; }
                    set { _divCssClass = value; }
                }

                #endregion

                #region Virtual Methods

                /// <summary>
                    /// Override Page Load Method
                    /// </summary>
                    /// <param name="sender"></param>
                    /// <param name="e"></param>
                    public virtual void Page_Load(object sender, EventArgs e)
                    {
                        //Condition to check is page postback
                        if (!Page.IsPostBack)
                        {
                            //Calling Populate Method
                            Populate();
                        }

                        IsEditable();
                    }



                    /// <summary>
                    /// IsEditable
                    /// </summary>
                    protected void IsEditable()
                    {
                        try
                        {
                            Sublayout sublayout = this.Parent as Sublayout;
                            if (sublayout != null)
                            {
                                String sublayoutPath = sublayout.Path;
                                Placeholder placeholder = sublayout.Parent as Placeholder;
                                //the sitecore placeholder key
                                string urlToTrack = HttpContext.Current.Request.Url.AbsolutePath;
                                if ((placeholder != null) && !(urlToTrack == @"/") && (placeholder.Key == "scFooter") && (Sitecore.Context.PageMode.IsPageEditorEditing))
                                {
                                    placeholder.Visible = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("SocialMedia_FontAwesome:IsEditable" + ex.Message);
                        }
                    }




                    /// <summary>
                    /// This method is used to bind the childern item to repeater
                    /// </summary>
                    public virtual void Populate()
                    {
                        try
                        {
                            // Get Social Media folder item
                            Item SocialFolderItem = DataSourceItem;

                            if (!String.IsNullOrEmpty(DivCssClass))
                            {
                                divTitle.Visible = true;
                                frTitle.Visible = true;
                                frTitle.Item = DataSourceItem;
                            }

                            //Condition to check the item isn't null
                            if (SocialFolderItem != null)
                            {
                                //Condition to check the page is in editing mode
                                if (this.IsPageEditorEditing)
                                {
                                    //Enable Addlink visible for use
                                    efAddLink.Visible = true;

                                    //Assign fullpath to EditFrame datasource
                                    efAddLink.DataSource = SocialFolderItem.Paths.FullPath;

                                    // IF page in edit mode show all the Social Media items
                                    rptData.DataSource = SocialFolderItem.Children;

                                    // Added Style for new link
                                    AddNewLink.Style.Add(HtmlTextWriterStyle.BorderStyle, "1px solid black");
                                    AddNewLink.Style.Add(HtmlTextWriterStyle.Height, "height:20px");
                                    AddNewLink.Style.Add(HtmlTextWriterStyle.Color, "Black");

                                }
                                else //else show only four top Social Media on UI     
                                    rptData.DataSource = SocialFolderItem.Children.Take(6);  // bind items                                                               

                                //Bind repeater control
                                rptData.DataBind();
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("SocialMedia_FontAwesome:Populate" + ex.Message);
                        }
                    }

                #endregion 

                #region Protected Methods

                    protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
                    {
                        //Condition to check the listItem from the repeater row
                        if (e.IsListingItem())
                        {
                            //Get item instance from Sitecore
                            Item item = e.GetSitecoreItem();

                            //Condition to check the item isn't null
                            if (item != null)
                            {
                                try
                                {
                                    //Set Social Media Logo fieldRenderer from Sitecore item
                                    e.FindAndSetFieldRenderer("frSocialMediaLogo", item);

                                    //Set Social Media Target LinkRenderer from Sitecore item
                                    e.FindAndSetLink("lnkSocialMediaTarget", item);

                                    // apply path for datasource                  
                                    e.FindControl<EditFrame>("efActions").DataSource = item.Paths.Path;

                                    string imgSrcActive = string.Empty;
                                    string imgSrc = string.Empty;
                                    if (((Sitecore.Data.Fields.ImageField)item.Fields["SocialMediaActiveLogo"]).MediaItem != null)
                                    {
                                        Sitecore.Data.Fields.ImageField SocialMediaActiveLogo = ((Sitecore.Data.Fields.ImageField)item.Fields["SocialMediaActiveLogo"]);
                                        imgSrcActive = Sitecore.Resources.Media.MediaManager.GetMediaUrl(SocialMediaActiveLogo.MediaItem);
                                    }
                                    if (((Sitecore.Data.Fields.ImageField)item.Fields["SocialMediaLogo"]).MediaItem != null)
                                    {
                                        Sitecore.Data.Fields.ImageField SocialMediaLogo = ((Sitecore.Data.Fields.ImageField)item.Fields["SocialMediaLogo"]);
                                        imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(SocialMediaLogo.MediaItem);
                                    }


                                    if (imgSrc != null && imgSrcActive != null)
                                    {
                                        var lnk = e.FindControl<Link>("lnkSocialMediaTarget");
                                        lnk.Attributes.Add("data-hover-icon", imgSrcActive);
                                        lnk.Attributes.Add("data-icon", imgSrc);

                                    }
                                }
                                catch (Exception ex)
                                {
                                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                                    logger.Info("SocialMedia:rptData_ItemDataBound" + ex.Message);
                                }
                            }
                        }
                    }

                    

                #endregion
            }
        #endregion
    }
#endregion