﻿#region System

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.IO;

#endregion

#region Deloitte Framework

    using Deloitte.SC.UI.Sublayouts.Framework;
    using Deloitte.SC.Framework.Extensions;
    using Deloitte.SC.UI.Extensions;

#endregion

#region Sitecore

    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;

#endregion


#region Namespace
    namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
    {
        #region Partial class
            public partial class QuickLinks : SublayoutBaseExtended
            {
                #region Declaration

                    Sitecore.Data.Fields.GroupedDroplinkField frmediatype = null;

                    private Item[] DataItems { get; set; }
                    private int DataItemsCount { get; set; }
                    private string DataSourceItemPath { get; set; }
                    private string _activeCssClass { get; set; }

                #endregion

                #region Protected Methods
                    
                    // Page load method
                    protected void Page_Load(object sender, EventArgs e)
                    {
                        //Condition to check is page postback
                        if (!IsPostBack)
                        {
                            if (base.SupportsTemplates("{2F5228A5-5D63-4FBF-9587-F1DC8E4F8DB9}"))
                            {
                                //Calling Populate Method
                                Populate();
                            }
                            else
                            {
                                HtmlGenericControl div = (HtmlGenericControl)FindControl("QuickLinksMainDiv");
                                div.Visible = false;
                                this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                            }
                        }
                    }

                    // ItemDataBound event of rptQuickData Repeater Control
                    protected void rptQuickData_ItemDataBound(object sender, RepeaterItemEventArgs e)
                    {
                        //Condition to check the listItem from the repeater row
                        if (e.IsListingItem())
                        {
                            //Get item instance from Sitecore
                            Item item = e.GetSitecoreItem();

                            //Condition to check the item isn't null
                            if (item != null)
                            {
                                try
                                {
                                    //Set EditFrame Action 
                                    e.FindAndSetEditFrame("efActions", item);

                                    //Set QuickLink Target LinkRenderer from Sitecore item
                                    e.FindAndSetLink("lnkQuickLinkTarget", item);


                                    //Find lnkTile Literal Control
                                    var QuickLinkName = e.FindControl<Literal>("lnkTitle");

                                    //Assign the QuickLink Header Name
                                    QuickLinkName.Text = item.Name.ToString();

                                    //Assigning the media type value to Groupdroplink
                                    Sitecore.Data.Fields.GroupedDroplinkField frmediatype = item.Fields["QuickLinkMediaType"];

                                    //find the li_MediaType
                                    var li = e.FindControl<HtmlGenericControl>("li_mediatype");

                                    //Condition to check the media type shouldn't be null or empty
                                    if (String.IsNullOrEmpty(frmediatype.Value.ToString()))
                                    {
                                        //Add css class in Media type
                                        li.Attributes.Add("class", "internal-website");
                                    }
                                    else
                                    {
                                        if (frmediatype.Value.ToString() == "video")
                                        {
                                            //Add Mediatype value in class
                                            li.Attributes.Add("class", frmediatype.Value.ToString() + " " + "aboutvideo");
                                        }
                                        else
                                        {
                                            //Add Mediatype value in class
                                            li.Attributes.Add("class", frmediatype.Value.ToString());
                                        }

                                    }

                                    //Add li in UL
                                    ulMain.Controls.Add(li);
                                }
                                catch (Exception ex)
                                {
                                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                                    logger.Info("QuickLinks:rptQuickData_ItemDataBound" + ex.Message);
                                }

                            }
                        }
                    }

                #endregion

                #region Virtual Methods

                    // can be override to change this logic
                    public virtual Item GetDataSourceItem()
                    {
                        try
                        {
                            // update property for populate data
                            if (this.DataSourceItem != null)
                            {
                                //Assign all the childern item to dataItem
                                DataItems = this.DataSourceItem.Children.ToArray();

                                // Assign the count of DataItem to DataItemCount variable
                                DataItemsCount = DataItems.Count();

                                //Assign the path value of sitecore datasource for quicklink in DataSourceItemPath value
                                DataSourceItemPath = DataSourceItem.Paths.Path;

                                // Style for Add new Link
                                AddNewLink.Style.Add(HtmlTextWriterStyle.BorderStyle, "1px solid black");
                                AddNewLink.Style.Add(HtmlTextWriterStyle.Height, "height:20px");
                                AddNewLink.Style.Add(HtmlTextWriterStyle.Color, "Black");
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("QuickLinks:GetDataSourceItem" + ex.Message);
                        }

                        // return source item
                        return this.DataSourceItem;
                    }
               
                    //Populate Method
                    public virtual void Populate()
                    {
                        try
                        {
                            //Take quicklink items from sitecore datasource
                            Item QuickLinkItem = GetDataSourceItem();

                            //Condition to check to quicklinkitems isn't null
                            if (QuickLinkItem != null)
                            {
                                // for page editor mode visible insert link edit frame
                                if (this.IsPageEditorEditing)
                                {
                                    //Enable Addlink for end user if user in page edit mode
                                    efAddLink.Visible = true;

                                    //Assign full path of QuickLink datasource
                                    efAddLink.DataSource = QuickLinkItem.Paths.FullPath;

                                    // IF page in edit mode show all the quicklinks
                                    rptQuickData.DataSource = QuickLinkItem.Children;

                                }
                                else //else show only six top quicklinks on UI                              
                                    rptQuickData.DataSource = QuickLinkItem.Children.Take(6);


                                //Bind the repeater Control
                                rptQuickData.DataBind();
                            }

                            // Display quicklinktitle as header on page
                            //frTitle.Item = "<h1>" + DataSourceItem.Name + "</h1>";

                            frTitle.Item = DataSourceItem;
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("QuickLinks:Populate" + ex.Message);
                        }
                    }

                #endregion
               
            }
        #endregion
        }
#endregion