﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SLISearch.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.SLISearch" %>
<!-- for sli searh added by VJ 10-02-2013 -->
	<link rel="stylesheet" type="text/css" href="/html/styles/sli-rac.1.5.css"/>
    <link rel="stylesheet" type="text/css" href="/html/styles/sli-override.css"/>
<!-- end for sli searh -->

<script type="text/javascript">
    jQuery(function () {
        $('.topsearch .search').focus(function () {
            if (this.value == 'Search...') {
                this.value = '';
            }
        });
        $('.topsearch .search').blur(function () {
            if (this.value == '') {
                this.value = 'Search...';
            }
        });
        $('.topsearch .search').on('keydown', function (e) {
            if (e.which == 13)
                startSearch();
        });
        $('.topsearch input[name="Submit"]').click(function () {
            startSearch();
        });
        $('.menuTemplate1 .dropimg img').hover(swapImage, restoreImage);
    });
    function startSearch() {
        window.location.href = 'http://trimble.resultspage.com/search?p=Q&ts=custom&Submit=Submit+Query&w=' + $('.topsearch .search').val();
    }
    function swapImage() {
        $(this).attr('src', $(this).parent().data('hover-icon'));
    }

    function restoreImage() {
        $(this).attr('src', $(this).parent().data('icon'));
    }

    $(document).ready(function () {

        $(".searchIcon").click(function () {
            $(".searchWrapper").toggle("slide", { direction: "right" });
        });
    });
	

</script>
<style type="text/css">
    /******************Search Box*********************/
    .topsearch {
	    width:200px;
	    margin-top:0;
	    /*float: right;*/
    }
    .topsearch .form {
	    background:url(/html/images/searchbg.jpg) center top no-repeat;
	    width:200px;
	    height:36px;
	    line-height:36px;
	    border:0;
	    z-index:0;
	    color:#999999;
	    font-size:16px;
	    font-family:'Titillium Web', Arial, Helvetica, sans-serif;
	    margin-top:0;
    }
    .topsearch input {
	    width:160px;
	    z-index:99999;
	    outline:none;
	    border:0;
	    color:#999999;
	    font-size:16px;
	    font-family:'Titillium Web', Arial, Helvetica, sans-serif;
    }
    .topsearch .button {
	    text-indent:-5000px;
	    background:url(/html/images/searchicon.png) right top no-repeat;
	    width:30px;
	    line-height:19px;
	    border:0;
	    margin-top:0px;
	    z-index:9999;
	    cursor:pointer;
    }
    
    .searchIcon
    {
        float:right;
        width:32px;
        height:32px;
        background:#fff url(/html/images/searchicon.png) center center no-repeat;
        border:1px solid #fff;
        margin-top:6px;
        border-right:1px solid #eee;
    }
    .searchWrapper {
        float:right;
        width: 240px;
        height: 36px;
        display:none;
        margin-top:6px;
        
    }
    
    
    .slitopsearch .button {
	    background-color:#005F9E;
	    width:60px;
	    height:36px;
	    font-size:15px;
	    font-weight:bold;
	    color:#fff;
	    z-index:9999;
	    cursor:pointer;
	    border: 0;
    }
    
    .slitopsearch input {
	    width:160px;
	    height:24px;
	    z-index:99999;	   
	    padding:5px;
	    border: 1px solid #cccccc;
	    font-size:14px;
	    font-family:'Titillium Web', Arial, Helvetica, sans-serif;
    }
    
    
    /****************** End of Search Box*********************/
</style>

<div class="searchIcon"></div>
<div class="searchWrapper">
<div class="slitopsearch">
      <div class="form">
        
        <input type="text" class="input search" name="w"  value="Search..." onfocus="this.value=''" id="sli_search_1" autocomplete="off" onkeydown="javascript:return fnTrapKD(event,this);"/> 
        <input name="Submit" type="button" class="button" onclick="javascript: SliSearchById('sli_search_1');" value="Search"/>
        
      </div>
</div>
</div>



