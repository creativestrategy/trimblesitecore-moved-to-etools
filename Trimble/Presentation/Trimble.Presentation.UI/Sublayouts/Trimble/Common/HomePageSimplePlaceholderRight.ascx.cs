﻿using System;
using System.Web.UI;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public partial class HomePageSimplePlaceholderRight : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            GetHomePageSimplePlaceHolderRight();
        }

        /// <summary>
        /// Load Homepage Simple Placeholder (Middle and Right Components which exists in 2nd and 3rd columns)
        /// </summary>
        private void GetHomePageSimplePlaceHolderRight()
        {
            try
            {
                Item HomepageSimpleitemright = DataSourceItem;
                if (HomepageSimpleitemright != null)
                {

                    if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[CommonFields.Title].Value))
                    {
                        Sitecore.Data.Fields.TextField quote = HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.Quote];
                        Sitecore.Data.Fields.TextField freetext = HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.FreeText];

                        frHomepageSimpleTitle.Item = HomepageSimpleitemright;


                        if (quote != null && freetext != null)
                        {
                            //Show image if all other fields are not null
                            if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.FreeText].Value) && !String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.Quote].Value))
                                renderImage(HomepageSimpleitemright);
                            //show image if image and Quote are not null
                            else if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.Quote].Value))
                                renderImage(HomepageSimpleitemright);
                            //show image if image and FreeText are not null
                            else if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[CommonFields.Image].Value) && !String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                renderImage(HomepageSimpleitemright);
                            //show Quote if FreeText and Quote are not null
                            else if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.Quote].Value) && !String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                renderQuote(HomepageSimpleitemright);

                            else
                            {
                                //Show Image if other fields are nulls
                                if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[CommonFields.Image].Value))
                                    renderImage(HomepageSimpleitemright);
                                //Show Quote if other fields are nulls
                                else if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.Quote].Value))
                                    renderQuote(HomepageSimpleitemright);
                                //Show FreeText if other fields are nulls
                                else if (!String.IsNullOrEmpty(HomepageSimpleitemright.Fields[RandomSimplePlaceHolder.FreeText].Value))
                                    renderFreeText(HomepageSimpleitemright);
                            }
                        }
                    }
                }

            }
            catch
            {

            }
        }
        /// <summary>
        /// Render Image (remove FreeText and Quote)
        /// </summary>
        /// <param name="dtsource"></param>
        private void renderImage(Item item)
        {
            try
            {
                frHomepageSimpleImage.Item = item;
                HtmlGenericControl divFreeText = (HtmlGenericControl)(FindControl(RandomSimpleDivID.FreeText));
                divFreeText.Visible = false;
                HtmlGenericControl spanQuote = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Quote));
                spanQuote.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomePageSimpleplaceholderRight:renderImage" + ex.Message);
            }
        }
        /// <summary>
        /// Render Quote (remove FreeText and Image)
        /// </summary>
        /// <param name="dtsource"></param>
        private void renderQuote(Item item)
        {
            try
            {
                frHomepageSimpleQuote.Item = item;
                HtmlGenericControl divFreeText = (HtmlGenericControl)(FindControl(RandomSimpleDivID.FreeText));
                divFreeText.Visible = false;
                HtmlGenericControl divImage = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Image));
                divImage.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomePageSimpleplaceholderRight:renderQuote" + ex.Message);
            }
        }
        /// <summary>
        /// Render FreeText (remove Image and Quote)
        /// </summary>
        /// <param name="dtsource"></param>
        private void renderFreeText(Item item)
        {
            try
            {
                frHomepageSimpleFreeText.Item = item;
                HtmlGenericControl divImage = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Image));
                divImage.Visible = false;
                HtmlGenericControl spanQuote = (HtmlGenericControl)(FindControl(RandomSimpleDivID.Quote));
                spanQuote.Visible = false;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomePageSimpleplaceholderRight:renderFreeText" + ex.Message);
            }
        }
    }
}