<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Logo.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Common.Logo" %>
  <div id="logo">
        <sc:Link ID="lnkHomeLink" runat="server" Field="HomeLink" DisableWebEditing="false" >                   
                <sc:fieldrenderer runat="server" id="frSiteLogo" FieldName ="SiteLogo" DisableWebEditing="true"></sc:fieldrenderer>                 
        </sc:Link> 
  </div> 


