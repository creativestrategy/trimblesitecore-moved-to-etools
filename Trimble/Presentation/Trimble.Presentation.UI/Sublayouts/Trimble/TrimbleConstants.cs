﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trimble.Presentation.UI.Sublayouts.Trimble
{
    public static class EscapeCharacters
    {
        public const string Space = "&nbsp;";
        public const string ampersand = "&amp;";
    }

    public enum MaxCount
    {
        TabMaxCount = 5,
        RandomPlaceHolderMaxCount = 4,
        BusinessFeaturedProductModule = 15,
        FeaturedProductModule = 15,
        BusinessBanner = 4,
        AnchorLinks=11,
        FeaturedSolutionsModuleMax=12,
        FeaturedVideoModuleMax=12
    }
   
    public static class GeneralTabFields
    {
        public const string Tab1TitleFieldName = "Tab1Title";
        public const string Tab2TitleFieldName = "Tab2Title";
        public const string Tab3TitleFieldName = "Tab3Title";
        public const string Tab4TitleFieldName = "Tab4Title";
        public const string Tab5TitleFieldName = "Tab5Title";

        public const string Tab1ContentFieldName = "Tab1Content";
        public const string Tab2ContentFieldName = "Tab2Content";
        public const string Tab3ContentFieldName = "Tab3Content";
        public const string Tab4ContentFieldName = "Tab4Content";
        public const string Tab5ContentFieldName = "Tab5Content";

        public const string TabNamesPrefix = "Tab";
        public const string TabTitleSuffix = "Title";
        public const string TabContentSuffix = "Content";
        public const string TabContentFolderSuffix = "ContentFolder";
    }


    public static class IndustryBusinessTab
    {
        public const string DefaultImage = "Tab1DefaultImage";

    }

    public static class CommonFields
    {
        public const string Title = "Title";
        public const string Image = "Image";
        public const string link = "GeneralLink";
        public const string Text = "Text";
        public const string DefaultImage = "DefaultImage";
        public const string Home = "Home";
        public const string CategoryLink = "CategoryLink";

    }

    public static class RandomSimplePlaceHolder
    {
        public const string Quote = "Quote";
        public const string FreeText = "FreeText";
    }

    public static class SimplePlaceholder
    {
        public const string Quote = "Quote";
        public const string FreeText = "FreeText";
    }

    public static class RandomSimpleDivID
    {
        public const string Image = "divImage";
        public const string FreeText = "divFreeText";
        public const string Quote = "spanQuote";
    }

    public static class TemplateNames
    {
        public const string BannerTeaserPosition = "BannerTeaser";
        public const string BusinessBanner = "Business Banner Folder"; 
    }

    //et
    public static class BusinessBannerFields
    {
        public const string TeaserPosition = "TeaserPosition";
        public const string TeaserBackGround = "TeaserBackGround";
        public const string TeaserBackGroundTransparentOn = "Transparent BG ON";
        public const string TeaserBackGroundTransparentOff = "Transparent BG OFF";
        public const string TeaserBackGroundTransparentDivID = "gray-transparant-bg";
    }

    public static class BusinessBannerTeaserPositionDivID
    {
        public const string TopLeft="bannercontentTL";
         public const string TopRight="bannercontentTR";
         public const string BottomRight="bannercontentRB";
         public const string BottomLeft = "bannercontentBL";
    }

    public static class ProductVideoModule
    {
        public const string VideoLink = "VideoLink";
        public const string VideoImage = "VideoImage";
        public const string Description = "Description";
    }

    public static class ProductImagesModule
    {
        public const string Image1 = "Image1";
        public const string Image2 = "Image2";
        public const string Image3 = "Image3";
        public const string Image4 = "Image4";
    }

    public static class ProductModuleFields
    {
        public const string Image1 = "Image1";
        public const string Image2 = "Image2";
        public const string Image3 = "Image3";
        public const string Image4 = "Image4";
        public const string Image5 = "Image5";
    }


    //It contains all the constant Fields of footer modules
    public static class FooterModule
    {
        public const string Column1CategoryLink = "Column1CategoryLink";
        public const string Column2CategoryLink = "Column2CategoryLink";
        public const string Column3CategoryLink = "Column3CategoryLink";
        public const string Column4CategoryLink = "Column4CategoryLink";
        public const string Column5CategoryLink = "Column5CategoryLink";
    }

    public static class SupportModuleFields
    {
        public static string SupportBucketImage = "SupportCategoryImage";
    }
    
    public static class SupportBucketFields
    {
        public static string SupportBucketLink1 = "SupportBucket1Link";
        public static string SupportBucketLink2 = "SupportBucket2Link";
        public static string SupportBucketLink3 = "SupportBucket3Link";
        public const string Image1 = "SupportBucket1Image";
        public const string Image2 = "SupportBucket2Image";
        public const string Image3 = "SupportBucket3Image";
    }

    public static class Constants
    {
        #region Constants
        /// <summary>
        /// Constant for: /sitecore/system/Tasks/Schedules
        /// </summary>
        public static readonly string SchedulesFolder = "/sitecore/system/Tasks/Schedules";
        /// <summary>
        /// Constant for: System/Tasks/Schedule
        /// </summary>
        public static readonly string ScheduleTemplatePath = "System/Tasks/Schedule";
        /// <summary>
        /// Constant for the format for AutomatedPublisher ItemId
        /// </summary>
        public static readonly string AutomatedPublisherItemNameFormat = "ItemId_{0}_AutoPub";
        /// <summary>
        /// Constant for the format for AutomatedUnPublisher ItemId
        /// </summary>
        public static readonly string AutomatedUnPublisherItemNameFormat = "ItemId_{0}_AutoUnPub";
        /// <summary>
        /// Constant for: /sitecore/system/Tasks/Commands/Auto Publish
        /// </summary>
        public static readonly string AutoPublishCommandPath = "/sitecore/system/Tasks/Commands/publish";
        /// <summary>
        /// Constant for the format for AutomatedPublisher ItemVersionId
        /// </summary>
        public static readonly string AutomatedPublisherItemVersionNameFormat = "ItemId_{0}_{1}_V_{2}_AutoPub";
        /// <summary>
        /// Constant for the format for AutomatedUnPublisher ItemVersionId
        /// </summary>
        public static readonly string AutomatedUnPublisherItemVersionNameFormat = "ItemId_{0}_{1}_V_{2}_AutoUnPub";

        /// <summary>
        /// Constant for: [orphan]
        /// </summary>
        public static readonly string Orphan = "[orphan]";

        /// <summary>
        /// Constant for: shell
        /// </summary>
        public static readonly string SiteNameShell = "shell";
        /// <summary>
        /// Constant for the database name: master
        /// </summary>
        public static readonly string MasterDatabaseName = "master";
        /// <summary>
        /// Constant for: /sitecore/system/publishing targets
        /// </summary>
        public static readonly string PublishingTargetsFolder = "/sitecore/system/publishing targets";
        /// <summary>
        /// Constant for: target database
        /// </summary>
        public static readonly string TargetDatabase = "target database";

        /// <summary>
        /// Constant for field name Command
        /// </summary>
        public static readonly string Command = "Command";
        /// <summary>
        /// Constant for field name Items 
        /// </summary>
        public static readonly string Items = "Items";
        /// <summary>
        /// Constant for field name Schedule 
        /// </summary>
        public static readonly string Schedule = "Schedule";
        /// <summary>
        /// Constant for field name Last run 
        /// </summary>
        public static readonly string LastRun = "Last run";
        #endregion
    }
}