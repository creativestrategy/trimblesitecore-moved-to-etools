﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Pipelines.Save;
using Sitecore.Diagnostics;

namespace Trimble.Presentation.UI.Sublayouts.Trimble
{
    public class CustomMessage 
    {
        public void Process(SaveArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            args.CustomData["showvalidationdetails"] = true;
        }
    } 
}