﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.HtmlControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;
using log4net;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.About
{
    public partial class VideoDisplay : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item DataSource = DataSourceItem;
                if (DataSource != null)
                {
                    AssigningDataSource(DataSource);
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("VideoDisplay:Page_Load"+ex.Message);
            }

        }

        #region AssigningDataSource

        public void AssigningDataSource(Item DataSource)
        {
            Sitecore.Data.Fields.ImageField imgfield = DataSource.Fields["VideoImage"];
            if (imgfield != null)
            {
                if (String.IsNullOrEmpty(imgfield.Value))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        DataSource.Editing.BeginEdit();
                        Sitecore.Data.Fields.ImageField standardimg = DataSource.Template.StandardValues.Fields["VideoImage"];
                        DataSource.Fields["VideoImage"].Value = standardimg.Value;
                        DataSource.Editing.EndEdit();
                    }
                    imgvideoimage.Item = DataSource;
                }
                else
                    imgvideoimage.Item = DataSource;
            }



            Sitecore.Data.Fields.LinkField lnkfield = DataSource.Fields[CommonFields.link];
            if (lnkfield != null)
            {
                lnkvideo.Item = DataSourceItem;
            }

           

        }

        #endregion
    }

   

}