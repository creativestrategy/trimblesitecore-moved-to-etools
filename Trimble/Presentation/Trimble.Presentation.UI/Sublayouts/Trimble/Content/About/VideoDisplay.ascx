﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoDisplay.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.About.VideoDisplay" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".aboutvideo").colorbox({ iframe: true, innerWidth: 750, innerHeight: 510 });
    }); 
</script>
 <div class="full-width-blue-container">
        <div class="center-content">
            <div class="center-right-content">
                <div class="video-container padding30">
                    <sc:Link ID="lnkvideo" Field="GeneralLink" runat="server" CssClass="aboutvideo">
                        <sc:Image ID="imgvideoimage" CssClass="aboutPlaceholder" Field="VideoImage" runat="server" />
                        </sc:Link>
                        <br />
                </div>
            </div>
        </div>
</div>
         


