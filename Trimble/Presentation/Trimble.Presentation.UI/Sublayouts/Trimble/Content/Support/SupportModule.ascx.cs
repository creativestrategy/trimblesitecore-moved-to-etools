﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;
using System.Web;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Support
{
    public partial class SupportModule : SublayoutBaseExtended
    {

        List<Item> Main = new List<Item>();
        List<Item> ChildDataSource = new List<Item>();
        String UrltoTrack = HttpContext.Current.Request.Url.AbsoluteUri.ToString();
        int categoryCount = 1;
        int AnchorLinksCount = 1;
        int maxCount = (int)MaxCount.AnchorLinks;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Populate();
            if (!Page.IsPostBack)
                GetSupportTabContentModule();
        }

        /// <summary>
        /// Load Industry Tab Content Module
        /// </summary>
        private void GetSupportTabContentModule()
        {
            try
            {

                List<String> listFields = new List<string>();
                //Assign DataSource for the Tab Title FieldRenderer
                frTab1Title.Item = frTab2Title.Item = frTab3Title.Item = frTab4Title.Item = frTab5Title.Item = DataSourceItem;

                //Proper alignment of TabTitles and TabContents in Preview Mode
                if (!SitecoreItem.IsPageEditorEditing)
                {
                    //Add Tab Titles to List
                    List<string> strTabsTitle = new List<string>() { DataSourceItem.Fields[GeneralTabFields.Tab1TitleFieldName].Value, DataSourceItem.Fields[GeneralTabFields.Tab2TitleFieldName].Value, DataSourceItem.Fields[GeneralTabFields.Tab3TitleFieldName].Value, DataSourceItem.Fields[GeneralTabFields.Tab4TitleFieldName].Value, DataSourceItem.Fields[GeneralTabFields.Tab5TitleFieldName].Value };
                    //To get the indexes of tab titles that are not null/empty
                    List<int> strFinalTabsIndex = new List<int>();
                    //To get the indexes of tab titles that are null/empty
                    List<int> strEmptyTabsIndex = new List<int>();
                    //Get the Tab null/empty indexes
                    for (int k = 0; k < strTabsTitle.Count; k++)
                    {
                        if (!String.IsNullOrEmpty(strTabsTitle[k]))
                            strFinalTabsIndex.Add(k + 1);

                        else
                            strEmptyTabsIndex.Add(k + 1);
                    }

                    //Form field Names for title that aren't null/empty
                    for (int i = 1; i <= 5; i++)
                    {
                        foreach (int finalcontentindex in strFinalTabsIndex)
                        {
                            if (i == finalcontentindex)
                            {
                                listFields.Add(GeneralTabFields.TabNamesPrefix + i + GeneralTabFields.TabContentFolderSuffix);
                                break;
                            }
                        }

                    }




                    //loop through the target folder items for the Tab Content
                    for (int index = 0; index < listFields.Count; index++)
                    {
                        try
                        {
                            Item i = Sitecore.Context.Database.GetItem((DataSourceItem.GetFieldValue(listFields[index])));
                            //Add Source for the Tabs Content which are not null.
                            if (i != null)
                                Main.Add(i);
                            //reform the indexes of tab titles that are not null/empty based on the content.
                            else
                            {
                                strFinalTabsIndex.Remove(index + 1);
                                foreach (int k in strEmptyTabsIndex)
                                {
                                    if (k == (index + 1))
                                        break;
                                    else
                                        strEmptyTabsIndex.Add(index + 1);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("SupportModule:GetSupportTabContentModule" + ex.Message);


                        }

                    }
                    ////Proper rendering of Tab Titles
                    RemoveEmptyTabTitle(strEmptyTabsIndex, strFinalTabsIndex);


                }

                 //In Page editor show all the tabs and their respective content irrespective of empty values.
                else
                {
                    for (int i = 1; i <= 5; i++)
                    {
                        listFields.Add(GeneralTabFields.TabNamesPrefix + i + GeneralTabFields.TabContentFolderSuffix);
                    }
                    for (int index = 0; index < listFields.Count; index++)
                    {
                        Item i = Sitecore.Context.Database.GetItem((DataSourceItem.GetFieldValue(listFields[index])));
                        Main.Add(i);
                    }
                }
                //bind data to repeater.
                rpMain.DataSource = Main;
                rpMain.DataBind();
            }
            catch
            {//Remove the component 
                for (int i = 1; i <= 5; i++)
                {
                    var li = Ul1.FindControl("li" + i);
                    Ul1.Controls.Remove(li);
                }
            }
        }

        /// <summary>
        /// Form the Tab Titles respectively based on not null/empty indexes.
        /// </summary>
        /// <param name="EmptyCountList"></param>
        /// <param name="nonEmptylist"></param>
        private void RemoveEmptyTabTitle(List<int> EmptyCountList, List<int> nonEmptylist)
        {

            List<string> strTabTitleslist = new List<string>();
            //Form the Tab Titles that are finally shown with proper content
            foreach (int nonEmpty in nonEmptylist)
            {
                if (nonEmpty != 1)
                    strTabTitleslist.Add(GeneralTabFields.TabNamesPrefix + nonEmpty + GeneralTabFields.TabTitleSuffix);
            }
            try
            {
                frTab2Title.FieldName = strTabTitleslist[0];
            }
            catch
            {
                frTab2Title.FieldName = string.Empty;
                frTab3Title.FieldName = string.Empty;
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab3Title.FieldName = strTabTitleslist[1];
            }
            catch
            {
                frTab3Title.FieldName = string.Empty;
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab4Title.FieldName = strTabTitleslist[2];
            }
            catch
            {
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab5Title.FieldName = strTabTitleslist[3];
            }
            catch
            {
                frTab5Title.FieldName = string.Empty;
            }

            #region Remove the empty valued tabs
            int maxTabCount = 5;
            int EmptyTabcount = EmptyCountList.Count;
            while (EmptyTabcount > 0)
            {
                var li = Ul1.FindControl("li" + maxTabCount);
                Ul1.Controls.Remove(li);
                maxTabCount--;
                EmptyTabcount--;
            }
            #endregion
        }


        /// <summary>
        /// MainContent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpMain_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item Parent = e.GetSitecoreItem();
                
                if (Parent != null)
                {
                    if (this.IsPageEditorEditing)
                    {
                        EditFrame edframe = e.FindControl<EditFrame>("efAddCategory");
                        edframe.Visible = true;
                        edframe.DataSource = Parent.Paths.Path;
                    }
                    Repeater CategoryLevel = e.FindControl<Repeater>("rptSupportcategory");
                    CategoryLevel.DataSource = Parent.GetChildren();
                    CategoryLevel.DataBind();

                    try
                    {
                        ChildDataSource = GetSourceforLinks(Parent);
                        if (ChildDataSource != null && ChildDataSource.Count>0)
                        {
                            Repeater set1links = e.FindControl<Repeater>("rptSupportlinksset1");
                            set1links.DataSource = ChildDataSource;
                            set1links.DataBind();
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("SupportModule:rpMain_ItemDataBound" + ex.Message);


                    }
                }
            }
        }


        /// <summary>
        /// Set1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptSupportlinksset1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.IsListingItem())
            {
                Item Parent = e.GetSitecoreItem();

                if (Parent != null)
                {
                    e.FindAndSetFieldRenderer("frsupportCategorytitlelinks1", Parent);
                    var anchor = e.FindControl<HtmlAnchor>("anchorlink1");
                    anchor.Attributes.Add("href", UrltoTrack + "#anchor" + (AnchorLinksCount));
                    AnchorLinksCount++;
                }
            }
        }


        private List<Item> GetSourceforLinks(Item Parent)
        {
            List<Item> getChildrens = new List<Item>();
            List<Item> childs = new List<Item>();

            foreach (Item i in Parent.GetChildren())
            {
                getChildrens.Add(i);
            }

            if (getChildrens.Count > 0)
            {
                for (int i = 0; i < getChildrens.Count; i++)
                {
                        if (getChildrens[i] != null)

                            if (i > maxCount)
                                break;

                            childs.Add(getChildrens[i]);
                }
            }

            return childs;

        }

        protected void rptSupportcategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item child = e.GetSitecoreItem();
                if (child != null)
                {
                    if (this.IsPageEditorEditing)
                    {
                        e.FindAndSetEditFrame("efAddCategoryActions", child);
                    }
                    e.FindAndSetFieldRenderer("frcategorytitle", child);

                    Sitecore.Data.Fields.ImageField img = child.Fields[SupportModuleFields.SupportBucketImage];
                    if (img != null)
                    {
                        if (String.IsNullOrEmpty(img.Value))
                        {
                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                child.Editing.BeginEdit();
                                Sitecore.Data.Fields.ImageField standardimg = child.Template.StandardValues.Fields[SupportModuleFields.SupportBucketImage];
                                child.Fields[SupportModuleFields.SupportBucketImage].Value = standardimg.Value;
                                child.Editing.EndEdit();
                            }
                            e.FindAndSetFieldRenderer("frSupportCategoryImage", child);
                        }
                        else
                            e.FindAndSetFieldRenderer("frSupportCategoryImage", child);
                    }


                    //e.FindAndSetFieldRenderer("frSupportCategoryImage", child);
                    e.FindAndSetFieldRenderer("frSupportCategoryTeaser", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineTitle1", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineTitle2", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineTitle3", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineTitle4", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineText1", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineText2", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineText3", child);
                    e.FindAndSetFieldRenderer("frSupportBusinessLineText4", child);

                    var anchor = e.FindControl<HtmlAnchor>("anchorLink");
                    anchor.Attributes.Add("name", "anchor" + (categoryCount));
                    categoryCount++;
                }
            }
        }
    }

}