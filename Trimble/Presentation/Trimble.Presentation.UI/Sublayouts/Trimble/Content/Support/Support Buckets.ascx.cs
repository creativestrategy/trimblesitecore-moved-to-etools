﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Support
{
    public partial class Support_Buckets : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sitecore.Data.Fields.ImageField imgfield1 = DataSourceItem.Fields[SupportBucketFields.Image1];
                if (imgfield1 != null)
                {
                    if (String.IsNullOrEmpty(imgfield1.Value))
                    {
                        using (new Sitecore.SecurityModel.SecurityDisabler())
                        {
                            DataSourceItem.Editing.BeginEdit();
                            Sitecore.Data.Fields.ImageField standardimg = DataSourceItem.Template.StandardValues.Fields[SupportBucketFields.Image1];
                            DataSourceItem.Fields[SupportBucketFields.Image1].Value = standardimg.Value;
                            DataSourceItem.Editing.EndEdit();
                        }
                        ImgSupportBucket1Image.Item = DataSourceItem;
                    }
                    else
                       ImgSupportBucket1Image.Item = DataSourceItem;
                }

                 Sitecore.Data.Fields.ImageField imgfield2 = DataSourceItem.Fields[SupportBucketFields.Image2];
                if (imgfield2 != null)
                {
                    if (String.IsNullOrEmpty(imgfield2.Value))
                    {
                        using (new Sitecore.SecurityModel.SecurityDisabler())
                        {
                            DataSourceItem.Editing.BeginEdit();
                            Sitecore.Data.Fields.ImageField standardimg = DataSourceItem.Template.StandardValues.Fields[SupportBucketFields.Image2];
                            DataSourceItem.Fields[SupportBucketFields.Image2].Value = standardimg.Value;
                            DataSourceItem.Editing.EndEdit();
                        }
                        ImgSupportBucket2Image.Item = DataSourceItem;
                    }
                    else
                       ImgSupportBucket2Image.Item = DataSourceItem;
                }

                 Sitecore.Data.Fields.ImageField imgfield3 = DataSourceItem.Fields[SupportBucketFields.Image3];
                if (imgfield3 != null)
                {
                    if (String.IsNullOrEmpty(imgfield3.Value))
                    {
                        using (new Sitecore.SecurityModel.SecurityDisabler())
                        {
                            DataSourceItem.Editing.BeginEdit();
                            Sitecore.Data.Fields.ImageField standardimg = DataSourceItem.Template.StandardValues.Fields[SupportBucketFields.Image3];
                            DataSourceItem.Fields[SupportBucketFields.Image3].Value = standardimg.Value;
                            DataSourceItem.Editing.EndEdit();
                        }
                        ImgSupportBucket3Image.Item = DataSourceItem;
                    }
                    else
                       ImgSupportBucket3Image.Item = DataSourceItem;
                }

                
                frSupportBucket1Teaser.Item = frSupportBucket1Title.Item = frSupportBucket2Teaser.Item = frSupportBucket2Title.Item = frSupportBucket3Teaser.Item = frSupportBucket3Title.Item = DataSourceItem;
                lnkSupportBucket1Title.Item = lnkSupportBucket2Title.Item = lnkSupportBucket3Title.Item = DataSourceItem;

                Sitecore.Data.Fields.LinkField lnksupportBucket1 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[SupportBucketFields.SupportBucketLink1]);

                if (lnksupportBucket1 != null && !String.IsNullOrEmpty(lnksupportBucket1.Value))
                {
                    string strBucketlink1 = GetURL(lnksupportBucket1);
                    HtmlAnchor lnkBucket1 = (HtmlAnchor)FindControl("lnkSupportBucket1");
                    lnkBucket1.HRef = strBucketlink1;
                }


                Sitecore.Data.Fields.LinkField lnksupportBucket2 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[SupportBucketFields.SupportBucketLink2]);

                if (lnksupportBucket2 != null && !String.IsNullOrEmpty(lnksupportBucket2.Value))
                {
                    string strBucketlink2 = GetURL(lnksupportBucket2);
                    HtmlAnchor lnkBucket2 = (HtmlAnchor)FindControl("lnkSupportBucket2");
                    lnkBucket2.HRef = strBucketlink2;
                }


                Sitecore.Data.Fields.LinkField lnksupportBucket3 = ((Sitecore.Data.Fields.LinkField)DataSourceItem.Fields[SupportBucketFields.SupportBucketLink3]);

                if (lnksupportBucket3 != null && !String.IsNullOrEmpty(lnksupportBucket3.Value))
                {
                    string strBucketlink3 = GetURL(lnksupportBucket3);
                    HtmlAnchor lnkBucket3 = (HtmlAnchor)FindControl("lnkSupportBucket3");
                    lnkBucket3.HRef = strBucketlink3;
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("SupportBuckets:Page_Load" + ex.Message);


            }
        }
        /// <summary>
        /// Get the URL based on the LinkType
        /// </summary>
        /// <param name="lnkField"></param>
        /// <returns></returns>
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {
            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    return lnkField.Url;

                case "anchor":
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;

                case "mailto":
                    return lnkField.Url;

                case "javascript":
                    return lnkField.Url;

                default:
                    return lnkField.Url;
            }

        }
    }

}