﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Support Buckets.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Support.Support_Buckets" %>

<div class="categoryTitles">
    <div class="supportproducts">
        <a id="lnkSupportBucket1" class="anchorTitle" runat="server">
            <sc:Image ID="ImgSupportBucket1Image" runat="server" Field="SupportBucket1Image"
                CssClass="supportproductimage" />
        </a><span class="tabcontent_title">
            <sc:Link ID="lnkSupportBucket1Title" runat="server" Field="SupportBucket1Link">
                <sc:FieldRenderer ID="frSupportBucket1Title" runat="server" FieldName="SupportBucket1Title" />
            </sc:Link>
        </span>
        <p class="tabcontent">
            <sc:FieldRenderer ID="frSupportBucket1Teaser" runat="server" FieldName="SupportBucket1Teaser" />
        </p>
    </div>
    <div class="supportproducts">
        <a id="lnkSupportBucket2" class="anchorTitle" runat="server">
            <sc:Image ID="ImgSupportBucket2Image" runat="server" Field="SupportBucket2Image"
                CssClass="supportproductimage" />
        </a><span class="tabcontent_title">
            <sc:Link ID="lnkSupportBucket2Title" runat="server" Field="SupportBucket2Link">
                <sc:FieldRenderer ID="frSupportBucket2Title" runat="server" FieldName="SupportBucket2Title" />
            </sc:Link>
        </span>
        <p class="tabcontent">
            <sc:FieldRenderer ID="frSupportBucket2Teaser" runat="server" FieldName="SupportBucket2Teaser" />
        </p>
    </div>
    <div class="supportproducts">
        <a id="lnkSupportBucket3" class="anchorTitle" runat="server">
            <sc:Image ID="ImgSupportBucket3Image" runat="server" Field="SupportBucket3Image"
                CssClass="supportproductimage" />
        </a><span class="tabcontent_title">
            <sc:Link ID="lnkSupportBucket3Title" runat="server" Field="SupportBucket3Link">
                <sc:FieldRenderer ID="frSupportBucket3Title" runat="server" FieldName="SupportBucket3Title" />
            </sc:Link>
        </span>
        <p class="tabcontent">
            <sc:FieldRenderer ID="frSupportBucket3Teaser" runat="server" FieldName="SupportBucket3Teaser" />
        </p>
    </div>
</div>
