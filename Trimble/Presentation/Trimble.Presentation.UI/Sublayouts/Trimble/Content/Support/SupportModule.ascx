﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupportModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Support.SupportModule" %>
<script type="text/javascript">
    $(document).ready(function () {
        var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
    });

</script>
<div class="industry_tab">
    <div id="TabbedPanels1" class="TabbedPanels">
        <ul class="TabbedPanelsTabGroup" id="Ul1" runat="server">
            <li class="TabbedPanelsTab" tabindex="0" id="li1" runat="server">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab1Title" runat="server" FieldName="Tab1Title" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" id="li2" runat="server">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab2Title" runat="server" FieldName="Tab2Title" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" id="li3" runat="server">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab3Title" runat="server" FieldName="Tab3Title" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" id="li4" runat="server">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab4Title" runat="server" FieldName="Tab4Title" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" id="li5" runat="server">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab5Title" runat="server" FieldName="Tab5Title" />
                    </span>
                </p>
            </li>
        </ul>
        <div class="TabbedPanelsContentGroup">
            <asp:Repeater ID="rpMain" runat="server" OnItemDataBound="rpMain_ItemDataBound">
                <ItemTemplate>
                    <div class="TabbedPanelsContent">
                        <div class="supportproducts2" style="width: 100%">
                            <ul class="supporttab_link" style="width: 100%">
                                <asp:Repeater ID="rptSupportlinksset1" runat="server" OnItemDataBound="rptSupportlinksset1_ItemDataBound">
                                    <ItemTemplate>
                                        <li style="float:left;"><a class="supporttab_link" runat="server" id="anchorlink1">
                                            <sc:FieldRenderer ID="frsupportCategorytitlelinks1" runat="server" FieldName="Title" />
                                        </a></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div class="supporttitles">
                        </div>
                        <asp:Repeater ID="rptSupportcategory" runat="server" OnItemDataBound="rptSupportcategory_ItemDataBound">
                            <ItemTemplate>
                            <sc:EditFrame runat="server" ID="efAddCategoryActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
                                <div class="support_boxcontent">
                                    <div class="marginLeft10">
                                        <div class="tabcontent_img">
                                            <sc:FieldRenderer ID="frSupportCategoryImage" runat="server" FieldName="SupportCategoryImage" />
                                        </div>
                                        <div class="tabcontent_title">
                                            <a id="anchorLink" runat="server"></a>
                                            <sc:FieldRenderer ID="frcategorytitle" runat="server" FieldName="Title" />
                                        </div>
                                        <div class="tabcontent">
                                            <sc:FieldRenderer ID="frSupportCategoryTeaser" runat="server" FieldName="SupportCategoryTeaser" />
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div style="clear: both">
                                    </div>
                                    <div class="supportproducts3">
                                        <a class="support_title">
                                            <sc:FieldRenderer ID="frSupportBusinessLineTitle1" runat="server" FieldName="SupportBusinessLineTitle1" />
                                        </a>
                                        <div class="supportcontent">
                                            <sc:FieldRenderer ID="frSupportBusinessLineText1" runat="server" FieldName="SupportBusinessLineText1" />
                                        </div>
                                    </div>
                                    <div class="supportproducts3">
                                        <a class="support_title">
                                            <sc:FieldRenderer ID="frSupportBusinessLineTitle2" runat="server" FieldName="SupportBusinessLineTitle2" />
                                        </a>
                                        <div class="supportcontent">
                                            <sc:FieldRenderer ID="frSupportBusinessLineText2" runat="server" FieldName="SupportBusinessLineText2" />
                                        </div>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                                    <div class="supportproducts3">
                                        <a class="support_title">
                                            <sc:FieldRenderer ID="frSupportBusinessLineTitle3" runat="server" FieldName="SupportBusinessLineTitle3" />
                                        </a>
                                        <div class="supportcontent">
                                            <sc:FieldRenderer ID="frSupportBusinessLineText3" runat="server" FieldName="SupportBusinessLineText3" />
                                        </div>
                                    </div>
                                    <div class="supportproducts3">
                                        <a class="support_title">
                                            <sc:FieldRenderer ID="frSupportBusinessLineTitle4" runat="server" FieldName="SupportBusinessLineTitle4" />
                                        </a>
                                        <div class="supportcontent">
                                            <sc:FieldRenderer ID="frSupportBusinessLineText4" runat="server" FieldName="SupportBusinessLineText4" />
                                        </div>
                                    </div>
                                </div>
                                </sc:EditFrame>
                            </ItemTemplate>
                        </asp:Repeater>

                         <sc:EditFrame  runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Category">
                            <div id="AddNewLink" runat="server">
                            Add Category
                        </div>
                        </sc:EditFrame>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
