﻿using System;
using System.Collections.Generic;
using System.Linq;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;
using System.Text.RegularExpressions;
using Trimble.Presentation.UI.Sublayouts.Trimble;
using Sitecore.Web.UI.WebControls;
using Sitecore.Data.Items;
using System.Web.UI.HtmlControls;
using System.Web.UI;


namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.ListModules
{
    public partial class TabContentDetails : SublayoutBaseExtended
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                int intMxTabCnt = (int)MaxCount.TabMaxCount;
                if (base.SupportsTemplates("{3C0D7A65-8E46-40FF-9D9C-FCC83DCC9763}"))  
                {
                    GetTabContent();
                }
                else
                {
                    HtmlGenericControl div = (HtmlGenericControl)FindControl("mainDiv");
                    div.Visible = false;
                    this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                }
            }
        }
        
       

        /// <summary>
        /// Load Tab Content
        /// </summary>
        private void GetTabContent()
        {
            if (!SitecoreItem.IsPageEditorEditing)
            {
                try
                {
                    //Max count of tabs maintained 
                    int intMxTabCnt = (int)MaxCount.TabMaxCount;

                    //List<Char> escapecharac = new List<Char>();
                    //Replace("&nbsp;","").Replace("&amp;","")
                    //Add Tab Contents to list.
                    List<string> strTabsContent = new List<string>() { DataSourceItem.Fields[GeneralTabFields.Tab1ContentFieldName].Value, 
                                                                       DataSourceItem.Fields[GeneralTabFields.Tab2ContentFieldName].Value, 
                                                                       DataSourceItem.Fields[GeneralTabFields.Tab3ContentFieldName].Value, 
                                                                       DataSourceItem.Fields[GeneralTabFields.Tab4ContentFieldName].Value, 
                                                                       DataSourceItem.Fields[GeneralTabFields.Tab5ContentFieldName].Value};

                    //Add Tab Titles to List
                    List<string> strTabsTitle = new List<string>() { DataSourceItem.Fields[GeneralTabFields.Tab1TitleFieldName].Value, 
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab2TitleFieldName].Value,
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab3TitleFieldName].Value,
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab4TitleFieldName].Value,
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab5TitleFieldName].Value};


                    //Condition to check whether the contents from tab 2-4 are empty/null and titles from tab 2-4 are empty/null. In either case the
                    //control will not be rendered.

                    if ((!(string.IsNullOrEmpty(strTabsContent[1]) &&
                        string.IsNullOrEmpty(strTabsContent[2]) &&
                        string.IsNullOrEmpty(strTabsContent[3]) &&
                        string.IsNullOrEmpty(strTabsContent[4]))) && (!(string.IsNullOrEmpty(strTabsTitle[1]) &&
                        string.IsNullOrEmpty(strTabsTitle[2]) &&
                        string.IsNullOrEmpty(strTabsTitle[3]) &&
                        string.IsNullOrEmpty(strTabsTitle[4]))))
                    {

                        //Assigning DataSource
                        frTab1Title.Item = frTab2Title.Item = frTab3Title.Item = frTab4Title.Item = frTab5Title.Item =
                        frTab1Content.Item = frTab2Content.Item = frTab3Content.Item = frTab4Content.Item = frTab5Content.Item = DataSourceItem;

                        //Maintain Tabcontent and TabTitles list. This list will be assigned for non-empty/not null tab titles and tab contents
                        List<string> strTabContentslist = new List<string>();
                        List<string> strTabTitleslist = new List<string>();

                        //This carries the index values for which either Tab Title or Tab Content is null/empty
                        List<int> intindex = new List<int>();


                        //Get the index for which either Tab Title or Tab Content is empty/Null
                        //|| strTabsTitle[index].Contains(EscapeCharacters.whitespace)
                        for (int index = 0; index < intMxTabCnt; index++)
                        {
                            if (string.IsNullOrEmpty(strTabsContent[index].Trim()) || string.IsNullOrEmpty(strTabsTitle[index].Trim()))
                            {
                                intindex.Add(index);
                            }
                        }


                        //For those indexes make the valu Zero (Since Content without title and vice-versa doesn't make any sense)
                        for (int intEmptyValuedIndex = 0; intEmptyValuedIndex < intindex.Count(); intEmptyValuedIndex++)
                        {
                            strTabsContent[intindex[intEmptyValuedIndex]] = strTabsTitle[intindex[intEmptyValuedIndex]] = string.Empty;
                        }


                        //TabContents
                        //Form the field Name attribute for the field renderers 
                        for (int index = 0; index < intMxTabCnt; index++)
                        {

                            if (!string.IsNullOrEmpty(strTabsContent[index]))
                            {
                                if (!(index == 0))
                                    strTabContentslist.Add(GeneralTabFields.TabNamesPrefix + (index + 1) + GeneralTabFields.TabContentSuffix);
                            }
                        }


                        //TabTitles
                        //Form the Field Name attribute for the field renderers 
                        for (int index = 0; index < intMxTabCnt; index++)
                        {

                            if (!string.IsNullOrEmpty(strTabsTitle[index]))
                            {
                                if (!(index == 0))
                                    strTabTitleslist.Add(GeneralTabFields.TabNamesPrefix + (index + 1) + GeneralTabFields.TabTitleSuffix);
                            }
                        }


                        //Assign the TabTitle values to the controls
                        try
                        {
                            frTab2Title.FieldName = strTabTitleslist[0];
                        }
                        catch
                        {
                            frTab2Title.FieldName = string.Empty;
                            frTab3Title.FieldName = string.Empty;
                            frTab4Title.FieldName = string.Empty;
                            frTab5Title.FieldName = string.Empty;
                        }
                        try
                        {
                            frTab3Title.FieldName = strTabTitleslist[1];
                        }
                        catch
                        {
                            frTab3Title.FieldName = string.Empty;
                            frTab4Title.FieldName = string.Empty;
                            frTab5Title.FieldName = string.Empty;
                        }
                        try
                        {
                            frTab4Title.FieldName = strTabTitleslist[2];
                        }
                        catch
                        {
                            frTab4Title.FieldName = string.Empty;
                            frTab5Title.FieldName = string.Empty;
                        }
                        try
                        {
                            frTab5Title.FieldName = strTabTitleslist[3];
                        }
                        catch
                        {
                            frTab5Title.FieldName = string.Empty;
                        }





                        //Assigning TabContent Values to the fieldrenderes
                        try
                        {
                            frTab2Content.FieldName = strTabContentslist[0];
                        }
                        catch
                        {
                            frTab2Content.FieldName = string.Empty;
                            frTab3Content.FieldName = string.Empty;
                            frTab4Content.FieldName = string.Empty;
                            frTab5Content.FieldName = string.Empty;
                        }
                        try
                        {
                            frTab3Content.FieldName = strTabContentslist[1];
                        }
                        catch
                        {
                            frTab3Content.FieldName = string.Empty;
                            frTab4Content.FieldName = string.Empty;
                            frTab5Content.FieldName = string.Empty;
                        }
                        try
                        {
                            frTab4Content.FieldName = strTabContentslist[2];
                        }
                        catch
                        {
                            frTab4Content.FieldName = string.Empty;
                            frTab5Content.FieldName = string.Empty;
                        }
                        try
                        {
                            frTab5Content.FieldName = strTabContentslist[3];
                        }
                        catch
                        {
                            frTab5Content.FieldName = string.Empty;
                        }


                        //Removing the empty valued Tab Titles
                        int cntTabs2Remove = intindex.Count();

                        while (cntTabs2Remove > 0)
                        {
                            var li = mylist.FindControl("li" + intMxTabCnt);
                            mylist.Controls.Remove(li);
                            intMxTabCnt--;
                            cntTabs2Remove--;
                        }
                    }

                    else
                    {
                        HtmlGenericControl div = (HtmlGenericControl)FindControl("mainDiv");
                        div.Visible = false;
                        HtmlGenericControl generalText = (HtmlGenericControl)FindControl("genenralTextDiv");
                        generalText.Visible = true;
                        frGeneralText.Item = DataSourceItem;
                        
                    }


                }


                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("TabContentDetails:GetTabContent" + ex.Message);
                }
            }
            else
            {
                //Bind the TabTitle and TabContent according to indexes irrespective of values - whether null or empty.
                try
                {

                    frTab2Content.FieldName = GeneralTabFields.Tab2ContentFieldName;
                    frTab3Content.FieldName = GeneralTabFields.Tab3ContentFieldName;
                    frTab4Content.FieldName = GeneralTabFields.Tab4ContentFieldName;
                    frTab5Content.FieldName = GeneralTabFields.Tab5ContentFieldName;

                    frTab2Title.FieldName = GeneralTabFields.Tab2TitleFieldName;
                    frTab3Title.FieldName = GeneralTabFields.Tab3TitleFieldName;
                    frTab4Title.FieldName = GeneralTabFields.Tab4TitleFieldName;
                    frTab5Title.FieldName = GeneralTabFields.Tab5TitleFieldName;

                    frTab1Title.Item = frTab2Title.Item = frTab3Title.Item = frTab4Title.Item = frTab5Title.Item =
                       frTab1Content.Item = frTab2Content.Item = frTab3Content.Item = frTab4Content.Item = frTab5Content.Item = DataSourceItem;
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("TabContentDetails:GetTabContent" + ex.Message);
                }
            }
        }
    }
}