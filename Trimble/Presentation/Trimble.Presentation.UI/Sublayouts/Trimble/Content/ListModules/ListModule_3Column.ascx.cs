﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.ListModules
{
    public partial class ListModule_3Column : SublayoutBaseExtended
    {
        public int ItemCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            Populate();
        }

        //For Adding a Product
        private Item[] DataItems { get; set; }
        private Item Imagesource { get; set; }
        private Sitecore.Data.Fields.CheckboxField imageChk { get; set; }
        List<string> DataSourceChildItemPath = new List<string>();

        protected void Populate()
        {
            try
            {
                Item listModuleData = this.DataSourceItem;
                //For Adding a Product
                GetDataSourceItem();
                imageChk = this.DataSourceItem.Fields["HideImage"];
                if (listModuleData != null)
                {
                    // for page editor mode visible insert link edit frame
                    if (this.IsPageEditorEditing)
                    {
                        //Enable Addlink for end user if user in page edit mode
                        efAddCategory.Visible = true;

                        //Assign full path of QuickLink datasource
                        efAddCategory.DataSource = listModuleData.Paths.FullPath;

                        // IF page in edit mode show all the Category Links
                        //rptcategory.DataSource = DataSourceChildItemPath;

                    }
                    rptcategory.DataSource = listModuleData.Children;
                    rptcategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ListModule:Populate" + ex.Message);
            }
        }

        // can be override to change this logic
        public virtual void GetDataSourceItem()
        {
            try
            {
                // update property for populate data
                if (this.DataSourceItem != null)
                {
                    DataItems = this.DataSourceItem.Children.ToArray();
                    int IndexCount = DataItems.Count();
                    for (int i = 0; i < IndexCount; i++)
                    {
                        DataSourceChildItemPath.Add(DataItems[i].Paths.Path);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ListModule:GetDataSourceItem" + ex.Message);
            }

        }

        protected void rptcategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.IsListingItem())
            {
                Item child = e.GetSitecoreItem();
                try
                {
                    //For Adding a Product
                    EditFrame edframe = e.FindControl<EditFrame>("efAddListItemwithImage");
                    // for page editor mode visible insert link edit frame
                    if (this.IsPageEditorEditing)
                    {
                        edframe.Visible = true;
                        //efAddListItemwithImage.DataSource = sourceItem.Paths.FullPath;
                        edframe.DataSource = DataSourceChildItemPath[e.Item.ItemIndex];
                        e.FindAndSetEditFrame("efAddCategoryActions", child);
                    }


                    if (child != null)
                    {
                        var div = e.FindControl<HtmlGenericControl>("outerContainer");
                        var divproduct = e.FindControl<HtmlGenericControl>("productContainer");
                        if (ItemCount == 0 || (ItemCount % 2) == 0)
                        {

                            div.Attributes.Add("class", "full-width-white-container");
                            divproduct.Attributes.Add("class", "productsContainer2");

                        }
                        else
                        {
                            div.Attributes.Add("class", "full-width-blue-container");
                            divproduct.Attributes.Add("class", "productsContainer3");
                        }

                        /* JM 01/25/21 */
                        Sitecore.Data.Fields.TextField titlefield = child.Fields[CommonFields.Title];
                        if (titlefield != null)
                        {
                            if (child.Fields["Title"].Value.ToString() != "")
                            {
                                e.FindAndSetFieldRenderer("frcategorytitle", child);
                            }else{

                                HtmlGenericControl span = e.FindControl<HtmlGenericControl>("categoryTitleHeading");
                                span.Attributes.Add("style", "display:none");
                            }
                        }
                        
                        if (child.Fields["TeaserText"].HasValue)
                        {
                            e.FindAndSetFieldRenderer("frTeaserText", child);
                        }
                        else
                        {
                            HtmlGenericControl span = e.FindControl<HtmlGenericControl>("categoryTeaserText");
                            span.Attributes.Add("style", "display:none");
                        }


                        if (child.Fields["CategoryLinkText"].HasValue)
                        {
                            e.FindAndSetLink("lnCategoryLink", child);
                            e.FindAndSetFieldRenderer("frCategoryLinkText", child);
                            
                        }
                        else
                        {
                            HtmlGenericControl span = e.FindControl<HtmlGenericControl>("categoryMoreLink");
                            span.Attributes.Add("style", "display:none");
                        }

                        Imagesource = child;
                        Sitecore.Collections.ChildList childitem = child.GetChildren();
                        //e.FindAndSetFieldRenderer("frcategorytitle", child);
                        Repeater rptproduct = e.FindControl<Repeater>("rptproduct");
                        rptproduct.DataSource = childitem;
                        rptproduct.DataBind();
                        ItemCount++;
                    }
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("ListModule:rptcategory_ItemDataBound" + ex.Message);
                }
            }
        }

        protected void rptproduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item childitem = e.GetSitecoreItem();
                if (childitem != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddProductAction", childitem);
                        }
                        Sitecore.Data.Fields.LinkField lnkfield = childitem.Fields[CommonFields.link];
                        if (lnkfield != null)
                        {
                            e.FindAndSetLink("lnkproductlink2", childitem);
                        }
                        Sitecore.Data.Fields.TextField titlefield = childitem.Fields[CommonFields.Title];
                        if (titlefield != null)
                        {
                            e.FindAndSetFieldRenderer("frproducttitle", childitem);
                        }
                        Sitecore.Data.Fields.LinkField categoryLinkField = childitem.Fields[CommonFields.CategoryLink];
                        if (categoryLinkField != null)
                        {
                            e.FindAndSetFieldRenderer("lnkcategorylink", childitem);
                        }                      

                        Sitecore.Data.Fields.TextField txtfield = childitem.Fields[CommonFields.Text];
                        if (txtfield != null)
                        {
                            e.FindAndSetFieldRenderer("frtext", childitem);
                        }

                        if (!imageChk.Checked)
                        {

                            var prodimagewidth = e.FindControl<HtmlGenericControl>("divProd");
                            prodimagewidth.Attributes.Add("class", "categoryProducts productSet3 listProdWidth");
                            Sitecore.Data.Fields.ImageField img = childitem.Fields[CommonFields.Image];
                            if (img != null)
                            {
                                if (String.IsNullOrEmpty(img.Value))
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        childitem.Editing.BeginEdit();
                                        Sitecore.Data.Fields.ImageField standardimg = childitem.Template.StandardValues.Fields[CommonFields.Image];
                                        childitem.Fields[CommonFields.Image].Value = standardimg.Value;
                                        childitem.Editing.EndEdit();
                                    }
                                    e.FindAndSetFieldRenderer("frimage", childitem);
                                }
                                else
                                    e.FindAndSetFieldRenderer("frimage", childitem);
                            }
                            Sitecore.Data.Fields.LinkField lnkfield1 = childitem.Fields[CommonFields.link];
                            if (lnkfield1 != null)
                            {
                                e.FindAndSetLink("lnkproductlink1", childitem);
                            }
                        }
                        else
                        {                            
                            var prodimagewidth = e.FindControl<HtmlGenericControl>("divProd");
                            prodimagewidth.Attributes.Add("class", "categoryProducts productSet3 listProdWidth prodCategory_withoutImg");
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("ListModule:ItemDataBound" + ex.Message);
                    }
                }
            }
        }

        private void WriteImage(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("img");
            writer.WriteAttribute("alt", "AtlValue");
            writer.WriteAttribute("src", @"~\images\grey_arrow.png");
            writer.WriteEndTag("img");
        }
    }
}