﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabContentDetails.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.ListModules.TabContentDetails" %>
    
 <script type="text/javascript">
     try {
         $(document).ready(function () {
             var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
         });
     }
     catch (e) {
     }
</script>


 <div id="genenralTextDiv" style="float:left" runat="server" visible="false">
    <sc:FieldRenderer ID="frGeneralText" runat="server" FieldName="Tab1Content" />
    </div>

<div runat="server" id="mainDiv">
    <div id="TabbedPanels1" class="TabbedPanels">
        <ul class="TabbedPanelsTabGroup" id="mylist" runat="server">
            <li class="TabbedPanelsTab" tabindex="0" runat="server" id="li1">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab1Title" runat="server" FieldName="Tab1Title" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" runat="server" id="li2">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab2Title" runat="server" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" runat="server" id="li3">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab3Title" runat="server" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" runat="server" id="li4">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab4Title" runat="server" />
                    </span>
                </p>
            </li>
            <li class="TabbedPanelsTab" tabindex="0" runat="server" id="li5">
                <p>
                    <span>
                        <sc:FieldRenderer ID="frTab5Title" runat="server" />
                    </span>
                </p>
            </li>
        </ul>


        <div class="TabbedPanelsContentGroup">
            <div class="TabbedPanelsContent">
                <sc:FieldRenderer ID="frTab1Content" runat="server" FieldName="Tab1Content" />
                <br />
            </div>

            <div class="TabbedPanelsContent">
                
                    <sc:FieldRenderer ID="frTab2Content" runat="server" />
                    <br />
                
            </div>
            <div class="TabbedPanelsContent">
                
                    <sc:FieldRenderer ID="frTab3Content" runat="server" />
                    <br />
                
            </div>
            <div class="TabbedPanelsContent">
                
                    <sc:FieldRenderer ID="frTab4Content" runat="server" />
                    <br />
                
            </div>
            <div class="TabbedPanelsContent">
                
                    <sc:FieldRenderer ID="frTab5Content" runat="server" />
                    <br />
                
            </div>
        </div>
    </div>

   
</div>
