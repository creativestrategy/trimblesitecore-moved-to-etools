<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListModule.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.ListModules.ListModule" %>
<asp:Repeater ID="rptcategory" runat="server" OnItemDataBound="rptcategory_ItemDataBound">
       <ItemTemplate>
       <sc:EditFrame runat="server" ID="efAddCategoryActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
             <div ID = "outerContainer" runat="server">
                 <div class="center-content"> 
                    <div id="productContainer" runat="server">
                        <div id="about_video" class="paddBottom0 autoWidth">
                            <div class="productsMainContainer paddTop0 completeWidth">                            
                            <div class="paddLeft10">
                                <span class="categoryTitleHeading">
                                <sc:FieldRenderer ID="frcategorytitle" runat="server" FieldName="Title" />
                                </span>
                                <div class="categoryTitles">
                                    <asp:Repeater ID="rptproduct" runat="server" OnItemDataBound="rptproduct_ItemDataBound">
                                     <ItemTemplate>
                                     <sc:EditFrame runat="server" ID="efAddProductAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
                                        <div id="divProd" runat="server">                                           
                                            <sc:Link ID="lnkproductlink1" runat="server" Field="GeneralLink">
                                                   <sc:FieldRenderer ID="frimage" runat="server" FieldName="Image"/>                                                   
                                            </sc:Link>
                                            <sc:Link ID="lnkproductlink2" runat="server" Field="GeneralLink">
                                                     <span class="categoryProductTitle">
                                                        <sc:FieldRenderer ID="frproducttitle" runat="server" FieldName="Title" />
                                                    </span>
                                            </sc:Link>
                                            <p class="categoryProductDescription">
                                                <sc:FieldRenderer ID="frtext" runat="server" FieldName="Text" />
                                            </p>
                                        </div>
                                        </sc:EditFrame>
                                    </ItemTemplate>
                                    </asp:Repeater>
                                                                                            <!-- For Adding a Product -->
<sc:EditFrame  runat="server" ID="efAddListItemwithImage" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
    <div style="border: 1px solid black; height:20px; color: Black;">
        Add a Product
    </div>
</sc:EditFrame>
                                </div>
                                </div>
                            </div>
                        
                        
                        </div>
                    </div>
                </div>

            </div>
            </sc:EditFrame>
        </ItemTemplate>
</asp:Repeater>

<div class="full-width-white-container">
     <div class="center-content"> 
          <div class="center-right-content">
            <sc:EditFrame  runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Category">
                <div id="AddNewLink" runat="server">
                Add Category
            </div>
        </sc:EditFrame>
    </div>
    </div>
</div>




 
                       
                   

   