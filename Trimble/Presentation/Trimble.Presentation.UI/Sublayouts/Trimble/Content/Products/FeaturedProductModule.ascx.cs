﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class FeaturedProductModule : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetFeaturedProductModule();
        }

        private void GetFeaturedProductModule()
        {
            Item FeaturedProductDataSource = DataSourceItem;
            int maxCount = (int)MaxCount.FeaturedProductModule;

            if (FeaturedProductDataSource != null)
            {
                List<Item> listFeaturedProductDataSource = new List<Item>();
                List<Item> FinalFeaturedlistProductDataSource = new List<Item>();

                Random rnd = new Random();
                int count = FeaturedProductDataSource.GetChildren().Count;
                int randomNumber = 0;


                try
                {
                    for (int i = 0; i < count; i++)
                    {
                        Item nItem = FeaturedProductDataSource.GetChildren()[i];
                        listFeaturedProductDataSource.Add(nItem);
                    }

                    //If it is not in pageEditor
                    if (!SitecoreItem.IsPageEditorEditing)
                    {
                        frtitle.Item = DataSourceItem;
                        //if the count is less than 16 randomised based on the datasource count
                        if (listFeaturedProductDataSource.Count > 0 && listFeaturedProductDataSource.Count < maxCount)
                        {
                            randomNumber = rnd.Next(1, (listFeaturedProductDataSource.Count + 1));
                            FinalFeaturedlistProductDataSource.Add(listFeaturedProductDataSource[randomNumber - 1]);
                        }
                        //if the count is greater than 15 limit to 15 and pick an item randomly
                        else if (listFeaturedProductDataSource.Count >= maxCount)
                        {
                            randomNumber = rnd.Next(1, 16);
                            FinalFeaturedlistProductDataSource.Add(listFeaturedProductDataSource[randomNumber - 1]);

                        }
                    }

                    else
                    {
                        frtitle.Item = DataSourceItem;
                        //If not in page edit mode show all the items
                        foreach (Item i in listFeaturedProductDataSource)
                        {
                            FinalFeaturedlistProductDataSource.Add(i);
                        }

                        //Enable Addlink for end user if user in page edit mode and items count is less than 14
                        if (FinalFeaturedlistProductDataSource.Count < maxCount)
                            efAddProduct.Visible = true;
                        else
                            efAddProduct.Visible = false;
                        
                        efAddProduct.DataSource = FinalFeaturedlistProductDataSource[0].Parent.Paths.FullPath;
                        
                    }

                    //Bind the data
                    rpFeaturedProduct.DataSource = FinalFeaturedlistProductDataSource;
                    rpFeaturedProduct.DataBind();
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("FeaturedProductModule:GetFeaturedProductModule" + ex.Message);
                }

            }

        }


        /// <summary>
        /// Bind the Feature Product Module
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpFeaturedProduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();


                if (item != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddProductActions", item);

                        }
                        FieldRenderer fldrenderImage = (FieldRenderer)e.Item.FindControl("frFeaturedProdImage");
                        Sitecore.Data.Fields.ImageField imgfield = item.Fields[CommonFields.Image];
                        if (imgfield != null)
                        {
                            if (String.IsNullOrEmpty(imgfield.Value))
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    item.Editing.BeginEdit();
                                    Sitecore.Data.Fields.ImageField standardimg = item.Template.StandardValues.Fields[CommonFields.Image];
                                    item.Fields[CommonFields.Image].Value = standardimg.Value;
                                    item.Editing.EndEdit();
                                }
                                e.FindAndSetFieldRenderer("frFeaturedProdImage", item);
                            }
                            else
                                e.FindAndSetFieldRenderer("frFeaturedProdImage", item);
                        }

                        Sitecore.Data.Fields.TextField titlefield = item.Fields[CommonFields.Title];
                        if (titlefield != null)
                        {
                            e.FindAndSetFieldRenderer("frFeaturedProdTitle", item);
                        }


                        Sitecore.Data.Fields.TextField txtfield = item.Fields[CommonFields.Text];
                        if (txtfield != null)
                        {
                            e.FindAndSetFieldRenderer("frFeaturedProdText", item);
                        }

                        Sitecore.Data.Fields.LinkField lnkfield = item.Fields[CommonFields.link];
                        if (lnkfield != null)
                        {
                            e.FindAndSetLink("lnFeatureProductLink", item);                          
                        }

                        Sitecore.Data.Fields.LinkField lnkfield1 = item.Fields[CommonFields.link];
                        if (lnkfield1 != null)
                        {
                            e.FindAndSetLink("lnFeatureProductLink1", item);
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("FeaturedProductModule:rpFeaturedProduct_ItemDataBound" + ex.Message);
                    }

                }

            }
        }



        /// <summary>
        /// Get the URL based on the LinkType
        /// </summary>
        /// <param name="lnkField"></param>
        /// <returns></returns>
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {
            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    return lnkField.Url;

                case "anchor":
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;

                case "mailto":
                    return lnkField.Url;

                case "javascript":
                    return lnkField.Url;

                default:
                    return lnkField.Url;
            }

        }
    }
}