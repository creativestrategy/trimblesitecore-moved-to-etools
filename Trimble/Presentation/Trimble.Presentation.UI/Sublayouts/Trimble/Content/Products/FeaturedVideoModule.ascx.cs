﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;
using System.Web.UI;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class FeaturedVideoModule : SublayoutBaseExtended
    {

        private string DataSourceChildItemPath { get; set; }


        // can be override to change this logic
        public virtual void GetChildDataSourceItem()
        {
            try
            {
                // update property for populate data
                if (this.DataSourceItem != null)
                {
                    DataSourceChildItemPath = this.DataSourceItem.Paths.Path;

                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("FeaturedVideoModule:GetChildDataSourceItem" + ex.Message);
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (base.SupportsTemplates("{7BEFA6A8-0847-4955-BFE7-3FDFB7C585DA}"))
            {
                GetFeaturedVideoModule();
            }
            else
            {
                HtmlGenericControl div = (HtmlGenericControl)FindControl("mainDiv");
                div.Visible = false;
                this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
            }
        }

        private void GetFeaturedVideoModule()
        {
            GetChildDataSourceItem();
            Item FeaturedVideomodule = DataSourceItem;
            int maxCount = (int)MaxCount.FeaturedVideoModuleMax;

            if (FeaturedVideomodule != null)
            {
                List<Item> listFeaturedProductVideoDataSource = new List<Item>();
                List<Item> FinalFeaturedlistProductVideoDataSource = new List<Item>();

                if (FeaturedVideomodule !=null)
                {
                    try
                    {
                        //Get the DataSource
                        for (int i = 0; i < FeaturedVideomodule.Children.Count; i++)
                        {
                            Item nItem = FeaturedVideomodule.GetChildren()[i];
                            listFeaturedProductVideoDataSource.Add(nItem);
                        }

                        //If it in page editor show 3 at random
                        if (!SitecoreItem.IsPageEditorEditing)
                        {
                            
                            //if the number of items are less than 12 based on the item count pick 3 at random
                            if (listFeaturedProductVideoDataSource.Count < maxCount)
                            {
                                List<int> test2 = GetRandom(listFeaturedProductVideoDataSource.Count);


                                for (int i = 0; i < test2.Count; i++)
                                {
                                    FinalFeaturedlistProductVideoDataSource.Add(listFeaturedProductVideoDataSource[test2[i] - 1]);
                                }

                            }
                                //else limit to 12 and pick 3 at random
                            else
                            {
                                List<int> test2 = GetRandom(maxCount);

                                for (int i = 0; i < test2.Count; i++)
                                {
                                    FinalFeaturedlistProductVideoDataSource.Add(listFeaturedProductVideoDataSource[test2[i] - 1]);
                                }

                            }
                        }

                        else
                        {
                            //if the count is less than 13 then enable add video edit frame.
                            if (!String.IsNullOrEmpty(DataSourceChildItemPath) && listFeaturedProductVideoDataSource.Count < maxCount)
                            {
                                lblAddVideo.Text = "Add a Video";
                                efAddFeaturedProductVideo.Visible = true;
                                efAddFeaturedProductVideo.DataSource = DataSourceChildItemPath;
                            }
                            //show all the videoproducts in pageedit mode.
                            FinalFeaturedlistProductVideoDataSource = listFeaturedProductVideoDataSource;
                        }


                        //DataBind
                        frtitle.Item = DataSourceItem;
                        rpFeaturedVideo.DataSource = FinalFeaturedlistProductVideoDataSource;
                        rpFeaturedVideo.DataBind();
                        
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("FeaturedVideoModule:GetFeaturedVideoModule" + ex.Message);
                    }
                }
                

            }

        }


        private List<int> GetRandom(int p)
        {
            List<int> randNum = new List<int>();
            List<int> test = new List<int>();
            Random rnd = new Random();
            int j = 1;

            for (int i = 1; i <= p; i++)
            {
                test.Add(i);
            }

            while (test.Count > 0)
            {
                int idx = rnd.Next(0, test.Count);
                randNum.Add(test[idx]);
                test.RemoveAt(idx);
                j++;
                if (j > 3)
                    break;
            }
            return randNum;
        }


        /// <summary>
        /// Bind the Feature Product Module
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpFeaturedVideoProduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();


                if (item != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efFeaturedProductVideoActions", item);
                        }

                        Sitecore.Data.Fields.LinkField lnkVideo = item.Fields[ProductVideoModule.VideoLink];
                        if (lnkVideo != null)
                        {
                            string strLinkURL = GetURL(lnkVideo);
                            //Make the Title as Link
                            HtmlAnchor lnkTitleAnchor = e.FindControl<HtmlAnchor>("anchorVideo");
                            lnkTitleAnchor.HRef = strLinkURL;
                            e.FindAndSetLink("lnkvideo", item);
                        }

                        Sitecore.Data.Fields.ImageField VideoImage = item.Fields[ProductVideoModule.VideoImage];
                        if (VideoImage != null)
                        {
                            e.FindAndSetFieldRenderer("frProductVideoModuleImage", item);
                        }

                        Sitecore.Data.Fields.TextField VideoTitle = item.Fields[CommonFields.Title];
                        if (VideoTitle != null)
                        {
                            e.FindAndSetFieldRenderer("frProductVideoModuleTitle", item);
                        }

                        Sitecore.Data.Fields.TextField VideoDescription = item.Fields[ProductVideoModule.Description];
                        if (VideoTitle != null)
                        {
                            e.FindAndSetFieldRenderer("frProductVideoModuleText", item);
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("FeaturedVideoModule:rpFeaturedVideoProduct_ItemDataBound" + ex.Message);
                    }

                }

            }
        }



        /// <summary>
        /// Get the URL based on the LinkType
        /// </summary>
        /// <param name="lnkField"></param>
        /// <returns></returns>
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {
            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    return lnkField.Url;

                case "anchor":
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;

                case "mailto":
                    return lnkField.Url;

                case "javascript":
                    return lnkField.Url;

                default:
                    return lnkField.Url;
            }

        }
    }
}