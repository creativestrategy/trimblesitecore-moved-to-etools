﻿using System;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;



namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class ProductModule : SublayoutBaseExtended
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (DataSourceItem != null)
                {
                    AssignDataSource(DataSourceItem);
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductModule:Page_Load" + ex.Message);
            }

        }

        #region AssignDataSource
        //Assigning the datasource to the respective fields
        private void AssignDataSource(Item DataSource)
        {
            //Assining DataSource for Link Feilds
            lnkproductlink1.Item = lnkproductlink2.Item = lnkproductlink3.Item =
                lnkproductlink4.Item = lnkproductlink5.Item = DataSource;

            Sitecore.Data.Fields.ImageField img1 = DataSource.Fields[ProductModuleFields.Image1];
            if (img1 != null)
            {
                if (String.IsNullOrEmpty(img1.Value))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        DataSource.Editing.BeginEdit();
                        Sitecore.Data.Fields.ImageField standardimg = DataSource.Template.StandardValues.Fields[ProductModuleFields.Image1];
                        DataSource.Fields[ProductModuleFields.Image1].Value = standardimg.Value;
                        DataSource.Editing.EndEdit();
                    }
                    imgproductimage1.Item = DataSource;
                }
                else
                    imgproductimage1.Item = DataSource;
            }

            Sitecore.Data.Fields.ImageField img2 = DataSource.Fields[ProductModuleFields.Image2];
            if (img2 != null)
            {
                if (String.IsNullOrEmpty(img2.Value))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        DataSource.Editing.BeginEdit();
                        Sitecore.Data.Fields.ImageField standardimg = DataSource.Template.StandardValues.Fields[ProductModuleFields.Image2];
                        DataSource.Fields[ProductModuleFields.Image2].Value = standardimg.Value;
                        DataSource.Editing.EndEdit();
                    }
                    imgproductimage2.Item = DataSource;
                }
                else
                    imgproductimage2.Item = DataSource;
            }

            Sitecore.Data.Fields.ImageField img3 = DataSource.Fields[ProductModuleFields.Image3];
            if (img3 != null)
            {
                if (String.IsNullOrEmpty(img3.Value))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        DataSource.Editing.BeginEdit();
                        Sitecore.Data.Fields.ImageField standardimg = DataSource.Template.StandardValues.Fields[ProductModuleFields.Image3];
                        DataSource.Fields[ProductModuleFields.Image3].Value = standardimg.Value;
                        DataSource.Editing.EndEdit();
                    }
                    imgproductimage3.Item = DataSource;
                }
                else
                    imgproductimage3.Item = DataSource;
            }

            Sitecore.Data.Fields.ImageField img4 = DataSource.Fields[ProductModuleFields.Image4];
            if (img4 != null)
            {
                if (String.IsNullOrEmpty(img4.Value))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        DataSource.Editing.BeginEdit();
                        Sitecore.Data.Fields.ImageField standardimg = DataSource.Template.StandardValues.Fields[ProductModuleFields.Image4];
                        DataSource.Fields[ProductModuleFields.Image4].Value = standardimg.Value;
                        DataSource.Editing.EndEdit();
                    }
                    imgproductimage4.Item = DataSource;
                }
                else
                    imgproductimage4.Item = DataSource;
            }

            Sitecore.Data.Fields.ImageField img5 = DataSource.Fields[ProductModuleFields.Image5];
            if (img5 != null)
            {
                if (String.IsNullOrEmpty(img5.Value))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        DataSource.Editing.BeginEdit();
                        Sitecore.Data.Fields.ImageField standardimg = DataSource.Template.StandardValues.Fields[ProductModuleFields.Image5];
                        DataSource.Fields[ProductModuleFields.Image5].Value = standardimg.Value;
                        DataSource.Editing.EndEdit();
                    }
                    imgproductimage5.Item = DataSource;
                }
                else
                    imgproductimage5.Item = DataSource;
            }
        }
        #endregion


    }
}