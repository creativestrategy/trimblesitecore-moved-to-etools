﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;
using Trimble.Presentation.UI.Sublayouts.Trimble;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class ProductListingModuleSet2 : SublayoutBaseExtended
    {
        private Item[] ChildDataItems { get; set; }

        private List<string> DataSourceChildItemPath = new List<string>();

        private Item imagedatasource { get; set; }

        private Sitecore.Data.Fields.CheckboxField imageChk { get; set; }
        
        public void GetChildDataSourceItem()
        {
            // update property for populate data
            if (this.DataSourceItem != null)
            {
                try
                {
                    ChildDataItems = this.DataSourceItem.Children.ToArray();
                    int IndexCount = ChildDataItems.Count();
                    for (int i = 0; i < IndexCount; i++)
                    {
                        DataSourceChildItemPath.Add(ChildDataItems[i].Paths.Path);
                    }
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("ProductListingModuleSet2:GetChildDataSourceItem" + ex.Message);
                }
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (DataSourceItem != null)
                {
                    Populate(DataSourceItem);
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductListingModuleSet2:Page_Load" + ex.Message);
            }
        }
        public int ItemCount = 0;
        public int childcount = 0;
        public int count = 0;
        protected void Populate(Item DataSource)
        {
            Item productmodulelistingdata = DataSourceItem;
            //For Adding a Product
            GetChildDataSourceItem();
            imageChk = this.DataSourceItem.Fields["HideImage"];
            if (productmodulelistingdata != null)
            {
                // for page editor mode visible insert link edit frame
                if (this.IsPageEditorEditing)
                {
                    //Enable Addlink for end user if user in page edit mode
                    efAddCategory.Visible = true;

                    //Assign full path of QuickLink datasource
                    efAddCategory.DataSource = productmodulelistingdata.Paths.FullPath;

                    // IF page in edit mode show all the Category Links
                    //rptcategory.DataSource = DataSourceChildItemPath;

                }
                rptcategory.DataSource = productmodulelistingdata.Children;
                rptcategory.DataBind();
            }
        }

        protected void rptcategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.IsListingItem())
            {
                Item child = e.GetSitecoreItem();

                if (child != null)
                {
                    try
                    {
                        //Page EditorfFor Adding a Product
                        EditFrame editframe = e.FindControl<EditFrame>("efAddProduct");
                        // for page editor mode visible insert product edit frame
                        if (this.IsPageEditorEditing)
                        {
                            editframe.Visible = true;

                            e.FindAndSetEditFrame("efAddCategoryActions", child);
                            editframe.DataSource = DataSourceChildItemPath[e.Item.ItemIndex];
                        }

                        var div = e.FindControl<HtmlGenericControl>("outerContainer");
                        var innerdiv = e.FindControl<HtmlGenericControl>("innerContainer");
                        var prodContainer = e.FindControl<HtmlGenericControl>("prodContainer");
                        
                        //var divproduct = e.FindControl<HtmlGenericControl>("productContainer");
                        if (ItemCount == 0 || (ItemCount % 2) == 0)
                        {

                            div.Attributes.Add("class", "full-width-blue-container marginTop32minus");
                            innerdiv.Attributes.Add("class", "productsContainer");
                            prodContainer.Attributes.Add("class", "paddTop10 productSet2-withBg");
                        }
                        else
                        {
                            div.Attributes.Add("class", "full-width-white-container marginTop32minus");
                            innerdiv.Attributes.Add("class", "productsContainer2");
                            prodContainer.Attributes.Add("class", "paddTop10 productSet2-withoutBg");
                        }
                        imagedatasource = child;
                        Sitecore.Collections.ChildList childitem = child.GetChildren();
                        childcount = childitem.Count;
                        e.FindAndSetFieldRenderer("frcategorytitle", child);
                        Repeater rptproduct = e.FindControl<Repeater>("rptproduct");
                        rptproduct.DataSource = childitem;
                        rptproduct.DataBind();
                        ItemCount++;
                        count = 0;
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("ProductListingModuleSet2:rptcategory_ItemDataBound" + ex.Message);
                    }
                }
            }
        }

        protected void rptproduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.IsListingItem())
            {
                Item childitem = e.GetSitecoreItem();

                try
                {
                    if (childitem != null)
                    {

                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddProductAction", childitem);
                        }
                        Sitecore.Data.Fields.TextField titlefield = childitem.Fields[CommonFields.Title];
                        if (titlefield != null)
                        {
                            e.FindAndSetFieldRenderer("frproducttitle", childitem);
                        }
                        Sitecore.Data.Fields.LinkField categoryLinkField = childitem.Fields[CommonFields.CategoryLink];
                        if (categoryLinkField != null)
                        {
                            e.FindAndSetFieldRenderer("lnkcategorylink", childitem);
                        }

                        //FieldRenderer fldrenderImage = (FieldRenderer)e.Item.FindControl("frimage");
                        var div = e.FindControl<HtmlGenericControl>("divCategoryList");
                        if (!imageChk.Checked)
                        {
                            div.Attributes.Add("class", "bizLineContent2");
                            Sitecore.Data.Fields.ImageField imgfield = childitem.Fields[CommonFields.Image];
                            if (imgfield != null)
                            {
                                if (String.IsNullOrEmpty(imgfield.Value))
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        childitem.Editing.BeginEdit();
                                        Sitecore.Data.Fields.ImageField standardimg = childitem.Template.StandardValues.Fields[CommonFields.Image];
                                        childitem.Fields[CommonFields.Image].Value = standardimg.Value;
                                        childitem.Editing.EndEdit();
                                    }
                                    e.FindAndSetFieldRenderer("frimage", childitem);
                                }
                                else
                                    e.FindAndSetFieldRenderer("frimage", childitem);
                            }
                        }
                        else
                            div.Attributes.Add("class", "bizLineContent2 industryCategory_withoutImg");

                        Sitecore.Data.Fields.TextField txtfield = childitem.Fields[CommonFields.Text];
                        if (txtfield != null)
                        {
                            e.FindAndSetFieldRenderer("frtext", childitem);
                        }
                        Sitecore.Data.Fields.LinkField lnkfield = childitem.Fields[CommonFields.link];
                        if (lnkfield != null)
                        {
                            e.FindAndSetLink("lnkproductlink", childitem);
                        }
                        Sitecore.Data.Fields.LinkField lnkfield1 = childitem.Fields[CommonFields.link];
                        if (lnkfield != null)
                        {
                            e.FindAndSetLink("lnktitlelink", childitem);
                        }
                        count++;
                    }

                    if (count == childcount)
                    {
                        var div1 = e.FindControl<HtmlGenericControl>("border");
                        div1.Visible = false;
                    }
                    else
                    {
                        var div1 = e.FindControl<HtmlGenericControl>("border");
                        div1.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("ProductListingModuleSet2:rptproduct_ItemDataBound" + ex.Message);
                }
                }
            }
        }
    }