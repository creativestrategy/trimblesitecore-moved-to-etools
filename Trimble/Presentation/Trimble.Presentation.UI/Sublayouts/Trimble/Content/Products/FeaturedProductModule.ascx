﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedProductModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.FeaturedProductModule" %>
<div class="featuredProduct">
    <div class="featuredProductTitle">
        <sc:FieldRenderer ID="frtitle" runat="server" FieldName="Title" />
    </div>
    <asp:Repeater runat="server" ID="rpFeaturedProduct" OnItemDataBound="rpFeaturedProduct_ItemDataBound">
        <ItemTemplate>
         <sc:EditFrame runat="server" ID="efAddProductActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
            <div class="featuredProductImageContainer">
               <sc:Link ID="lnFeatureProductLink1" runat="server" Field="GeneralLink" >  
                    <sc:FieldRenderer ID="frFeaturedProdImage" FieldName="Image" runat="server"/>
               </sc:Link>
            </div>
            <div class="featuredProductContainer">
                <div class="featuredProductDescriptionContainer">
                    <sc:Link ID="lnFeatureProductLink" runat="server" Field="GeneralLink" >                  
                        <sc:FieldRenderer ID="frFeaturedProdTitle" runat="server" FieldName="Title" />
                   </sc:Link>
                    <p class="featuredProductDescription">
                        <sc:FieldRenderer ID="frFeaturedProdText" runat="server" FieldName="Text" />
                    </p>
                </div>
            </div>
            </sc:EditFrame>
        </ItemTemplate>
    </asp:Repeater>
     <sc:EditFrame  runat="server" ID="efAddProduct" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
     <div id="AddNewLink" runat="server">
        Add Product
    </div>
</sc:EditFrame>
</div>
