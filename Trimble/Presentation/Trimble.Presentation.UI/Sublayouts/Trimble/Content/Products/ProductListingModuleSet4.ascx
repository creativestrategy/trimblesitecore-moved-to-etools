﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListingModuleSet4.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.ProductListingModuleSet4" %>
<%@ Register TagPrefix="uc" TagName="BaseListingModule" Src="~/Sublayouts/Trimble/Content/Products/BaseListingModule.ascx" %>

    <div>
        <uc:BaseListingModule id="ProdListing4" runat="server" />
    </div>
