﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using System.IO;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Web;
using Sitecore.Web.UI.WebControls;
using Trimble.Presentation.UI.Sublayouts.Trimble;
using log4net;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class SimplePlaceholderModule : SublayoutBaseExtended
    {
        Item dataSourceItem = null;
        protected Item GetDataSourceItem()
        {
            this.dataSourceItem = this.DataSourceItem;
            return this.dataSourceItem;
        } 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetDataSourceItem();
                if (base.SupportsTemplates("{978FB2E6-4D17-4E90-BD4F-202ABDE4E17D}"))
                {
                    GetSimplePlaceholder();
                }
                else
                {
                    this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                }
            }
            
        }
        #region GetSimplePlaceholder
        private void GetSimplePlaceholder()
        {
            try
            {
                Item item = dataSourceItem;
                if (!String.IsNullOrEmpty(item.Fields[CommonFields.Title].Value.ToString()))
                {
                    Sitecore.Data.Fields.TextField quote = item.Fields[SimplePlaceholder.Quote];
                    Sitecore.Data.Fields.TextField freetext = item.Fields[SimplePlaceholder.FreeText];

                    if (quote != null && freetext != null)
                    {
                        if (String.IsNullOrEmpty(item.Fields[CommonFields.Image].Value.ToString()) && String.IsNullOrEmpty(item.Fields[SimplePlaceholder.Quote].Value.ToString()) && String.IsNullOrEmpty(item.Fields[SimplePlaceholder.FreeText].Value.ToString()))
                        {
                            if (this.IsPageEditorEditing)
                            {
                                RenderAll(item);
                            }
                        }
                        else if (!String.IsNullOrEmpty(item.Fields[CommonFields.Image].Value.ToString()) && !String.IsNullOrEmpty(item.Fields[SimplePlaceholder.Quote].Value.ToString()) && !String.IsNullOrEmpty(item.Fields[SimplePlaceholder.FreeText].Value.ToString()))
                        {
                            RenderImage(item);
                        }

                        else if (!String.IsNullOrEmpty(item.Fields[CommonFields.Image].Value.ToString()) && !String.IsNullOrEmpty(item.Fields[SimplePlaceholder.Quote].Value.ToString()))
                            RenderImage(item);

                        else if (!String.IsNullOrEmpty(item.Fields[CommonFields.Image].Value.ToString()) && !String.IsNullOrEmpty(item.Fields[SimplePlaceholder.FreeText].Value.ToString()))
                            RenderImage(item);

                        else if (!String.IsNullOrEmpty(item.Fields[SimplePlaceholder.Quote].Value.ToString()) && !String.IsNullOrEmpty(item.Fields[SimplePlaceholder.FreeText].Value.ToString()))
                            RenderQuote(item);

                        else
                        {
                            if (!String.IsNullOrEmpty(item.Fields[CommonFields.Image].Value.ToString()))
                                RenderImage(item);

                            else if (!String.IsNullOrEmpty(item.Fields[SimplePlaceholder.Quote].Value.ToString()))
                                RenderQuote(item);

                            else if (!String.IsNullOrEmpty(item.Fields[SimplePlaceholder.FreeText].Value.ToString()))
                                RenderFreeText(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("SimplePlaceholderModule:GetSimplePlaceholder" + ex.Message);
            }
            }
        //}
        #endregion

        #region Render Quote Control
        /// <summary>
        /// Rendering the Quote Control
        /// </summary>
        /// <param name="item"></param>
        public void RenderQuote(Item item)
        {
            try
            {
                frquote.Item = item;

                imgproductimage.Visible = false;

                dvfreetext.Visible = false;

            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("SimplePlaceholderModule:RenderQuote" + ex.Message);
            }
        }
        #endregion

        #region Render FreeText Control

        /// <summary>
        /// Rendering the Free Text Control
        /// </summary>
        /// <param name="item"></param>
        private void RenderFreeText(Item item)
        {
            try
            {

                frtitle.Item = frfreetext.Item = item;
                dvfreetext.Visible = true;

                spquote.Visible = false;
                imgproductimage.Visible = false;

            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("SimplePlaceholderModule:RenderFreeText" + ex.Message);
            }
        }
        #endregion

        #region Render Image Control
        /// <summary>
        /// Rendering the Image Control
        /// </summary>
        /// <param name="item"></param>
        private void RenderImage(Item item)
        {
            try
            {
                imgproductimage.Item = item;

                dvfreetext.Visible = false;
                spquote.Visible = false;

            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("SimplePlaceholderModule:RenderImage" + ex.Message);
            }
          
            }
        #endregion

        private void RenderAll(Item item)
        {
            try
            {
                frquote.Item = item;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("SimplePlaceholderModule:RenderAll" + ex.Message);
            }
        }

       
    }
}