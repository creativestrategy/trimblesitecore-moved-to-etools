﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListingModuleSet2.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.ProductListingModuleSet2" %>
<%--<div class="marginTop40">
</div>--%>
<asp:Repeater ID="rptcategory" runat="server" OnItemDataBound="rptcategory_ItemDataBound">
    <ItemTemplate>
        <sc:EditFrame runat="server" ID="efAddCategoryActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
            <div id="outerContainer" runat="server">
                <div class="center-content">
                    <div class="center-right-content">
                        <div runat="server" id="innerContainer">
                            <div runat="server" id="prodContainer">
                            <br />
                                <div class="categoryTitleHeading">
                                    <sc:FieldRenderer ID="frcategorytitle" runat="server" FieldName="Title" />
                                </div>
                                <sc:Link ID="lnkcategorylink" runat="server" Field="CategoryLink" />
                                <asp:Repeater ID="rptproduct" runat="server" OnItemDataBound="rptproduct_ItemDataBound">
                                    <ItemTemplate>
                                        <sc:EditFrame runat="server" ID="efAddProductAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
                                            <div class="bizLineItemContainer">
                                                <br />
                                                <sc:Link ID="lnkproductlink" runat="server" Field="GeneralLink">
                                                    <div class="tabcontent_img">
                                                        <sc:FieldRenderer ID="frimage" runat="server" FieldName="Image" />
                                                    </div>
                                                </sc:Link>
                                                <div id="divCategoryList" runat="server">
                                                    <sc:Link ID="lnktitlelink" runat="server" Field="GeneralLink">
                                                        <div class="tabcontent_title">
                                                            <sc:FieldRenderer ID="frproducttitle" runat="server" FieldName="Title" />
                                                        </div>
                                                    </sc:Link>
                                                    <div class="tabcontent">
                                                        <sc:FieldRenderer ID="frtext" runat="server" FieldName="Text" />
                                                    </div>
                                                </div>
                                            </div>
                                        </sc:EditFrame>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <sc:EditFrame runat="server" ID="efAddProduct" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
                                    <div id="AddNewLink" runat="server">
                                        Add Product
                                    </div>
                                </sc:EditFrame>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </sc:EditFrame>
    </ItemTemplate>
</asp:Repeater>
<div class="full-width-white-container">
     <div class="center-content"> 
          <div class="center-right-content">
            <sc:EditFrame  runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Category">
                <div id="AddNewLink" runat="server">
                Add Category
            </div>
        </sc:EditFrame>
    </div>
    </div>
</div>
