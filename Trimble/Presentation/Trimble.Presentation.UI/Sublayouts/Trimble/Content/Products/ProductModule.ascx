﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.ProductModule" %>
     <div class="product_detail">
    <sc:Link ID="lnkproductlink1" runat="server" Field="Link1">
        <sc:Image ID="imgproductimage1" Field="Image1" runat="server" Alt="products" CssClass="product_detail_img" />
    </sc:Link>
    <sc:Link ID="lnkproductlink2" runat="server" Field="Link2">
        <sc:Image ID="imgproductimage2" Field="Image2" runat="server" Alt="products" CssClass="product_detail_img" />
    </sc:Link>
    <sc:Link ID="lnkproductlink3" runat="server" Field="Link3">
        <sc:Image ID="imgproductimage3" Field="Image3" runat="server" Alt="products" CssClass="product_detail_img" />
    </sc:Link>
    <sc:Link ID="lnkproductlink4" runat="server" Field="Link4">
        <sc:Image ID="imgproductimage4" Field="Image4" runat="server" Alt="products" CssClass="product_detail_img" />
    </sc:Link>
    <sc:Link ID="lnkproductlink5" runat="server" Field="Link5">
        <sc:Image ID="imgproductimage5" Field="Image5" runat="server" Alt="products" CssClass="product_detail_img" />
    </sc:Link>
    </div>
