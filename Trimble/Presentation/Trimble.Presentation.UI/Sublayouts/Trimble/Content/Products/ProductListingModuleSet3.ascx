﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListingModuleSet3.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.ProductListingModuleSet3" %>
<%@ Register TagPrefix="uc" TagName="BaseListingModule" Src="~/Sublayouts/Trimble/Content/Products/BaseListingModule.ascx" %>
<div class="marginTop40">
</div>
<asp:Repeater ID="rptcategory" runat="server" OnItemDataBound="rptcategory_ItemDataBound">
    <ItemTemplate>
        <sc:EditFrame runat="server" ID="efAddCategoryActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
            <div id="outerContainer" runat="server">
                <div class="center-content">
                    <div class="center-right-content">
                        <div class="autoWidth paddTop32">
                            <div class="productsMainContainer paddTop0">
                                <div class="padding30">
                                    <span class="categoryTitleHeading">
                                        <sc:FieldRenderer ID="frcategorytitle" runat="server" FieldName="Title" />
                                    </span>
                                    <sc:Link ID="lnkcategorylink" runat="server" Field="CategoryLink" />
                                    <asp:Image ID="image1" runat="server" alt="arrowimage" src="../../../html/images/grey_arrow.png"
                                        Visible="false" />
                                    <div class="categoryTitles">
                                        <asp:Repeater ID="rptproduct" runat="server" OnItemDataBound="rptproduct_ItemDataBound">
                                            <ItemTemplate>
                                                <sc:EditFrame runat="server" ID="efAddProductAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
                                                    <div class="prodFamilySet3Container">
                                                        <div class="prodFamilySet3Content">
                                                            <span>
                                                                <sc:FieldRenderer ID="frtext" runat="server" FieldName="Text" />
                                                            </span>
                                                        </div>
                                                        <div class="main_content_border margin-auto">
                                                        </div>
                                                        <div class="prodFamilySet3Title">
                                                            <sc:Link ID="lnkproductlink" runat="server" Field="GeneralLink">
                                                                <sc:FieldRenderer ID="frimage" runat="server" FieldName="Image" />
                                                                <sc:FieldRenderer ID="frproducttitle" runat="server" FieldName="Title" />
                                                                <asp:Image ID="image1" runat="server" alt="arrowimage" src="../../../html/images/grey_arrow.png"
                                                                    Visible="false" />
                                                            </sc:Link>
                                                        </div>
                                                    </div>
                                                </sc:EditFrame>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <sc:EditFrame runat="server" ID="efAddProduct" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
                                            <div id="AddNewLink" runat="server">
                                                Add Product
                                            </div>
                                        </sc:EditFrame>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </sc:EditFrame>
    </ItemTemplate>
</asp:Repeater>
<div class="full-width-white-container">
     <div class="center-content"> 
          <div class="center-right-content">
            <sc:EditFrame  runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Category">
                <div id="AddNewLink" runat="server">
                Add Category
            </div>
        </sc:EditFrame>
    </div>
    </div>
</div>
