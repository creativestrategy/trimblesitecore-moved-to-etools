﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class BaseListingModule : SublayoutBaseExtended
    {
        public BaseListingHelperClass _prodDataSource = new BaseListingHelperClass();

        public BaseListingHelperClass prodDataSource
        {
            get { return _prodDataSource; }
            set { _prodDataSource = value; }
        }

        private Item[] ChildDataItems { get; set; }

        private Item imagedatasource { get; set; }

        private List<string> DataSourceChildItemPath = new List<string>();

        private Sitecore.Data.Fields.CheckboxField imageChk { get; set; }

        public int ItemCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (DataSourceItem != null)
                {
                    Populate(DataSourceItem);                   
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("BaseListingModule:Page_Load" + ex.Message);


            }
        }

      

        // can be override to change this logic
        public virtual void GetChildDataSourceItem()
        {
            try
            {
                // update property for populate data
                if (this.prodDataSource.DataSourceValue != null)
                {
                    ChildDataItems = this.prodDataSource.DataSourceValue.Children.ToArray();
                    int IndexCount = ChildDataItems.Count();
                    for (int i = 0; i < IndexCount; i++)
                    {
                        DataSourceChildItemPath.Add(ChildDataItems[i].Paths.Path);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("BaseListingModule:GetChildDataSource"+ex.Message);

            }

        }
        protected void Populate(Item DataSource)
        {
            Item productmodulelistingdata = this.prodDataSource.DataSourceValue;
            //For Adding a Product
            GetChildDataSourceItem();
            imageChk = this.prodDataSource.DataSourceValue.Fields["HideImage"];
            if (productmodulelistingdata != null)
            {
                // for page editor mode visible insert link edit frame
                if (this.IsPageEditorEditing)
                {
                    //Enable Addlink for end user if user in page edit mode
                    efAddCategory.Visible = true;

                    //Assign full path of QuickLink datasource
                    efAddCategory.DataSource = productmodulelistingdata.Paths.FullPath;

                    // IF page in edit mode show all the Category Links
                    //rptcategory.DataSource = DataSourceChildItemPath;

                }
                rptcategory.DataSource = productmodulelistingdata.Children;
                rptcategory.DataBind();
            }
        }

        protected void rptcategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
           
            if (e.IsListingItem())
            { 
                Item child = e.GetSitecoreItem();
                try
                {

                    //Page EditorfFor Adding a Product
                    EditFrame editframe = e.FindControl<EditFrame>("efAddProduct");
                    // for page editor mode visible insert product edit frame
                    if (this.IsPageEditorEditing)
                    {
                        editframe.Visible = true;

                        e.FindAndSetEditFrame("efAddCategoryActions", child);
                        editframe.DataSource = DataSourceChildItemPath[e.Item.ItemIndex];
                    }


                    if (child != null)
                    {

                        var div = e.FindControl<HtmlGenericControl>("outerContainer");
                        //var divproduct = e.FindControl<HtmlGenericControl>("productContainer");
                        if (ItemCount == 0 || (ItemCount % 2) == 0)
                        {

                            div.Attributes.Add("class", "full-width-white-container");
                            // divproduct.Attributes.Add("class", "productsContainer2");

                        }
                        else
                        {
                            div.Attributes.Add("class", "full-width-blue-container");
                            // divproduct.Attributes.Add("class", "productsContainer3");
                        }
                        Sitecore.Collections.ChildList childitem = child.GetChildren();
                        Sitecore.Data.Fields.TextField categoryTitle = child.Fields[CommonFields.Title];
                        if (categoryTitle != null)
                        {
                            e.FindAndSetFieldRenderer("frcategorytitle", child);
                        }
                        Sitecore.Data.Fields.LinkField categoryLinkField = child.Fields[CommonFields.CategoryLink];
                        if (categoryLinkField != null && !String.IsNullOrEmpty(categoryLinkField.Value))
                        {

                            var anchor = e.FindControl<Sitecore.Web.UI.WebControl>("lnkcategorylink");
                            anchor.Attributes.Add("class", "categoryComparison");
                            e.FindAndSetLink("lnkcategorylink", child);
                            var img = e.FindControl<System.Web.UI.Control>("image1");
                            img.Visible = true;

                        }
                        imagedatasource = child;
                        Repeater rptproduct = e.FindControl<Repeater>("rptproduct");
                        rptproduct.DataSource = childitem;
                        rptproduct.DataBind();
                        ItemCount++;
                    }
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("BaseListingModule:rptcategory_ItemDataBound" + ex.Message);


                }
            }
        }

        protected void rptproduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item childitem = e.GetSitecoreItem();
                if (childitem != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddActions", childitem);
                        }                      

                        Sitecore.Data.Fields.TextField titlefield = childitem.Fields[CommonFields.Title];
                        if (titlefield != null)
                        {
                            e.FindAndSetFieldRenderer("frproducttitle", childitem);
                        }
                        var div = e.FindControl<HtmlGenericControl>("categoryList");
                        if (!imageChk.Checked)
                        {
                            div.Attributes.Add("class", "categoryProducts productSet4");

                            Sitecore.Data.Fields.ImageField img = childitem.Fields[CommonFields.Image];
                            if (img != null)
                            {
                                if (String.IsNullOrEmpty(img.Value))
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        childitem.Editing.BeginEdit();
                                        Sitecore.Data.Fields.ImageField standardimg = childitem.Template.StandardValues.Fields[CommonFields.Image];
                                        childitem.Fields[CommonFields.Image].Value = standardimg.Value;
                                        childitem.Editing.EndEdit();
                                    }
                                    e.FindAndSetFieldRenderer("frimage", childitem);
                                }
                                else
                                    e.FindAndSetFieldRenderer("frimage", childitem);
                            }
                        }
                        else 
                        {
                            div.Attributes.Add("class", "categoryProducts productSet4 prodCategory_withoutImg");
                        }
                       

                        Sitecore.Data.Fields.TextField txtfield = childitem.Fields[CommonFields.Text];
                        if (txtfield != null)
                        {
                            e.FindAndSetFieldRenderer("frtext", childitem);
                        }
                        Sitecore.Data.Fields.LinkField lnkfield = childitem.Fields[CommonFields.link];
                        if (lnkfield != null)
                        {
                            e.FindAndSetLink("lnkproductlink", childitem);
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("BaseListingModule:rptproduct_ItemDataBound" + ex.Message);


                    }
                }

            }
        }

        private void WriteImage(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("img");
            writer.WriteAttribute("alt", "AtlValue");
            writer.WriteAttribute("src", @"~\images\grey_arrow.png");
            writer.WriteEndTag("img");
        }
    }
}