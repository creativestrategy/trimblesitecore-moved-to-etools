﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Sublayouts.Framework;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class ProductListingModuleSet4 : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.ProdListing4.prodDataSource.DataSourceValue = this.DataSourceItem;
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductListingModuleSet4:Page_Load" + ex.Message);
            }
        }
    }
}