﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;


namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class BusinessLineFutureProductModule : SublayoutBaseExtended
    {

        private Item[] ChildDataItems { get; set; }

        private string DataSourceChildItemPath { get; set; }


        // can be override to change this logic
        public virtual void GetChildDataSourceItem()
        {
            // update property for populate data
            if (this.DataSourceItem != null)
            {
                DataSourceChildItemPath = this.DataSourceItem.Paths.Path;

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (base.SupportsTemplates("{72BB680C-5888-461E-BAB1-71FBCF3D52D8}"))
                {
                    //Calling Populate Method
                    GetBusinessLineFeaturedProductModule();
                }
                else
                {
                    HtmlGenericControl div = (HtmlGenericControl)FindControl("container_transportation");
                    div.Visible = false;
                    this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                }
            }



        }


        /// <summary>
        /// Load  Business Line Featured Product Module
        /// </summary>
        private void GetBusinessLineFeaturedProductModule()
        {

            Item BLFeaturedProductDataSource = DataSourceItem;
            GetChildDataSourceItem();
            if (BLFeaturedProductDataSource != null)
            {
                List<Item> BlistFeaturedProductDataSource = new List<Item>();
                List<Item> BlistFeaturedFinal = new List<Item>();


                int count = BLFeaturedProductDataSource.GetChildren().Count;
                int maxCount = (int)MaxCount.BusinessFeaturedProductModule;

                try
                {
                    if (count > 0)
                    {
                        //Get the children to form the source
                        for (int i = 0; i < count; i++)
                        {
                            Item nItem = BLFeaturedProductDataSource.GetChildren()[i];
                            BlistFeaturedProductDataSource.Add(nItem);
                        }
                        //In preview mode show only 3 items
                        if (!SitecoreItem.IsPageEditorEditing)
                        {
                            //if the child count is less than 12 based on the count form the data source
                            if (BlistFeaturedProductDataSource.Count < maxCount)
                            {
                                //On each page load get any 3 non repetitive random numbers
                                List<int> test2 = GetRandom(BlistFeaturedProductDataSource.Count);

                                //Load the 3 random childs
                                for (int i = 0; i < test2.Count; i++)
                                {
                                    var dtsource = BlistFeaturedProductDataSource[test2[i] - 1];
                                    //DataBind(dtsource,i+1);
                                    BlistFeaturedFinal.Add(dtsource);
                                }

                                //frFeaturedProdTitle.Item = frFeaturedProdImage.Item = frFeaturedProdText.Item = dtsource;

                            }
                            //if the count is more than 12, take top 12 items and pick 3 non repetitive random numbers
                            else
                            {
                                List<int> test2 = GetRandom(maxCount);
                                for (int i = 0; i < test2.Count; i++)
                                {
                                    var dtsource = BlistFeaturedProductDataSource[test2[i] - 1];
                                    //DataBind(dtsource,i+1);
                                    BlistFeaturedFinal.Add(dtsource);
                                }

                            }
                        }
                        //In edit mode show all the items.
                        else
                        {
                            //Enable Addlink for end user if user in page edit mode and items count is less than 14
                            if (BlistFeaturedProductDataSource.Count < maxCount)
                                efAddCategory.Visible = true;
                            else
                                efAddCategory.Visible = false;
                            //Assign full path of QuickLink datasource
                            efAddCategory.DataSource = DataSourceChildItemPath;

                            BlistFeaturedFinal = BlistFeaturedProductDataSource;
                        }

                        //bind data to repeater
                        frTitle.Item = DataSourceItem;
                        rpBlProduct.DataSource = BlistFeaturedFinal;
                        rpBlProduct.DataBind();


                    }
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("BusinessLineFutureProductModule:GetBusinessLineFeaturedProductModule" + ex.Message);
                }

            }
        }

        /// <summary>
        /// Get 3 non-repetitive random numbers
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private List<int> GetRandom(int p)
        {
            List<int> randNum = new List<int>();
            List<int> test = new List<int>();
            Random rnd = new Random();
            int j = 1;

            for (int i = 1; i <= p; i++)
            {
                test.Add(i);
            }

            while (test.Count > 0)
            {
                int idx = rnd.Next(0, test.Count);
                randNum.Add(test[idx]);
                test.RemoveAt(idx);
                j++;
                if (j > 3)
                    break;
            }
            return randNum;
        }

        /// <summary>
        /// DataBound for Business Line Featured Product Module
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpBlProduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();


                if (item != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddCategoryAction", item);
                        }
                        FieldRenderer fldrenderImage = (FieldRenderer)e.Item.FindControl("frBusinesProductModuleImage");
                        Sitecore.Data.Fields.ImageField imgfield = item.Fields[CommonFields.Image];
                        if (imgfield != null)
                        {

                            if (String.IsNullOrEmpty(imgfield.Value))
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    item.Editing.BeginEdit();
                                    Sitecore.Data.Fields.ImageField standardimg = item.Template.StandardValues.Fields[CommonFields.Image];
                                    item.Fields[CommonFields.Image].Value = standardimg.Value;
                                    item.Editing.EndEdit();
                                }
                                e.FindAndSetFieldRenderer("frBusinesProductModuleImage", item);
                            }
                            else
                                e.FindAndSetFieldRenderer("frBusinesProductModuleImage", item);
                        }


                        Sitecore.Data.Fields.TextField titlefield = item.Fields[CommonFields.Title];
                        if (titlefield != null)
                        {
                            e.FindAndSetFieldRenderer("frBusinesProductModuleTitle", item);
                        }


                        Sitecore.Data.Fields.TextField txtfield = item.Fields[CommonFields.Text];
                        if (txtfield != null)
                        {
                            e.FindAndSetFieldRenderer("frBusinesProductModuleText", item);
                        }

                        Sitecore.Data.Fields.LinkField lnkfield = item.Fields[CommonFields.link];
                        if (lnkfield != null)
                        {
                            e.FindAndSetLink("lnkBusinesProductModule", item);
                            e.FindAndSetLink("lnkField", item);

                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("BusinessLineFutureProductModule:rpBlProduct_ItemDataBound" + ex.Message);
                    }

                }

            }
        }


        /// <summary>
        /// Get the URL based on the LinkType
        /// </summary>
        /// <param name="lnkField"></param>
        /// <returns></returns>
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {
            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    return lnkField.Url;

                case "anchor":
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;

                case "mailto":
                    return lnkField.Url;

                case "javascript":
                    return lnkField.Url;

                default:
                    return lnkField.Url;
            }

        }
    }
}