﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessLineTabContentModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.BusinessLineTabContentModule" %>
<script type="text/javascript">
    try {
        $(document).ready(function () {
            var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
        });
    }
    catch (e) {
    }
</script>
<div id="TabbedPanels1" class="TabbedPanels">
    <ul class="TabbedPanelsTabGroup" id="BusinessTabContent" runat="server">
        <li class="TabbedPanelsTab" tabindex="0" id="litab1" runat="server">
            <p>
                <span>
                    <sc:FieldRenderer ID="frTab1Title" runat="server" FieldName="Tab1Title" />
                </span>
            </p>
        </li>
        <li class="TabbedPanelsTab" tabindex="0" id="litab2" runat="server">
            <p>
                <span>
                    <sc:FieldRenderer ID="frTab2Title" runat="server" FieldName="Tab2Title" />
                </span>
            </p>
        </li>
        <li class="TabbedPanelsTab" tabindex="0" id="litab3" runat="server">
            <p>
                <span>
                    <sc:FieldRenderer ID="frTab3Title" runat="server" FieldName="Tab3Title" />
                </span>
            </p>
        </li>
        <li class="TabbedPanelsTab" tabindex="0" id="litab4" runat="server">
            <p>
                <span>
                    <sc:FieldRenderer ID="frTab4Title" runat="server" FieldName="Tab4Title" />
                </span>
            </p>
        </li>
        <li class="TabbedPanelsTab" tabindex="0" id="litab5" runat="server">
            <p>
                <span>
                    <sc:FieldRenderer ID="frTab5Title" runat="server" FieldName="Tab5Title" />
                </span>
            </p>
        </li>
    </ul>
    <div class="TabbedPanelsContentGroup">
        <%--First Tab Content --%>
        <asp:Repeater runat="server" ID="rpMain" OnItemDataBound="rpMain_ItemDataBound">
            <ItemTemplate>
                <div class="TabbedPanelsContent">
                    <div class="bizLineTabPanel">
                        <asp:Repeater runat="server" ID="rpfirstcolproducts" OnItemDataBound="rpfirstcolproducts_ItemDataBound">
                            <ItemTemplate>
                                <sc:EditFrame runat="server" ID="efAddfirstProductAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
                                    <div class="bizLineItemContainer">
                                        <br />
                                        <div class="tabcontent_img">
                                            <sc:Link ID="lnkImage" Field="GeneralLink" runat="server">
                                                <span>
                                                    <sc:FieldRenderer ID="frBusinessTabContentImage" runat="server" FieldName="Image" />
                                                </span>
                                            </sc:Link>
                                        </div>
                                        <div class="bizLineContent2">
                                            <div class="tabcontent_title">
                                                <sc:Link ID="firstcollinks" Field="GeneralLink" runat="server">
                                                    <sc:FieldRenderer ID="frBusinessTabContentTitle" runat="server" FieldName="Title" />
                                                </sc:Link>
                                            </div>
                                            <div class="tabcontent">
                                                <sc:FieldRenderer ID="frBusinessTabContentText" runat="server" FieldName="Text" />
                                            </div>
                                        </div>
                                    </div>
                                </sc:EditFrame>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="bizLineTabPanel">
                        <asp:Repeater runat="server" ID="rpseccolproducts" OnItemDataBound="rpseccolproducts_ItemDataBound">
                            <ItemTemplate>
                                <sc:EditFrame runat="server" ID="efAddsecProductAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
                                    <div class="bizLineItemContainer">
                                        <br />
                                        <div class="tabcontent_img">
                                            <sc:Link ID="lnkImage" Field="GeneralLink" runat="server">
                                                <span>
                                                    <sc:FieldRenderer ID="frBusinessTabContentImage" runat="server" FieldName="Image" />
                                                </span>
                                            </sc:Link>
                                        </div>
                                        <div class="bizLineContent2">
                                            <div class="tabcontent_title">
                                                <sc:Link ID="seccollinks" Field="GeneralLink" runat="server">
                                                    <sc:FieldRenderer ID="frBusinessTabContentTitle" runat="server" FieldName="Title" />
                                                </sc:Link>
                                            </div>
                                            <div class="tabcontent">
                                                <sc:FieldRenderer ID="frBusinessTabContentText" runat="server" FieldName="Text" />
                                            </div>
                                        </div>
                                    </div>
                                </sc:EditFrame>
                            </ItemTemplate>
                        </asp:Repeater>
                        <sc:EditFrame runat="server" ID="efAddProduct" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
                            <div id="AddNewLink" runat="server">
                                Add Product
                            </div>
                        </sc:EditFrame>
                    </div>
<%--                    <sc:EditFrame runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Category">
                        <div id="Div1" runat="server">
                            Add Category
                        </div>
                    </sc:EditFrame>--%>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
