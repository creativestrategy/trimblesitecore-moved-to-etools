﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;
using Trimble.Presentation.UI.Sublayouts.Trimble;
using log4net;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class ProductImages : SublayoutBaseExtended
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (DataSourceItem != null)
                {
                    AssingDatasource();
                    AssignUrl(DataSourceItem);
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductImages:Page_Load" + ex.Message);
            }
        }

        #region AssigningURL

        //checking whether the Image field value is  null
        //Getting the Image url and assigning it to the Anchor HRef Value
        private void AssignUrl(Item DataSource)
        {
            try
            {
                Sitecore.Data.Fields.ImageField img1 = DataSource.Fields[ProductImagesModule.Image1];
                if (img1 != null)
                {
                    if (!String.IsNullOrEmpty(img1.Value))
                    {
                       image1link.HRef = GetImageURL(img1);         
                    }
                    else
                    {
                        if(!IsPageEditorEditing)
                        dvimg1.Visible = false;
                    }
                }

                Sitecore.Data.Fields.ImageField img2 = DataSource.Fields[ProductImagesModule.Image2];
                if (img2 != null)
                {
                    if (!String.IsNullOrEmpty(img2.Value))
                    {
                       image2link.HRef = GetImageURL(img2);  
                    }
                    else
                    {
                        if (!IsPageEditorEditing)
                        dvimg2.Visible=false;
                    }
                }

                Sitecore.Data.Fields.ImageField img3 = DataSource.Fields[ProductImagesModule.Image3];
                if (img3 != null)
                {
                    if (!String.IsNullOrEmpty(img3.Value))
                    {
                        image3link.HRef = GetImageURL(img3);
                    }
                    else
                    {
                        if (!IsPageEditorEditing)
                        dvimg3.Visible = false;
                    }
                }
                Sitecore.Data.Fields.ImageField img4 = DataSource.Fields[ProductImagesModule.Image4];
                if (img4 != null)
                {
                    if (!String.IsNullOrEmpty(img4.Value))
                    {                       
                      image4link.HRef = GetImageURL(img4); 
                    }
                    else
                    {
                        if (!IsPageEditorEditing)
                        dvimg4.Visible = false;
                    }
                }
            }

            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductImages:AssignUrl" + ex.Message);
            }
        }
        #endregion

        #region GetImageURL
        public static string GetImageURL(Sitecore.Data.Fields.ImageField imageField)
        {
            string imageURL = string.Empty;
            try
            {
                if (imageField != null && imageField.MediaItem != null)
                {
                    Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                    imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));

                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductImages:GetImageURL" + ex.Message);
            }
            return imageURL;
        }
        #endregion

        public void AssingDatasource()
        {
            imgimage1.Item = imgimage2.Item=imgimage3.Item=imgimage4.Item = DataSourceItem;
            imgimage1thumb.Item = imgimage2thumb.Item = imgimage3thumb.Item = imgimage4thumb.Item = DataSourceItem;
        }
    }
}