﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessLineFeaturedProductsModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.BusinessLineFutureProductModule" %>
<div id="container_transportation" class="marginTop32" runat="server">
    <div class="innertransport">
        <div class="texthead3">
            <!-- ******************************** Business Line Featured Products Module Title ******************************** -->
            <sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" />
            <!-- ******************************** End of Business Line Featured Products Module Title******************************** -->
        </div>
        <div id="transport_middle">
            <asp:Repeater runat="server" ID="rpBlProduct" OnItemDataBound="rpBlProduct_ItemDataBound">
                <ItemTemplate>
                    <sc:EditFrame runat="server" ID="efAddCategoryAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
                        <div class="middle1">
                            <sc:Link ID="lnkField" Field="GeneralLink" runat="server">
                                <span>
                                    <sc:FieldRenderer ID="frBusinesProductModuleImage" runat="server" Width="157" Height="128"
                                        FieldName="Image" DisableWebEditing="false" />
                                </span>
                            </sc:Link>
                            <div class="middle-test">
                                <sc:Link ID="lnkBusinesProductModule" Field="GeneralLink" runat="server">
                                    <sc:FieldRenderer ID="frBusinesProductModuleTitle" runat="server" FieldName="Title"
                                        DisableWebEditing="false" />
                                </sc:Link>
                            </div>
                            <p>
                                <sc:FieldRenderer ID="frBusinesProductModuleText" runat="server" FieldName="Text"
                                    DisableWebEditing="false" />
                            </p>
                        </div>
                    </sc:EditFrame>
                </ItemTemplate>
            </asp:Repeater>
            <sc:EditFrame runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
                <div id="AddNewLink" runat="server">
                    Add Product
                </div>
            </sc:EditFrame>
        </div>
        <div class="clear-both">
        </div>
    </div>
</div>
