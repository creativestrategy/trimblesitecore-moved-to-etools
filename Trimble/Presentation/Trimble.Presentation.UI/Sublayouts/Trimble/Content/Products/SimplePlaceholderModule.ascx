﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimplePlaceholderModule.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.SimplePlaceholderModule" %>


<span id="spquote" runat="server" class="productHomeQuote"><sc:FieldRenderer ID="frquote" runat="server" FieldName="Quote" /></span> 
<div id="dvfreetext" runat="server" class="industrybox" visible="false">
    		<h3><sc:FieldRenderer ID="frtitle" runat="server" FieldName="Title" /></h3>
    		<p><sc:FieldRenderer ID="frfreetext" runat="server" FieldName="FreeText" /></p>
  		</div>
        <sc:Image ID="imgproductimage" runat="server" Field="Image" />


