﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductImages.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.ProductImages" %>

  <script type="text/javascript">
      $(document).ready(function () {
         if($("div.prod_slides").children().get(0)){
              $("div.prod_slides").children().get(0).className = "prod_img active";
              productImageSlider();
              $('.prod_slides .prod_img > img').click(function () {
                  $('.prod_slides .prod_img').removeClass('active');
                  $(this).parent().addClass('active');
                  productImageSlider();
              });
              $(".light-box-old").colorbox({ iframe: false, scalePhotos: true, scrolling: false, innerWidth: 580, innerHeight: 580 });
              $(".light-box").colorbox({ iframe: false, photo: true, scalePhotos: true, scrolling: false });
          }
      });

</script>

<div>
        <div class="prod_slides">
        <!--*****Product Slidshow*****-->
          <div id="dvimg1" runat="server"  class="prod_img">
            <a id="image1link" runat="server" class="lightBox" ><sc:Image  ID="imgimage1" runat="server" Field="Image1" /></a>
            <sc:Image   ID="imgimage1thumb" runat="server" Field="Image1" />
            
          </div>
          <div id="dvimg2" runat="server" class="prod_img">
            <a id="image2link" runat="server" class="lightBox" ><sc:Image  ID="imgimage2" runat="server" Field="Image2" /></a>
            <sc:Image   ID="imgimage2thumb" runat="server" Field="Image2" />
            
          </div>
          <div id="dvimg3" runat="server" class="prod_img">
            <a id="image3link" runat="server" class="lightBox" ><sc:Image  ID="imgimage3" runat="server" Field="Image3" /></a>
            <sc:Image   ID="imgimage3thumb" runat="server" Field="Image3" />
            
          </div>
          <div id="dvimg4" runat="server" class="prod_img">
            <a id="image4link" runat="server" class="lightBox" ><sc:Image  ID="imgimage4" runat="server" Field="Image4" /></a>
            <sc:Image   ID="imgimage4thumb" runat="server" Field="Image4" />
            
          </div>
        <!--*****Product Slidshow End*****-->
        </div> 
      </div>

<!--*****Product Slidshow End*****--> 