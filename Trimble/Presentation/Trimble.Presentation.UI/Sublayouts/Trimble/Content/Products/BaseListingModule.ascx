﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BaseListingModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.BaseListingModule" %>

<asp:Repeater ID="rptcategory" runat="server" OnItemDataBound="rptcategory_ItemDataBound">
       <ItemTemplate>
       <sc:EditFrame runat="server" ID="efAddCategoryActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
             <div ID = "outerContainer" runat="server">
                 <div class="center-content"> 
                    <div class="center-right-content">
                       <div class="productsMainContainer padd20"> 
                                          
                        <div class="padding30">
                        <div>
                            <span class="categoryTitleHeading">
                            <sc:FieldRenderer ID="frcategorytitle" runat="server" FieldName="Title" />
                            </span>
                            <sc:Link ID="lnkcategorylink" runat="server" Field="CategoryLink"/>
                            <asp:Image ID="image1"  runat="server" alt="arrowimage" src="../../../html/images/grey_arrow.png" visible= "false"/>                                                 
                            <div class="categoryTitles">
                                <asp:Repeater ID="rptproduct" runat="server" OnItemDataBound="rptproduct_ItemDataBound">
                                <ItemTemplate>
                                  <sc:EditFrame runat="server" ID="efAddActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
                                    <div id="categoryList" runat="server">
                                        <sc:Link ID="lnkproductlink" runat="server" Field="GeneralLink">
                                          <sc:FieldRenderer ID="frimage" runat="server" FieldName="Image"/>
                                            <span class="categoryProductTitle">
                                                <sc:FieldRenderer ID="frproducttitle" runat="server" FieldName="Title" />
                                            </span>
                                        </sc:Link>
                                        <p class="categoryProductDescription">
                                            <sc:FieldRenderer ID="frtext" runat="server" FieldName="Text" />
                                        </p>
                                    </div>
                                   </sc:EditFrame>
                                    </ItemTemplate>
                                </asp:Repeater>                                                               
                                    
                                 <sc:EditFrame  runat="server" ID="efAddProduct" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
                                    <div id="AddNewLink" runat="server">
                                            Add Product
                                    </div>
                                </sc:EditFrame>
                            </div>
                          
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
             </sc:EditFrame>
        </ItemTemplate>
</asp:Repeater>
<div class="full-width-white-container">
     <div class="center-content"> 
          <div class="center-right-content">
            <sc:EditFrame  runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Category">
                <div id="AddNewLink" runat="server">
                Add Category
            </div>
        </sc:EditFrame>
    </div>
    </div>
</div>


 
                       
                   
