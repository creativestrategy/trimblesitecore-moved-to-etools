﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;


namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class ProductListingModuleSet3 : SublayoutBaseExtended
    {
        private Item[] ChildDataItems { get; set; }

        private Item imagedatasource { get; set; }

        private List<string> DataSourceChildItemPath = new List<string>();

        // can be override to change this logic
        public virtual void GetChildDataSourceItem()
        {
            try
            {
                // update property for populate data
                if (this.DataSourceItem != null)
                {
                    ChildDataItems = this.DataSourceItem.Children.ToArray();
                    int IndexCount = ChildDataItems.Count();
                    for (int i = 0; i < IndexCount; i++)
                    {
                        DataSourceChildItemPath.Add(ChildDataItems[i].Paths.Path);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductListingModuleSet3:GetChildDataSourceItem" + ex.Message);
            }

        }
        public int ItemCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            Populate();
        }

        protected void Populate()
        {
            try
            {
                Item productmodulelistingdata = this.DataSourceItem;
                //For Adding a Product
                GetChildDataSourceItem();
                if (productmodulelistingdata != null)
                {
                    // for page editor mode visible insert link edit frame
                    if (this.IsPageEditorEditing)
                    {
                        //Enable Addlink for end user if user in page edit mode
                        efAddCategory.Visible = true;

                        //Assign full path of QuickLink datasource
                        efAddCategory.DataSource = productmodulelistingdata.Paths.FullPath;

                        // IF page in edit mode show all the Category Links
                        //rptcategory.DataSource = DataSourceChildItemPath;

                    }
                    rptcategory.DataSource = productmodulelistingdata.Children;
                    rptcategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("ProductListingModuleSet3:Populate" + ex.Message);
            }
        }

        protected void rptcategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.IsListingItem())
            {
                Item child = e.GetSitecoreItem();

                if (child != null)
                {
                    try
                    {
                        //Page EditorfFor Adding a Product
                        EditFrame editframe = e.FindControl<EditFrame>("efAddProduct");
                        // for page editor mode visible insert product edit frame
                        if (this.IsPageEditorEditing)
                        {
                            editframe.Visible = true;

                            e.FindAndSetEditFrame("efAddCategoryActions", child);
                            editframe.DataSource = DataSourceChildItemPath[e.Item.ItemIndex];
                        }
                        var div = e.FindControl<HtmlGenericControl>("outerContainer");
                        //var divproduct = e.FindControl<HtmlGenericControl>("productContainer");
                        if (ItemCount == 0 || (ItemCount % 2) == 0)
                        {
                            div.Attributes.Add("class", "full-width-blue-container");
                            // divproduct.Attributes.Add("class", "productsContainer2");

                        }
                        else
                        {

                            div.Attributes.Add("class", "full-width-white-container");

                            // divproduct.Attributes.Add("class", "productsContainer3");
                        }
                        Sitecore.Collections.ChildList childitem = child.GetChildren();
                        Sitecore.Data.Fields.TextField categoryTitle = child.Fields[CommonFields.Title];
                        if (categoryTitle != null)
                        {
                            e.FindAndSetFieldRenderer("frcategorytitle", child);
                        }
                        Sitecore.Data.Fields.LinkField categoryLinkField = child.Fields[CommonFields.CategoryLink];
                        if (categoryLinkField != null && !String.IsNullOrEmpty(categoryLinkField.Value))
                        {

                            var anchor = e.FindControl<Sitecore.Web.UI.WebControl>("lnkcategorylink");
                            anchor.Attributes.Add("class", "categoryComparison");
                            e.FindAndSetLink("lnkcategorylink", child);
                            var img = e.FindControl<System.Web.UI.Control>("image1");
                            if (img != null)
                            {
                                img.Visible = true;
                            }

                        }
                        imagedatasource = child;
                        Repeater rptproduct = e.FindControl<Repeater>("rptproduct");
                        rptproduct.DataSource = childitem;
                        rptproduct.DataBind();
                        ItemCount++;
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("ProductListingModuleSet3:rptcategory_ItemDataBound" + ex.Message);
                    }
                }
            }
        }

        protected void rptproduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item childitem = e.GetSitecoreItem();
                if (childitem != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddProductAction", childitem);
                        }
                        Sitecore.Data.Fields.TextField titlefield = childitem.Fields[CommonFields.Title];
                        if (titlefield != null && titlefield.Value != null)
                        {
                            e.FindAndSetFieldRenderer("frproducttitle", childitem);
                        }

                        Sitecore.Data.Fields.ImageField imgfield = childitem.Fields[CommonFields.Image];
                        if (imgfield != null)
                        {
                            if (String.IsNullOrEmpty(imgfield.Value))
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    childitem.Editing.BeginEdit();
                                    Sitecore.Data.Fields.ImageField standardimg = childitem.Template.StandardValues.Fields[CommonFields.Image];
                                    childitem.Fields[CommonFields.Image].Value = standardimg.Value;
                                    childitem.Editing.EndEdit();
                                }
                                e.FindAndSetFieldRenderer("frimage", childitem);
                            }
                            else
                                e.FindAndSetFieldRenderer("frimage", childitem);
                        }
                     

                        Sitecore.Data.Fields.TextField txtfield = childitem.Fields[CommonFields.Text];
                        if (txtfield != null && txtfield.Value != null)
                        {
                            e.FindAndSetFieldRenderer("frtext", childitem);
                        }
                        Sitecore.Data.Fields.LinkField lnkfield = childitem.Fields[CommonFields.link];
                        if (lnkfield != null && lnkfield.Value != null)
                        {
                            e.FindAndSetLink("lnkproductlink", childitem);
                            var img = e.FindControl<System.Web.UI.Control>("image1");
                            img.Visible = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("ProductListingModuleSet3:rptproduct_ItemDataBound" + ex.Message);
                    }
                }
            }
        }
    }
}