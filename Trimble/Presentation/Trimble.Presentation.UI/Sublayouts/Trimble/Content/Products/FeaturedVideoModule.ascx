﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedVideoModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products.FeaturedVideoModule" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".aboutVideo").colorbox({ iframe: true, innerWidth: 750, innerHeight: 510 });
    }); 
</script>
<div class="featured-solutions hideLHS" runat="server" id="mainDiv">
    <li class="group-heading">
        <sc:FieldRenderer ID="frtitle" runat="server" FieldName="Title" />
    </li>
    <asp:Repeater runat="server" ID="rpFeaturedVideo" OnItemDataBound="rpFeaturedVideoProduct_ItemDataBound">
        <ItemTemplate>
            <sc:EditFrame runat="server" ID="efFeaturedProductVideoActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/FeaturedVideoActions">
                <div class="video-container">
                    <sc:Link ID="lnkvideo" Field="VideoLink" runat="server" CssClass="aboutvideo">
                        <a class="aboutVideo" id="anchorVideo" runat="server">
                            <sc:FieldRenderer ID="frProductVideoModuleImage" runat="server" FieldName="VideoImage"
                                DisableWebEditing="false" />
                        </a>
                    </sc:Link>
                    <div class="title">
                        <sc:FieldRenderer ID="frProductVideoModuleTitle" runat="server" FieldName="Title"
                            DisableWebEditing="false" />
                    </div>
                    <div class="description">
                        <sc:FieldRenderer ID="frProductVideoModuleText" runat="server" FieldName="Description"
                            DisableWebEditing="false" />
                    </div>
                </div>
            </sc:EditFrame>
        </ItemTemplate>
    </asp:Repeater>
    <sc:EditFrame runat="server" ID="efAddFeaturedProductVideo" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert ProductVideo">
        <div id="divAddFeaturedProductVideo" runat="server">
            <asp:Label ID="lblAddVideo" runat="server"></asp:Label>
        </div>
    </sc:EditFrame>
</div>


