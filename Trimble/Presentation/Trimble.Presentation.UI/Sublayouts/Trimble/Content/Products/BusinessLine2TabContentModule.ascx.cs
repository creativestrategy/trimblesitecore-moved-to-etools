﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;
namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Products
{
    public partial class BusinessLine2TabContentModule : SublayoutBaseExtended
    {

        List<Item> Main = new List<Item>();
        List<Item> MainColumnSource = new List<Item>();
        bool alltitlesnonempty = false;
        private Item[] ChildDataItems { get; set; }
        private List<string> DataSourceChildItemPath = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            GetBusinessLineTabContentModule();
        }


        private void GetBusinessLineTabContentModule()
        {
            try
            {
                List<String> listFields = new List<string>();
                if (!SitecoreItem.IsPageEditorEditing)
                {

                    frTab1Title.Item = frTab2Title.Item = frTab3Title.Item = frTab4Title.Item = frTab5Title.Item = DataSourceItem;

                    //Add Tab Titles to List
                    List<string> strTabsTitle = new List<string>() { DataSourceItem.Fields[GeneralTabFields.Tab1TitleFieldName].Value, 
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab2TitleFieldName].Value,
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab3TitleFieldName].Value,
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab4TitleFieldName].Value,
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab5TitleFieldName].Value};
                    
                    List<int> strFinalTabsIndex = new List<int>();
                    List<int> strEmptyTabsIndex = new List<int>();

                    for (int k = 0; k < strTabsTitle.Count; k++)
                    {
                        if (!String.IsNullOrEmpty(strTabsTitle[k]))
                            strFinalTabsIndex.Add(k + 1);

                        else
                            strEmptyTabsIndex.Add(k + 1);
                    }
                    //case where all the tabs have titles
                    if (strEmptyTabsIndex.Count < 1)
                        alltitlesnonempty = true;

                    //Form all the field Names
                    for (int i = 1; i <= 5; i++)
                    {
                        foreach (int finalcontentindex in strFinalTabsIndex)
                        {
                            if (i == finalcontentindex)
                            {
                                listFields.Add(GeneralTabFields.TabNamesPrefix + i + GeneralTabFields.TabContentFolderSuffix);
                                break;
                            }
                        }

                    }

                    //loop through the target folder items for the Tab Content
                    for (int index = 0; index < listFields.Count; index++)
                    {
                        try
                        {
                            Item i = Sitecore.Context.Database.GetItem((DataSourceItem.GetFieldValue(listFields[index])));
                            if (i != null)
                                Main.Add(i);

                            if (i==null || i.Children.Count <1)
                            {
                                strFinalTabsIndex.Remove(index + 1);
                                if (!alltitlesnonempty)
                                {
                                    foreach (int k in strEmptyTabsIndex)
                                    {
                                        if (k == (index + 1))
                                            break;
                                        else
                                            strEmptyTabsIndex.Add(index + 1);
                                    }
                                }
                                else
                                {
                                    strEmptyTabsIndex.Add(index + 1);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("BusinessLine2TabContentModule:GetBusinessLineTabContentModule" + ex.Message);
                        }

                    }
                    //remove the Listitem if the title is empty
                    RemoveEmptyTabTitle(strEmptyTabsIndex, strFinalTabsIndex);
                }
                else
                {
                    frTab1Title.Item = frTab2Title.Item = frTab3Title.Item = frTab4Title.Item = frTab5Title.Item = DataSourceItem;
                    for (int i = 1; i <= 5; i++)
                    {
                        listFields.Add(GeneralTabFields.TabNamesPrefix + i + GeneralTabFields.TabContentFolderSuffix);
                    }
                    for (int index = 0; index < listFields.Count; index++)
                    {
                        Item i = Sitecore.Context.Database.GetItem((DataSourceItem.GetFieldValue(listFields[index])));
                        Main.Add(i);
                    }

                }
                rpMain.DataSource = Main;
                rpMain.DataBind();
            }
            catch
            {
                for (int i = 1; i <= 5; i++)
                {
                    var li = BusinessTabContent.FindControl("litab" + i);
                    BusinessTabContent.Controls.Remove(li);
                }
            }
        }


        /// <summary>
        /// MainContent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpMain_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item Parent = e.GetSitecoreItem();

                if (Parent != null)
                {
                    try
                    {

                        Repeater ChildFirstCol = e.FindControl<Repeater>("rpfirstcolproducts");
                        MainColumnSource = getDataSourceforChilds(Parent);

                        if (this.IsPageEditorEditing)
                        {
                            EditFrame edframe = e.FindControl<EditFrame>("efAddProduct");
                            edframe.Visible = true;
                            edframe.DataSource = Parent.Paths.FullPath;
                        }
                        ChildFirstCol.DataSource = MainColumnSource;
                        ChildFirstCol.DataBind();
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("BusinessLine2TabContentModule:rpMain_ItemDataBound" + ex.Message);
                    }
                   
                }
            }
        }


        private void RemoveEmptyTabTitle(List<int> EmptyCountList, List<int> nonEmptylist)
        {

            List<string> strTabTitleslist = new List<string>();

            foreach (int nonEmpty in nonEmptylist)
            {
                if (nonEmpty != 1)
                    strTabTitleslist.Add(GeneralTabFields.TabNamesPrefix + nonEmpty + GeneralTabFields.TabTitleSuffix);
            }


            try
            {
                frTab2Title.FieldName = strTabTitleslist[0];
            }
            catch
            {
                frTab2Title.FieldName = string.Empty;
                frTab3Title.FieldName = string.Empty;
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab3Title.FieldName = strTabTitleslist[1];
            }
            catch
            {
                frTab3Title.FieldName = string.Empty;
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab4Title.FieldName = strTabTitleslist[2];
            }
            catch
            {
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab5Title.FieldName = strTabTitleslist[3];
            }
            catch
            {
                frTab5Title.FieldName = string.Empty;
            }


            int maxTabCount = 5;
            int EmptyTabcount = EmptyCountList.Count;
            while (EmptyTabcount > 0)
            {
                var li = BusinessTabContent.FindControl("litab" + maxTabCount);
                BusinessTabContent.Controls.Remove(li);
                maxTabCount--;
                EmptyTabcount--;
            }
        }

        public List<Item> getDataSourceforChilds(Item Parent)
        {
            List<Item> itemListItemwithImagetemplate = new List<Item>();
            try
            {
                if (Parent.Children.Count > 0)
                {
                    foreach (Item itemchild in Parent.GetChildren())
                    {
                        itemListItemwithImagetemplate.Add(itemchild);
                    }
                }

                return itemListItemwithImagetemplate;
            }
            catch
            {
                return null;
            }
        }



        /// <summary>
        /// Tab1Column1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpfirstcolproducts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();


                if (item != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddActions", item);
                        }
                        
                        Sitecore.Data.Fields.ImageField img = item.Fields[CommonFields.Image];
                        if (img != null)
                        {
                            if (String.IsNullOrEmpty(img.Value))
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    item.Editing.BeginEdit();
                                    Sitecore.Data.Fields.ImageField standardimg = item.Template.StandardValues.Fields[CommonFields.Image];
                                    item.Fields[CommonFields.Image].Value = standardimg.Value;
                                    item.Editing.EndEdit();
                                }
                                e.FindAndSetFieldRenderer("frBusinessTabContentImage", item);
                            }
                            else
                                e.FindAndSetFieldRenderer("frBusinessTabContentImage", item);
                        }
                        e.FindAndSetFieldRenderer("frBusinessTabContentTitle", item);
                        e.FindAndSetFieldRenderer("frBusinessTabContentText", item);
                        e.FindAndSetFieldRenderer("lnkBusinessTabContent", item);
                        e.FindAndSetLink("firstcollinks", item);
                        e.FindAndSetFieldRenderer("frGeneralText", item);
                        e.FindAndSetLink("frImageLink1", item);
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("BusinessLine2TabContentModule:rpfirstcolproducts_ItemDataBound" + ex.Message);
                    }
                }
            }
        }



        /// <summary>
        /// Get the URL based on the LinkType
        /// </summary>
        /// <param name="lnkField"></param>
        /// <returns></returns>
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {
            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    return lnkField.Url;

                case "anchor":
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;

                case "mailto":
                    return lnkField.Url;

                case "javascript":
                    return lnkField.Url;

                default:
                    return lnkField.Url;
            }

        }
    }
}