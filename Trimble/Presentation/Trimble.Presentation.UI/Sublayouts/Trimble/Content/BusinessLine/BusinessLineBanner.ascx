﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessLineBanner.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.BusinessLine.BusinessLineBanner" %>
<div id="container_innerbanner">
    <div id="tmpSlideshow" class="bigger-banner">
        <asp:Repeater runat="server" ID="rptData" OnItemDataBound="rptData_ItemDataBound">
            <ItemTemplate>
                <div id="divimgBanner" class='tmpSlide' title="" runat="server">
                  <sc:FieldRenderer ID="frBannerImage" runat="server" FieldName="BannerImage" DisableWebEditing="false" />
                   <div id="bannerPosition" runat="server" class="bannercontentRB"> <sc:FieldRenderer ID="frBannerText" runat="server" FieldName="BannerTeaserRichText" DisableWebEditing="false" /> </div>                   
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
