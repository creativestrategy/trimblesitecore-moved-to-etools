﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.BusinessLine
{
    public partial class BusinessLineBanner : SublayoutBaseExtended
    {
        int i = 1;
        Item BannerPosition;
        
        string TeaserPosition;
        string TeaserBackGround;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //if (base.SupportsTemplates("{0E640342-E4F6-45F5-86ED-2B5FAA863917}"))
                if (base.SupportsTemplates("{F6F73A1E-C4A7-424C-851B-D676EF288E3D}"))
                {
                    Populate();
                    //Response.Redirect(Request.RawUrl); 
                }
                else
                {
                    this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                }

            }
        }

        public virtual void Populate_OLD()
        {

            // Get banners folder item
            Item bannersFolderItem = DataSourceItem;
            int maxCount = (int)MaxCount.BusinessBanner;
            if (bannersFolderItem != null)
            {
                List<Item> rotatingBanners = new List<Item>();

                int index = 1;

                foreach (Item i in bannersFolderItem.GetChildren())
                {
                    
                    if (i.TemplateName == TemplateNames.BannerTeaserPosition)
                    {
                        if (index == 1)
                        {
                            BannerPosition = i;
                            TeaserPosition = BannerPosition.Fields[BusinessBannerFields.TeaserPosition].Value;
                            if (BannerPosition.Fields[BusinessBannerFields.TeaserBackGround].Value == BusinessBannerFields.TeaserBackGroundTransparentOn)
                           {
                               TeaserBackGround = " "+BusinessBannerFields.TeaserBackGroundTransparentDivID;
                           }
                        }
                        else
                            break;
                    }
                }

                foreach (Item j in BannerPosition.GetChildren())
                {
                    rotatingBanners.Add(j);
                    if (rotatingBanners.Count > maxCount)
                        break;
                }

                rptData.DataSource = rotatingBanners;
                rptData.DataBind();

            }

        }

        public virtual void Populate()
        {

            // Get banners folder item
            Item bannersFolderItem = DataSourceItem;
            int maxCount = (int)MaxCount.BusinessBanner;
            if (bannersFolderItem != null)
            {
                List<Item> rotatingBanners = new List<Item>();

                int index = 1;

                foreach (Item i in bannersFolderItem.GetChildren())
                {

                    if (bannersFolderItem.TemplateName == TemplateNames.BusinessBanner)
                    {
                        if (index == 1)
                        {
                            BannerPosition = bannersFolderItem;
                            TeaserPosition = BannerPosition.Fields[BusinessBannerFields.TeaserPosition].Value;
                            if (BannerPosition.Fields[BusinessBannerFields.TeaserBackGround].Value == BusinessBannerFields.TeaserBackGroundTransparentOn)
                            {
                                TeaserBackGround = " " + BusinessBannerFields.TeaserBackGroundTransparentDivID;
                            }
                        }
                        else
                            break;
                    }
                }

                foreach (Item j in BannerPosition.GetChildren())
                {
                    rotatingBanners.Add(j);
                    if (rotatingBanners.Count > maxCount)
                        break;
                }

                rptData.DataSource = rotatingBanners;
                rptData.DataBind();

            }

        }

        public virtual void Populate_JM()
        {
            try
            {
                // Get banners folder item
                Item bannersFolderItem = DataSourceItem;
                if (bannersFolderItem != null)
                {
                    List<Item> rotatingBanners = new List<Item>();

                    int index = 1;

                    foreach (Item i in bannersFolderItem.GetChildren())
                    {
                        //Response.Write("bannersFolderItem=" + bannersFolderItem.TemplateName + " | i.TemplateName=" + i.TemplateName + " | TemplateNames.BannerTeaserPosition=" + TemplateNames.BannerTeaserPosition);
                        //Response.Write("BannerImage=" + i.Fields["BannerImage"]);
                        Response.Write(bannersFolderItem.Fields[BusinessBannerFields.TeaserPosition].Value);
                        rotatingBanners.Add(i);
                        if (index == 6)
                            break;
                        index++;

                    }



                    if (rotatingBanners.Count < 7)
                    {
                        // bind items
                        //rptData.DataSource = bannersFolderItem.Children;
                        //rptData.DataBind();
                    }

                    rptData.DataSource = rotatingBanners;
                    rptData.DataBind();

                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("TrimbleBanner:Populate" + ex.Message);
            }

        }


        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {

                Item item = e.GetSitecoreItem();
                try
                {
                    e.FindAndSetFieldRenderer("frBannerText", item);
                }
                catch(Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("BusinessLineBanner:rptData_ItemDataBound" + ex.Message);
                }
                e.FindAndSetFieldRenderer("frBannerImage", item);


                if (item != null)
                {
                    try
                    {
                        string divClass = "tmpSlide tmpSlide-" + i;
                        var div = e.FindControl<HtmlGenericControl>("divimgBanner");
                        div.Attributes.Add("class", divClass);
                        div.Attributes.Add("height", "450px");
                        if (i != 1)
                            div.Attributes.Add("style", "display:none");
                        i++;

                        var div1 = e.FindControl<HtmlGenericControl>("bannerPosition");
                        switch (TeaserPosition)
                        {
                            case "TopLeft":
                                div1.Attributes.Add("class", BusinessBannerTeaserPositionDivID.TopLeft + TeaserBackGround);
                                break;

                            case "TopRight":
                                div1.Attributes.Add("class", BusinessBannerTeaserPositionDivID.TopRight + TeaserBackGround);
                                break;

                            case "BottomRight":
                                div1.Attributes.Add("class", BusinessBannerTeaserPositionDivID.BottomRight + TeaserBackGround);
                                break;

                            case "BottomLeft":
                                div1.Attributes.Add("class", BusinessBannerTeaserPositionDivID.BottomLeft + TeaserBackGround);
                                break;

                            default:
                                div1.Attributes.Add("class", BusinessBannerTeaserPositionDivID.BottomRight + TeaserBackGround);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("BusinessLineBanner:rptData_ItemDataBound" + ex.Message);
                    }

                }
            }
        }
        protected void rptData_ItemDataBound_JM(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                try
                {
                    Item item = e.GetSitecoreItem();
                    e.FindAndSetFieldRenderer("frBannerText", item);
                    e.FindAndSetFieldRenderer("frBannerImage", item);

                    if (item != null)
                    {

                        string divClass = "tmpSlide tmpSlide-" + i;
                        var div = e.FindControl<HtmlGenericControl>("divimgBanner");
                        div.Attributes.Add("class", divClass);
                        div.Attributes.Add("height", "500px");

                        i++;
                    }
                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("TrimbleBanner:rptData_ItemDataBound" + ex.Message);
                }
            }
            
        }

    }
}



