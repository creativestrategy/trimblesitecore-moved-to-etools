﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;
using System.Web.UI;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Industries
{
    public partial class IndustryFeaturedProductModdule : SublayoutBaseExtended
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (base.SupportsTemplates("{72BB680C-5888-461E-BAB1-71FBCF3D52D8}"))
                {
                    GetIndustryFutureProductModule();
                }
                else
                {
                    HtmlGenericControl div = (HtmlGenericControl)FindControl("mainDiv");
                    div.Visible = false;
                    this.Controls.Add(new LiteralControl("<div>Datasource Item is not Supported.</div>"));
                }
            }
        }

        private void GetIndustryFutureProductModule()
        {
            Item DsIndustryFeaturedProduct = this.DataSourceItem;
           
            if (DsIndustryFeaturedProduct != null)
            {
                              
                List<Item> lstIndustryFeaturedProductDS = new List<Item>();
                List<Item> lstFinalProduct = new List<Item>();
                int maxCount = (int)MaxCount.FeaturedSolutionsModuleMax;
                
                int count = DsIndustryFeaturedProduct.GetChildren().Count;
                
                try
                {
                        for (int i = 0; i < count; i++)
                        {
                            Item nItem = DsIndustryFeaturedProduct.GetChildren()[i];
                            lstIndustryFeaturedProductDS.Add(nItem);
                        }
                        if (!SitecoreItem.IsPageEditorEditing)
                        {

                            if (lstIndustryFeaturedProductDS.Count < maxCount)
                            {
                                List<int> test2 = GetRandom(lstIndustryFeaturedProductDS.Count);


                                for (int i = 0; i < test2.Count; i++)
                                {
                                    var dtsource = lstIndustryFeaturedProductDS[test2[i] - 1];
                                    lstFinalProduct.Add(dtsource);
                                }

                            }

                            else
                            {
                                List<int> test2 = GetRandom(maxCount);

                                for (int i = 0; i < test2.Count; i++)
                                {
                                    var dtsource = lstIndustryFeaturedProductDS[test2[i] - 1];
                                    lstFinalProduct.Add(dtsource);
                                }

                            }
                        }
                        else 
                        {
                            if (!String.IsNullOrEmpty(this.DataSourceItem.Paths.Path) && lstIndustryFeaturedProductDS.Count < maxCount)
                            {
                            //Enable Addlink for end user if user in page edit mode
                            efAddCategory.Visible = true;

                            //Assign full path of QuickLink datasource
                            efAddCategory.DataSource = this.DataSourceItem.Paths.Path;
							}
                            lstFinalProduct = lstIndustryFeaturedProductDS;
                        }
						frHeading.Item = DataSourceItem;
                        rpIndustryProduct.DataSource = lstFinalProduct;
                        rpIndustryProduct.DataBind();

                }
                catch (Exception ex)
                {
                    var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                    logger.Info("IndutryFeaturedSolutionsModule:GetIndustryFutureProductModule" + ex.Message);
                }

            }
        }

        private List<int> GetRandom(int p)
        {
            List<int> randNum = new List<int>();
            List<int> test = new List<int>();
            Random rnd = new Random();
            int j = 1;

            for (int i = 1; i <= p; i++)
            {
                test.Add(i);
            }

            while (test.Count > 0)
            {
                int idx = rnd.Next(0, test.Count);
                randNum.Add(test[idx]);
                test.RemoveAt(idx);
                j++;
                if (j > 3)
                    break;
            }
            return randNum;
        }


        protected void rpIndustryProduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();


                if (item != null)
                {
                    try
                    {
                        //Page EditorfFor Adding a Product
                        EditFrame editframe = e.FindControl<EditFrame>("efAddProduct");
                        // for page editor mode visible insert product edit frame
                        if (this.IsPageEditorEditing)
                        {
                            e.FindAndSetEditFrame("efAddProductAction", item);

                        }


                        FieldRenderer fldrenderImage = (FieldRenderer)e.Item.FindControl("frIndustryProductModuleImage");
                        Sitecore.Data.Fields.ImageField imgfield = item.Fields[CommonFields.Image];
                        if (imgfield != null)
                        {
                            if (String.IsNullOrEmpty(imgfield.Value))
                            {
                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {
                                    item.Editing.BeginEdit();
                                    Sitecore.Data.Fields.ImageField standardimg = item.Template.StandardValues.Fields[CommonFields.Image];
                                    item.Fields[CommonFields.Image].Value = standardimg.Value;
                                    item.Editing.EndEdit();
                                }
                                e.FindAndSetFieldRenderer("frIndustryProductModuleImage", item);
                            }
                            else
                                e.FindAndSetFieldRenderer("frIndustryProductModuleImage", item);
                        }
                        //e.FindAndSetFieldRenderer("frIndustryProductModuleImage", item);
                        e.FindAndSetFieldRenderer("frIndustryProductModuleTitle", item);
                        e.FindAndSetFieldRenderer("frIndustryProductModuleText", item);
                        e.FindAndSetLink("lnkIndustryProductModule", item);
                        e.FindAndSetLink("lnkIndustryProductModule1", item);

                        Sitecore.Data.Fields.LinkField genLink = ((Sitecore.Data.Fields.LinkField)item.Fields[CommonFields.link]);
                        string strLinkURL = GetURL(genLink);
                        //Assign the Url to the Anchor
                        HtmlAnchor lnkTitleAnchor = e.FindControl<HtmlAnchor>("frImageLink1");
                        lnkTitleAnchor.HRef = strLinkURL;
                        //HtmlAnchor lnkImageAnchor = e.FindControl<HtmlAnchor>("frTitleLink1");
                        //lnkImageAnchor.HRef = strLinkURL;

                        //Link link = e.FindControl<Link>("lnkIndustryProductModule");
                        //link.Visible = SitecoreItem.IsPageEditorEditing; 

                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("IndutryFeaturedSolutionsModule:rpIndustryProduct_ItemDataBound" + ex.Message);
                    }
                }
            }
        }



        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {

            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    // Use LinkMananger for internal links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    // Use MediaManager for media links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    // Just return external links 
                    return lnkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty 
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link 
                    return lnkField.Url;
                case "javascript":
                    // Just return javascript 
                    return lnkField.Url;
                default:
                    // Just please the compiler, this 
                    // condition will never be met 
                    return lnkField.Url;
            }

        }
    }
    }
