﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;
using Deloitte.SC.Framework.Helpers;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.Industries
{
    public partial class IndustryTabContent : SublayoutBaseExtended
    {
        List<Item> Main = new List<Item>();
        private Item[] ChildDataItems { get; set; }
        bool alltitlesnonempty = false;
        private Sitecore.Data.Fields.CheckboxField imageChk { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                GetBusinessLineTabContentModule();
        }

        private void GetBusinessLineTabContentModule()
        {
            try
            {
                List<String> listFields = new List<string>();
                if (!SitecoreItem.IsPageEditorEditing)
                {
                    frTab1Title.Item = frTab2Title.Item = frTab3Title.Item = frTab4Title.Item = frTab5Title.Item = DataSourceItem;

                    //Add Tab Titles to List
                    List<string> strTabsTitle = new List<string>() { DataSourceItem.Fields[GeneralTabFields.Tab1TitleFieldName].Value, 
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab2TitleFieldName].Value, 
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab3TitleFieldName].Value, 
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab4TitleFieldName].Value, 
                                                                     DataSourceItem.Fields[GeneralTabFields.Tab5TitleFieldName].Value };

                    List<int> strFinalTabsIndex = new List<int>();
                    List<int> strEmptyTabsIndex = new List<int>();

                    for (int k = 0; k < strTabsTitle.Count; k++)
                    {
                        if (!String.IsNullOrEmpty(strTabsTitle[k]))
                            strFinalTabsIndex.Add(k + 1);

                        else
                            strEmptyTabsIndex.Add(k + 1);
                    }

                    //case where all the tabs have titles
                    if (strEmptyTabsIndex.Count < 1)
                        alltitlesnonempty = true;

                    //Form all the field Names
                    for (int i = 1; i <= 5; i++)
                    {
                        foreach (int finalcontentindex in strFinalTabsIndex)
                        {
                            if (i == finalcontentindex)
                            {
                                listFields.Add(GeneralTabFields.TabNamesPrefix + i + GeneralTabFields.TabContentFolderSuffix);
                                break;
                            }
                        }

                    }

                    //loop through the target folder items for the Tab Content
                    for (int index = 0; index < listFields.Count; index++)
                    {
                        try
                        {
                            Item i = Sitecore.Context.Database.GetItem((DataSourceItem.GetFieldValue(listFields[index])));
                            if (i != null)
                                Main.Add(i);
                            else
                            {
                                strFinalTabsIndex.Remove(index + 1);
                                if (!alltitlesnonempty)
                                {
                                    foreach (int k in strEmptyTabsIndex)
                                    {
                                        if (k == (index + 1))
                                            break;
                                        else
                                            strEmptyTabsIndex.Add(index + 1);
                                    }
                                }
                                else
                                {
                                    strEmptyTabsIndex.Add(index + 1);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                            logger.Info("IndutryTabContent:rpIndustryProduct_ItemDataBound" + ex.Message);
                        }

                    }
                    //remove the Listitem if the title is empty
                    RemoveEmptyTabTitle(strEmptyTabsIndex, strFinalTabsIndex);

                }


                else
                {
                    frTab1Title.Item = frTab2Title.Item = frTab3Title.Item = frTab4Title.Item = frTab5Title.Item = DataSourceItem;
                    for (int i = 1; i <= 5; i++)
                    {
                        listFields.Add(GeneralTabFields.TabNamesPrefix + i + GeneralTabFields.TabContentFolderSuffix);
                    }
                    for (int index = 0; index < listFields.Count; index++)
                    {
                        Item i = Sitecore.Context.Database.GetItem((DataSourceItem.GetFieldValue(listFields[index])));
                        Main.Add(i);
                    }
                }


                rpMain.DataSource = Main;
                rpMain.DataBind();
            }
            catch
            {
                for (int i = 1; i <= 5; i++)
                {
                    var li = Ul1.FindControl("li" + i);
                    Ul1.Controls.Remove(li);
                }
            }
        }

        private void RemoveEmptyTabTitle(List<int> EmptyCountList, List<int> nonEmptylist)
        {

            List<string> strTabTitleslist = new List<string>();

            foreach (int nonEmpty in nonEmptylist)
            {
                if (nonEmpty != 1)
                    strTabTitleslist.Add(GeneralTabFields.TabNamesPrefix + nonEmpty + GeneralTabFields.TabTitleSuffix);
            }


            try
            {
                frTab2Title.FieldName = strTabTitleslist[0];
            }
            catch
            {
                frTab2Title.FieldName = string.Empty;
                frTab3Title.FieldName = string.Empty;
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab3Title.FieldName = strTabTitleslist[1];
            }
            catch
            {
                frTab3Title.FieldName = string.Empty;
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab4Title.FieldName = strTabTitleslist[2];
            }
            catch
            {
                frTab4Title.FieldName = string.Empty;
                frTab5Title.FieldName = string.Empty;
            }
            try
            {
                frTab5Title.FieldName = strTabTitleslist[3];
            }
            catch
            {
                frTab5Title.FieldName = string.Empty;
            }


            int maxTabCount = 5;
            int EmptyTabcount = EmptyCountList.Count;
            while (EmptyTabcount > 0)
            {
                var li = Ul1.FindControl("li" + maxTabCount);
                Ul1.Controls.Remove(li);
                maxTabCount--;
                EmptyTabcount--;
            }
        }



        /// <summary>
        /// MainContent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpMain_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item Parent = e.GetSitecoreItem();

                if (Parent != null)
                {
                    try
                    {
                        if (this.IsPageEditorEditing)
                        {
                            EditFrame edframe = e.FindControl<EditFrame>("efAddCategory");
                            edframe.Visible = true;
                            edframe.DataSource = Parent.Paths.FullPath;
                        }
                        imageChk = Parent.Fields["HideImage"];
                        Repeater CategoryLevel = e.FindControl<Repeater>("rpMaincategorylevelchilds");
                        CategoryLevel.DataSource = Parent.GetChildren();
                        CategoryLevel.DataBind();
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("IndutryTabContent:rpMain_ItemDataBound" + ex.Message);
                    }
                }
            }
        }


        /// <summary>
        /// Tab1Column2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpMaincategorylevelchilds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item Child = e.GetSitecoreItem();

                if (Child != null)
                {
                    EditFrame edframeMain = e.FindControl<EditFrame>("efAddProduct");                  
                    if (this.IsPageEditorEditing)
                    {
                        e.FindAndSetEditFrame("efAddCategoryAction", Child);                       
                        edframeMain.Visible = true;               
                        edframeMain.DataSource = Child.Paths.FullPath;                      
                    }                   
                    e.FindAndSetFieldRenderer("frIndustryCatTitle", Child);
                    Sitecore.Collections.ChildList childitem = Child.GetChildren();
                    Repeater CategoryChilds = e.FindControl<Repeater>("rpMaincategorylevelSubchilds");
                    CategoryChilds.DataSource = childitem;
                    CategoryChilds.DataBind();
                }
            }
        }



        protected void rpMaincategorylevelSubchilds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item subChildItem = e.GetSitecoreItem();
                if (subChildItem != null)
                {
                    if (this.IsPageEditorEditing)
                    {
                        e.FindAndSetEditFrame("efAddActions", subChildItem);
                    }
                    try
                    {
                        Sitecore.Data.Fields.TextField titlefield = subChildItem.Fields[CommonFields.Title];
                        if (titlefield != null)
                        {
                            e.FindAndSetFieldRenderer("frIndustryTabContentTitle", subChildItem);
                        }

                        var div = e.FindControl<HtmlGenericControl>("divIndustry");
                        if (!imageChk.Checked)
                        {
                            //class="bizLineContent2"
                           
                            div.Attributes.Add("class", "bizLineContent2");
                            Sitecore.Data.Fields.ImageField img = subChildItem.Fields[CommonFields.Image];
                            if (img != null)
                            {
                                if (String.IsNullOrEmpty(img.Value))
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        subChildItem.Editing.BeginEdit();
                                        Sitecore.Data.Fields.ImageField standardimg = subChildItem.Template.StandardValues.Fields[CommonFields.Image];
                                        subChildItem.Fields[CommonFields.Image].Value = standardimg.Value;
                                        subChildItem.Editing.EndEdit();
                                    }
                                    e.FindAndSetFieldRenderer("frIndustryTabContentImage", subChildItem);
                                }
                                else
                                    e.FindAndSetFieldRenderer("frIndustryTabContentImage", subChildItem);
                            }
                        }
                        else 
                        {
                            div.Attributes.Add("class", "bizLineContent2 industryCategory_withoutImg");
                        }

                        Sitecore.Data.Fields.TextField txtfield = subChildItem.Fields[CommonFields.Text];
                        if (txtfield != null)
                        {
                            e.FindAndSetFieldRenderer("frIndustryTabContentText", subChildItem);
                        }
                        Sitecore.Data.Fields.LinkField lnkfield = subChildItem.Fields[CommonFields.link];
                        if (lnkfield != null)
                        {
                            e.FindAndSetLink("lkIndustrylnk", subChildItem);
                        }
                        Sitecore.Data.Fields.LinkField lnkfield1 = subChildItem.Fields[CommonFields.link];
                        if (lnkfield1 != null)
                        {
                            e.FindAndSetLink("lkIndustrylnk1", subChildItem);
                        }

                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("IndutryTabContent:rpMaincategorylevelSubchilds_ItemDataBound" + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Get the URL based on the LinkType
        /// </summary>
        /// <param name="lnkField"></param>
        /// <returns></returns>
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {
            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    return lnkField.Url;

                case "anchor":
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;

                case "mailto":
                    return lnkField.Url;

                case "javascript":
                    return lnkField.Url;

                default:
                    return lnkField.Url;
            }

        }
    }
}