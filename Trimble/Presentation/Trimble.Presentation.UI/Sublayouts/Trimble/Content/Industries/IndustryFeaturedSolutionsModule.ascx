﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndustryFeaturedSolutionsModule.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Industries.IndustryFeaturedProductModdule" %>

<div class="featured-solutions hideLHS" runat="server" id="mainDiv">
    <li class="group-heading">
        <sc:FieldRenderer ID="frHeading" runat="server" FieldName="Title" />       
    </li>
    <asp:Repeater runat="server" ID="rpIndustryProduct" OnItemDataBound="rpIndustryProduct_ItemDataBound">
        <ItemTemplate>
        <sc:EditFrame runat="server" ID="efAddProductAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
            <li class="item"> 
            <sc:Link ID="lnkIndustryProductModule1" Field="GeneralLink" runat="server">
                <sc:FieldRenderer ID="frIndustryProductModuleImage" runat="server" FieldName="Image"
                    DisableWebEditing="false" />
           </sc:Link>            
               
                <div class="title description">
                 <sc:Link ID="lnkIndustryProductModule" Field="GeneralLink" runat="server">
                    <sc:FieldRenderer ID="frIndustryProductModuleTitle" runat="server" FieldName="Title"
                        DisableWebEditing="false" />
                         </sc:Link>                 
                </div>
                <div class="description">
                    
                        <sc:FieldRenderer ID="frIndustryProductModuleText" runat="server" FieldName="Text"
                            DisableWebEditing="false" />
                    
                </div>
            </li>
            </sc:EditFrame>
        </ItemTemplate>
    </asp:Repeater>
     <sc:EditFrame  runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">
     <div id="AddNewLink" runat="server">
        Add Product
    </div>
    </sc:EditFrame>
</div>

