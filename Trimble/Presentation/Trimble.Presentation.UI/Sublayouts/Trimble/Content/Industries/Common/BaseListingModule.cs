﻿using System;
using System.Linq;
using System.Web.UI;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.Framework.Helpers;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.Web.UI;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Common
{
    public class BaseListingModule : WebControl
    {
        #region private variables

        #endregion

        #region Public Properties

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Set homepage item
            HomePageItem = SitecoreItem.HomeItem;

            // Set current item
            CurrentMenuItem = SitecoreItem.CurrentItem;

            // Set menu item
            SetMenuItem();
        }

        /// <summary>
        /// Sets the menu item.
        /// </summary>
        private void SetMenuItem()
        {
            this.MenuItem = SitecoreItem.GetItem(DataSource);
        } 

        protected virtual Item CurrentMenuItem { get; set; }
        protected virtual Item HomePageItem { get; set; }
        protected virtual Item MenuItem { get; set; }

        //uncomment this if its needed
        //public static string AddCssClass(string currentCssClass, string addCssClass)
        //{
        //    if (!string.IsNullOrEmpty(currentCssClass))
        //    {
        //        return string.Format("{0} {1}", currentCssClass, addCssClass);
        //    }

        //    return addCssClass;
        //}

        protected override void DoRender(HtmlTextWriter output)
        {
            if (MenuItem != null)
            {
                Item[] children = GetChildrenForListing(MenuItem);

                if (children != null && children.Count() > 0)
                {
                    // Rendering specified levels of menu
                     RenderMenuStatic(output, children, children.FirstOrDefault(), children.LastOrDefault());
                }
            }
        }

        /// <summary>
        /// Renders the menu.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="children">The children.</param>
        /// <param name="firstItem">The first item.</param>
        /// <param name="lastItem">The last item.</param>
        private void RenderMenuStatic(HtmlTextWriter output, Item[] children, Item firstItem, Item lastItem)
        {
            if (children != null && children.Count() > 0)
            {
                // process children items
                foreach (Item child in children)
                {
                    
                    if(child.Template.Name == "Category")
                    {
                        RenderCategory(output, child);
                    }
                    else //must be a node
                    {
                        RenderListItem(output, child);
                    }

                    if (child.HasChildren && child.Template.Name == "Category") //only recurse if the child item is a category
                    {
                        Item[] grandChildren = GetChildrenForListing(child);
                        if (grandChildren.Count() > 0)
                        {  
                            RenderMenuStatic(output, grandChildren, grandChildren.FirstOrDefault(), grandChildren.LastOrDefault());
                        }
                    }
                    if (child.Template.Name == "Category")
                    {
                        output.RenderEndTag();
                    }
                }
            }
        }

        public static Item[] GetChildrenForListing(Item item)
        {
            using (new SecurityDisabler())
            {
                if (item != null && item.HasChildren)
                {
                    Item[] children = item.GetChildren().ToArray();
                    return children;
                }

                return null;
            }
        }

        public virtual void RenderCategory(HtmlTextWriter output, Item child)
        {
            output.RenderBeginTag(HtmlTextWriterTag.Ul);
            output.Write(child.Fields["Title"].Value);   
            return;
        }

        public virtual void CloseCategory(HtmlTextWriter output)
        {
            output.RenderEndTag();
            return;
        }

        public virtual void RenderListItem(HtmlTextWriter output, Item child)
        {
            output.RenderBeginTag(HtmlTextWriterTag.Li);
            Sitecore.Data.Fields.LinkField linkfield = child.Fields["ItemLink"];
            if (linkfield.Url != "")
                output.AddAttribute(HtmlTextWriterAttribute.Href, linkfield.Url);
            else
                output.AddAttribute(HtmlTextWriterAttribute.Href, "#");
            output.RenderBeginTag(HtmlTextWriterTag.A);
            output.Write(child.Fields["Title"].Value);
            output.RenderEndTag();
            output.RenderEndTag();
            return;
        }
    }
}