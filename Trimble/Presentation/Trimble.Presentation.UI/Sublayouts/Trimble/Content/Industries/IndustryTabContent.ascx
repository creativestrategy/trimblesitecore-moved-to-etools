﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndustryTabContent.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.Industries.IndustryTabContent" %>
<script type="text/javascript">
    $(document).ready(function () {
        var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
    });

</script>
<div id="rhs_container">
    <div class="industry_tab">
        <div id="TabbedPanels1" class="TabbedPanels">
            <ul class="TabbedPanelsTabGroup" id="Ul1" runat="server">
                <li class="TabbedPanelsTab" tabindex="0" id="li1" runat="server">
                    <p>
                        <span>
                            <sc:FieldRenderer ID="frTab1Title" runat="server" FieldName="Tab1Title" />
                        </span>
                    </p>
                </li>
                <li class="TabbedPanelsTab" tabindex="0" id="li2" runat="server">
                    <p>
                        <span>
                            <sc:FieldRenderer ID="frTab2Title" runat="server" FieldName="Tab2Title" />
                        </span>
                    </p>
                </li>
                <li class="TabbedPanelsTab" tabindex="0" id="li3" runat="server">
                    <p>
                        <span>
                            <sc:FieldRenderer ID="frTab3Title" runat="server" FieldName="Tab3Title" />
                        </span>
                    </p>
                </li>
                <li class="TabbedPanelsTab" tabindex="0" id="li4" runat="server">
                    <p>
                        <span>
                            <sc:FieldRenderer ID="frTab4Title" runat="server" FieldName="Tab4Title" />
                        </span>
                    </p>
                </li>
                <li class="TabbedPanelsTab" tabindex="0" id="li5" runat="server">
                    <p>
                        <span>
                            <sc:FieldRenderer ID="frTab5Title" runat="server" FieldName="Tab5Title" />
                        </span>
                    </p>
                </li>
            </ul>
            <div class="TabbedPanelsContentGroup">
                <asp:Repeater ID="rpMain" runat="server" OnItemDataBound="rpMain_ItemDataBound">
                    <ItemTemplate>
                        <div class="TabbedPanelsContent">
                            <asp:Repeater ID="rpMaincategorylevelchilds" runat="server" OnItemDataBound="rpMaincategorylevelchilds_ItemDataBound">
                                <ItemTemplate>
                                    <sc:EditFrame runat="server" ID="efAddCategoryAction" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/CategoryRendererActions">
                                        <div class="bizLineTabPanel2">                                         
                                            <div class="paddTop10">
                                                <div class="tab_heading">
                                                    <sc:FieldRenderer ID="frIndustryCatTitle" runat="server" FieldName="Title" />
                                                </div>
                                                <asp:Repeater ID="rpMaincategorylevelSubchilds" runat="server" OnItemDataBound="rpMaincategorylevelSubchilds_ItemDataBound">
                                                    <ItemTemplate>
                                                    <sc:EditFrame runat="server" ID="efAddActions" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/ProductRendererActions">
                                                        <div class="bizLineItemContainer">
                                                            <br />
                                                            <div class="tabcontent_img">
                                                                 <sc:Link ID="lkIndustrylnk1" runat="server" Field="GeneralLink">
                                                                    <sc:FieldRenderer ID="frIndustryTabContentImage" runat="server" FieldName="Image"/>
                                                                </sc:Link>
                                                            </div>
                                                            <div id="divIndustry" runat="server">
                                                                <div class="tabcontent_title">                                                                   
                                                                    <sc:Link ID="lkIndustrylnk" runat="server" Field="GeneralLink">
                                                                        <sc:FieldRenderer ID="frIndustryTabContentTitle" runat="server" FieldName="Title" />
                                                                    </sc:Link>                                                                   
                                                                </div>
                                                                <div class="tabcontent">
                                                                    <sc:FieldRenderer ID="frIndustryTabContentText" runat="server" FieldName="Text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </sc:EditFrame>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                   <sc:EditFrame  runat="server" ID="efAddProduct" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Product">                                                    
                                                        Add Product                                                   
                                                    </sc:EditFrame>
                                               </div>
                                            <br />
                                        </div>
                                    </sc:EditFrame>
                                </ItemTemplate>                           
                            </asp:Repeater>
                           <sc:EditFrame runat="server" ID="efAddCategory" Visible="false" Buttons="/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Trimble/Insert Category">
                             
                                Add Category
                           
                        </sc:EditFrame>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
               
            </div>
        </div>
    </div>
</div>
