﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomepageBanner.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage.HomepageBanner" %>
<script type="text/javascript">
    var images = Array();
    <asp:Repeater runat="server" ID="rptData2">
        <ItemTemplate>
            images.push('<asp:Literal runat="server" ID="imgSOurce" Text="<%# Container.DataItem %>" />');
        </ItemTemplate>
    </asp:Repeater>
    MM_preloadImages(images);

    jQuery(function () {
        $('#allinone_thumbnailsBanner_simple').allinone_thumbnailsBanner({
            skin: 'simple',
            width: 1250,
            height: 450,
            width100Proc: false,
            thumbsReflection: 0,
            responsive: true,
            thumbsWrapperMarginTop: -125,
            defaultEffect: 'fade',
            resizeImages: true,
            showPreviewThumbs: true,
            enableTouchScreen: 0
        });
    });  
  
</script>
<div id="container_banner" style="margin: 0 auto">
    <div id="allinone_thumbnailsBanner_simple" style="display: none;">
        <ul class="allinone_thumbnailsBanner_list">
            <asp:Repeater runat="server" ID="rptData" OnItemDataBound="rptData_ItemDataBound">
                <ItemTemplate>
                    <li id="bannerThumb" runat="server">
                        <sc:FieldRenderer ID="frBannerImage" runat="server" FieldName="BannerImage" DisableWebEditing="false" />
                        
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <asp:Repeater runat="server" ID="rptText" OnItemDataBound="rptText_ItemDataBound">
            <ItemTemplate>
                <div id="bannerText" class="allinone_thumbnailsBanner_texts" runat="server" clientidmode="Static">
                    <div class="allinone_thumbnailsBanner_text_line textElement21_simpleFullWidth" data-initial-left="155"
                        data-initial-top="30" data-final-left="155" data-final-top="140" data-duration="0.5"
                        data-fade-start="0" data-delay="0">
                        <span class="largefont">
                            <sc:FieldRenderer ID="frBannerTitle" runat="server" FieldName="BannerTitle" DisableWebEditing="false" />
                        </span>
                        <br />
                        <span class="smallfont">
                            <sc:FieldRenderer ID="frBannerTeaser" runat="server" FieldName="BannerTeaser" DisableWebEditing="false" />
                        </span>
                        <br />
                        <span class="liasd"><a id="lnkTargetLocation" runat="server">
                            <sc:FieldRenderer ID="frLinkTitle" runat="server" FieldName="CallToActionButtonLabel" />
                        </a></span>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
