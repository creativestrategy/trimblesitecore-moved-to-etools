﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeroQuote.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage.HeroQuote" %>


<div class="heroQuoteItem" runat="server">
    <div data-aos="fade-up" data-aos-duration="3000">
        <sc:Link ID="lnHeroLinkImage" runat="server" Field="GeneralLink" >  
            <sc:FieldRenderer ID="frHeroImage" runat="server" FieldName="Image"/>
        </sc:Link>
        <div class="heroQuoteText">
            <div class="heroText"><sc:FieldRenderer ID="frHeroText" runat="server" FieldName="GeneralText" /></div>
            <div class="heroPersonName"><sc:FieldRenderer ID="frHeroPersonName" runat="server" FieldName="PersonName" /></div>
            <div class="heroPersonTitle"><sc:FieldRenderer ID="frHeroPosition" runat="server" FieldName="Position" /></div>
            <span runat="server" id="heroPersonCTAButton"><sc:Link ID="lnHeroLinkButton" runat="server" Field="GeneralLink" ><span class="heroButton"><sc:FieldRenderer ID="frCTAButton" runat="server" FieldName="CTA_ButtonText" /></span></sc:Link></span>
        </div>
    </div>
</div>

