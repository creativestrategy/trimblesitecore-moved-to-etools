﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Sublayouts.Framework;
using Trimble.Business.Model.News.Repository;
using Trimble.Business.Model.News;
using Sitecore.Data.Items;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Web.UI.WebControls;
using System.Configuration;
using Sitecore.Data;
using Sitecore.Globalization;
using Sitecore.Publishing;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage
{
    public partial class News : SublayoutBaseExtended
    {

        #region Properties

        Item dataSourceItem = null;

        #endregion

        protected Item GetDataSourceItem()
        {
            const string path = "/sitecore/content/Website/Content/Pages/Home/News";
            dataSourceItem = Sitecore.Context.Database.GetItem(path);
            return dataSourceItem;
        }

        private string _newsArticleLink;

        protected void Page_Load(object sender, EventArgs e)
        {
            GetDataSourceItem();
            txtTrimbleNews.Item = dataSourceItem;
            lnkviewAllLink.Item = dataSourceItem;

            Sitecore.Data.Fields.LinkField newsPage = ((Sitecore.Data.Fields.LinkField)dataSourceItem.Fields["NewsArticlePageLink"]);
            if(newsPage != null)
                _newsArticleLink = newsPage.Url;

            //get the news articles from the database
            int numberOfNewsArticles = 2;
            NewsRepository newsRepository = new NewsRepository();

            List<NewsArticle> newsItems = newsRepository.GetTopNNewsArticles(numberOfNewsArticles);

            rptData.DataSource = newsItems;
            rptData.DataBind();
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                NewsArticle newsArticle = (NewsArticle)e.Item.DataItem;

                // Display news article
                Literal publishDate = (Literal)e.Item.FindControl("publishDate");
                publishDate.Text = String.Format("{0:MMM dd, yyyy}", newsArticle.PublishDate);

                HyperLink newsLink = (HyperLink)e.Item.FindControl("newsLink");
                newsLink.Attributes.Add("target", "_blank");
                newsLink.NavigateUrl = _newsArticleLink + newsArticle.FileName;
                newsLink.Text = newsArticle.Title;
            }
        }
    }
}