﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.HtmlControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.UI.Sublayouts.Framework;
using Deloitte.SC.Framework.Helpers;
using log4net;
using System.Web.UI.HtmlControls;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage
{
    public partial class HeroQuote : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Condition for is page postback
                if (!Page.IsPostBack)
                {

                    frHeroImage.Item = DataSourceItem;
                    lnHeroLinkImage.Item = DataSourceItem;
                    frHeroText.Item = DataSourceItem;
                    frHeroPersonName.Item = DataSourceItem;
                    frHeroPosition.Item = DataSourceItem;
                    lnHeroLinkButton.Item = DataSourceItem;
                    frCTAButton.Item = DataSourceItem;

                    //Response.Write(frCTAButton.Item.Fields["CTA_ButtonText"]);
                    if ( frCTAButton.Item.Fields["CTA_ButtonText"].ToString() == "")
                    {
                        //Response.Write("null");
                        HtmlGenericControl ctaSpan = (HtmlGenericControl)FindControl("heroPersonCTAButton");
                        ctaSpan.Attributes.Add("style", "display:none");
                    }

                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HeroQuote:Page_Load" + ex.Message);
            }
        }

        /*
        #region AssigningDataSource

        public void AssigningDataSource(Item DataSource)
        {
            Sitecore.Data.Fields.ImageField imgfield = DataSource.Fields["Image"];
            if (imgfield != null)
            {
                if (String.IsNullOrEmpty(imgfield.Value))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        DataSource.Editing.BeginEdit();
                        Sitecore.Data.Fields.ImageField standardimg = DataSource.Template.StandardValues.Fields["VideoImage"];
                        DataSource.Fields["VideoImage"].Value = standardimg.Value;
                        DataSource.Editing.EndEdit();
                    }
                    imgvideoimage.Item = DataSource;
                }
                else
                    imgvideoimage.Item = DataSource;
            }



            Sitecore.Data.Fields.LinkField lnkfield = DataSource.Fields[CommonFields.link];
            if (lnkfield != null)
            {
                lnkvideo.Item = DataSourceItem;
            }



        }

        #endregion
        */
    }
}