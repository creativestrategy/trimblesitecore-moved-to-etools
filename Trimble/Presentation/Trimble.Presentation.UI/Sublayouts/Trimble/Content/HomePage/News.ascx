﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage.News" %>
    <div class="headerContainer">
        <h1 class="trimbleNewsHeader">
            <sc:Text ID="txtTrimbleNews" runat="server" Field="Title"/> 
         </h1>
         <span class="viewall"> 
                <sc:Link ID="lnkviewAllLink" runat="server" Field="ViewAllLink" CssClass="viewall"/>
         </span>
    </div>
    
    <asp:Repeater runat="server" ID="rptData" OnItemDataBound="rptData_ItemDataBound">
        <ItemTemplate>
                <div class="content1_inner"> 
                <span><asp:Literal ID="publishDate" runat="server"></asp:Literal></span> 
                    <%--<sc:Link ID="lnknewsArticle" runat="server" Field="NewsArticlePageLink" />--%>
                <asp:HyperLink ID="newsLink" runat="server" CssClass="content1_inner">News Article Title</asp:HyperLink> 
                </div>
        </ItemTemplate>
    </asp:Repeater>
  