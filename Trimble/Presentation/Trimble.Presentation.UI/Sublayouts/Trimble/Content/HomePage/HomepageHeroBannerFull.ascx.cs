﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage
{
    public partial class HomepageHeroBannerFull : SublayoutBaseExtended
    {
        private string _locationLink;
        private string _locationTitle;
        private string dataElement;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Populate();

                //frBannerImage.Item = DataSourceItem;
                frBannerTitle.Item = DataSourceItem;
                frBannerTeaser.Item = DataSourceItem;
                frCallToActionButtonLink.Item = DataSourceItem;
                frCallToActionButtonLabel.Item = DataSourceItem;
               
     
                
                Sitecore.Data.Items.Item item = DataSourceItem;

                Sitecore.Data.Fields.ImageField imgBannerActive = ((Sitecore.Data.Fields.ImageField)item.Fields["BannerImage"]);
                string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgBannerActive.MediaItem);
                HtmlImage imgB = (HtmlImage)FindControl("imgBanner");
                imgB.Src = imgSrc;
                //imgB.Attributes.Add("style", "width:100%");
                imgB.Attributes.Add("class", "bannerwidth");

                //HtmlImage cta = (HtmlImage)FindControl("ctaHeroButtton");
                //cta.Attributes.Add("data-clickElement", "test");
                //Sitecore.Data.Fields.LinkField lf = item.Fields["BannerLink"];
                //Response.Write("frDataClickElement  = " + item.Fields["DataClickElement"]);
                dataElement = item.Fields["DataClickElement"].ToString();
                frCallToActionButtonLink.Attributes.Add("id", "cta_hero_" + dataElement);
                frCallToActionButtonLink.Attributes.Add("data-clickElement", dataElement);
            }
        }

        

        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {

            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    // Use LinkMananger for internal links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    // Use MediaManager for media links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    // Just return external links 
                    return lnkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty 
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link 
                    return lnkField.Url;
                case "javascript":
                    // Just return javascript 
                    return lnkField.Url;
                default:
                    // Just please the compiler, this 
                    // condition will never be met 
                    return lnkField.Url;
            }

        }
    }
}