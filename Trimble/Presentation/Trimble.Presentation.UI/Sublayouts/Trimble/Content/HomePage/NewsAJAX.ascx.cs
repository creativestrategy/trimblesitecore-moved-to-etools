﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Sublayouts.Framework;
using Trimble.Business.Model.News.Repository;
using Trimble.Business.Model.News;
using Sitecore.Data.Items;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Web.UI.WebControls;
using System.Configuration;
using Sitecore.Data;
using Sitecore.Globalization;
using Sitecore.Publishing;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage
{
    public partial class NewsAJAX : SublayoutBaseExtended
    {

        #region Properties

        Item dataSourceItem = null;

        #endregion
        /*
        protected Item GetDataSourceItem()
        {
            const string path = "/sitecore/content/Website/Content/Home/NewsAJAX";
            dataSourceItem = Sitecore.Context.Database.GetItem(path);
            return dataSourceItem;
        }

        private string _newsArticleLink;

        protected void Page_Load(object sender, EventArgs e)
        {
            GetDataSourceItem();
            txtTrimbleNews.Item = dataSourceItem;
            lnkviewAllLink.Item = dataSourceItem;
            txtTrimbleNewsContent.Item = dataSourceItem;

        }
        */


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item DataSource = DataSourceItem;
                if (DataSource != null)
                {
                    txtTrimbleNews.Item = DataSource;
                    lnkviewAllLink.Item = DataSource;
                    txtTrimbleNewsContent.Item = DataSource;
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("NewsAJAX:Page_Load" + ex.Message);
            }

        }
        
    }
}