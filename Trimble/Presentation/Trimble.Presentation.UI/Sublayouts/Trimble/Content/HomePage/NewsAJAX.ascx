﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsAJAX.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage.NewsAJAX" %>
    <div class="headerContainer">
        <h1 class="trimbleNewsHeader">
            <sc:Text ID="txtTrimbleNews" runat="server" Field="Title"/> 
         </h1>
         <span class="viewall"> 
                <sc:Link ID="lnkviewAllLink" runat="server" Field="ViewAllLink" CssClass="viewall"/>
         </span>
    </div>
    <div class="content1_inner"> 
        <sc:Text ID="txtTrimbleNewsContent" runat="server" Field="NewsContent"/> 
    </div>

  