﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomepageBannerFull.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage.HomepageBannerFull" %>
<script type="text/javascript">

</script>

<div id="container_banner" style="margin: 0 auto">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
        <div class="banner_plusBG"></div>
        <!-- Indicators -->        
        <ol class="carousel-indicators">
            <asp:Repeater runat="server" ID="rptIndicators" OnItemDataBound="rptIndicators_ItemDataBound">
                <ItemTemplate>
                    <li id="liIndicator" runat="server"><sc:FieldRenderer ID="frIndicator" runat="server" FieldName="liIndicator" DisableWebEditing="false" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ol>

        <div class="carousel-inner">
            <asp:Repeater runat="server" ID="rptBannerData" OnItemDataBound="rptBannerData_ItemDataBound">
                <ItemTemplate>

                    <div id="bannerText"  clientidmode="Static" runat="server">
                        <img id="imgBanner" runat="server" />
                        <div class="container">
                            <div class="carousel-caption">
                                <div class="CTAcontainer">
                                <h1 class="banner"><sc:FieldRenderer ID="frBannerTitle" runat="server" FieldName="BannerTitle" DisableWebEditing="false" /></h1>
                                <p class="bannerinnertxt"><sc:FieldRenderer ID="frBannerTeaser" runat="server" FieldName="BannerTeaser" DisableWebEditing="false" />
                                <br><br>
                                <a id="lnkTargetLocation"  class="btn btn-lg btn-primary" role="button" runat="server"><sc:FieldRenderer ID="frLinkTitle" runat="server" FieldName="CallToActionButtonLabel" /><span class="faNIL fa-arrow-right"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </ItemTemplate>
            </asp:Repeater>
        </div><!--carousel-inner-->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="fa fa-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="fa fa-chevron-right"></span></a>
    </div> <!--myCarousel-->
</div><!--container_banner-->




