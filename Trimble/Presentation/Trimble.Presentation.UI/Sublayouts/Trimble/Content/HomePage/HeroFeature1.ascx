﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeroFeature1.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage.HeroFeature1" %>


<div class="heroFeat1Item" runat="server">
    <sc:Link ID="lnHeroFeat1LinkImage" runat="server" Field="GeneralLink" >  
        <sc:FieldRenderer ID="frHeroFeat1Image" runat="server" FieldName="Image"/>
    </sc:Link>
    <div class="heroFeat1Title"><sc:FieldRenderer ID="frHeroFeat1Title" runat="server" FieldName="Title" /></div>
    <div class="heroFeat1Text"><sc:FieldRenderer ID="frHeroFeat1Text" runat="server" FieldName="GeneralText" /></div>
    <sc:Link ID="lnHeroFeat1LinkButton" runat="server" Field="GeneralLink" Parameters="class=btn btn-lg btn-primary-blue"><sc:FieldRenderer ID="frCTAButton" runat="server" FieldName="CTA_ButtonText" /><span class="faNIL fa-caret-right"></span></sc:Link>
</div>