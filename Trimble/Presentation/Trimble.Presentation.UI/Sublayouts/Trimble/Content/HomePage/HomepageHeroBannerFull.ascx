﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomepageHeroBannerFull.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage.HomepageHeroBannerFull" %>


<div id="myCarousel" class="carousel slide" data-ride="carousel"> 
    <div class="carousel-inner">
        <div id="container_banner" style="margin: 0 auto" runat="server">
            <div id="bannerText"  clientidmode="Static" class="item active" runat="server">
                <img id="imgBanner" runat="server" alt="Hero Banner" />
                <div class="container">
                    <div class="carousel-caption hero-banner-caption">
                        <div class="CTAcontainer" data-aos="zoom-out-down">
                        <h1 class="banner hero-banner-title"><sc:FieldRenderer ID="frBannerTitle" runat="server" FieldName="BannerTitle" DisableWebEditing="false" /></h1>
                        <div>
                            <p class="bannerinnertxt hero-teaser-text"><sc:FieldRenderer ID="frBannerTeaser" runat="server" FieldName="BannerTeaser" DisableWebEditing="false" />
                            <br>
                            </p>
                            <sc:Link ID="frCallToActionButtonLink" runat="server" Field="BannerLink" Parameters="class=btn btn-lg btn-primary-blue"><sc:FieldRenderer ID="frCallToActionButtonLabel" runat="server" FieldName="CallToActionButtonLabel" /><span class="faNIL fa-caret-right"></span></sc:Link>
                            <br /><br />
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




