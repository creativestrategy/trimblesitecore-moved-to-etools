﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Deloitte.SC.UI.Sublayouts.Framework;
using System.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Content.HomePage
{
    public partial class HomepageBanner : SublayoutBaseExtended
    {
        private string _locationLink;
        private string _locationTitle;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Populate();
            }
        }

        public virtual void Populate()
        {
            try
            {
                // Get banners folder item
                Item bannersFolderItem = DataSourceItem;
                if (bannersFolderItem != null)
                {
                    List<Item> rotatingBanners = new List<Item>();


                    var randomNumber = new Random(DateTime.Now.Millisecond);
                    int count = bannersFolderItem.GetChildren().Count;

                    if (count > 7)
                    {
                        count = 7;
                    }
                    for (int i = 1; i < count - 1; i++)
                    {
                        Item nItem = bannersFolderItem.GetChildren()[i];
                        rotatingBanners.Add(nItem);
                    }

                    if (rotatingBanners.Count > 0)
                    {

                        var n = rotatingBanners.Count;
                        while (n > 1)
                        {
                            n--;
                            var k = randomNumber.Next(n + 1);
                            var value = rotatingBanners[k];
                            rotatingBanners[k] = rotatingBanners[n];
                            rotatingBanners[n] = value;
                        }





                        Item nItemfirst = bannersFolderItem.GetChildren()[0];
                        rotatingBanners.Insert(0, nItemfirst);


                        Item nItemlast = bannersFolderItem.GetChildren()[count - 1];
                        rotatingBanners.Insert(count - 1, nItemlast);




                        List<string> imgsource = new List<string>();
                        foreach (Item i in rotatingBanners)
                        {
                            Sitecore.Data.Fields.ImageField img = i.Fields["BannerImage"];
                            imgsource.Add(Sitecore.Resources.Media.MediaManager.GetMediaUrl(img.MediaItem));
                        }
                        rptData2.DataSource = imgsource;
                        rptData2.DataBind();
                        // bind banner image items
                        rptData.DataSource = rotatingBanners;
                        rptData.DataBind();

                        // bind banner text items
                        rptText.DataSource = rotatingBanners;
                        rptText.DataBind();



                    }
                }
            }
            catch (Exception ex)
            {
                var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                logger.Info("HomepageBanner:Populate" + ex.Message);
            }
        }


        //protected void rptData2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{

        //}


        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();


                if (item != null)
                {
                    try
                    {
                        e.FindAndSetFieldRenderer("frBannerImage", item);

                        Sitecore.Data.Fields.ImageField imgBannerActive = ((Sitecore.Data.Fields.ImageField)item.Fields["BannerThumbnailActive"]);
                        string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgBannerActive.MediaItem);
                        Sitecore.Data.Fields.LinkField locationLink = ((Sitecore.Data.Fields.LinkField)item.Fields["BannerLink"]);

                        if (locationLink != null)
                            _locationLink = locationLink.Url;


                        if (imgSrc != null)
                        {
                            string bannerImgID = "#" + item.ID.ToString().Replace("{", "").Replace("-", "").Replace("}", ""); ;

                            var li = e.FindControl<HtmlGenericControl>("bannerThumb");
                            li.Attributes.Add("data-bottom-thumb", imgSrc);
                            li.Attributes.Add("data-text-id", bannerImgID);
                            li.Attributes.Add("data-thumb-url", _locationLink);
                        }
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("HomepageBanner:rptData_ItemDataBound" + ex.Message);
                    }

                }
            }
        }

        protected void rptText_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.IsListingItem())
            {
                Item item = e.GetSitecoreItem();


                if (item != null)
                {
                    try
                    {
                        e.FindAndSetFieldRenderer("frBannerTeaser", item);
                        e.FindAndSetFieldRenderer("frBannerTitle", item);
                        e.FindAndSetFieldRenderer("frCallToActionButtonLabel", item);

                        Sitecore.Data.Fields.TextField bannerTeaser = ((Sitecore.Data.Fields.TextField)item.Fields["BannerTeaser"]);

                        Sitecore.Data.Fields.LinkField locationLink = ((Sitecore.Data.Fields.LinkField)item.Fields["BannerLink"]);

                        if (locationLink != null)
                            _locationLink = locationLink.Url;

                        Sitecore.Data.Fields.TextField locationLTitle = ((Sitecore.Data.Fields.TextField)item.Fields["CallToActionButtonLabel"]);

                        //Get the field renderer and assign its datasource
                        FieldRenderer fldrenLinkTitle = (FieldRenderer)e.Item.FindControl("frLinkTitle");
                        fldrenLinkTitle.Item = item;

                        if (locationLTitle != null)
                            _locationTitle = locationLTitle.Value;


                        string bannerImgID = item.ID.ToString().Replace("{", "").Replace("-", "").Replace("}", "");

                        var div = e.FindControl<HtmlGenericControl>("bannerText");
                        div.ID = bannerImgID;

                        //Get the URL based on LinkType
                        Sitecore.Data.Fields.LinkField linkFieldcolumn1 = ((Sitecore.Data.Fields.LinkField)item.Fields["BannerLink"]);
                        string strLinkURL = GetURL(linkFieldcolumn1);
                        //Assign the Url to the Anchor
                        HtmlAnchor lnkAnchor = (HtmlAnchor)e.Item.FindControl("lnkTargetLocation");
                        lnkAnchor.HRef = strLinkURL;
                    }
                    catch (Exception ex)
                    {
                        var logger = log4net.LogManager.GetLogger("Sitecore.Diagnostics.Custom");

                        logger.Info("HomepageBanner:rptText_ItemDataBound" + ex.Message);
                    }

                }
            }
        }

        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {

            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    // Use LinkMananger for internal links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    // Use MediaManager for media links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    // Just return external links 
                    return lnkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty 
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link 
                    return lnkField.Url;
                case "javascript":
                    // Just return javascript 
                    return lnkField.Url;
                default:
                    // Just please the compiler, this 
                    // condition will never be met 
                    return lnkField.Url;
            }

        }
    }
}