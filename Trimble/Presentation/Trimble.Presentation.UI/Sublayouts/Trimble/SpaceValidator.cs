﻿using System;
using System.Runtime.Serialization;
using Sitecore.Data.Validators;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Trimble.Presentation.UI.Sublayouts.Trimble
{
    [Serializable]
    public class SpaceValidator : Sitecore.Data.Validators.StandardValidator
    {
        public SpaceValidator() { }
        public SpaceValidator(SerializationInfo info, StreamingContext context)
            : base(info, context) { }


        protected override ValidatorResult Evaluate()
        {
            // set initial return value
            ValidatorResult result = ValidatorResult.Valid;

            Sitecore.Data.Fields.TextField txtField = GetField();
            

            if (this.Parameters.ContainsKey("Except"))
            {
                    if ((txtField.Value.StartsWith(EscapeCharacters.ampersand)) || 
                        (txtField.Value.StartsWith(EscapeCharacters.Space)))
                    {
                        result = ValidatorResult.FatalError;
                        Text = "Tab Titles should not Start with Spaces";
                        return (ValidatorResult.FatalError);
                    }
                
            }
            

            return (GetFailedResult(result));
            
        }

        public override string Name
        {
            get
            {
                return ("SpaceValidator");
            }
        }
        protected override ValidatorResult GetMaxValidatorResult()
        {
            return (GetFailedResult(ValidatorResult.FatalError));
        }
       
    }
}
