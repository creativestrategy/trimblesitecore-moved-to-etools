<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupportPage.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.SupportPage" %>
<script type="text/javascript">
    $(document).ready(function () {
        $('.mega-1').dcVerticalMegaMenu({
            rowItems: '3',
            speed: 'fast',
            effect: 'show',
            direction: 'right'
        });

    });
</script>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scInnerBanner" runat="server" Key="scInnerBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div style="clear: both">
</div>
<div id="container_body">
    <!-- ******************************** Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <!-- scSideMenubarResponsive begin -->
            <sc:Placeholder ID="scSideMenubarResponsive" runat="server" Key="scSideMenubarResponsive" />
            <!-- scSideMenubarResponsive end -->
            <div class="sidebar-container clear positionRel">
                <sc:Placeholder ID="scSideMenubar" runat="server" Key="scSideMenubar" />
                <div class="quick-links hideLHS">
                    <!-- ******************************** Quick Links ******************************** -->
                    <sc:Placeholder ID="scQuickLinks" runat="server" Key="scQuickLinks" />
                    <!-- ******************************** End Of Quick Links ******************************** -->
                </div> 
                <div class="leftNavSocialIcons hideLHS"> 
                <!-- ********************************Social Media ******************************** -->            
                    <sc:Placeholder ID="scSupportSocialMedia" runat="server" Key="scSupportSocialMedia" />
                      <!-- ******************************** End Social Media******************************** -->  
                </div>
                    <!-- ********************************Featured Product Module******************************** -->             
                        <sc:Placeholder ID="scSupportFeaturedSolution" runat="server" Key="scSupportFeaturedSolution" />     
                    <!-- ******************************** End of featured Product Module******************************** -->              

                     <!-- ********************************Featured Video Module******************************** -->                 
                       <sc:Placeholder ID="scSupportFeaturedVideoModule" runat="server" Key="scSupportFeaturedVideoModule" />
                     <!-- ******************************** End of featured Module******************************** -->
                 
            </div>
        </div>
    </div>
    <!-- ******************************** End of Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="center-right-content">
                <div class="breadcrumb">
                    <!-- ******************************** Breadcrumb *********************** -->
                    <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
                    <!-- ****************************** End of Breadcrumb  ******************* -->
                </div>
                <div class="main_content">
                   
                      <div class="mainHeadContainer"> 
                        <!-- ******************************** General Text Titles ******************************** -->
                        <h3>
                            <sc:Placeholder ID="scGeneralTextTitle" runat="server" Key="scGeneralTextTitle" />
                        </h3>
                        <span>
                            <sc:Placeholder ID="scGeneralTextSubLineTitle" runat="server" Key="scGeneralTextSubLineTitle" />
                        </span>
                        </div>
                         <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                        <div class="main_content_border"></div>
                        <!-- ******************************** End of General Text Titles ******************************** -->
                        <!-- ******************************** General Text ******************************** -->
                        <div class="contentLeft">
                        <sc:Placeholder ID="scGeneralText" runat="server" Key="scGeneralText" />
                        </div>
                        <!-- ******************************** End of General Text ******************************** -->
                        <!-- ******************************** Simple Place holder ******************************** -->
                        <div class="contentRight">
                            <sc:Placeholder ID="scSimplePlaceholder" runat="server" Key="scSimplePlaceholder" />
                        </div>
                       <!-- ******************************** End Of Simple Place holder ******************************** -->
                    
                    <div class="marginTop40"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- ******************************** Products******************************** -->
    <div class="full-width-blue-container">
        <div class="center-content">
            <div class="center-right-content">
                <div class="supportContainer">
                    <div id="about_video" class="padd20 autoWidth">
                        <div class="productsMainContainer paddTop0">
                            <div class="padding30">
                                <sc:Placeholder ID="scProducts" runat="server" Key="scProducts" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ******************************** End of Products******************************** -->
    <br />
    <!-- ******************************** Tab Module******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="center-right-content">
                <sc:Placeholder ID="scSupportTabModule" runat="server" Key="scSupportTabModule" />
            </div>
        </div>
    </div>
    <!-- ********************************End Of Tab Module******************************** -->
    <div style="clear: both">
    </div>
</div>
<div class="full-width-white-container">
    <div class="center-content">
        <div class="product_detail">
            <!-- ******************************** Product Details Module******************************** -->
            <sc:Placeholder ID="scSupportProductModule" runat="server" Key="scSupportProductModule" />
            <!-- ******************************** End of Product Details Module******************************** -->
        </div>
    </div>
</div>
