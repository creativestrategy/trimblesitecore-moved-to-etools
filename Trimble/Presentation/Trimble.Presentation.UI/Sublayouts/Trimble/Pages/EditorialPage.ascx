﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditorialPage.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.EditorialPage" %>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scInnerBanner" runat="server" Key="scInnerBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
       
<div id="container_body" class="containerWidth">
    <!-- scSideMenubarResponsive begin -->
    <sc:Placeholder ID="Placeholder1" runat="server" Key="scSideMenubarResponsive" />
    <!-- scSideMenubarResponsive end -->

    <div class="sidebar-container clear positionRel">
        <!-- ******************************** Side Menu bar ******************************** -->
        <sc:Placeholder ID="scSideMenubar" runat="server" Key="scSideMenubar" />
        <div class="quick-links hideLHS">
            <!-- ******************************** Quick Links ******************************** -->
            <sc:Placeholder ID="scQuickLinks" runat="server" Key="scQuickLinks" />
            <!-- ******************************** End Of Quick Links ******************************** -->
        </div>
        <div class="leftNavSocialIcons hideLHS">
            <!-- ******************************** Social Media ******************************** -->
            <sc:Placeholder ID="scEditorialSocialMedia" runat="server" Key="scEditorialSocialMedia" />
            <!-- ******************************** End of Social Media ******************************** -->
        </div>
            <!-- ********************************Featured Product Module******************************** -->             
                <sc:Placeholder ID="scEditorialFeaturedSolution" runat="server" Key="scEditorialFeaturedSolution" />     
            <!-- ******************************** End of featured Product Module******************************** -->              

            <!-- ********************************Featured Video Module******************************** -->                 
            <sc:Placeholder ID="scEditorialFeaturedVideoModule" runat="server" Key="scEditorialFeaturedVideoModule" />
            <!-- ******************************** End of featured Module******************************** -->


    </div>
    <!-- ******************************** End Of Side Menu bar ******************************** -->
    <div id="rhs_container">
        <!-- ******************************** Breadcrumb *********************** -->
        <div class="breadcrumb">
            <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
        </div>
        <!-- ****************************** End of Breadcrumb  ******************* -->
        <div class="main_content">
               <div>
                <h3><sc:Placeholder ID="scEditorialTitle" runat="server" Key="scEditorialTitle" /></h3>
                <span><sc:Placeholder ID="scEditorialSubTitle" runat="server" Key="scEditorialSubTitle" /></span>
            </div>
            <div class="main_content_border"></div>
            <!-- ******************************** General Text ******************************** -->
            <sc:Placeholder ID="scGeneralTextSwap" runat="server" Key="scGeneralTextSwap" />
            <!-- ******************************** End of General Text ******************************** -->
        </div>
        <div class="rhs-tab">
            <!-- ******************************** Tab Module******************************** -->
            <sc:Placeholder ID="scTabModule" runat="server" Key="scTabModule" />
            <!-- ******************************** End of Tab Module******************************** -->
        </div>
        <div style="clear: both">
        </div>
    </div>

    <div style="clear: both">
    </div>
</div>
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="product_detail">
                <!-- ******************************** Product Details Module******************************** -->
                <sc:Placeholder ID="scEditorialProducModule" runat="server" Key="scEditorialProducModule" />
                <!-- ******************************** End of Product Details Module******************************** -->
            </div>
        </div>
    </div>