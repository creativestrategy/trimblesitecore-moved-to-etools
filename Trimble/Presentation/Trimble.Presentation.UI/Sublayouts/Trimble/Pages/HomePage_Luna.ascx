﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePage_Luna.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.HomePage_Luna" %>
<div class="clear-both"></div>



<div id="dvContainer_Body">
    <!-- ******************************** Homepage Banner ******************************** -->
    <sc:Placeholder ID="scHeroBanner" runat="server" Key="scHeroBanner" />
    <sc:Placeholder ID="scCarousel" runat="server" Key="scCarousel" />
    <!-- ******************************** End Of Homepage Banner ******************************** -->
    <div style="clear: both"></div>



    <div style="clear: both"></div>

    <!-- ******************************** List Module - Who We Are ******************************** -->
    <sc:Placeholder ID="scListModule_Column3_About" runat="server" Key="scListModule_Column3_About" />
    <!-- ******************************** End List Module  ******************************** -->   

    <!-- ******************************** Hero Feat ******************************** -->
    <div class="full-width-lgrey-container">
    <sc:Placeholder ID="scHeroFeat" runat="server" Key="scHeroFeat" />
    </div>
    <!-- ******************************** End Hero Feat  ******************************** -->

    <!-- ******************************** List Module - Industries******************************** -->
    <sc:Placeholder ID="scListModule_Column3_Industry" runat="server" Key="scListModule_Column3_Industry" />
    <!-- ******************************** End List Module  ******************************** -->

    <!-- ******************************** Hero Quote ******************************** -->
    <div class="full-width-dblue-container">
    <sc:Placeholder ID="scHeroQuote" runat="server" Key="scHeroQuote" />
    </div>
    <!-- ******************************** End Hero Quote  ******************************** -->

  
    <!-- ******************************** List Module - Resources******************************** -->
    <div class="listcol3_resources">
    <sc:Placeholder ID="scListModule_Column3_Resources" runat="server" Key="scListModule_Column3_Resources" />
    </div>
    <!-- ******************************** End List Module  ******************************** -->

    <!-- ******************************** List Module Featured ******************************** -->    
    <sc:Placeholder ID="scListModule_Column3_Featured" runat="server" Key="scListModule_Column3_Featured" />    
    <!-- ******************************** End List Module Featured ******************************** -->



    <div style="clear: both"></div>




</div><!-- end dvContainer_Body -->