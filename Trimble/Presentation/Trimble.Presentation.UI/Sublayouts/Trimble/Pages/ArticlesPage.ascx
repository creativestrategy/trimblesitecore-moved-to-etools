﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticlesPage.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ArticlesPage" %>
<div class="clear-both"></div>

<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scArticleBanner" runat="server" Key="scArticleBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div class="clear-both"></div>
<div id="dvContainer_Body">
    <div class="full-width-white-container">
        <div class="center-content">
            <!-- ******************************** Breadcrumb *********************** -->
            <div class="breadcrumb">
                <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
            </div>
            <!-- ****************************** End of Breadcrumb  ******************* -->
            <div class="main_content paddIndustryTitle">
                <h3><sc:Placeholder ID="scArticleTitle" runat="server" Key="scArticleTitle" /></h3>
                <span><sc:Placeholder ID="scArticleSubTitle" runat="server" Key="scArticleSubTitle" /></span>
                <div class="main_content_border"></div>
                <!-- ******************************** General Text ******************************** -->
                <sc:Placeholder ID="scGeneralText" runat="server" Key="scGeneralText" />
                <!-- ******************************** End of General Text ******************************** -->
            </div>
        </div>
    </div>
    <div class="listpage2">
    <!-- ******************************** List Module ******************************** -->
    <sc:Placeholder ID="scListModule" runat="server" Key="scListModule" />
    <!-- ******************************** End of General Text ******************************** -->
    </div>

    <div style="clear: both"></div>




</div><!-- end dvContainer_Body -->

