﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutPage.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.TwoColumnsPage" %>
<script type="text/javascript">
    $(document).ready(function () {
        $('.mega-1').dcVerticalMegaMenu({
            rowItems: '3',
            speed: 'fast',
            effect: 'show',
            direction: 'right'
        });

    });
</script>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scInnerBanner" runat="server" Key="scInnerBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div class="clear-both"></div>


<div id="container_body">
    <!-- ******************************** Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <!-- scSideMenubarResponsive begin -->
            <sc:Placeholder ID="scSideMenubarResponsive" runat="server" Key="scSideMenubarResponsive" />
            <!-- scSideMenubarResponsive end -->
            <div class="sidebar-container clear positionRel">
                <sc:Placeholder ID="scSideBarContainer" runat="server" Key="scSideBarContainer" />

                <div class="quick-links hideLHS">
                    <!-- ******************************** Quick Links ******************************** -->
                    <sc:Placeholder ID="scAboutQuickLinks" runat="server" Key="scAboutQuickLinks" />
                    <!-- ******************************** End Of Quick Links ******************************** -->
                </div> 
                <div class="leftNavSocialIcons hideLHS"> 
                <!-- ********************************Social Media ******************************** -->            
                    <sc:Placeholder ID="scAboutSocialMedia" runat="server" Key="scAboutSocialMedia" />
                      <!-- ******************************** End Social Media******************************** -->  
                </div>
                    <!-- ********************************Featured Product Module******************************** -->             
                        <sc:Placeholder ID="scAboutFeaturedSolution" runat="server" Key="scAboutFeaturedSolution" />     
                    <!-- ******************************** End of featured Product Module******************************** -->              

                     <!-- ********************************Featured Video Module******************************** -->                 
                       <sc:Placeholder ID="scAboutFeaturedVideoModule" runat="server" Key="scAboutFeaturedVideoModule" />
                     <!-- ******************************** End of featured Module******************************** -->
            </div>
        </div>
    </div>
    <!-- **************************** End of Side Menu bar ***************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="center-right-content">
                <!-- ******************************** Breadcrumb *********************** -->
                <div class="breadcrumb">
                    <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
                </div>
                <!-- ****************************** End of Breadcrumb  ******************* -->
                <!-- ******************************** Main Content Container *********************** -->               
              
                <div class="main_content">
                    <div class="mainHeadContainer"> 
                    <h3><sc:Placeholder ID="scAboutGeneralTitle" runat="server" Key="scAboutGeneralTitle" /></h3>
                    <span><sc:Placeholder ID="scAboutGeneralSubTitle" runat="server" Key="scAboutGeneralSubTitle" /></span>
                    </div>
                      <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                    <div class="main_content_border"></div>
                    <sc:Placeholder ID="scMainContent" runat="server" Key="scMainContent" />
                </div>
                <!-- ******************************End of Main Content Container ******************* -->
            </div>
        </div>
    </div>
    <!-- ******************************** Video Container ******************************** -->
   
        <sc:Placeholder ID="scVideoContent" runat="server" Key="scVideoContent" />
               
    <!-- ********************************End of Video Container ******************************** -->
          
    <div style="clear: both">
    </div>
</div>
<div class="full-width-white-container">
    <div class="center-content">
        <div class="product_detail">
            <!-- ******************************** Product Details Module******************************** -->
            <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
            <!-- ******************************** End of Product Details Module******************************** -->
        </div>
    </div>
</div>
