﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessLinePage2.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.BusinessLinePage2" %>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scBusinessLine2Banner" runat="server" Key="scBusinessLine2Banner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div class="clear-both">
</div>
<div id="container_body" class="containerWidth">
    <div class="industry">
        <!-- ******************************** Breadcrumb *********************** -->
        <div class="breadcrumb">
            <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
        </div>
        <!-- ****************************** End of Breadcrumb  ******************* -->
        <div class="clear-both">
        </div>
        <div class="transportleft">
            <div class="main_content padd0 paddTop0">
            
            <div>
                <h3><sc:Placeholder ID="scBusinessLine2Title" runat="server" Key="scBusinessLine2Title" /></h3>
                <span><sc:Placeholder ID="scBusinessLine2SubTitle" runat="server" Key="scBusinessLine2SubTitle" /></span>
            </div>
            <div class="main_content_border"></div>
                <div class="mainHeadContainer"> 
                <!-- ******************************** General Text ******************************** -->
                <sc:Placeholder ID="scBusinessLine2Text" runat="server" Key="scBusinessLine2Text" />
                <!-- ******************************** End of General Text ******************************** -->
                </div>
                <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                <div class="transportright">
            <!-- ******************************** Quick Links ******************************** -->
            <sc:Placeholder ID="scQuickLinks" runat="server" Key="scQuickLinks" />
            <!-- ******************************** End Of Quick Links ******************************** -->
        </div>
            </div>
        </div>
        
    </div>
</div>
<div class="clear-both">
</div>
<div id="container_transportation" class="marginTop32">
    <div class="innertransport">
        <div class="texthead3">
            <!-- ******************************** Business Line Featured Products Module Title ******************************** -->
            <sc:Placeholder ID="scBusinessLine2FeaturedProductsTitle" runat="server" Key="scBusinessLine2FeaturedProductsTitle" />
            <!-- ******************************** End of Business Line Featured Products Module Title******************************** -->
        </div>
        <div id="transport_middle">
            <!-- ******************************** Business Line Featured Products Module ******************************** -->
            <sc:Placeholder ID="scBusinessLine2FeaturedProducts" runat="server" Key="scBusinessLine2FeaturedProducts" />
            <!-- ******************************** End of Business Line Featured Products Module ******************************** -->
        </div>
        <div class="clear-both">
        </div>
    </div>
</div>
<div class="clear-both">
</div>
<div class="bizLineContainer">
    <!-- ******************************** Business Line Tab Module******************************** -->
    <sc:Placeholder ID="scBusinessLine2TabContent" runat="server" Key="scBusinessLine2TabContent" />
    <!-- ******************************** End of Business Line Tab Module******************************** -->

    <div class="clear-both">
</div>
    <div class="product_detail">
        <!-- ******************************** Product Details Module******************************** -->
        <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
        <!-- ******************************** End of Product Details Module******************************** -->
    </div>
</div>
<div style="clear: both">
</div>
<div class="clear-both">
</div>

