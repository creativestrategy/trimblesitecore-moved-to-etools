﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessLinePage.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.BusinessLinePage" %>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scBusinessLineBanner" runat="server" Key="scBusinessLineBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div class="clear-both">
</div>
<div id="container_body" class="containerWidth">
    <div class="industry">
        <!-- ******************************** Breadcrumb *********************** -->
        <div class="breadcrumb">
            <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
        </div>
        <!-- ****************************** End of Breadcrumb  ******************* -->
<div class="main_content paddIndustryTitle"> 
	
		<div class="mainHeadContainer"> 
            
                <h3><sc:Placeholder ID="scBusinessLineTitle" runat="server" Key="scBusinessLineTitle" /></h3>
                <span><sc:Placeholder ID="scBusinessLineSubTitle" runat="server" Key="scBusinessLineSubTitle" /></span>
            
            </div>
             <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                
        <div class="main_content_border"></div>

		</div>
		<div class="clear-both">
</div>
        <div class="industryleft">
            
             
                <!-- ******************************** General Text ******************************** -->
                <sc:Placeholder ID="scBusinessLineText" runat="server" Key="scBusinessLineText" />
                <!-- ******************************** End of General Text ******************************** -->
                
            
        </div>
		<div class="contentRight">
            <!-- ******************************** Quick Links ******************************** -->
            <sc:Placeholder ID="scQuickLinks" runat="server" Key="scQuickLinks" />
            <!-- ******************************** End Of Quick Links ******************************** -->
        </div>
        
    </div>
</div>
<div class="clear-both">
</div>

            <!-- ******************************** Business Line Featured Products Module ******************************** -->
            <sc:Placeholder ID="scBusinessLineFeaturedProducts" runat="server" Key="scBusinessLineFeaturedProducts" />
            <!-- ******************************** End of Business Line Featured Products Module ******************************** -->
       
<div class="clear-both">
</div>
<div class="bizLineContainer">
    <!-- ******************************** Business Line Tab Module******************************** -->
    <sc:Placeholder ID="scBusinessLineTabContent" runat="server" Key="scBusinessLineTabContent" />
    <!-- ******************************** End of Business Line Tab Module******************************** -->

    <div class="clear-both">
</div>
    <div class="product_detail">
        <!-- ******************************** Product Details Module******************************** -->
        <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
        <!-- ******************************** End of Product Details Module******************************** -->
    </div>
</div>
<div style="clear: both">
</div>
<div class="clear-both">
</div>
