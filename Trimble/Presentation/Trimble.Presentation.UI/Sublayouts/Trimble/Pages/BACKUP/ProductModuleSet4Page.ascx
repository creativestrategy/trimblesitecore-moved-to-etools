﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductModuleSet4Page.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ProductModuleSet4Page" %>
<script type="text/javascript">
    $(document).ready(function () {

        if ($('.contentRight').css('height') == "0px") {
            $(".contentLeft").css("border-right", "none");
        }

    });
</script>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scInnerBanner" runat="server" Key="scInnerBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div class="clear-both">
</div>
<div id="container_body">
    <!-- ******************************** Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="sidebar-container clear positionRel">
                <sc:Placeholder ID="scSideMenubar" runat="server" Key="scSideMenubar" />
                <!-- ******************************** Quick Links ******************************** -->
                <div class="quick-links">
                    <sc:Placeholder ID="scProductset4QuickLinks" runat="server" Key="scProductset4QuickLinks" />
                </div>
                <!-- ******************************** End Of Quick Links ******************************** -->
                <!-- ******************************** SocialIcons ******************************** -->
                <div class="leftNavSocialIcons">
                    <sc:Placeholder ID="scProductset4SocialMedia" runat="server" Key="scProductset4SocialMedia" />
                </div>
                <!-- ******************************** End Of SocialIcons ******************************** -->
                <!-- ******************************** FeaturedSolution******************************** -->
                <sc:Placeholder ID="scProductset4FeaturedSolution" runat="server" Key="scProductset4FeaturedSolution" />
                <!-- ******************************** FeaturedSolution ******************************** -->
                <!-- ********************************Featured Video Module******************************** -->
                <sc:Placeholder ID="scProductset4FeaturedVideoModule" runat="server" Key="scProductset4FeaturedVideoModule" />
                <!-- ******************************** End of featured Module******************************** -->
            </div>
        </div>
    </div>
    <!-- ******************************** End of Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="center-right-content">
                <!-- ******************************** Breadcrumb *********************** -->
                <div class="breadcrumb">
                    <sc:Placeholder ID="scProductSet4Breadcrumb" runat="server" Key="scProductSet4Breadcrumb" />
                </div>
                <!-- ****************************** End of Breadcrumb  ******************* -->
                <div class="main_content">
                    <div class="leftContentContainer">
                    <div class="mainHeadContainer"> 
                    <!-- ******************************** General Text Titles ******************************** -->
                        <h3>
                            <sc:Placeholder ID="scGeneralTextTitle" runat="server" Key="scGeneralTextTitle" />
                        </h3>
                        <span>
                            <sc:Placeholder ID="scGeneralTextSubLineTitle" runat="server" Key="scGeneralTextSubLineTitle" />
                        </span>
                        </div>
                          <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                        <div class="main_content_border"></div>
                        <!-- ******************************** End of General Text Titles ******************************** -->
                        <!-- ******************************** General Text ******************************** -->
                        <sc:Placeholder ID="scProductSet4GeneralText" runat="server" Key="scProductSet4GeneralText" />
                        <!-- ******************************** End of General Text ******************************** -->
                        <div class="contentRight">
                            <sc:Placeholder ID="scSimplePlaceholder" runat="server" Key="scSimplePlaceholder" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ******************************** Product Details Module******************************** -->
    <sc:Placeholder ID="scProductModuleSet4" runat="server" Key="scProductModuleSet4" />
    <!-- ******************************** End of Product Details Module******************************** -->
    <div class="clear-both">
    </div>
</div>

    <div class="full-width-white-container">
        <div class="center-content">
            <div class="product_detail">
                <!-- ******************************** Product Details Module******************************** -->
                <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
                <!-- ******************************** End of Product Details Module******************************** -->
            </div>
        </div>
    </div>
