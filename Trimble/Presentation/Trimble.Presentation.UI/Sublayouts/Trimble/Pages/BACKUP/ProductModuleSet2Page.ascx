﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductModuleSet2Page.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ProductModuleSet2Page" %>
<script type="text/javascript">
    $(document).ready(function () {
        $('.mega-1').dcVerticalMegaMenu({
            rowItems: '3',
            speed: 'fast',
            effect: 'show',
            direction: 'right'
        });

    });
</script>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner *********************************-->
<div id="container_banner">
    <sc:Placeholder ID="scInnerBanner" runat="server" Key="scInnerBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div style="clear: both">
</div>
<div id="container_body">
    <!-- ******************************** Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="sidebar-container clear positionRel">
                <sc:Placeholder ID="scSideMenubar" runat="server" Key="scSideMenubar" />
                 <!-- ******************************** Quick Links ******************************** -->
                <div class="quick-links">
                    <sc:Placeholder ID="scProductset2QuickLinks" runat="server" Key="scProductset2QuickLinks" />
                </div>
                <!-- ******************************** End Of Quick Links ******************************** -->
                <div class="leftNavSocialIcons">
                    <!-- ******************************** Social Media ******************************** -->
                    <sc:Placeholder ID="scProductset2SocialMedia" runat="server" Key="scProductset2SocialMedia" />
                    <!-- ******************************** End of Social Media ******************************** -->
                </div>
                <!-- ******************************** FeaturedSolution******************************** -->
                <sc:Placeholder ID="scProductset2FeaturedSolution" runat="server" Key="scProductset2FeaturedSolution" />
                <!-- ******************************** FeaturedSolution ******************************** -->
                <!-- ********************************Featured Video Module******************************** -->
                <sc:Placeholder ID="scProductset2FeaturedVideoModule" runat="server" Key="scProductset2FeaturedVideoModule" />
                <!-- ******************************** End of featured Module******************************** -->
            </div>
        </div>
    </div>
    <!-- ******************************** End of Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="center-right-content">
                <!-- ******************************** Breadcrumb *********************** -->
                <div class="breadcrumb">
                    <sc:Placeholder ID="scProductSet2Breadcrumb" runat="server" Key="scProductSet2Breadcrumb" />
                </div>
                <!-- ****************************** End of Breadcrumb  ******************* -->
                <div class="main_content">
                 <div class="mainHeadContainer"> 
                 <!-- ******************************** General Text Titles ******************************** -->
                        <h3>
                            <sc:Placeholder ID="scGeneralTextTitle" runat="server" Key="scGeneralTextTitle" />
                        </h3>
                        <span>
                            <sc:Placeholder ID="scGeneralTextSubLineTitle" runat="server" Key="scGeneralTextSubLineTitle" />
                        </span>
                        </div>
                         <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                        <!-- ******************************** End of General Text Titles ******************************** -->
                        <div class="main_content_border"></div>
                    <!-- ******************************** General Text ******************************** -->
                    <sc:Placeholder ID="scProductSet2GeneralText" runat="server" Key="scProductSet2GeneralText" />
                    <!-- ******************************** End of General Text ******************************** -->
                </div>
            </div>
        </div>
    </div>
        <!-- ******************************** Product Details Module******************************** -->
        <sc:Placeholder ID="scProductModuleSet2" runat="server" Key="scProductModuleSet2" />
        <!-- ******************************** End of Product Details Module******************************** -->
    <div style="clear: both">
    </div>
</div>
<div class="full-width-white-container">
    <div class="center-content">
        <div class="product_detail">
            <!-- ******************************** Product Details Module******************************** -->
            <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
            <!-- ******************************** End of Product Details Module******************************** -->
        </div>
    </div>
</div>
