﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndustryPage.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.IndustryPage" %>
<div class="clear-both">
</div>
<!-- ******************************** Trimble Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scTrimbleBanner" runat="server" Key="scTrimbleBanner" />
</div>
<!-- ***************************** End of Trimble Banner ***************************** -->
<div class="clear-both">
</div>
<div id="container_body" class="containerWidth">
    <div class="industry">
        <!-- ******************************** Breadcrumb *********************** -->
        <div class="breadcrumb">
            <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
        </div>
        <!-- ****************************** End of Breadcrumb  ******************* -->
        <!-- ******************************** Industry Title *********************** -->
        <div class="main_content paddIndustryTitle"> 
            <div class="main_content_border industriesMainContentBorder">
            <div class="mainHeadContainer"> 
            <h3>
                <sc:Placeholder ID="scIndustryTitle" runat="server" Key="scIndustryTitle" /></h3>
            </div>
             <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
            </div>
        </div>
        <!-- ********************************End Of Industry Title  *********************** -->
        <div class="clear-both">
        </div>
        <!-- ******************************** General Text *********************** -->
        <div class="industryleft">
            <sc:Placeholder ID="scIndustryLeft" runat="server" Key="scIndustryLeft" />
        </div>
        <!-- ******************************** End Of General Text  *********************** -->
        <!-- ******************************** SimplePlaceholder*********************** -->
        
            <div class="contentRight">
                <sc:Placeholder ID="scSimplePlaceHolder" runat="server" Key="scSimplePlaceHolder" />
         </div>
        
        <!-- ******************************** End Of SimplePlaceholder *********************** -->
        <div class="clear-both">
        </div>
    <div class="clear-both">
    </div>
    <!-- ******************************** Side Menu bar ******************************** -->
    <div class="sidebar-container clear positionRel marginTop0">
        <sc:Placeholder ID="scSideMenubar" runat="server" Key="scSideMenubar" />
        <div class="quick-links">
            <sc:Placeholder ID="scIndustryQuickLinks" runat="server" Key="scIndustryQuickLinks" />
        </div>
        <div class="leftNavSocialIcons">
            <sc:Placeholder ID="scSocialMedia" runat="server" Key="scSocialMedia" />
        </div>
         <!-- ********************************Featured Image Module******************************** -->
        <sc:Placeholder ID="scFeaturedSolution" runat="server" Key="scFeaturedSolution" />
         <!-- ********************************Featured Image Module******************************** -->

         <!-- ********************************Featured Video Module******************************** -->
        <sc:Placeholder ID="scFeaturedVideoModule" runat="server" Key="scFeaturedVideoModule" />
         <!-- ******************************** End of featured Video Module******************************** -->


    </div>
    <!-- ******************************** End Of Side Menu bar ******************************** -->
    <!-- ******************************** Rhs Container******************************** -->
    <sc:Placeholder ID="scTabModule" runat="server" Key="scTabModule" />
    <!-- ********************************End Of Rhs Container******************************** -->
    <div class="clear-both">
    </div>
    
</div>
</div>

    <div class="full-width-white-container">
        <div class="center-content">
            <div class="product_detail">
                <!-- ******************************** Product Details Module******************************** -->
                <sc:Placeholder ID="ProductDetails" runat="server" Key="ProductDetails" />
                <!-- ******************************** End of Product Details Module******************************** -->
            </div>
        </div>
    </div>
