﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductModuleSet1Page.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ProductModuleSet1Page" %>
<script type="text/javascript">
    $(document).ready(function () {

        if ($('.contentRight').css('height') == "0px") {
            $(".contentLeft").css("border-right", "none");
        }

    });
</script>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scInnerBanner" runat="server" Key="scInnerBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div class="clear-both">
</div>
<div id="container_body">
    <!-- ******************************** Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="sidebar-container clear positionRel">
                <sc:Placeholder ID="scSideMenubar" runat="server" Key="scSideMenubar" />
                <!-- ******************************** Quick Links ******************************** -->
                <div class="quick-links">
                    <sc:Placeholder ID="scProductset1QuickLinks" runat="server" Key="scProductset1QuickLinks" />
                </div>
                <!-- ******************************** End Of Quick Links ******************************** -->
                <!-- ******************************** SocialIcons ******************************** -->
                <div class="leftNavSocialIcons">
                    <sc:Placeholder ID="scProductset1SocialMedia" runat="server" Key="scProductset1SocialMedia" />
                </div>
                <!-- ******************************** End Of SocialIcons ******************************** -->
                <!-- ******************************** FeaturedSolution******************************** -->
                <sc:Placeholder ID="scProductset1FeaturedSolution" runat="server" Key="scProductset1FeaturedSolution" />
                <!-- ******************************** FeaturedSolution ******************************** -->
                <!-- ********************************Featured Video Module******************************** -->
                <sc:Placeholder ID="scProductset1FeaturedVideoModule" runat="server" Key="scProductset1FeaturedVideoModule" />
                <!-- ******************************** End of featured Module******************************** -->
            </div>
        </div>
    </div>
    <!-- ******************************** End of Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="center-right-content">
                <!-- ******************************** Breadcrumb *********************** -->
                <div class="breadcrumb">
                    <sc:Placeholder ID="scProductSet1Breadcrumb" runat="server" Key="scProductSet1Breadcrumb" />
                </div>
                <!-- ****************************** End of Breadcrumb  ******************* -->
                <div class="main_content">
                    <div class="leftContentContainer">
                    <div class="mainHeadContainer"> 
                        <!-- ******************************** General Text Titles ******************************** -->
                        <h3>
                            <sc:Placeholder ID="scGeneralTextTitle" runat="server" Key="scGeneralTextTitle" />
                        </h3>
                        <span>
                            <sc:Placeholder ID="scGeneralTextSubLineTitle" runat="server" Key="scGeneralTextSubLineTitle" />
                        </span>
                        <!-- ******************************** End of General Text Titles ******************************** -->
                        </div>
                         <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                        <div class="main_content_border">
                        </div>
                        <!-- ******************************** General Text ******************************** -->
                        <sc:Placeholder ID="scProductSet1GeneralText" runat="server" Key="scProductSet1GeneralText" />
                        <div class="contentRight">
                            <sc:Placeholder ID="scSimplePlaceHolder" runat="server" Key="scSimplePlaceHolder" />
                        </div>
                    </div>
                    <!-- ******************************** End of General Text ******************************** -->
                </div>
            </div>
        </div>
    </div>
    <div class="full-width-blue-container">
        <div class="center-content">
            <div class="center-right-content">
                <!-- ******************************** Product Details Module******************************** -->
                <sc:Placeholder ID="scProductSet1Details" runat="server" Key="scProductSet1Details" />
                <!-- ******************************** End of Product Details Module******************************** -->
            </div>
        </div>
    </div>
    <!-- ******************************** Product Details Module******************************** -->
    <sc:Placeholder ID="scProductModule" runat="server" Key="scProductModule" />
    <!-- ******************************** End of Product Details Module******************************** -->
    <div class="clear-both">
    </div>
</div>
<div class="full-width-white-container">
    <div class="center-content">
        <div class="product_detail">
            <!-- ******************************** Product Details Module******************************** -->
            <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
            <!-- ******************************** End of Product Details Module******************************** -->
        </div>
    </div>
</div>
