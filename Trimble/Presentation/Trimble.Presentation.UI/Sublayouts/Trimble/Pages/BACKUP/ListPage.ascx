﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListPage.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ListPage" %>
<div class="clear-both">
</div>
<div id="container_body">
    <div class="full-width-white-container">
        <div class="center-content">
            <!-- ******************************** Breadcrumb *********************** -->
            <div class="breadcrumb">
                <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
            </div>
            <!-- ****************************** End of Breadcrumb  ******************* -->
            <div class="main_content paddIndustryTitle">
             <div class="mainHeadContainer"> 
            <h3><sc:Placeholder ID="scListTitle" runat="server" Key="scListTitle" /></h3>
                    <span><sc:Placeholder ID="scListSubTitle" runat="server" Key="scListSubTitle" /></span>
                    </div>
                    <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
                <div class="main_content_border">
                </div>
                <div class="contentLeft paddingTop10">
                  <!-- ******************************** General Text ******************************** -->
                    <sc:Placeholder ID="scGeneralText" runat="server" Key="scGeneralText" />
                    <!-- ******************************** End of General Text ******************************** -->
                </div>
                <div class="contentRight">
                    <!-- ******************************** Simple Palceholder ******************************** -->
                    <sc:Placeholder ID="scSimplePlaceHolder" runat="server" Key="scSimplePlaceHolder" />
                    <!-- ******************************** End of General Text ******************************** -->
                </div>
            </div>
        </div>
    </div>
    <!-- ******************************** List Module ******************************** -->
    <sc:Placeholder ID="scListModule" runat="server" Key="scListModule" />
    <!-- ******************************** End of General Text ******************************** -->
</div>
<div class="full-width-white-container">
    <div class="center-content">
        <div class="product_detail">
            <!-- ******************************** Product Details Module******************************** -->
            <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
            <!-- ******************************** End of Product Details Module******************************** -->
        </div>
    </div>
</div>
