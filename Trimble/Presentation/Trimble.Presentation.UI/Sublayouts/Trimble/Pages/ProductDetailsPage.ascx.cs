﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Deloitte.SC.Framework.Extensions;
using Deloitte.SC.UI.Extensions;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Deloitte.SC.Framework.Helpers;
using Deloitte.SC.UI.Sublayouts.Framework;

namespace Trimble.Presentation.UI.Sublayouts.Trimble.Pages
{
    public partial class ProductDetailsPage : SublayoutBaseExtended
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AssigningDataSource();

            AssignLinkUrl();
        }
        #region AssignLinkUrl

        public void AssignLinkUrl()
        {

            if (!string.IsNullOrEmpty(DataSourceItem.Fields["GeneralLink"].Value))
            {
                titlelink.HRef = GetURL((LinkField)DataSourceItem.Fields["GeneralLink"]);
            }
        }

        #endregion

        #region GetUrl

        /// <summary> 
        /// Get URL for General Link 
        /// </summary> 
        /// <param name="lnkField"></param> 
        /// <returns></returns> 
        public string GetURL(Sitecore.Data.Fields.LinkField lnkField)
        {

            switch (lnkField.LinkType.ToLower())
            {

                case "internal":
                    // Use LinkMananger for internal links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;

                case "media":
                    // Use MediaManager for media links, if link is not empty 
                    return lnkField.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem) : string.Empty;

                case "external":
                    // Just return external links 
                    return lnkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty 
                    return !string.IsNullOrEmpty(lnkField.Anchor) ? "#" + lnkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link 
                    return lnkField.Url;
                case "javascript":
                    // Just return javascript 
                    return lnkField.Url;
                default:
                    // Just please the compiler, this 
                    // condition will never be met 
                    return lnkField.Url;
            }

        }

        #endregion

        #region AssigningDataSource

        public void AssigningDataSource()
        {

            frtitle.Item = lnktitlelink.Item = DataSourceItem;

        }

        #endregion
    }
}