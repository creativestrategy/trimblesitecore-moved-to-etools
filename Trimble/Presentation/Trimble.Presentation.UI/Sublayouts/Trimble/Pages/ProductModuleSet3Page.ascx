﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductModuleSet3Page.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ProductModuleSet3Page" %>
<div class="clear-both">
</div>
<!-- ******************************** Container Banner ******************************** -->
<div id="container_banner">
    <sc:Placeholder ID="scInnerBanner" runat="server" Key="scInnerBanner" />
</div>
<!-- ***************************** End of Container Banner ***************************** -->
<div class="clear-both"></div>

<div id="container_body">
    <!-- ******************************** Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <!-- scSideMenubarResponsive begin -->
            <sc:Placeholder ID="Placeholder1" runat="server" Key="scSideMenubarResponsive" />
            <!-- scSideMenubarResponsive end -->
            <div class="sidebar-container clear positionRel">
                <sc:Placeholder ID="scSideMenubar" runat="server" Key="scSideMenubar" />
                <!-- ******************************** Quick Links ******************************** -->
                <div class="quick-links hideLHS">
                    <sc:Placeholder ID="scProductset3QuickLinks" runat="server" Key="scProductset3QuickLinks" />
                </div>
                <!-- ******************************** End Of Quick Links ******************************** -->
                <!-- ******************************** SocialIcons ******************************** -->
                <div class="leftNavSocialIcons hideLHS">
                    <sc:Placeholder ID="scProductset3SocialMedia" runat="server" Key="scProductset3SocialMedia" />
                </div>
                <!-- ******************************** End Of SocialIcons ******************************** -->
                <!-- ******************************** FeaturedSolution******************************** -->
                <sc:Placeholder ID="scProductset3FeaturedSolution" runat="server" Key="scProductset3FeaturedSolution" />
                <!-- ******************************** FeaturedSolution ******************************** -->
                <!-- ********************************Featured Video Module******************************** -->
                <sc:Placeholder ID="scProductset3FeaturedVideoModule" runat="server" Key="scProductset3FeaturedVideoModule" />
                <!-- ******************************** End of featured Module******************************** -->
            </div>
        </div>
    </div>
    <!-- ******************************** End of Side Menu bar ******************************** -->
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="center-right-content">
                <!-- ******************************** Breadcrumb *********************** -->
                <div class="breadcrumb">
                    <sc:Placeholder ID="scProductSet3Breadcrumb" runat="server" Key="scProductSet3Breadcrumb" />
                </div>
                <!-- ****************************** End of Breadcrumb  ******************* -->
                <div class="main_content">
                <!-- ******************************** General Text Titles ******************************** -->
                        <h3>
                            <sc:Placeholder ID="scGeneralTextTitle" runat="server" Key="scGeneralTextTitle" />
                        </h3>
                        <span>
                            <sc:Placeholder ID="scGeneralTextSubLineTitle" runat="server" Key="scGeneralTextSubLineTitle" />
                        </span>
                        <div class="main_content_border"></div>
                        <!-- ******************************** End of General Text Titles ******************************** -->
                    <!-- ******************************** General Text ******************************** -->
                    <sc:Placeholder ID="scProductSet3GeneralText" runat="server" Key="scProductSet3GeneralText" />
                    <!-- ******************************** End of General Text ******************************** -->
                </div>
            </div>
        </div>
    </div>
    <!-- ******************************** Product Details Module******************************** -->
    <sc:Placeholder ID="scProductModuleSet3" runat="server" Key="scProductModuleSet3" />
    <!-- ******************************** End of Product Details Module******************************** -->
    <div style="clear: both">
    </div>
</div>
<div class="full-width-white-container">
    <div class="center-content">
        <div class="product_detail">
            <!-- ******************************** Product Details Module******************************** -->
            <sc:Placeholder ID="scProductDetails" runat="server" Key="scProductDetails" />
            <!-- ******************************** End of Product Details Module******************************** -->
        </div>
    </div>
</div>
