﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePage.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.HomePage" %>
<div style="clear: both">
</div>
<div id="dvContainer_Body">
    <!-- ******************************** Homepage Banner ******************************** -->
    <sc:Placeholder ID="scCarousel" runat="server" Key="scCarousel" />
    <!-- ******************************** End Of Homepage Banner ******************************** -->
    <div style="clear: both">
    </div>
    <div id="container_content" class="paddlLeft10">
        <div class="content1 marginLeft0">
            <!-- ******************************** News ******************************** -->
            <sc:Placeholder ID="scNews" runat="server" Key="scNews" />
            <!-- ******************************** End Of News ******************************** -->
        </div>
        <div class="content1">
            <!-- ******************************** General Text ******************************** -->

            <sc:Placeholder ID="scGeneralText" runat="server" Key="scGeneralText" />
            <!-- ******************************** End Of General Text ******************************** -->
        </div>
        <div class="content1" runat="server">
            <!-- ******************************** Randomized Placeholder ******************************** -->
            <sc:Placeholder ID="scRandomized" runat="server" Key="scRandomized" />
            <!-- ******************************** End Of Randomized Placeholder ************** ****************** -->
        </div>
       
       <!-- ******************************** HomepageSimple1 ******************************** -->
        <sc:Placeholder ID="scHomepageSimple1" runat="server" Key="scHomepageSimple1" />
        <!-- ******************************** End Of HomepageSimple1 ******************************** -->
        
        
        <!-- ******************************** HomepageSimple2 ******************************** -->
        <sc:Placeholder ID="scHomepageSimple2" runat="server" Key="scHomepageSimple2" />
        <!-- ******************************** End Of HomepageSimple2 ******************************* -->
       
        
        <!-- ******************************** HomepageSimple3 ******************************** -->
        <sc:Placeholder ID="scHomepageSimple3" runat="server" Key="scHomepageSimple3" />
        <!-- ******************************** End Of HomepageSimple3 ******************************** -->
        
    </div>
    <div style="clear: both">
    </div>
</div>

