﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailsPage.ascx.cs"
    Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ProductDetailsPage" %>
<script type="text/javascript">
    $(document).ready(function () {
        $('.mega-1').dcVerticalMegaMenu({
            rowItems: '3',
            speed: 'fast',
            effect: 'show',
            direction: 'right'
        });

        if ($('.contentRight').css('height') == "0px") {
            $(".highlights").css("border-right", "none");
            $('.contentRight').css('height', $(".highlights").css('height'));
        }

    });
</script>
<div id="container_body" class="containerWidth">
    <!-- ******************************** Breadcrumb ******************************** -->
      <div class="breadcrumb">
            <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
        </div>
    <!-- ******************************** End of Breadcrumb ******************************** -->

    <div class="main_content paddIndustryTitle">
         <div class="mainHeadContainer"> 
            <h3>
                <sc:FieldRenderer ID="frtitle" runat="server" FieldName="Title" />
            </h3>
            <div class="productfamily_title">
                <a id="titlelink" runat="server">
                    <img src="../../../html/images/arrow.png" alt="" title="" /></a>
                <sc:Link ID="lnktitlelink" runat="server" Field="GeneralLink" />
            </div>
        </div>
        
        <sc:Placeholder ID="scCustomerLogin" runat="server" Key="scCustomerLogin" />
        <!-- ******************************** Product Title******************************** -->
        <%--<sc:Placeholder ID="scProductTitle" runat="server" Key="scProductTitle" />--%>
        <!-- ********************************End of Product Title******************************** -->
        <div class="main_content_border">
        </div>
        <div style="clear:both"></div>
    </div>
    <div style="clear: both">
    </div>
    <div class="full-width-white-container">
        <div class="center-content">
            <!-- scSideMenubarResponsive begin -->
            <sc:Placeholder ID="scSideMenubarResponsive" runat="server" Key="scSideMenubarResponsive" />
            <!-- scSideMenubarResponsive end -->
            <div class="sidebar-container clear positionRel marginTop0">
                <!-- ******************************** Product Slidshow ******************************** -->
                <sc:Placeholder ID="scProductSlideshow" runat="server" Key="scProductSlideshow" ToolTip="ProductSlideShow" />
                <!-- ******************************** End of Product Slidshow ******************************** -->
                <sc:Placeholder ID="scSideNavigation" runat="server" Key="scSideNavigation" />
                <div class="quick-links hideLHS">
                    <!-- ******************************** Quick Links ******************************** -->
                    <sc:Placeholder ID="scProductQuciklinks" runat="server" Key="scProductQuciklinks" />
                    <!-- ******************************** End Of Quick Links  ******************************** -->
                </div>
                <div class="leftNavSocialIcons hideLHS">
                    <!-- ******************************** Social Media******************************** -->
                    <sc:Placeholder ID="scProductSocialMedia" runat="server" Key="scProductSocialMedia" />
                    <!-- ******************************** End Of Social Media ******************************** -->
                </div>
                <!-- ******************************** FeaturedSolution******************************** -->
                <sc:Placeholder ID="scProductFeaturedSolution" runat="server" Key="scProductFeaturedSolution" />
                <!-- ******************************** FeaturedSolution ******************************** -->
                <!-- ********************************Featured Video Module******************************** -->
                <sc:Placeholder ID="scProductFeaturedVideoModule" runat="server" Key="scProductFeaturedVideoModule" />
                <!-- ******************************** End of featured Module******************************** -->
            </div>
        </div>
    </div>
    <div id="rhs_container">
        <div class="highlights_container">
            <div class="highlights higlightsWidth">
                <!-- ******************************** Features ******************************** -->
                <sc:Placeholder ID="scGeneralTextSwap" runat="server" Key="scGeneralTextSwap" />
                <!-- ******************************** End of Features ******************************** -->
            </div>
           <%-- <div class="highlights_border">
            </div>--%>
            <!-- ******************************** Simple Placeholder Module ******************************** -->
            <div id="dvspm" runat="server" class="contentRight">
                <sc:Placeholder ID="scSimplePlaceholder" runat="server" Key="scSimplePlaceholder" />
            </div>
            <!-- ******************************** End of Simple Placeholder Module ******************************** -->
        </div>
        <div class="industry_tab" style="margin-top:75px">
            <!-- ******************************** Tab Module******************************** -->
            <sc:Placeholder ID="scTabModule" runat="server" Key="scTabModule" />
            <!-- ******************************** End of Tab Module******************************** -->
        </div>
    </div>
    <div style="clear: both">
    </div>
</div>
<div class="full-width-white-container">
    <div class="center-content">
        <div class="product_detail">
            <!-- ******************************** Product Details Module******************************** -->
           <sc:Placeholder ID="scProductModule" runat="server" Key="scProductModule" />
            <!-- ******************************** End of Product Details Module******************************** -->
        </div>
    </div>
</div>
