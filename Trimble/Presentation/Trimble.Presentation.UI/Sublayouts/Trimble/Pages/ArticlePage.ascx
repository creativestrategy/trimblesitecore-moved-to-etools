﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticlePage.ascx.cs" Inherits="Trimble.Presentation.UI.Sublayouts.Trimble.Pages.ArticlePage" %>
<div class="clear-both"></div>


<div class="clear-both"></div>
<div id="dvContainer_Body">
    <div class="full-width-white-container">
        <div class="center-content">
            <div class="main_content">
            <!-- ******************************** Breadcrumb *********************** -->
            <div class="breadcrumb">
                <sc:Placeholder ID="scBreadcrumb" runat="server" Key="scBreadcrumb" />
            </div>
            <!-- ****************************** End of Breadcrumb  ******************* -->
            <div class="main_content_article">
                
                <!-- ******************************** Container Banner ******************************** -->
                <div style="text-align:center">
                    <sc:FieldRenderer runat="server" id="frArticleBanner" FieldName="Article_Banner" />
                    <div class="article_banner_caption"><sc:FieldRenderer runat="server" id="frArticleBannerCaption" FieldName="Article_Banner_Caption" /></div>
                </div>
                <!-- ***************************** End of Container Banner ***************************** -->

                <h3><sc:FieldRenderer runat="server" id="frArticleTitle" FieldName="Article_Title"/></h3>
                <span><sc:FieldRenderer runat="server" id="frArticleSubTitle" FieldName="Article_SubTitle" /></span>
                <div class="clear-both"><br /></div>


                <div class="clear-both"></div>
                <div class="article_content">
                <!-- ******************************** General Text ******************************** -->
                <sc:FieldRenderer runat="server" id="frArticleContent" FieldName="Article_Content" />
                <!-- ******************************** End of General Text ******************************** -->
                </div>
                <div class="clear-both"></div>
            </div>
            </div>
        </div>
    </div>


    <div style="clear: both"></div>




</div><!-- end dvContainer_Body -->

