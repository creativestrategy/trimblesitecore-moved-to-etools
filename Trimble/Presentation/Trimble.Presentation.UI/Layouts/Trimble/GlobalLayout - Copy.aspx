﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GlobalLayout.aspx.cs" Inherits="Trimble.Presentation.UI.Layouts.Trimble.GlobalLayout" %>
<%@ Register Src="~/Includes/Includes.ascx" TagName="Includes" TagPrefix="ucIncludes" %>
<%@ Import Namespace="Sitecore" %>
<% Sitecore.Data.Items.Item config = Sitecore.Configuration.Factory.GetDatabase("master").GetItem("/sitecore/content/Google Analytics/Google Analytics Configuration"); %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Trimble - Transforming the Way the World Works</title>
    <ucIncludes:Includes ID="includes" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <!--***************************************TopHeader********************************-->
    <div id="container_top">
        <sc:Placeholder runat="server" Key="scTopHeader" ID="scTopHeader"   />
    </div>
    <!--***************************************End TopHeader********************************-->

     <!--***************************************Header********************************-->
    <div class="topNavContainer">
        <div id="container_header">
            <sc:Placeholder runat="server" Key="scHeader" ID="scHeader"/>
        </div>
    </div>
    <!--***************************************End Header********************************-->

     <!--***************************************Body********************************-->
    
     <sc:Placeholder ID="scBody" runat="server" Key="scBody" /> 
   
     <!--***************************************End Body********************************-->

     <!--***************************************Footer********************************-->
    <div class="clear-both" ></div>
    <div class="container_footer">
        <div class="container">    
            <sc:Placeholder ID="scFooter" runat="server" Key="scFooter" /> 
        </div>
    </div>
     <!--***************************************End Footer********************************-->
    </form>
    <%= config.Fields["Google Analytics Tracking Code"] %>
</body>
</html>
