﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GlobalLayout.aspx.cs" Inherits="Trimble.Presentation.UI.Layouts.Trimble.GlobalLayout" %>
<%@ Register Src="~/Includes/Includes.ascx" TagName="Includes" TagPrefix="ucIncludes" %>
<%@ Import Namespace="Sitecore" %>
<%
	string[] covidProps;
	string sc_curLang = Sitecore.Context.Language.ToString().ToUpper();
	
	if (sc_curLang == "PT-BR"){
		covidProps = new string[] { "A Message to our Trimble Community about COVID-19", "/covid19-message.aspx" };      
	}else if (sc_curLang == "ZH-CN"){
		covidProps = new string[] { "天宝对新冠肺炎的回应", "/covid19-message.aspx" };  
	}else{
		covidProps = new string[] { "A Message to our Trimble Community about COVID-19", "/covid19-message.aspx" };  
	}
	covidLink.NavigateUrl = covidProps[1];
	covidLink.Text = covidProps[0];
%>

<!doctype html>
<html lang="en">
<head runat="server">
    <title><sc:FieldRenderer runat="server" FieldName="MetaTitle" /></title>
    <meta name="description" content="<sc:FieldRenderer runat="server" FieldName="MetaDescription"/>">
    <meta name="keywords" content="<sc:FieldRenderer runat="server" FieldName="MetaKeywords"/>">
    <ucIncludes:Includes ID="includes" runat="server" />
    <!--Added by Jimmy M 4/10/2014 for DMS-->
    <sc:VisitorIdentification runat="server" />
    <!-- End By Jimmy M -->
	
	
    <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css" />
</head>
<body>
	<%//Added by Jimmy M 01/12/2021 Moved GA code from GlobalLayout to Include File
	Sitecore.Data.Items.Item scItem = Sitecore.Configuration.Factory.GetDatabase("master").GetItem("/sitecore/content/Google Analytics/Google Analytics Configuration"); 
	Sitecore.Data.Fields.Field scField = scItem.Fields["Google Analytics No Script"];
	Response.Write(scField.Value);%>

    <!--Added by JimmyM 3/20/2017 -->
    <script type='text/javascript' src='/js/leadForensic_Global.js'></script>
    <!-- End By JimmyM -->

    <form id="form1" runat="server">
<!--<div style="position:fixed; overflow:auto; width:100%; height:100%; z-index:1;">-->
<header>
<div class="headerContents">
    <div style="padding:10px; background-color:#eee; color:#000; text-align:center; display:none;">
    	<style>
			a.covidLink {font-size:14px; font-weight:bold;}
			a.covidLink:hover{color:#000; font-weight:bold; color:#F90; }
		</style>
        <asp:HyperLink id="covidLink"  NavigateUrl="#" CssClass="covidLink" runat="server"/> 
    </div>
    <!--***************************************TopHeader********************************-->
    <div id="container_top">
        <sc:Placeholder runat="server" Key="scTopHeader" ID="scTopHeader"   />
    </div>
    <!--***************************************End TopHeader********************************-->

     <!--***************************************Header********************************-->
    <div class="topNavContainer">        
        <sc:Placeholder runat="server" Key="scHeader" ID="scHeader"/>
        <!--added by JimmmyM 04-05-2017 -->
        <!--
        <script src="/html/js/cbpHorizontalMenu.min.js"></script> 
        <script>
            $(function () {
                cbpHorizontalMenu.init();
            });
        </script>
        <script src="/html/js/main.js" type="text/jscript"></script> 
        -->
    </div>
</div>
</header>
<!--</div>-->
    <!--***************************************End Header********************************-->

     <!--***************************************Body********************************-->
    
     <sc:Placeholder ID="scBody" runat="server" Key="scBody" /> 
   
     <!--***************************************End Body********************************-->

    <!--***************************************Footer********************************-->
    <div class="clear-both" ></div>
    <!--***************************************container_footer********************************-->
    <div class="container_footer">
        <div class="container">    
            <sc:Placeholder ID="scFooter" runat="server" Key="scFooter" /> 
        </div>        
    </div>
    <!--***************************************End container_footer********************************-->
    <div class="clear-both" ></div>
    <!--***************************************bottom_footer********************************-->
    <div class="bottom_footer">    
        <div class="container"> 
            <sc:Placeholder ID="scFooterBottom" runat="server" Key="scFooterBottom" /> 
        </div>
    </div>
    <!--***************************************End bottom_footer********************************-->


     <!--***************************************End Footer********************************-->
    </form>
    
    <!-- for sli searh added by VJ 10-03-2013 -->
    <script type="text/javascript" src="/html/js/sli-rac.config.js"></script>
    <!-- end sli searh -->

    <!-- URL Param Script to open specific Tab by JM 10-08-2013 -->
    <script type="text/javascript" src="/html/js/getSpryTab.js"></script>
    <!-- end By JM -->

    <!--Added by Jimmy M 04-03-2017 -->
    <script type='text/javascript' src='/js/footTracking_Global.js'></script>    
    <!-- End By Jimmy M -->
    
    
    <!--Added by Jimmy M 5/12/2015 for Google ReMarketing-->
    <script type='text/javascript' src='/js/reMarketing_Global.js'></script>    
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/953208118/?value=0&amp;guid=ON&amp;script=0"/></div></noscript>
    <!-- End By Jimmy M -->
    
    <script type="text/javascript">
        var headerheight;
        $(document).ready(function () {
            headerheight = $('.headerContents')[0].scrollHeight;
            container_top = $('#container_top')[0].scrollHeight;
            topNavContainer = $('.topNavContainer')[0].scrollHeight;
            headerheight = container_top + topNavContainer;
            //alert(scrollHeight + " | " + clientHeight);
        });

        $(window).scroll(function () {

            if ($(window).scrollTop() >= 1) {
                //alert(headerheight);
                $('header').css("height", headerheight)
                $('.headerContents').addClass('fixed-header');

            }
            else if ($(window).scrollTop() <= 0) {
                $('header').css("height", "");
                $('.headerContents').removeClass('fixed-header');
            }
        });
    </script>
    <style type="text/css">
        .fixed-header{position:fixed; z-index:2;  width:100%;  }
    </style>
	<!-- Start of Survicate (www.survicate.com) code -->
	<script type="text/javascript">
	  (function (w) {
		var s = document.createElement('script');
		s.src = '//survey.survicate.com/workspaces/8ae8bfa1d936d881ff813a0946e0403e/web_surveys.js';
		s.async = true;
		var e = document.getElementsByTagName('script')[0];
		e.parentNode.insertBefore(s, e);
	  })(window);
	</script>
	<!-- End of Survicate code -->


    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init({
            easing: 'ease-out-back',
            duration: 1000
        });
	</script>
</body>
</html>
