﻿<%@ Page Language="C#" AutoEventWireup="true"%>
<%@ Import Namespace="Sitecore.Data" %>

<script language="C#" runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        string publishtype = Request["PublishType"] ?? "smart";

        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        Sitecore.Data.Database web = Sitecore.Data.Database.GetDatabase("web");

        switch (publishtype.ToLower())
        {
            case "incremental":

                Sitecore.Publishing.PublishManager.WaitFor(Sitecore.Publishing.PublishManager.PublishIncremental(master,
                                                                     new Sitecore.Data.Database[] { web },
                                                                    new Sitecore.Globalization.Language[] { Sitecore.Globalization.Language.Current }));

                break;

            case "smart":

                Sitecore.Publishing.PublishManager.WaitFor(Sitecore.Publishing.PublishManager.PublishSmart(master,
                                                                new Sitecore.Data.Database[] { web },
                                                                new Sitecore.Globalization.Language[] { Sitecore.Globalization.Language.Current }));

                break;

            case "full":

                Sitecore.Publishing.PublishManager.WaitFor(Sitecore.Publishing.PublishManager.Republish(master, new Database[] { web },
                                                             new Sitecore.Globalization.Language[]
                                                                 {Sitecore.Globalization.Language.Current}));

                break;
        }
    } 
</script>
<html></html>