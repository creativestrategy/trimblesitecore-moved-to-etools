﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trimble.Business.Common.Helpers;
using Trimble.Business.Model.News;
using Trimble.Business.Model.News.Interface;

namespace Trimble.DataAccess.Database.News
{
    public class NewsDao: INewsDao
    { 
        public List<NewsArticle> GetNewsArticles()
        {
            using (var ctxt = new NewsDataContext(ConfigurationHelper.GetNewsConnectionString()))
            {
                //DateTime EST = DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0)); //added by JLM 12-13-2013

                //added by JLM 08-12-2014
                DateTime thisTime = DateTime.Now;
                TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                bool isDaylight = tst.IsDaylightSavingTime(thisTime);
                DateTime EST = isDaylight == true ? DateTime.UtcNow.Subtract(new TimeSpan(0, 4, 0, 0)) : DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0, 0));
                
                var data = from NewsManagerArticle in ctxt.NewsManagerArticles
                           where NewsManagerArticle.Status == 1 && NewsManagerArticle.Publish_Date <= EST //added by JLM 12-13
                           orderby NewsManagerArticle.Publish_Date descending
                           select NewsManagerArticle;

                var list = new List<NewsArticle>();
                foreach (var n in data)
                {
                    list.Add(new NewsArticle
                                 {
                                     FileName = n.FileName,
                                     PublishDate = n.Publish_Date,
                                     Title = n.Title
                                 });
                }

                return list;
            }
        }
    }
}
