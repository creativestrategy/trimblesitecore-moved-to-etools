@ECHO OFF
IF EXIST Web*.config DEL Web*.config
IF EXIST ConfigOutput RMDIR ConfigOutput /S /Q

COPY "..\Presentation\Trimble.Presentation.UI\Web*.config" .\
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\Msbuild.exe "TrimbleTransform.proj" /t:All

MKDIR ConfigOutput
MOVE *.Transformed.config ConfigOutput
DEL Web*.config