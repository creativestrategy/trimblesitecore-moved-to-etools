﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trimble.Business.Model.News.Interface
{
    public interface INewsDao
    {
        List<NewsArticle> GetNewsArticles();
    }
}
