﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trimble.Business.Model.News
{
    public class NewsArticle
    {
        public String FileName { get; set; }
        public String Title { get; set; }
        public DateTime? PublishDate { get; set; }
    }
}

