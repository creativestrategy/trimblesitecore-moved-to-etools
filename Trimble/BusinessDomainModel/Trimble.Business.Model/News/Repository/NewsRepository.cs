﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trimble.Business.Model.News.Interface;
using Trimble.Business.Common.Helpers;
using Microsoft.Practices.Unity;

namespace Trimble.Business.Model.News.Repository
{
    public class NewsRepository
    {

        public List<NewsArticle> GetNewsArticles()
        {
            var dao = UnityHelper.Container.Resolve<INewsDao>();
            return dao.GetNewsArticles();
        }

        public List<NewsArticle> GetTopNNewsArticles(int numberOfNewsArticles)
        {
            return (List<NewsArticle>)GetNewsArticles().Take(numberOfNewsArticles).ToList();
        }
    }
}
