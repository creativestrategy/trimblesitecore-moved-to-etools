﻿using System;
using Microsoft.Practices.Unity;

namespace Trimble.Business.Common.Helpers
{
    public static class UnityHelper
    {
        private static IUnityContainer _container;
        public static IUnityContainer Container 
        {
            get
            {
                if (_container == null)
                    throw new NullReferenceException("The UnityHelper Container property must be initialised before it is referenced.");
                return _container;
            }
            set 
            {
                _container = value;
            } 
        }
    }
}