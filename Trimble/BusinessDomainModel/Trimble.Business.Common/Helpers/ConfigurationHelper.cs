﻿using System.Configuration;

namespace Trimble.Business.Common.Helpers
{
    public static class ConfigurationHelper
    {
        public static string GetNewsConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["News"].ConnectionString;
        }
    }
}

