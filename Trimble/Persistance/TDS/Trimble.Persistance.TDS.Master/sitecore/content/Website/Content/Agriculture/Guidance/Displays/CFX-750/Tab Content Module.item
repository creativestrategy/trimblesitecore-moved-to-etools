----item----
version: 1
id: {48CA1AEF-D082-437A-9FE7-31694FF6DE29}
database: master
path: /sitecore/content/Website/Content/Agriculture/Guidance/Displays/CFX-750/Tab Content Module
parent: {92FD3552-C605-4D76-A52C-D8D7EBCD40A3}
name: Tab Content Module
master: {00000000-0000-0000-0000-000000000000}
template: {3C0D7A65-8E46-40FF-9D9C-FCC83DCC9763}
templatekey: Tab Content

----field----
field: {BA3F86A2-4A1C-4D78-B63D-91C2779C1B5E}
name: __Sortorder
key: __sortorder
content-length: 3

275
----version----
language: en
version: 1
revision: d3e30cce-b385-42e3-ac9b-0ad0b3a769be

----field----
field: {91717ED5-C863-4658-8CA0-A6BB8EEED12E}
name: Tab1Title
key: tab1title
content-length: 16

Product Overview
----field----
field: {D3449604-5D60-448D-90E9-233D6749C025}
name: Tab1Content
key: tab1content
content-length: 2061

<p><strong>Guidance and Mapping</strong></p>
<div style="padding-top: 8px;">
<ul>
    <li>Coverage, point, line, and area mapping</li>
    <li>Manual guidance for a number of field patterns, or add hands-free guidance with the<a href="ez-steer.aspx"> EZ-Steer®</a>, <a href="ez-pilot.aspx">EZ-Pilot™</a>, or <a href="autopilot.aspx">Autopilot™</a> steering systems.</li>
</ul>
</div>
<p><strong>Planting and Nutrient/Pest Management</strong></p>
<div style="padding-top: 8px;">
<ul>
    <li><a href="field-iq.aspx">Field-IQ™</a> crop input control capabilities, including seed monitoring, 2-product variable rate application control, boom height control, and automatic section control</li>
</ul>
</div>
<p><strong>Harvesting</strong></p>
<div style="padding-top: 8px;">
<ul>
    <li>Yield monitoring capabilities for grain crops, including load tracking and auto-cut width</li>
</ul>
</div>
<p><strong>Information Exchange and Farm Software</strong></p>
<div style="padding-top: 8px;">
<ul>
    <li>Wireless data transfer between vehicles in the field or from the field to the office with <a href="http://www.connectedfarm.com" target="_blank">Connected Farm™</a></li>
    <li>Data analysis with <a href="farm-software.aspx">Farm Works Software®</a></li>
</ul>
</div>
<p><strong>Available </strong><a href="http://www.trimble.com/agriculture/CorrectionServices/" target="_blank"><strong>Correction Services</strong></a><strong> and accuracy levels for the FmX display</strong></p>
<br>
<table class="table" border="1" cellpadding="8" cellspacing="0" width="350">
    <tbody>
        <tr>
            <td style="width: 194px;" class="tableSubHeader">
            <p><strong>Correction Services</strong></p>
            </td>
            <td style="width: 124px;" class="tableSubHeader">
            <p><strong>Accuracy</strong></p>
            </td>
        </tr>
        <tr>
            <td>
            <p>SBAS (WAAS, EGNOS, MSAS)</p>
            </td>
            <td>
            <p>6-8" (15-20 cm)</p>
            </td>
        </tr>
    </tbody>
</table>

----field----
field: {FB47DE07-7DBE-42B8-A9F2-E701261FE804}
name: Tab2Title
key: tab2title
content-length: 17

DisplayComparison
----field----
field: {4C31037D-2399-4E63-A055-D298EB7EF428}
name: Tab2Content
key: tab2content
content-length: 9143

<table width="665" align="center" class="table" border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td>&nbsp;</td>
            <td align="center"><img title="EZ-Guide 250 System" style="border: 0px solid;" alt="EZ-Guide 250 System" src="../../graphics/product_images/EZ-Guide-250-system.png" /></td>
            <td align="center"><img title="CFX-750 Display" style="border: 0px solid;" alt="CFX-750 Display" src="../../graphics/product_images/CFX-750-display.png" /></td>
            <td align="center"><img title="FmX Integrated Display" style="border: 0px solid;" alt="FmX Integrated Display" src="../../graphics/product_images/FmX-integrated-display.png" /></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="center" class="tableProductHeader"><strong>EZ-Guide 250 System</strong></td>
            <td align="center" class="tableProductHeader"><strong>CFX-750 Display</strong></td>
            <td align="center" class="tableProductHeader"><strong>FmX Integrated Display</strong></td>
        </tr>
        <tr>
            <td class="tableSubHeader" style="width: 212px;">
            <p><strong>Features</strong></p>
            </td>
            <td class="tableSubHeader" style="width: 140px;">&nbsp;</td>
            <td class="tableSubHeader" style="width: 140px;">&nbsp;</td>
            <td class="tableSubHeader" style="width: 140px;">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Size of color screen</p>
            </td>
            <td align="center">4.3&rdquo;</td>
            <td align="center">8.0&rdquo;</td>
            <td align="center">12.1&rdquo;</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Touchscreen</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Video camera inputs</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">2</td>
            <td align="center">4</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Built-on GPS receiver</p>
            </td>
            <td align="center">1</td>
            <td align="center">1</td>
            <td align="center">2</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>GLONASS compatibility</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td class="tableSubHeader" valign="top" style="width: 212px;">
            <p><strong>Correction Services</strong></p>
            </td>
            <td class="tableSubHeader" style="width: 140px;">&nbsp;</td>
            <td class="tableSubHeader" style="width: 140px;">&nbsp;</td>
            <td class="tableSubHeader" style="width: 140px;">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>CenterPoint RTK <br />
            &lt;1&rdquo; (2.5 cm) accuracy</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>CenterPoint VRS<br />
            &lt;1&rdquo; (2.5 cm) accuracy</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>CenterPoint RTX <br />
            1.5&rdquo; (3.8 cm) accuracy</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>OmniSTAR HP/XP&nbsp; <br />
            2-4&rdquo; (5-10 cm) accuracy</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>OmniSTAR G2&nbsp; <br />
            3-4&rdquo; (8-10 cm) accuracy</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>RangePoint RTX&nbsp; <br />
            6&rdquo; (15 cm) pass-to-pass&nbsp; <br />
            20&rdquo; (50 cm) repeatable </p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>OmniSTAR VBS&nbsp; <br />
            &lt; 1 meter accuracy</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>SBAS&nbsp; <br />
            6-8&rdquo; (15-20 cm) accuracy</p>
            </td>
            <td align="center">X</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td class="tableSubHeader" valign="top" style="width: 212px;">
            <p><strong>Product Compatibility</strong></p>
            </td>
            <td align="center" class="tableSubHeader">&nbsp;</td>
            <td align="center" class="tableSubHeader">&nbsp;</td>
            <td align="center" class="tableSubHeader">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Assisted steering</p>
            </td>
            <td align="center">X</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Automated steering</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Implement control</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Row guidance</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Flow and application control</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">Select capabilities</td>
            <td align="center">Select capabilities</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>On-the-go VRA with GreenSeeker Sensors</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Water management</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Yield monitoring</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">Select capabilities</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Wireless vehicle to vehicle data exchange</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Wireless office to field data exchange</p>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Office software</p>
            </td>
            <td align="center">X</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
        <tr>
            <td valign="top" style="width: 212px;">
            <p>Field data recording</p>
            </td>
            <td align="center">X</td>
            <td align="center">X</td>
            <td align="center">X</td>
        </tr>
    </tbody>
</table>
----field----
field: {1EF22771-4107-41D7-B428-EA1E40CFB29F}
name: Tab3Title
key: tab3title
content-length: 19

Compatible Products
----field----
field: {2AEA0A95-5696-4048-B2BD-4BE23FAB368D}
name: Tab3Content
key: tab3content
content-length: 2844

<div class="bizLineItemContainer" style="margin-top: -20px;">
<br />
<a href="ez-steer.aspx">
<div class="tabcontent_img">
<img src="graphics/product_images/EZ-Steer-assisted-steering-system.png" alt="product1" width="140" height="90" />
</div>
</a>
<div id="scbody_0_scproductmoduleset2_0_rptcategory_rptproduct_0_divCategoryList_0" class="bizLineContent2">
<a href="ez-steer.aspx">
<div class="tabcontent_title">
EZ-Steer&reg; assisted steering system
</div>
</a>
<div class="tabcontent">
Assisted steering system providing portable, hands-free farming for more than 1200 vehicle makes and models.
</div>
</div>
</div>
<div class="bizLineItemContainer">
<br />
<a href="farm-software.aspx">
<div class="tabcontent_img">
<img src="graphics/product_images/EZ-Steer-assisted-steering-system.png" alt="product1" width="140" height="90" />
</div>
</a>
<div class="bizLineContent2">
<a href="farm-software.aspx">
<div class="tabcontent_title">
Farm Works Software&reg;
</div>
</a>
<div class="tabcontent">
Farm management software offering a complete range of solutions for the field and farm office, including mapping, accounting, water management, and more.
</div>
</div>
</div>
<div class="bizLineItemContainer">
<br />
<a href="farm-software.aspx">
<div class="tabcontent_img">
<img src="graphics/product_images/EZ-Steer-assisted-steering-system.png" alt="product1" width="140" height="90" />
</div>
</a>
<div class="bizLineContent2">
<a href="farm-software.aspx">
<div class="tabcontent_title">
Farm Works Software&reg;
</div>
</a>
<div class="tabcontent">
Farm management software offering a complete range of solutions for the field and farm office, including mapping, accounting, water management, and more.
</div>
</div>
</div>
<div class="bizLineItemContainer">
<br />
<a href="farm-software.aspx">
<div class="tabcontent_img">
<img src="graphics/product_images/EZ-Steer-assisted-steering-system.png" alt="product1" width="140" height="90" />
</div>
</a>
<div class="bizLineContent2">
<a href="farm-software.aspx">
<div class="tabcontent_title">
Farm Works Software&reg;
</div>
</a>
<div class="tabcontent">
Farm management software offering a complete range of solutions for the field and farm office, including mapping, accounting, water management, and more.
</div>
</div>
</div>
<div class="bizLineItemContainer">
<br />
<a href="farm-software.aspx">
<div class="tabcontent_img">
<img src="graphics/product_images/EZ-Steer-assisted-steering-system.png" alt="product1" width="140" height="90" />
</div>
</a>
<div class="bizLineContent2">
<a href="farm-software.aspx">
<div class="tabcontent_title">
Farm Works Software&reg;
</div>
</a>
<div class="tabcontent">
Farm management software offering a complete range of solutions for the field and farm office, including mapping, accounting, water management, and more.
</div>
</div>
</div>
----field----
field: {FB1F145F-0A88-4F2D-A910-8A1A0E639585}
name: Tab4Title
key: tab4title
content-length: 17

Technical Support
----field----
field: {362316D9-ADD4-466E-902C-9CBC19CE86AB}
name: Tab4Content
key: tab4content
content-length: 520

<ul style="padding: 0px 0px 10px 30px; line-height: 30px; margin: 0px; outline: invert none medium; font-family: 'Titillium Web', sans-serif;">
    <li style="padding: 0px; margin: 0px; outline: invert none medium;">Software Downloads</li>
    <li style="padding: 0px; margin: 0px; outline: invert none medium;">Product Manuals</li>
    <li style="padding: 0px; margin: 0px; outline: invert none medium;">Trobleshooting</li>
    <li style="padding: 0px; margin: 0px; outline: invert none medium;">Installation</li>
</ul>
----field----
field: {39D058FF-3C7D-491A-9689-BAE4E3973A85}
name: Tab5Title
key: tab5title
content-length: 0


----field----
field: {8FBF500B-518D-4FFB-9B34-09544EF88512}
name: Tab5Content
key: tab5content
content-length: 11

<p>Tab5</p>
----field----
field: {25BED78C-4957-4165-998A-CA1B52F67497}
name: __Created
key: __created
content-length: 15

20130509T204400
----field----
field: {8CDC337E-A112-42FB-BBB4-4143751E123F}
name: __Revision
key: __revision
content-length: 36

d3e30cce-b385-42e3-ac9b-0ad0b3a769be
----field----
field: {D9CF14B1-FA16-4BA6-9288-E8A174D4D522}
name: __Updated
key: __updated
content-length: 34

20130725T122659:635103520191185000
----field----
field: {BADD9CF9-53E0-4D0C-BCC0-2D784C282F6A}
name: __Updated by
key: __updated by
content-length: 14

sitecore\admin
----field----
field: {001DD393-96C5-490B-924A-B0F25CD9EFD8}
name: __Lock
key: __lock
content-length: 5

<r />
